﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->

<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="ads-lg-format" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Advertising</li>
  <li>Large format ads</li>
  </ol>
  
  
  
  <h2>Large format ads
  <a href="pdf/p-lg-format-ads.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
  <div class="well intro"><p>Large format ads – such as those
that might appear at a trade show –
require special consideration when
using the graphic elements.</p></div>
 
<div class="row">
<div class="col-md-4">
<p><strong>Horizontal formats</strong></p>
<p>Horizontal panels – whether on a balcony or
escalator banister – can be extremely wide, but
relatively short. For applications in this format
it may be necessary to repeat graphic elements
and/or content. This includes the logo and
secondary graphic.</p>
<p>Position the Edwards logo at both ends of
the graphic panel, either on a white or light
background, or over a image. The secondary
graphic – either small or large – may appear
twice: once towards the left side and again
towards the right side. Place the headline in
the secondary graphic; support copy or other
text should appear outside the secondary
graphic.</p>

<p>For horizontal formats that are narrower than
these samples, refer to the horizontal digital
ad samples for guidance. Adjust the logo,
secondary graphic and type sizes as necessary.</p>
</div>

<div class="col-md-8">
<img src="img/ads-lg-format.png" class="img-responsive">
</div>

</div>

<div class="row">
<div class="col-md-12">
<p><strong>Vertical formats</strong></p>
<p>For large scale vertical formats, refer to the
vertical digital ad samples and adjust the
type sizes and graphics as necessary. Both
the small and large versions of the secondary
graphic in the digital ad samples may be used.</p>
</div>

</div>


  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
