﻿using Saml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class SSOLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var samlEndpoint = "https://login.microsoftonline.com/c8fe7995-06f0-4bdf-8f2a-0c8a7986480d/saml2";

        var request = new AuthRequest(
            "https://standards.edwards.com", //TODO: put your app's "unique ID" here
            "https://standards.edwards.com/SamlResponse.aspx" //TODO: put Assertion Consumer URL (where the provider should redirect users after authenticating)
            );

        //generate the provider URL
        string url = request.GetRedirectUrl(samlEndpoint);

        //then redirect your user to the above "url" var
        //for example, like this:
        Response.Redirect(url);
        
    }
}