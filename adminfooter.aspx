﻿
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Api.Management.Account" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Exception" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Models.ResponseModels.UserProfile" %>
<%@ Import Namespace="ProfileApi=LoginRadiusSDK.V2.Api.Authentication.Account.ProfileApi" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Public bool isAdmin = True;
    Public String fullName = "";
    Public void LogOut_Onclick(Object sender, EventArgs args)
    {
        FormsAuthentication.SignOut();
        Session.Clear();
        FormsAuthentication.RedirectToLoginPage();
    }
    Protected void Page_Load(Object sender, EventArgs e) {
        var profile = Session["profile"] As Identity;
        If (profile == null)
        {
            FormsAuthentication.RedirectToLoginPage();
        }
        Else
        {
            var roles = Session["roles"] As List<String>;
            fullName = profile.FullName;
            If (roles == null || !roles.Contains("Admin"))
            {
                isAdmin = false;
            }
        }
    }
</script>
<form runat="server">
<div class="user-display">
    <span class='user-actions welcome'> Welcome, <%=fullName%>!</span>
    <% if (isAdmin == true) {%> <span class='user-actions'> <a href='Admin.aspx'>Admin</a></span> | <% } %>
    
    <asp:LinkButton  ID="logout" runat="server" OnClick="LogOut_Onclick" Text="Log Out" />
</div>
        </form>
