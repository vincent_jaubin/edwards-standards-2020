﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body>
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="/dev/header.html" -->
            <div id="hvt-overview" class="page">
                <div class="jumbotron banner-edw banner-employer banner-home">
                    <div class="container">
                        <div class="redboxes">
                            <div class="redbox-sm"></div>
                            <div class="redbox-lg">
                                <h1>Employer Brand Resources</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.banner-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 main-content">
                            <div class="clearfix"></div>
                            <div class="row row dot-borders-adjust">
                                <div class="col-md-3 col-sm-2">
                                    <div class="row">
                                        <div class="col-md-5"><img src="../img/hr-home-photo-thumb.jpg" alt="Logo usage" class="img-responsive">
                                        </div>
                                        <div class="col-md-7">
                                            <h4>Photo & video library</h4>
                                            <p><a href="/hr/photos.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Browse</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-2">
                                    <div class="row">
                                        <div class="col-md-5"><img src="../img/hr-home-print-thumb.jpg" alt="Image library" class="img-responsive">
                                        </div>
                                        <div class="col-md-7">
                                            <h4>Print collateral<br>&nbsp;</h4>
                                            <p><a href="/hr/flyers.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Browse</a></p>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-md-3 col-sm-2">
                                    <div class="row">
                                        <div class="col-md-5"><img src="../img/hr-home-dig-thumb.jpg" alt="Image library" class="img-responsive">
                                        </div>
                                        <div class="col-md-7">
                                            <h4>Digital collateral<br>&nbsp;</h4>
                                            <p><a href="/hr/banner-ads.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Browse</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-2">
                                    <div class="row">
                                        <div class="col-md-5"><img src="../img/hr-home-article-thumb.jpg" alt="Digital standards" class="img-responsive">
                                        </div>
                                        <div class="col-md-7">
                                            <h4>Article archive<br>&nbsp;</h4>
                                            <p><a href="/hr/articles.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Browse</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /row -->
                        </div>
                    </div>
                </div>
                <div class="container-fluid edw-gray-bg">
                    <div class="row dot-borders nomargin">
                        <div class="col-md-6 match-height">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="../img/home-tutorial-img.png" alt="Edwards Overview" class="img-responsive"></div>
                                <div class="col-md-7">
                                    <h3>Tutorials & FAQs</h3>
                                    <p>[Content to be provided]</p>
                                    <p><a href="/hr/tutorial.aspx" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 match-height">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-1">
                                    <h3>Our employer brand vision</h3>
                                     <p>[Coming Soon!]</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
        <!-- end page -->
        <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
