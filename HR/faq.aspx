﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <title>Employer brand resources | Edwards Lifesciences</title>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="/dev/header.html" -->
            <div id="hr-faq" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Employer brand resources</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/hr/index.aspx">Employer brand resources</a></li>
                                <li>Tutorials & FAQs</li>
                                <li>Frequently asked questions</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Frequently asked questions<a href="#" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Lorem ipsum</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Lorem ipsum</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Lorem ipsum</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>Lorem ipsum</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Lorem ipsum</h3>
                                        <div class="well intro">
                                            <h4>Lorem ipsum</h4>
                                        </div>
                                       <div class="panel-group" id="accordion-faq">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapse1-faq">
        FAQ 1<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-faq" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>FAQ 1</h3>
                                                      <p>response</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapse2-faq">
        FAQ 2<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-faq" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>FAQ 2</h3>
                                                      <p>response</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapse3-faq">
        FAQ 3<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-faq" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                      <h3>FAQ 3</h3>
                                                      <p>response</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapse4-faq">
        FAQ 4<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse4-faq" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                       <h3>FAQ 4</h3>
                                                      <p>response</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapse5-faq">
        FAQ 5<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse5-faq" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                       <h3>FAQ 5</h3>
                                                      <p>response</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      
                                    </div>
                                    <div class="pane" id="tabs-2">
                                         <h3>Lorem ipsum</h3>
                                        <div class="well intro">
                                            <h4>Lorem ipsum</h4>
                                        </div>
                                        <p><strong>Lorem ipsum</strong></p>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Lorem ipsum</h3>
                                        <div class="well intro">
                                            <h4>Lorem ipsum</h4>
                                        </div>
                                        <p><strong>Lorem ipsum</strong></p>
                                        
                                        
                                    </div>
                                     <div class="pane" id="tabs-4">
                                    <h3>Lorem ipsum</h3>
                                        <div class="well intro">
                                            <h4>Lorem ipsum</h4>
                                        </div>
                                        <p><strong>Lorem ipsum</strong></p>
                                     </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hr.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
