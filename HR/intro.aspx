﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <title>Employer brand resources | Edwards Lifesciences</title>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="/dev/header.html" -->
            <div id="hr-intro" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Employer brand resources</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/hr/index.aspx">Employer brand resources</a></li>
                                <li>Our employer brand vision</li>
                                <li>Introduction</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Introduction<a href="#" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Lorem ipsum</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Lorem ipsum</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Lorem ipsum</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>Lorem ipsum</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Lorem ipsum</h3>
                                        <div class="well intro">
                                            <h4>Lorem ipsum</h4>
                                        </div>
                                        <p><strong>Lorem ipsum</strong></p>
                                      
                                    </div>
                                    <div class="pane" id="tabs-2">
                                         <h3>Lorem ipsum</h3>
                                        <div class="well intro">
                                            <h4>Lorem ipsum</h4>
                                        </div>
                                        <p><strong>Lorem ipsum</strong></p>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Lorem ipsum</h3>
                                        <div class="well intro">
                                            <h4>Lorem ipsum</h4>
                                        </div>
                                        <p><strong>Lorem ipsum</strong></p>
                                        
                                        
                                    </div>
                                     <div class="pane" id="tabs-4">
                                    <h3>Lorem ipsum</h3>
                                        <div class="well intro">
                                            <h4>Lorem ipsum</h4>
                                        </div>
                                        <p><strong>Lorem ipsum</strong></p>
                                     </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hr.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
