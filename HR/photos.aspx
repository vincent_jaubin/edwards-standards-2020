﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <title>Employer brand resources | Edwards Lifesciences</title>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="/dev/header.html" -->
            <div id="hr-photos" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Employer brand resources</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/hr/index.aspx">Employer brand resources</a></li>
                                <li>Photo & video library</li>
                                <li>Photos</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Photos<a href="#" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>North America</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Central America</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>EMEA</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>JPAC</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                     
                                        <h3>Irvine</h3>
                                        
                                       
                                         <div class="panel-group" id="accordion-irv">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-irv" href="#collapse1-irv">
        February 2018 employee photo shoot<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-irv" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        
                                                      <div id="irvine-feb18employees" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-irv" href="#collapse2-irv">
        December 2017 employee photo shoot<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-irv" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                      
                                                     
                                                      <div id="irvine-dec17employees" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                             </div>
                                             
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-irv" href="#collapse3-irv">
        September 2016 employee photo shoot<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-irv" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                      
                                                      <div id="irvine-sep16employees" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    
                                    
                                      <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-irv" href="#collapse4-irv">
        Edwards University recruiting team<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse4-irv" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                      
                                                      <div id="irvine-eurecruiting" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    
                                    
                                    
                                    </div>
                                        
                                        
                                        
                                        <h3>Draper</h3>
                                     
                                          <div class="panel-group" id="accordion-dr">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-dr" href="#collapse1-dr">
        2018 employee photo shoot<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-dr" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        
                                                      <div id="draper-2018" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-dr" href="#collapse2-dr">
        2017 employee photo shoot<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-dr" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                      
                                                     
                                                      <div id="draper-2017" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                             </div>
                                             
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-dr" href="#collapse3-dr">
        2016 employee photo shoot<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-dr" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                      
                                                      <div id="draper-2016" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    
                                    </div>
                                     
                                     
                                     
                                     
                                        
                                    </div>
                                    <div class="pane" id="tabs-2">
                                    
                                    
                                        <h3>Costa Rica</h3>
                                        
                                        
                                         <div class="panel-group" id="accordion-cr">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-cr" href="#collapse1-cr">
        Headshots<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-cr" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        
                                                      <div id="costarica-headshots" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-cr" href="#collapse2-cr">
        Plant<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-cr" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                      
                                                     
                                                      <div id="costarica-plant" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                             </div>
                                             
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-cr" href="#collapse3-cr">
        Employee celebration<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-cr" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                      
                                                      <div id="costarica-celebration" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    
                                    </div>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                      
                                        <h3>Ireland</h3>
                                        
                                         <div class="panel-group" id="accordion-ire">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-ire" href="#collapse1-ire">
        2017 employee photo shoot<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-ire" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        
                                                      <div id="ireland-2017" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            
                                            </div>
                                            
                                        
                                        
                                        <h3>Prague</h3>
                                      
                                      
                                           <div class="panel-group" id="accordion-prag">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-prag" href="#collapse1-prag">
        City tour<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-prag" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        
                                                      <div id="prague-city" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            
                                            </div>
                                        
                                        
                                    </div>
                                     <div class="pane" id="tabs-4">
                                     
                                     
                                        <h3>Singapore</h3>
                                        <div class="panel-group" id="accordion-sing">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-sing" href="#collapse1-sing">
        Employees<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-sing" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        
                                                      <p>[coming soon]</p>
                                                      
                                                      
                                                      <div id="singapore" class="row imagethumbs text-center"> 
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            
                                            </div>
                                   
                                     </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hr.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
        
         <script>

        // Irvine Section
		
		// September 16 Employee

        var imagesIrvSep16 = ['001.jpg','002.jpg','003.jpg','004.jpg','005.jpg','006.jpg','007.jpg','008.jpg','009.jpg','010.jpg','011.jpg','012.jpg','013.jpg','014.jpg','015.jpg','016.jpg','017.jpg','018.jpg','019.jpg','020.jpg','021.jpg','022.jpg','023.jpg','024.jpg','025.jpg','026.jpg','027.jpg','028.jpg','029.jpg','030.jpg','031.jpg','032.jpg','033.jpg','034.jpg','035.jpg','036.jpg','037.jpg','038.jpg','039.jpg','040.jpg','041.jpg','042.jpg','043.jpg','044.jpg','045.jpg','046.jpg','047.jpg','048.jpg','049.jpg','050.jpg','051.jpg','052.jpg','053.jpg','054.jpg','055.jpg','056.jpg','057.jpg','058.jpg','059.jpg','060.jpg','061.jpg','062.jpg','063.jpg','064.jpg','065.jpg','066.jpg','067.jpg','068.jpg','069.jpg','070.jpg','071.jpg','072.jpg','073.jpg','074.jpg','075.jpg','076.jpg','077.jpg','078.jpg','079.jpg','080.jpg','081.jpg','082.jpg','083.jpg','084.jpg','085.jpg','086.jpg','087.jpg','088.jpg','089.jpg','090.jpg','091.jpg','092.jpg','093.jpg','094.jpg','095.jpg','096.jpg','097.jpg','098.jpg','099.jpg','100.jpg','101.jpg','102.jpg','103.jpg','104.jpg','105.jpg','106.jpg','107.jpg','108.jpg','109.jpg','110.jpg','111.jpg','112.jpg','113.jpg','114.jpg','115.jpg','116.jpg','117.jpg','118.jpg','119.jpg','120.jpg','121.jpg','122.jpg','123.jpg','124.jpg','125.jpg','126.jpg','127.jpg','128.jpg','129.jpg','130.jpg','131.jpg','132.jpg','133.jpg','134.jpg','135.jpg','136.jpg','137.jpg','138.jpg','139.jpg','140.jpg','141.jpg','142.jpg','143.jpg','144.jpg','145.jpg','146.jpg','147.jpg','148.jpg','149.jpg','150.jpg','151.jpg','152.jpg','153.jpg','154.jpg','155.jpg','156.jpg','157.jpg','158.jpg','159.jpg','160.jpg','161.jpg','162.jpg','163.jpg','164.jpg','165.jpg','166.jpg','167.jpg','168.jpg','169.jpg','170.jpg','171.jpg','172.jpg','173.jpg','174.jpg','175.jpg','176.jpg','177.jpg','178.jpg','179.jpg','180.jpg','181.jpg','182.jpg','183.jpg','184.jpg','185.jpg','186.jpg','187.jpg','188.jpg','189.jpg','190.jpg','191.jpg','192.jpg','193.jpg','194.jpg','195.jpg','196.jpg','197.jpg','198.jpg','199.jpg','200.jpg','201.jpg','202.jpg','203.jpg','204.jpg','205.jpg','206.jpg','207.jpg','208.jpg','209.jpg','210.jpg','211.jpg','212.jpg','213.jpg','214.jpg','215.jpg','216.jpg','217.jpg','218.jpg','219.jpg','220.jpg','221.jpg','222.jpg','223.jpg','224.jpg','225.jpg','226.jpg','227.jpg'];

        var counterIrvSep16 = 0;

        while (counterIrvSep16 < imagesIrvSep16.length) {
            $('#irvine-sep16employees').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/irvine/thumb/sep-16-employees/' + imagesIrvSep16[counterIrvSep16] + '" > <div class="caption"><p><a href="/templates/image-library/hr/irvine/sep-16-employees/' + imagesIrvSep16[counterIrvSep16] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterIrvSep16++;
        }


		// December 17 Employee

        var imagesIrvDec17 = ['01.jpg','02.jpg','03.jpg','04.jpg','05.jpg','06.jpg','07.jpg','08.jpg','09.jpg','10.jpg','11.jpg','12.jpg','13.jpg','14.jpg','15.jpg','16.jpg','17.jpg','18.jpg','19.jpg','20.jpg','21.jpg','22.jpg','23.jpg','24.jpg','25.jpg'];

        var counterIrvDec17 = 0;

        while (counterIrvDec17 < imagesIrvDec17.length) {
            $('#irvine-dec17employees').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/irvine/thumb/dec-17-employees/' + imagesIrvDec17[counterIrvDec17] + '" > <div class="caption"><p><a href="/templates/image-library/hr/irvine/dec-17-employees/' + imagesIrvDec17[counterIrvDec17] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterIrvDec17++;
        }
		
		
		// February 18 Employee

        var imagesIrvFeb18 = ['001.jpg','002.jpg','003.jpg','004.jpg','005.jpg','006.jpg','007.jpg','008.jpg','009.jpg','010.jpg','011.jpg','012.jpg','013.jpg','014.jpg','015.jpg','016.jpg','017.jpg','018.jpg','019.jpg','020.jpg','021.jpg','022.jpg','023.jpg','024.jpg','025.jpg','026.jpg','027.jpg','028.jpg','029.jpg','030.jpg','031.jpg','032.jpg','033.jpg','034.jpg','035.jpg','036.jpg','037.jpg','038.jpg','039.jpg','040.jpg','041.jpg','042.jpg','043.jpg','044.jpg','045.jpg','046.jpg','047.jpg','048.jpg','049.jpg','050.jpg','051.jpg','052.jpg','053.jpg','054.jpg','055.jpg','056.jpg','057.jpg','058.jpg','059.jpg','060.jpg','061.jpg','062.jpg','063.jpg','064.jpg','065.jpg','066.jpg','067.jpg','068.jpg','069.jpg','070.jpg','071.jpg','072.jpg','073.jpg','074.jpg','075.jpg','076.jpg','077.jpg','078.jpg','079.jpg','080.jpg','081.jpg','082.jpg','083.jpg','084.jpg','085.jpg','086.jpg','087.jpg','088.jpg','089.jpg','090.jpg','091.jpg','092.jpg','093.jpg','094.jpg','095.jpg','096.jpg','097.jpg','098.jpg','099.jpg','100.jpg','101.jpg','102.jpg','103.jpg','104.jpg','105.jpg','106.jpg','107.jpg','108.jpg','109.jpg','110.jpg','111.jpg','112.jpg','113.jpg','114.jpg','115.jpg','116.jpg','117.jpg','118.jpg','119.jpg','120.jpg','121.jpg','122.jpg','123.jpg','124.jpg','125.jpg','126.jpg','127.jpg'];

        var counterIrvFeb18 = 0;

        while (counterIrvFeb18 < imagesIrvFeb18.length) {
            $('#irvine-feb18employees').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/irvine/thumb/feb-18-employees/' + imagesIrvFeb18[counterIrvFeb18] + '" > <div class="caption"><p><a href="/templates/image-library/hr/irvine/feb-18-employees/' + imagesIrvFeb18[counterIrvFeb18] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterIrvFeb18++;
        }
		
		
		// EU Recruiting Team

        var imagesEUTeam = ['01.jpg','02.jpg','03.jpg','04.jpg','05.jpg','06.jpg','07.jpg','08.jpg','09.jpg','10.jpg','11.jpg','12.jpg','13.jpg','14.jpg','15.jpg','16.jpg','17.jpg','18.jpg','19.jpg','20.jpg','21.jpg','22.jpg','23.jpg','24.jpg','25.jpg','26.jpg','27.jpg','28.jpg','29.jpg','30.jpg','31.jpg','32.jpg','33.jpg','34.jpg','35.jpg','36.jpg','37.jpg','38.jpg','39.jpg','40.jpg','41.jpg','42.jpg','43.jpg','44.jpg','45.jpg','46.jpg','47.jpg','48.jpg','49.jpg','50.jpg','51.jpg','52.jpg','53.jpg','54.jpg','55.jpg','56.jpg'];

        var counterEUTeam = 0;

        while (counterEUTeam < imagesEUTeam.length) {
            $('#irvine-eurecruiting').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/irvine/thumb/edwards-university-recruiting/' + imagesEUTeam[counterEUTeam] + '" > <div class="caption"><p><a href="/templates/image-library/hr/irvine/edwards-university-recruiting/' + imagesEUTeam[counterEUTeam] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterEUTeam++;
        }				


 
         // Draper Section
		 
		 //2016 Shoot

        var imagesDraper2016 = ['01.jpg','02.jpg','03.jpg','04.jpg','05.jpg','06.jpg','07.jpg','08.jpg','09.jpg','10.jpg','11.jpg','12.jpg','13.jpg','14.jpg','15.jpg','16.jpg','17.jpg','18.jpg','19.jpg','20.jpg','21.jpg','22.jpg','23.jpg','24.jpg','25.jpg','26.jpg','27.jpg','28.jpg','29.jpg','30.jpg','31.jpg','32.jpg','33.jpg','34.jpg','35.jpg'];

        var counterDraper2016 = 0;

        while (counterDraper2016 < imagesDraper2016.length) {
            $('#draper-2016').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/draper/thumb/2016-employees/' + imagesDraper2016[counterDraper2016] + '" > <div class="caption"><p><a href="/templates/image-library/hr/draper/2016-employees/' + imagesDraper2016[counterDraper2016] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterDraper2016++;
        }
		
		 //2017 Shoot

        var imagesDraper2017 = ['01.jpg','02.jpg','03.jpg','04.jpg','05.jpg','06.jpg','07.jpg','08.jpg','09.jpg','10.jpg','11.jpg','12.jpg','13.jpg','14.jpg','15.jpg','16.jpg','17.jpg','18.jpg','19.jpg','20.jpg','21.jpg'];

        var counterDraper2017 = 0;

        while (counterDraper2017 < imagesDraper2017.length) {
            $('#draper-2017').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/draper/thumb/2017-employees/' + imagesDraper2017[counterDraper2017] + '" > <div class="caption"><p><a href="/templates/image-library/hr/draper/2017-employees/' + imagesDraper2017[counterDraper2017] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterDraper2017++;
        }
		
		 //2018 Shoot

        var imagesDraper2018 = ['01.jpg','02.jpg','03.jpg','04.jpg','05.jpg','06.jpg','07.jpg','08.jpg','09.jpg','10.jpg','11.jpg','12.jpg','13.jpg','14.jpg','15.jpg','16.jpg','17.jpg','18.jpg','19.jpg','20.jpg','21.jpg','22.jpg'];

        var counterDraper2018 = 0;

        while (counterDraper2018 < imagesDraper2018.length) {
            $('#draper-2018').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/draper/thumb/2018-employees/' + imagesDraper2018[counterDraper2018] + '" > <div class="caption"><p><a href="/templates/image-library/hr/draper/2018-employees/' + imagesDraper2018[counterDraper2018] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterDraper2018++;
        }				
 
 
          // Costa Rica Section
		  
		  // headshots

        var imagesCostahead = ['001.jpg','002.jpg','003.jpg','004.jpg','005.jpg','006.jpg','007.jpg','008.jpg','009.jpg','010.jpg','011.jpg','012.jpg','013.jpg','014.jpg','015.jpg','016.jpg','017.jpg','018.jpg','019.jpg','020.jpg','021.jpg','022.jpg','023.jpg','024.jpg','025.jpg','026.jpg','027.jpg','028.jpg','029.jpg','030.jpg','031.jpg','032.jpg','033.jpg','034.jpg','035.jpg','036.jpg','037.jpg','038.jpg','039.jpg','040.jpg','041.jpg','042.jpg','043.jpg','044.jpg','045.jpg','046.jpg','047.jpg','048.jpg','049.jpg','050.jpg','051.jpg','052.jpg','053.jpg','054.jpg','055.jpg','056.jpg','057.jpg','058.jpg','059.jpg','060.jpg','061.jpg','062.jpg','063.jpg','064.jpg','065.jpg','066.jpg','067.jpg','068.jpg','069.jpg','070.jpg','071.jpg','072.jpg','073.jpg','074.jpg','075.jpg','076.jpg','077.jpg','078.jpg','079.jpg','080.jpg','081.jpg','082.jpg','083.jpg','084.jpg','085.jpg','086.jpg','087.jpg','088.jpg','089.jpg','090.jpg','091.jpg','092.jpg','093.jpg','094.jpg','095.jpg','096.jpg','097.jpg','098.jpg','099.jpg','100.jpg','101.jpg','102.jpg','103.jpg','104.jpg','105.jpg','106.jpg','107.jpg','108.jpg','109.jpg','110.jpg','111.jpg','112.jpg','113.jpg','114.jpg','115.jpg','116.jpg','117.jpg','118.jpg','119.jpg','120.jpg','121.jpg','122.jpg','123.jpg','124.jpg','125.jpg','126.jpg','127.jpg','128.jpg','129.jpg','130.jpg','131.jpg','132.jpg','133.jpg','134.jpg','135.jpg','136.jpg','137.jpg','138.jpg','139.jpg','140.jpg','141.jpg','142.jpg','143.jpg','144.jpg','145.jpg','146.jpg','147.jpg','148.jpg','149.jpg','150.jpg','151.jpg','152.jpg','153.jpg','154.jpg','155.jpg','156.jpg','157.jpg','158.jpg','159.jpg','160.jpg','161.jpg','162.jpg','163.jpg','164.jpg','165.jpg','166.jpg','167.jpg','168.jpg','169.jpg','170.jpg','171.jpg','172.jpg','173.jpg','174.jpg','175.jpg','176.jpg','177.jpg','178.jpg','179.jpg','180.jpg','181.jpg','182.jpg','183.jpg','184.jpg','185.jpg','186.jpg','187.jpg','188.jpg','189.jpg','190.jpg','191.jpg','192.jpg','193.jpg','194.jpg','195.jpg','196.jpg','197.jpg','198.jpg','199.jpg','200.jpg','201.jpg','202.jpg','203.jpg','204.jpg','205.jpg','206.jpg','207.jpg','208.jpg','209.jpg','210.jpg','211.jpg','212.jpg'];

        var counterCostahead = 0;

        while (counterCostahead < imagesCostahead.length) {
            $('#costarica-headshots').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/costarica/thumb/headshots/' + imagesCostahead[counterCostahead] + '" > <div class="caption"><p><a href="/templates/image-library/hr/costarica/headshots/' + imagesCostahead[counterCostahead] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterCostahead++;
        }  
		
		// plant
		
        var imagesCostaplant = ['001.jpg','002.jpg','003.jpg','004.jpg','005.jpg','006.jpg','007.jpg','008.jpg','009.jpg','010.jpg','011.jpg','012.jpg','013.jpg','014.jpg','015.jpg','016.jpg','017.jpg','018.jpg','019.jpg','020.jpg','021.jpg','022.jpg','023.jpg','024.jpg','025.jpg','026.jpg','027.jpg','028.jpg','029.jpg','030.jpg','031.jpg','032.jpg','033.jpg','034.jpg','035.jpg','036.jpg','037.jpg','038.jpg','039.jpg','040.jpg','041.jpg','042.jpg','043.jpg','044.jpg','045.jpg','046.jpg','047.jpg','048.jpg','049.jpg','050.jpg','051.jpg','052.jpg','053.jpg','054.jpg','055.jpg','056.jpg','057.jpg','058.jpg','059.jpg','060.jpg','061.jpg','062.jpg','063.jpg','064.jpg','065.jpg','066.jpg','067.jpg','068.jpg','069.jpg','070.jpg','071.jpg','072.jpg','073.jpg','074.jpg','075.jpg','076.jpg','077.jpg','078.jpg','079.jpg','080.jpg','081.jpg','082.jpg','083.jpg','084.jpg','085.jpg','086.jpg','087.jpg','088.jpg','089.jpg','090.jpg','091.jpg','092.jpg','093.jpg','094.jpg','095.jpg','096.jpg','097.jpg','098.jpg','099.jpg','100.jpg','101.jpg','102.jpg','103.jpg','104.jpg','105.jpg','106.jpg','107.jpg','108.jpg','109.jpg','110.jpg','111.jpg','112.jpg','113.jpg','114.jpg','115.jpg','116.jpg','117.jpg','118.jpg','119.jpg','120.jpg','121.jpg','122.jpg','123.jpg','124.jpg','125.jpg','126.jpg','127.jpg','128.jpg','129.jpg','130.jpg','131.jpg','132.jpg','133.jpg','134.jpg','135.jpg','136.jpg','137.jpg','138.jpg','139.jpg','140.jpg','141.jpg','142.jpg','143.jpg','144.jpg','145.jpg','146.jpg','147.jpg','148.jpg','149.jpg','150.jpg','151.jpg','152.jpg','153.jpg','154.jpg','155.jpg','156.jpg','157.jpg','158.jpg','159.jpg','160.jpg','161.jpg','162.jpg','163.jpg','164.jpg'];

        var counterCostaplant = 0;

        while (counterCostaplant < imagesCostaplant.length) {
            $('#costarica-plant').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/costarica/thumb/plant/' + imagesCostaplant[counterCostaplant] + '" > <div class="caption"><p><a href="/templates/image-library/hr/costarica/plant/' + imagesCostaplant[counterCostaplant] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterCostaplant++;
        }  
		
		// celebration
		
        var imagesCostacel = ['001.jpg','002.jpg','003.jpg','004.jpg','005.jpg','006.jpg','007.jpg','008.jpg','009.jpg','010.jpg','011.jpg','012.jpg','013.jpg','014.jpg','015.jpg','016.jpg','017.jpg','018.jpg','019.jpg','020.jpg','021.jpg','022.jpg','023.jpg','024.jpg','025.jpg','026.jpg','027.jpg','028.jpg','029.jpg','030.jpg','031.jpg','032.jpg','033.jpg','034.jpg','035.jpg','036.jpg','037.jpg','038.jpg','039.jpg','040.jpg','041.jpg','042.jpg','043.jpg','044.jpg','045.jpg','046.jpg','047.jpg','048.jpg','049.jpg','050.jpg','051.jpg','052.jpg','053.jpg','054.jpg','055.jpg','056.jpg','057.jpg','058.jpg','059.jpg','060.jpg','061.jpg','062.jpg','063.jpg','064.jpg','065.jpg','066.jpg','067.jpg','068.jpg','069.jpg','070.jpg','071.jpg','072.jpg','073.jpg','074.jpg','075.jpg','076.jpg','077.jpg','078.jpg','079.jpg','080.jpg','081.jpg','082.jpg','083.jpg','084.jpg','085.jpg','086.jpg','087.jpg','088.jpg','089.jpg','090.jpg','091.jpg','092.jpg','093.jpg','094.jpg','095.jpg','096.jpg','097.jpg','098.jpg','099.jpg','100.jpg','101.jpg','102.jpg','103.jpg','104.jpg','105.jpg','106.jpg','107.jpg','108.jpg','109.jpg','110.jpg','111.jpg','112.jpg','113.jpg','114.jpg','115.jpg','116.jpg','117.jpg','118.jpg','119.jpg','120.jpg','121.jpg','122.jpg','123.jpg','124.jpg','125.jpg','126.jpg','127.jpg','128.jpg','129.jpg','130.jpg','131.jpg','132.jpg','133.jpg','134.jpg','135.jpg','136.jpg','137.jpg','138.jpg','139.jpg','140.jpg','141.jpg','142.jpg','143.jpg','144.jpg','145.jpg','146.jpg','147.jpg','148.jpg','149.jpg','150.jpg','151.jpg','152.jpg','153.jpg','154.jpg','155.jpg','156.jpg','157.jpg','158.jpg','159.jpg','160.jpg','161.jpg','162.jpg','163.jpg','164.jpg','165.jpg','166.jpg','167.jpg','168.jpg','169.jpg','170.jpg','171.jpg','172.jpg','173.jpg','174.jpg','175.jpg','176.jpg','177.jpg','178.jpg','179.jpg','180.jpg','181.jpg','182.jpg','183.jpg','184.jpg','185.jpg','186.jpg','187.jpg','188.jpg','189.jpg','190.jpg','191.jpg','192.jpg','193.jpg','194.jpg','195.jpg','196.jpg','197.jpg','198.jpg','199.jpg','200.jpg'];

        var counterCostacel = 0;

        while (counterCostacel < imagesCostacel.length) {
            $('#costarica-celebration').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/costarica/thumb/celebration/' + imagesCostacel[counterCostacel] + '" > <div class="caption"><p><a href="/templates/image-library/hr/costarica/celebration/' + imagesCostacel[counterCostacel] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterCostacel++;
        }  				


          // Ireland Section
		  
		  //2017 employees

        var imagesIreland2017 = ['001.jpg','002.jpg','003.jpg','004.jpg','005.jpg','006.jpg','007.jpg','008.jpg','009.jpg','010.jpg','011.jpg','012.jpg','013.jpg','014.jpg','015.jpg','016.jpg','017.jpg','018.jpg','019.jpg','020.jpg','021.jpg','022.jpg','023.jpg','024.jpg','025.jpg'];

        var counterIreland2017 = 0;

        while (counterIreland2017 < imagesIreland2017.length) {
            $('#ireland-2017').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/ireland/thumb/2017-employees/' + imagesIreland2017[counterIreland2017] + '" > <div class="caption"><p><a href="/templates/image-library/hr/ireland/2017-employees/' + imagesIreland2017[counterIreland2017] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterIreland2017++;
        }  


          // Prague Section
		  
		  // City tour

        var imagesPragueCity = ['001.jpg','002.jpg','003.jpg','004.jpg','005.jpg','006.jpg','007.jpg','008.jpg','009.jpg','010.jpg','011.jpg','012.jpg','013.jpg','014.jpg','015.jpg','016.jpg','017.jpg','018.jpg','019.jpg','020.jpg','021.jpg'];

        var counterPragueCity = 0;

        while (counterPragueCity < imagesPragueCity.length) {
            $('#prague-city').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/prague/thumb/city-tour/' + imagesPragueCity[counterPragueCity] + '" > <div class="caption"><p><a href="/templates/image-library/hr/prague/city-tour/' + imagesPragueCity[counterPragueCity] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterPragueCity++;
        }  


          // Singapore Section

        var imagesSingapore = [];

        var counterSingapore = 0;

        while (counterSingapore < imagesSingapore.length) {
            $('#singapore').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="/templates/image-library/hr/singapore/thumb/' + imagesSingapore[counterSingapore] + '" > <div class="caption"><p><a href="/templates/image-library/hr/singapore/' + imagesSingapore[counterSingapore] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterSingapore++;
        }  


        </script>
    </body>

    </html>
