﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="pat-color" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Patient awareness</li>
  <li>Color and typography</li>
  </ol>
  
  
  
  <h2>Color and typography
  <a href="pdf/p-patient-awareness.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
  <div class="well intro"><p>Materials directed toward patients may incorporate additional cooler colors for more flexibility and to communicate a calming and comforting tone</p></div>
 
<div class="row">
<div class="col-md-4">
  <p><strong>Extended palette</strong></p>

  <p>
    <em>
      These additional colors may only be used for
      direct-to-patient materials.
    </em>
  </p>

  <p>
    Although red and titanium will always be
    Edwards’ signature colors, this palette
    was chosen to integrate with the primary,
    secondary and neutral palettes.
  </p>

  <p>
    The extended palette may be used solid
    (at 100%) in small areas, or tinted for
    backgrounds. These colors may be used for
    call-to-action buttons in digital applications,
    as a tinted background for a text call-out or
    for infographics.
  </p>

  

  



</div>
<div class="col-md-8">
    <p><img src="img/pat-color-image.png" class="img-responsive"></p>
</div>

</div>
<div class="row">
    <div class="col-md-12">
      <p>
        Use the dark gray (PANTONE 432) when
        using a gray title as described on page 102,
        as it provides sufficient contrast on light
        backgrounds. Do not use any of these other
        colors for copy.
      </p>
    
      <p>
        Do not use any of these colors in place of red
        within the secondary graphic.
      </p>
      <p><strong>Color contrast</strong></p>

      <p>
        For call-outs or UI elements use white
        type on a red background or red, dark gray
        (PANTONE 432) or black type on a white or
        light background. Use tints of the extended
        palette (not red) as background colors to
        ensure messages are clearly legible.
      </p>
    
      <p><strong>Incorporating colors with imagery</strong></p>
    
      <p>
        The extended palette colors may also be
        incorporated into photography, illustrations,
        charts, diagrams and infographics.
      </p>
    </div>
</div>



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
