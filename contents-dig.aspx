﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-dig" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>Digital content</li>
  </ol>
  
  
  
  <h2>Digital content<a href="pdf/p-digital-content.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  
<h3>Website URL</h3>

<p>The Edwards website – edwards.com – should be referenced on all printed
materials. Region specific materials may reference regional URL (example:
edwards.com/JP). The URL should be lowercase, including when it appears in body copy, however it may be italic or bold weight to differentiate it from normal text.</p>
<p><em>Exception:</em> Exhibit graphics or other display materials where it would not be
appropriate to list the website.</p>
      
<h3>Email signature line</h3>
      
<p>The following email signature line is the only acceptable form for emails
distributed by Edwards employees. Include your relevant contact information
only. Please note, the Edwards logo, graphic images, taglines or quotes may not
be included in the email signature line. Email and signature font should be
Arial or the default system font available in Edwards’ email system.</p>

<div class="row">
<div class="col-md-8">

<div class="well">

<div class="row">
<div class="col-md-6">
<p>Jane Smith<br>
Manager, Global Communications<br>
Edwards Lifesciences<br>
One Edwards Way<br>
Irvine CA 92614<br>
Tel: 949-250-2500<br>
Fax: 949-250-2525<br>
Mobile: 949-250-2500<br>
Jane_Smith@edwards.com<br>
edwards.com</p>
</div>


<div class="col-md-6">

<p class="edw-titan text-right"><em>
name<br>
title<br>
Edwards Lifesciences<br>
address<br>
address<br>
phone<br>
fax, if applicable<br>
mobile, if applicable<br>

email address<br>
website


</em></p>

</div>

</div>



</div>
</div>
</div>




</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
