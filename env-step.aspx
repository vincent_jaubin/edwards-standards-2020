﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="env-step" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Environmental Graphics</li>
  <li>Step and repeat banners</li>
  </ol>
  
  
  
  <h2>Step and repeat banners
  <a href="pdf/p-patient-awareness.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
  <div class="well intro"><p>The step and repeat banner is a backdrop
    which is used for special events. It includes
    either a repeating Edwards logo, or the
    Edwards logo plus another logo (co-branded).
    It measures 8'W x 8'H or 244 cm x 244 cm.</p></div>
 
<div class="row">
<div class="col-md-4">
<p><strong>Logo</strong></p>
<p>
    The step and repeat banner uses version 1
    of the Edwards logo. It is scaled to 6.0" or
    152 mm, based on the height of the E symbol.
    The logos are arranged in a staggered pattern
    and are spaced using the width (X) and height
    (Y) of the logo as shown.
</p>

<p>
    This is the only application where the Edwards
    logo may be repeated.
</p>

<p><strong>Co-branding</strong></p>

<p>
    The banner may include another logo for a cobranded
    application. The logos with a dotted
    line ( ) indicate which Edwards logos may
    be replaced with the other logo. Be sure the
    other logo is scaled so that it is proportionate
    to the Edwards logo.
</p>


</div>
<div class="col-md-8">
    <p><img src="img/env-step-image.png" class="img-responsive"></p>
</div>

</div>
<div class="row">
  <div class="col-md-12">
    <p><strong>Background colors</strong></p>

<p>
    The background of the step and repeat banner
    may be white, a 50% tint of primary gray, or a
    50% tint of a color from the neutral palette.
    On any of these background options use
    the positive version of the logo where the
    Edwards name is 100% black.
</p>

<p>
    A reverse Edwards logo, where the Edwards
    name is white, may appear on a 100% red
    background.
</p>

<p>
    Only use one continuous color for the banner
    background.
</p>
  </div>
</div>



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
