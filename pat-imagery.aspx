﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="pat-imagery" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Patient awareness</li>
  <li>Imagery</li>
  </ol>
  
  
  
  <h2>Imagery
  <a href="pdf/p-patient-awareness.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
  <div class="well intro"><p>Approachable images will help make patient materials more engaging. Choosing images with blue tones will make communications more approachable, especially with patient audiences.</p></div>
 
<div class="row">
<div class="col-md-4">
<p><strong>Cooler colors</strong></p>

<p>
  Cooler tones, such as blues and lavenders,
  are more calming, comforting and stable for
  patients. For these reasons, incorporate cool
  tones into photography, either as a natural
  element – such as sky or water – or as a light
  background color.
</p>

<p>
  Light backgrounds will allow for more
  flexibility across various patient formats and
  applications. This will also help to distinguish
  patient communications from corporate
  communications while remaining consistent
  across distribution channels and media.
</p>


  

  



</div>
<div class="col-md-8">
    <p><img src="img/pat-imagery-image.png" class="img-responsive"></p>
</div>

</div>
<div class="row">
    <div class="col-md-12">
      <p><strong>Red accents</strong></p>

<p>
  Red is an important part of the Edwards
  brand. As described on the previous page,
  if red is not used as the headline or title,
  red must be incorporated into the image.
  Either select images that have noticeable
  red accents within the photo, or modify the
  photo to incorporate bold areas of red.
</p>

<p><strong>Legibility</strong></p>

<p>
  On materials where the title or headline
  is placed over the image, ensure there is
  sufficient contrast between the message and
  the image background, particularly if using
  smaller type sizes. In some cases the image
  background will need to be lightened or
  blurred to ensure the typography is clearly
  visible.
</p>

    </div>
</div>



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
