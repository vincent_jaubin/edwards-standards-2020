﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-visual" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>Visual Representation</li>
  </ol>
  
  
  
  <h2>Visual Representation<a href="pdf/p-visual-rep.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  
  
  
  <div class="well intro">
  <p>In exploring ways to translate our brand promise to a visual
style, it is important to be authentic to who and what Edwards is.
Our brand system has been designed to do just that.</p>
</div>


<p>Leveraging Edwards’ known and differentiating association with the color red
while incorporating a modern holding shape, we are able to communicate our
key personality attributes of passion and collaboration. At the same time, we
visually convey our important positioning message of transformation.</p>

<p>The elements of our visual system do not work in isolation; they have been
designed to live with and complement each other so no one component needs
to do the heavy lifting to tell the complete Edwards brand story. Here is how
the pieces come together to tell that story.</p>

<p>The brand standards that follow have been developed with collective input and
deliberate intent to carefully build an integrated system that celebrates the
history of Edwards Lifesciences while preparing to carry us long into the future.</p>

<p>As you work with these standards, we hope they will infuse your efforts with a refreshed spirit, as you play a part in Edwards Lifesciences’ continued commitment to working with clinicians to improve patient outcomes and enhance lives worldwide.</p>




<img src="img/visual-presentation.jpg" class="img-responsive" />

<!--
<ul>

<li>Red is used as the dominant color within both the primary and secondary
color palettes to reinforce Edwards’ passion and differentiate the company
among our competitors</li>
<li>The secondary graphic communicates transformation and collaboration
with two fixed elements both working together, as well as growing from
small to large. It is a statement of where we have been and where we are
going, as well as metaphor for our collaborative role in helping patients
return to well-being and health.</li>
<li>Our recommended photography style, whether for portraits, products
or lifestyle images, is light and bright, incorporating a neutral background
to reinforce the emphasis of our passion by retaining the focus on our red
palette.</li>
<li>Non-product-related imagery also emphasizes people, reinforcing our
dedication to patients and our collaboration with clinicians as a primary
pillar of our brand.</li>
<li>And finally, a subtle background pattern has been added to the system
for vibrancy and texture. The background is comprised of stylized “Es” and
communicates the collaborative nature of three elements coming together
as one visual element.</li>
</ul>

<p>As you work with these standards, we hope they will infuse your efforts
with a refreshed spirit, as you play a part in Edwards Lifesciences’ continued
commitment to working with clinicians to improve patient outcomes and
enhance lives worldwide.</p>-->



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
