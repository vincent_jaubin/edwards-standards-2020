﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="elements-altlogo" class="page">
  
  <div class="jumbotron banner-edw banner-branding">
    <div class="container">
      <div class="redboxes">
      <div class="redbox-sm"></div>
      <div class="redbox-lg">
        <h1>Edwards Lifesciences privacy statement</h1>
      </div>
      </div>
    </div>
  </div><!-- /.banner-edw --> 
  
  <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-12 main-content">
  
  <ol class="breadcrumb">
  <li><a href="index.aspx">Home</a></li>
  <li>Edwards Lifesciences privacy statement</li>
  </ol>
  
  
  
  <h2>Edwards Lifesciences privacy statement</h2>
  

<p>(Effective October&nbsp; 1, 2015 &ndash; United States)</p>
<p><span><a href="http://edwardsprod.blob.core.windows.net/media/Default/legal/edwards%20lifesciences%20privacy%20statement.pdf" target="_blank"><i class="icon-file-pdf"></i> Download PDF</a>&nbsp;</span></p>
<p>Edwards Lifesciences Corporation, Edwards Lifesciences LLC, and their affiliated companies (collectively &ldquo;Edwards Lifesciences,&rdquo; &ldquo;our,&rdquo; or &ldquo;we&rdquo;) recognize the importance of online privacy and are committed to protecting the privacy and privacy choices of our online visitors. This Privacy Statement describes our policies and practices on the collection, use, and storage of information on &ldquo;www.edwards.com&rdquo; or any website owned, operated or controlled by us (&ldquo;Sites&rdquo;).&nbsp;&nbsp;</p>
<p>Please note that our websites might offer links to other websites that are owned or controlled by third parties.&nbsp; This Privacy Statement applies only to Edwards Lifesciences&rsquo; Sites.&nbsp; We do not control or endorse the content of any third-party sites and cannot accept responsibility for the privacy practices of any websites not controlled by us.&nbsp; You will be notified when a link takes you to a site that is not owned or controlled by Edwards Lifesciences, and we encourage you to review the privacy policies of any third-party sites.</p>
<p><a href="#Info">Information We Collect on Our Sites <i class="icon-circle-right"></i></a><br>
<a href="#HOW">How We Use the Information We Collect <i class="icon-circle-right"></i></a><br>
<a href="#DISCLOSURE">Disclosure of Information to Third Parties <i class="icon-circle-right"></i></a><br>
<a href="#PRIVACY">Your Privacy Choices  <i class="icon-circle-right"></i></a><br>
<a href="#CHILDREN">Children&rsquo;s Privacy  <i class="icon-circle-right"></i></a><br>
<a href="#COMMITMENT">Our Commitment to Data Security and Protection  <i class="icon-circle-right"></i></a><br>
<a href="#CONCERNS">If You Have Concerns about Your Personal Information  <i class="icon-circle-right"></i></a><br>
<a href="#CHANGES">Changes to Our Privacy Statement  <i class="icon-circle-right"></i></a></p>
<h4><strong><a name="Info"></a>INFORMATION WE COLLECT ON OUR SITES</strong></h4>
<p>We have designed our Sites so that visitors can navigate and view Site content without providing any personal information to us. This section describes information that visitors might choose to provide, as well as our practices for the automatic collection and use of non-personal information such as an IP address and a visitor&rsquo;s navigation through the Sites.</p>
<h4><strong>Consent to Processing in the U.S. and Elsewhere</strong></h4>
<p>Although this Site content is intended for a U.S. audience, any information that you provide or we collect, will be available to Edwards Lifesciences affiliates and vendors around the world.&nbsp; This means that your information may be communicated to a country other than your country of residence for processing or storage, and it may also be communicated to third parties hired by us to provide services&nbsp;such as website hosting, database management, or analytics. We have taken measures to ensure your personal information remains protected to the standards described in this policy, wherever it is processed.&nbsp;Your use of the Sites means you consent to these transfers.&nbsp; For European visitors, please see below for information on our participation in U.S-EU and U.S.-Swiss Safe Harbor programs.</p>
<h4><strong>Information You Provide</strong></h4>
<p>Through your use of the Sites, you may choose to provide us with personal information that identifies you or your family members.&nbsp; Personal information includes a person&rsquo;s name, postal or email address, phone number, Social Security Number, information about your health or medical condition, or any other information that would allow Edwards Lifesciences to identify or contact you.&nbsp; For example, a patient who has received one of Edwards Lifesciences&rsquo; products may choose to share his or her experiences with us. A person seeking employment may choose to apply for a job online or request additional information about job opportunities.&nbsp; A health care professional might ask that we send information about one or more of our products, or report information about a specific patient.&nbsp; A charitable organization might request a donation.&nbsp; &nbsp;&nbsp;&nbsp;</p>
<p>In addition, some aspects of our Sites may be restricted to use only by licensed health care professionals, and access to those portions of the Sites may require personal information to facilitate registration and verification.&nbsp;</p>
<p>In these instances, in order to fulfill your request, we may ask for personal information such as your name, postal or email address, phone number, and perhaps a medical license number or other information to verify eligibility to view certain restricted content or to allow you to register for an event. &nbsp;In limited circumstances, such as an adverse event with one of our products, we may also be required to obtain personal information about your medical condition to enable required reporting to government regulatory agencies. Health care professionals should ensure that they have obtained their patient&rsquo;s consent (where required) before submitting personal or medical information about their patients.</p>
<h4><strong>Information We Collect Automatically</strong></h4>
<p>In order to improve the online experience, we routinely collect certain non-personal information about our visitors and their use of our Sites. For example, we record the IP address of each computer that logs onto our Sites.&nbsp; This IP address does not identify you personally, but it does allow us to maintain communication with your computer during your visit. It also allows us to identify the country you are visiting from, so that we may direct you automatically to the content that is appropriate for your country.</p>
<p>We also use &ldquo;cookies,&rdquo; which are small text files that are placed on your computer.&nbsp; Cookies allow us to identify your navigation of the Sites and any specific pages or information that you visit.&nbsp; Cookies also allow us to identify your computer the next time you visit so that we can bring you to information on the Sites that may be of particular interest.&nbsp; For aspects of the Sites that require registration, cookies allow us to remember your login information.&nbsp; In addition, we hire third parties to provide services for us, and these third parties may also place cookies on your computer.&nbsp;</p>
<p>Your Web browser allows you to disable these &ldquo;cookies,&rdquo; although you should be aware that some aspects of the Sites may not work efficiently without cookies.&nbsp;</p>
<p>We may also place advertisements on third parties&rsquo; websites.&nbsp; These third parties may track website usage information, as well as information on &ldquo;click throughs&rdquo; from an advertisement to our Sites.&nbsp; This helps us understand the effectiveness of our advertising. This information is not personally identifiable.</p>
<h4><strong><a name="HOW"></a>HOW WE USE THE INFORMATION WE COLLECT</strong></h4>
<p>If you choose to provide personal information to us, we will use that information to respond to your specific inquiry or request. We will also give you an opportunity to tell us whether we may use your personal information to send you other information about Edwards Lifesciences&rsquo; products, activities, or opportunities. If at any time you wish to &ldquo;unsubscribe&rdquo; from these communications, you may do so by completing this <a href="http://www.edwards.com/unsubscribe-remove-from-mailing-lists" target="_blank">form</a>. Edwards Lifesciences does not sell or otherwise provide personal information to third parties for the third parties&rsquo; marketing purposes.&nbsp;</p>
<p>We may use the device-related and analytics information we and our partners collect (about your computer&rsquo;s IP address and through the use of cookies) during your future visits.&nbsp; This information allows us to direct you to specific content on the Sites that may be of interest and to remember your computer the next time you visit.&nbsp; If you visit multiple Edwards Lifesciences Sites , we may aggregate the information from your visits to better understand usage of our Sites.</p>
<p>We may also aggregate this <span>device-related and analytics</span>&nbsp;information we collect, which helps us to improve our Sites by monitoring interest in the various Site content and understanding how our visitors navigate the Sites. This information may also help us diagnose any problems with our Sites.&nbsp;</p>
<p>If you choose to provide personal information to us, we may combine that information with the non-personal information collected through IP address recognition and cookies, to help us understand your specific interests and to deliver pertinent information to you.</p>
<h4><strong><a name="DISCLOSURE"></a>DISCLOSURE OF INFORMATION TO THIRD PARTIES</strong></h4>
<p>Edwards Lifesciences does not routinely disclose to third parties the information collected from our Sites. We do have third parties that perform services for us, and those third parties have agreed to maintain the privacy and security of any personal information they receive from us. In addition, we may request your consent to share your personal information with a third party.&nbsp;</p>
<p>If a third party buys any of our group members or their assets, we may disclose information to that third party in connection with the sale. We will make clear that it may only use personal information it receives from us for the purposes for which it was originally intended.</p>
<p>In some instances, we may be required by law, subpoena, or court order to disclose information. For example, if you use a Site to report an adverse product experience, we may be required to report that information to the U.S. Food and Drug Administration (FDA) or to similar regulatory agencies in other countries.&nbsp;</p>
<h4><strong><a name="PRIVACY"></a>YOUR PRIVACY CHOICES</strong></h4>
<p>In your use of the Sites, you will be given opportunities to make choices about the data we receive and how we use it.</p>
<p><strong>Do Not Track Signals/Online Tracking</strong>.&nbsp; Some Web browsers may be set to send a &ldquo;Do Not Track&rdquo; (DNT) &nbsp;signal to the websites you visit.&nbsp; This signal instructs the website to not collect information about your activities across different websites. We currently do not respond to DNT signals. &nbsp;&nbsp;</p>
<p>However, Edwards Lifesciences does not track your activities on any third-party websites or applications (except for monitoring of your interaction with any of our advertisements on third-party sites), and we do not select content based on your activities on third party sites or applications.&nbsp; However, if you visit more than one Edwards Site, we do track your activities on all Edwards-run websites, so that we can improve your overall experience with Edwards. &nbsp;</p>
<p><strong>Receive information about Edwards</strong>.&nbsp; If you provide personal information to us, you may be asked whether you wish to &ldquo;opt in&rdquo; to future communications.&nbsp;&nbsp; If you choose to &ldquo;opt in,&rdquo; we may from time to time send you information about products or services.&nbsp; Even if you permit Edwards to send you additional information, you may change your mind at any time by completing this <a href="http://www.edwards.com/unsubscribe-remove-from-mailing-lists" target="_blank">form</a>&nbsp;or by following the instructions in any email communication we send you.</p>
<p><strong>Cookies</strong>.&nbsp; As noted, your Web browser settings allow you to reject all cookies.&nbsp; Please be aware that the Site functions may not work well if you choose to do so.</p>
<p><strong>Correcting or Changing Your Personal Information.</strong>&nbsp;&nbsp; If you wish to update or correct the personal information you have previously given us, please complete this <a href="http://www.edwards.com/correct-change-personal-information" target="_blank">form</a>.</p>
<p><strong>Removing Your Personal Information.</strong>&nbsp; You may request that Edwards Lifesciences delete your personal information by completing this <a href="http://www.edwards.com/remove-personal-information" target="_blank">form</a>. Edwards Lifesciences will use commercially reasonable efforts to identify and delete your personal information, unless we are prohibited by law from doing so.&nbsp; Please be aware that, because information may be stored in multiple locations, it may not always be possible to identify every location where your information is stored, and in some instances it may appear in a non-erasable format. &nbsp;</p>
<h4><strong><a name="CHILDREN"></a>CHILDREN&rsquo;S PRIVACY</strong></h4>
<p>Edwards Lifesciences does not intend to collect personal information from children under age 13, without consent from the child&rsquo;s parent or legal guardian. We do not share personal information of anyone under 13 years of age with anyone outside of Edwards (except where required by law, and to our agents who have committed to maintain data privacy).</p>
<p>Children under 13 years of age should not submit any personal information to us without the express permission of their parent or legal guardian. If you believe that your child has submitted personal information and you would like to request that their information be removed from our systems, please complete this <a class="btn btn-link c-red" href="/remove-info-under-age-13" target="_blank">form</a>.&nbsp; We will make reasonable efforts to comply with your request.</p>
<p><strong><a name="COMMITMENT"></a>OUR COMMITMENT TO DATA SECURITY AND PROTECTION</strong></p>
<p>Edwards Lifesciences understands that the security of your personal information is of the utmost importance, and we work to maintain your trust. &nbsp;We use industry-standard technologies to protect the security of the information that we collect and that which you provide to us.</p>
<p>We also comply with the U.S.-EU Safe Harbor Framework and the U.S.-Swiss Safe Harbor Framework, as set forth by the U.S. Department of Commerce, regarding the collection, use, and retention of personal information from European Union member countries and from Switzerland.&nbsp; Edwards Lifesciences has certified that it adheres to the Safe Harbor Privacy Principles of notice, choice, onward transfer, security, data integrity, access, and enforcement. To learn more about the Safe Harbor Program and to view the company&rsquo;s certification, please visit <a href="https://safeharbor.export.gov/">https://safeharbor.export.gov/</a><a href="https://safeharbor.export.gov/"></a></p>
<p>If you have any questions or concerns regarding our Safe Harbor certification, you can contact us using the contact information below and we will attempt to resolve your question or concerns. We will also cooperate and comply with any investigation, decision or advice made or given by competent EU or Swiss data protection authorities regarding personal information we receive under Safe Harbor.</p>
<p>Despite these reasonable efforts to safeguard personal information from loss, theft, alteration, or other types of misuse, we cannot guarantee that such loss will never occur.&nbsp; There is always a risk that a third party, without our authorization, might unlawfully access our systems or otherwise access personal information.&nbsp;</p>
<p><strong><a name="CONCERNS"></a>IF YOU HAVE CONCERNS ABOUT YOUR PERSONAL INFORMATION</strong></p>
<p>If you have any questions about this Privacy Statement or about your personal information, you may contact us at the below address:</p>
<div class="row"><div class="col-md-4"><div class="well"><p>Privacy Officer<br />Edwards Lifesciences<br />One Edwards Way<br />Irvine, California 92614<br /><br />Email: <a href="mailto:privacy@edwards.com">privacy@edwards.com</a></p></div></div></div>
<p>If you wish to receive a response by email, please be sure to include your name, postal address, and email address. &nbsp;If we do not receive an email address, we will respond by postal mail.</p>
<p><strong><a name="CHANGES"></a>CHANGES TO OUR PRIVACY STATEMENT</strong></p>
<p>From time to time, we may revise our Privacy Statement.&nbsp; When we do so, the changes will be prominently posted on this section of our Sites, along with the date the changes were made.&nbsp;</p>
<p>Our intention is to use personal information in accordance with the Privacy Statement in place at the time the information was collected.&nbsp; If we change the way in which we use or share personal information, you will be given an opportunity to choose whether to allow us to use your information in those new or different ways.</p>

  
 </div>
 </div>
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
