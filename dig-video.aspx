﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="dig-video" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Digital media</li>
  <li>Videos & animations</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Videos & animations<a href="pdf/p-video.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Basics</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Secondary graphic</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Content screens</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Basics</h3>   
 
 <div class="well intro">
 <p>Whether online or in a trade show
environment, simple, effective videos
and animations can bring messages,
products and other concepts to life.</p>
 </div>
 <div class="row">
<div class="col-md-4">
<p>Videos should incorporate Edwards’ basic
identity elements, from the logo, colors and
typography to the image style and secondary
graphic. Always keep the title-safe area in
mind when placing these elements.</p>
<p>The secondary graphic should be used on
title and transition screens. It may also be
used on other screens for headlines or titles.
The secondary graphic does not need to
appear on every screen, but it should appear
on a primary screen at least once, not only
on a secondary or end screen.</p>

<p>For call-outs, captions, support messages,
facts and figures, use shapes and graphics that
don’t distract from the secondary graphic,
such as rectangles with soft, feathered edges,
or simple lines or rules.</p>
</div>

<div class="col-md-8"><img src="img/video-basics-1.png" class="img-responsive"></div>
</div>
<div class="row">
<div class="col-md-4">
<p><strong>Logo introduction & end screens</strong></p>
<p>The Edwards logo – version 1 – must appear at
the beginning of a video, either scaled large
on a white or light background, or by using
one of the approved animated versions. The
logo should also appear on the bottom right
corner on an end screen or closing frame with
the legal and address information.</p>

</div>
<div class="col-md-8"><img src="img/video-basics-2.png" class="img-responsive"></div>
</div>

<div class="row">
<div class="col-md-4">

<p><strong>Dark backgrounds</strong></p>
<p>While a white or light background is always
preferred, if the environment or viewing
platform would be more effective with
a dark background, use the dark gray from
the secondary palette. Do not use black
Lifestyle images and footage should still
retain a bright, light background.</p>
</div>

<div class="col-md-8"><img src="img/video-basics-3.png" class="img-responsive"></div>
</div>



    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Secondary graphic</h3>   

<div class="well intro">
<p>When paired with video or static
images, animating the secondary
graphic reinforces the ideas of
transformation and collaboration.</p>
</div>

<div class="row">
<div class="col-md-4">
<p>When animating the secondary graphic, the
smaller red square may fade in near the left
edge of the frame, followed by the larger square
growing up and to the right, fading in, or wiping
from left to right. These techniques may apply
to the small or large secondary graphic.</p>
<p>If using a large secondary graphic that fills the
frame, video or static images may be used in
the larger square in place of a solid field of red.
Videos or static images may also appear behind
a small secondary graphic – in this case the
larger square may include a multiply effect as
described <a href="elements-secondary.aspx" target="_blank">here</a>.</p>

<p>The secondary graphic should appear at the start
of a video and for transitions. It does not need to
appear on every frame Be sure the graphic fits
within the title-safe area so it is not cropped.</p>
</div>

<div class="col-md-8"><img src="img/video-secondary-1.png" class="img-responsive"></div>
</div>
<p><strong>Incorrect uses</strong></p>
<p>The secondary graphic should be treated with
care. Avoid the following mistakes:</p>
<ul>
<li>Do not place the secondary graphic on the
right side of the frame, only on the left;</li>
<li>Do not use other solid shapes with the
secondary graphic;</li>
<li>Do not add shapes to the secondary graphic;</li>
<li>Do not use multiple secondary graphics in
one frame, or scale it small (i.e., for captions)</li>
</ul>
<p><img src="img/video-secondary-2.png" class="img-responsive"></p>


<p>Avoid gimmicky animations, such as bouncing,
spinning, flashing, checkered, circular or striped
effects with the secondary graphic, logo or other
graphics. Animations should be simple and tasteful.</p>



    
    
   </div>
   
   <div class="pane" id="tabs-3">
    <h3>Content screens</h3>   

<div class="row">
<div class="col-md-4">
<p>The secondary graphic does not need to
appear on every screen. On content screens
(screens that are not titles or transitions),
display products, people, charts or copy by
using the approved imagery style, color
palette and typography.</p>
<p><strong>Infographics</strong></p>
<p>Avoid shapes that mimic the secondary
graphic. Instead, use simple lines, soft-edged
gradient shapes or simple bands of color.
These types of graphics may appear opaque or
translucent, depending on the background.</p>
<p><strong>Colors</strong></p>
<p>Although not required, use primary red and
gray when possible, even if as an accent.</p>
</div>
<div class="col-md-8"><img src="img/video-content.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-12">

<p>Secondary and neutral colors may be used
at full opacity for backgrounds or captions
on videos or animations, but limit the use to
one area. For example, use the color as a full
background (see bottom right), a split screen
(see top right) or for a caption call-outs (see
center, left).</p>
<p>When placing elements on-screen, always
keep in mind the title-safe area.</p>
</div>
</div>

    
    
   </div>
   
   
   
    

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
