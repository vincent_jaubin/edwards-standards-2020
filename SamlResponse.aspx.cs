﻿using LoginRadiusSDK.V2.Models.ResponseModels.UserProfile;
using Saml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Configuration;

public partial class SamlResponse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string samlCertificate = ConfigurationManager.AppSettings["SamlCertificate"];



        // 2.Let's read the data - SAML providers usually POST it into the "SAMLResponse" var
        Saml.Response samlResponse = new Response(samlCertificate, Page.Request.Form["SAMLResponse"]);
        

        //// 3. We're done!
        if (samlResponse.IsValid())
        {            
            var profile = new Identity();
          
            try
            {
                profile.UserName = samlResponse.GetNameID();
                profile.FirstName = samlResponse.GetFirstName();
                profile.LastName = samlResponse.GetLastName();
                profile.FullName = profile.FirstName + profile.LastName;
                profile.Company = samlResponse.GetCompany();


                Session["roles"] = new List<string> { "User" };
                Session["profile"] = profile;

                FormsAuthentication.RedirectFromLoginPage(profile.FullName, true);
            }
            catch (Exception ex)
            {
                //insert error handling code
                //no, really, please do
                throw new Exception();
            }

            //user has been authenticated, put your code here, like set a cookie or something...
            //or call FormsAuthentication.SetAuthCookie() or something
        }
        else
        {
            Response.Redirect("/Login.aspx");
        }

    }
}