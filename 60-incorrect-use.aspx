﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->

<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="60-incorrect-use" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg banner-anniversary">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>60th Anniversary logo lockup standards</li>
  <li>Incorrect lockup uses</li>
  </ol>
  
  
  
  <h2>Incorrect lockup uses
  <a href="pdf/60-incorrect-use.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
  <div class="well intro"><p>The Edwards 60th Anniversary logo lockup is an important
part of our visual identity. Always follow these standards
and use the approved lockup artwork.</p></div>
 
<div class="row">
  <div class="col-md-7 col-md-push-5">
<p><img src="img/incorrect-lockup-1.png" class="img-responsive"></p>
</div>
<div class="col-md-5 col-md-pull-7">
<ol>
  <li><strong>Don’t change the logo lockup colors</strong><br>Use only the approved logo color versions as described on page 3 of
these standards.</li>
  <li><strong>Don’t reset the typography within the logo lockup</strong><br>Edwards and <em>60 Years of Discovery</em> have been carefully drawn and
should not be replaced with another typeface or message.</li>
  <li><strong>Don’t alter the gradients in the logo lockup</strong><br>Do not replace the gradients in the <em>60</em> letterforms, or use a gradient
for the E symbol. Only use an approved color version.</li>
  <li><strong>Don’t modify the elements of the logo lockup</strong><br>Do not alter the proportions or placement of the lockup.</li>
</ol>
</div>



</div>
<div class="row">
  <div class="col-md-7 col-md-push-5">
<p><img src="img/incorrect-lockup-2.png" class="img-responsive"></p>
</div>
<div class="col-md-5 col-md-pull-7">
<ol style="counter-reset: li 5">
  <li style="counter-increment: initial"><strong>Don’t distort the logo lockup</strong><br>Do not stretch, skew, rotate or otherwise distort the logo.</li>
  <li><strong>Don’t remove elements of the logo lockup</strong><br>The Edwards logo and <em>60 Years of Discovery</em> treatment are a lockup—
always use them together.</li>
  <li><strong>Don’t place the lockup on a background with insufficient contrast</strong><br>Always be sure the logo lockup is clearly visible, whether placing it on a solid colored background, or over a photo.</li>
  <li><strong>Don’t anchor the logo lockup to a background color or photo</strong><br>The logo lockup should not be anchored to a shape the way the stand alone Edwards logo would be. Only place the lockup on a continuous background, whether it is a field of color or a photo.</li>
</ol>
</div>



</div>


  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
