﻿<%@ Page Language="C#" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Api.Management.Account" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Exception" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Models.ResponseModels.UserProfile" %>
<%@ Import Namespace="ProfileApi=LoginRadiusSDK.V2.Api.Authentication.Account.ProfileApi" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Forgot Password</title>
    <script src="https://auth.lrcontent.com/v2/js/LoginRadiusV2.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" type="text/javascript"></script>
    <script src="/loginradius/config.js" type="text/javascript"></script>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:300);

        #login-form {
            width: 360px;
            padding: 8% 0 0;
            margin: auto;
        }

        #forgot-password-container {
            text-align: left;
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 360px;
            margin: 0 auto 100px;
            padding: 45px;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

            form input {
                font-family: "Roboto", sans-serif;
                outline: 0;
                background: #f2f2f2;
                width: 100%;
                border: 0;
                margin: 0 0 15px;
                padding: 15px;
                box-sizing: border-box;
                font-size: 14px;
            }

        .loginradius-submit {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #c8102e !important;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
        }


        form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

            form .message a {
                color: #4CAF50;
                text-decoration: none;
            }

        form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

            .container:before, .container:after {
                content: "";
                display: block;
                clear: both;
            }

            .container .info {
                margin: 50px auto;
                text-align: center;
            }

                .container .info h1 {
                    margin: 0 0 15px;
                    padding: 0;
                    font-size: 36px;
                    font-weight: 300;
                    color: #1a1a1a;
                }

                .container .info span {
                    color: #4d4d4d;
                    font-size: 12px;
                }

                    .container .info span a {
                        color: #000000;
                        text-decoration: none;
                    }

                    .container .info span .fa {
                        color: #EF3B3A;
                    }

        body {
            background: #505759; /* fallback for old browsers */
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .chk-public {
            display: inline;
            float: left;
            width: 36px;
        }
    </style>
</head>
<body>

    <div id="forgot-password-container">
        <div id="forgotpassword-container"></div>
        <div id="error-msg" style="color: red;"></div>
        <div id="success-msg" style="color: green;"></div>
        <asp:HyperLink ID="backToLogin" runat="server" NavigateUrl="~/Login.aspx" Text="Back to Login" />
    </div>
    <script>
        commonOptions.resetPasswordUrl = "ResetPassword.aspx";
        var LRObject = new LoginRadiusV2(commonOptions);

        var forgotpassword_options = {};

        forgotpassword_options.container = "forgotpassword-container";
        forgotpassword_options.onSuccess = function (response) {
            console.log(response);
            if (response.IsPosted) {
                var errorDiv = document.getElementById('error-msg');
                errorDiv.innerHTML = "";
                var successDiv = document.getElementById('success-msg');
                successDiv.innerHTML = "Password reset information sent to your provided email address, please check your email for further instructions";
            }


        };
        forgotpassword_options.onError = function (errors) {
            console.log(errors);
            var successDiv = document.getElementById('success-msg');
            successDiv.innerHTML = "";
            var errorDiv = document.getElementById('error-msg');
            if (errors[0].Description) {
                errorDiv.innerHTML = errors[0].Description;
            } else {
                errorDiv.innerHTML = errors[0].Message;
            }

        };

        LRObject.util.ready(function () {
            LRObject.init("forgotPassword", forgotpassword_options);
            $(".loginradius-submit").after($("#backToLogin"));
            $(".loginradius-submit").after($("#success-msg"));
            $(".loginradius-submit").after($("#error-msg"));
        });

    </script>
</body>

</html>
