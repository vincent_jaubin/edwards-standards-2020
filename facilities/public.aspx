﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body>
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



 <div id="public" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Public areas</li>
  <li>Corporate ads, Elevator wrap, & Timeline</li>
  </ol>
  
  
  
  <h2>Public areas<a href="assets/pdf/public-areas.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>


<h3>Edwards humanity</h3>
 <div class="well intro">
<p>The more we learn about and understand
patients, the better job we'll do of focusing our
decisions in the right way. Placing patients first
will guide our company to success.</p>
</div>


<p>Public areas for corporate messaging, include:</p>
<ul>
<li>Elevators</li>
<li>Conference rooms</li>
<li>Public hallways</li>
<li>Entrances/exits</li></ul>

<p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4964845" target="_blank">Edwards Humanity</a></small></p>

<p><small>Multiple files are provided with image options.<br>
These can be scaled for hallway areas
and materials are specified.</small></p>
  


<div class="row">
<div class="col-md-5">
<h4>Corporate advertising - example</h4>
<p><img src="images/corp-ad.PNG" width="371" height="495" class="img-responsive" /></p>
</div>


<div class="col-md-7 submarker elevator+" id="elevator">
<h4>Elevator wrap - example</h4>
<div class="row"><div class="col-md-7">
<p><img src="images/elevator-wrap1.PNG" width="326" height="542" class="img-responsive" /></p></div>
<div class="col-md-5"><p><img src="images/elevator-wrap2.PNG" width="159" height="205" class="img-responsive" /></p></div>
</div>
</div></div>

<a id="timeline" href="#" class="submarker timeline+"></a>
<h3>Timeline</h3>
<p>A corporate timeline is available for display
which maps out key Edwards achievements
globally. This is an excellent display for public
areas where tours are given and will provide an
overview of Edwards' history.</p>
<p>Additional Edwards legacy product may be
displayed in lobby cases.</p>

<p><strong>Acrylic display is preferred,
  <a href="display-specs.aspx">click here</a>.</strong></p>
  
<h4>Timeline - example</h4>  

<p><img src="images/timeline.PNG" width="648" height="802" class="img-responsive" /></p>

<p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4966419" target="_blank">Timeline</a></small></p>

<p><small>These can be scaled for hallway areas
and materials are specified</small></p>
</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
