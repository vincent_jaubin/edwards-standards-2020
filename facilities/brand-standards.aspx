﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



  <div id="brand-standards" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Brand standards</li>
  <li>Logo usage basics & Typography</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Brand standards<a href="assets/pdf/branding.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>


 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab active"><a href="#tabs-1" class="match-height"><span>Logo usage basics</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Typography</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Logo usage basics</h3>   
 
<p>There are many sizes and formats
available for the Edwards logo.
It is imperative that the logo always
be presented in a manner consistent
with Edwards' Brand Identity Standards.</p>
<p>
Please regularly check the standards
to ensure that your facility is
executing the appropriate size,
format and artwork in every
presentation of the Edwards logo.</p>

<p><small>Refer to <a href="../elements.aspx">Brand Identity Standards</a> for complete logo usage detail.</small></p>


<p><img src="images/brand-1.PNG" width="973" height="619" class="img-responsive" /></p>
<p><img src="images/brand-2.PNG" class="img-responsive" /></p>

</div>


    <div class="pane" id="tabs-2">
 <h3>Typography</h3>
 
 <p>Typography isn’t just what we say, it’s how we say it.
Vesta Std is a humanist typeface that balances the
simple lines of a sans-serif with the legibility of a serif.
In essence, it’s modern yet approachable.</p>
<p>Vesta Std is our preferred typeface and should be
used for print, promotional and environmental
applications. It is available in four weights and styles,
however, keep the number of styles and sizes to a
minimum.</p>
<p>Merriweather Sans is used for digital applications, and
Arial is used for office applications.</p>
  
<p><strong>Refer to <a href="../elements-type.aspx">Edwards Brand Identity Standards</a> for complete font usage detail.</strong></p>
  
<h4>Font selection</h4>
<p><img src="images/brand-3.PNG" class="img-responsive" /></p>


</div>


</div>

</div>
 </div>


    <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    

  </div>
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
