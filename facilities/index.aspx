﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body>
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
 <div class="page" id="home">
  
  <div class="jumbotron banner-edw banner-home banner-facilities">
    <div class="container">
      <div class="redboxes">
      <div class="redbox-sm"></div>
      <div class="redbox-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
    </div>
  </div><!-- /.banner-edw --> 
  
  <div class="container-fluid">
 
 <div class="row dot-borders">
 
  		<div class="col-md-3 col-sm-2">
        <div class="row">
        	<div class="col-md-5"><img src="images/home-thumb-ext.PNG" alt="Logo usage" class="img-responsive">
            </div>
            <div class="col-md-7">
            <h4>Exterior<br>&nbsp;</h4>
            <p><a href="exterior.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
            </div>
        </div>
        </div>
 
  		<div class="col-md-3 col-sm-2">
         <div class="row">
        	<div class="col-md-5"><img src="images/home-thumb-interior.PNG" alt="Image library" class="img-responsive">
            </div>
            <div class="col-md-7">
            <h4>Interior<br>&nbsp;</h4>
            <p><a href="interior.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
            
            
            </div>
        </div>
        </div>
 


 		<div class="col-md-3 col-sm-2">
         <div class="row">
        	<div class="col-md-5"><img src="images/home-thumb-culture.PNG" alt="Digital standards" class="img-responsive">
            </div>
            <div class="col-md-7">
            <h4>Edwards culture<br>&nbsp;</h4>
            <p><a href="culture.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
            </div>
        </div>
        </div>
        
        
        <div class="col-md-3 col-sm-2">
         <div class="row">
        	<div class="col-md-5"><img src="images/home-thumb-public.PNG" alt="60th Anniversary" class="img-responsive">
            </div>
            <div class="col-md-7">
            <h4>Public areas<br>&nbsp;</h4>
            <p><a href="public.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
            </div>
        </div>
        </div>
        

 
 </div> <!-- /row -->
 </div>
 
  <div class="container-fluid edw-gray-bg home-one">

 <div class="row edw-media">
  <div class="col-md-4 col-md-offset-2">
    
  <h3>One Edwards</h3>
  <p>Edwards Lifesciences is the global leader in patient-focused medical innovations for structural heart disease, as well as critical care and surgical monitoring. When people hear the Edwards name, they should feel an inherent sense of trust, knowing that the company has a heritage of high quality, innovative technologies developed in close partnership with the clinical community. That trust is reinforced anytime a patient, customer, or current or prospective employee comes in contact with a person, product or facility that represents Edwards.</p>
<p>We have adopted a strategy of “One Edwards.” We believe in
reinforcing our corporate brand internally and externally to
build equity and awareness. We endeavor to portray one
company, one brand and one consistent image.</p>
<p><strong>To help deliver on the “One Edwards” strategy, we have developed these
    <a href="contents.aspx">Facilities Standards</a>, which will help you execute a consistent look for
Edwards facilities and offices throughout the world</strong></p>

  </div>
  
      	<div class="col-md-6">
        	<div class="row">
            	<div class="col-md-2 text-home-credo"><span class="edw-circle-icon"><span class="icon-download"></span></span></div>
                <div class="col-md-10"><h3>Assets</h3>
                <p>For more information or clarification about the use of these standards, please contact:</p>
                
<p class="pad20"><strong>Global Communications & Branding<br>
Nolan Taira</strong><br>
<a href="mailto:nolan_taira@edwards.com">nolan_taira@edwards.com</a><br>
949-250-6870<br><br>
Becky Kibbey</strong><br>
<a href="mailto:becky_kibbey@edwards.com">becky_kibbey@edwards.com</a><br>
949-250-3455<br><br>
Erin Brankov</strong><br>
<a href="mailto:erin_brankov@edwards.com">erin_brankov@edwards.com</a><br>
949-250-2436<br><br>
For access to any of the templates mentioned in this document, artwork or photos, please visit <a href="/">standards.edwards.com</a> or contact:
<br><br>
<strong>Jade Jourdan</strong><br>
<a href="mailto:jade_jourdan@edwards.com">jade_jourdan@edwards.com</a><br>
949–250–3560</p>
<hr>
                <div class="row">
                <div class="col-md-6">
               
                
                <p><a href="assets/19 EW Facilities Standards.pdf" role="button" class="btn btn-block btn-success home-down-btn btn-sm"><span class="icon-file-pdf"></span> Facilities standards</a></p> 
                
                
                <p><a href="https://edwards.webdamdb.com/cloud/#folder/4964757" target="_blank" role="button" class="btn btn-block btn-success home-down-btn btn-sm"><span class="icon-file-text2"></span> Facilities templates</a></p> 
                
                <p><a href="assets/Edwards Lifesciences Standards Manual 8-16-18.xlsx" role="button" class="btn btn-block btn-success home-down-btn btn-sm"><span class="icon-file-text2"></span> Facilities resource guide</a></p> 
                </div>
                <div class="col-md-6">
                 <p><a href="/pdf/Edwards_Brand_Identity_Standards_V2_25Oct2016.pdf" role="button" class="btn btn-block btn-success home-down-btn btn-sm"><span class="icon-file-pdf"></span> US brand identity standards</a></p>
                <p><a href="/pdf/Edwards_Brand_Identity_Standards_V2_Metric_25Oct2016.pdf" role="button" class="btn btn-block btn-success home-down-btn btn-sm"><span class="icon-file-pdf"></span> Metric brand identity standards</a></p> 
              
                </div>
                </div>
                
                </div>
            </div>        
        </div>
</div> <!-- /row -->






  </div>
  
  


 
 
 </div> <!-- end page -->
 
        <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
