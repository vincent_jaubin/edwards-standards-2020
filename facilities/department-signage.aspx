﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



  <div id="department-signage" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Interior</li>
  <li>Department messaging & Shared workspaces</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Department messaging<a href="assets/pdf/messaging.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>


 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab active"><a href="#tabs-1" class="match-height"><span>Department messaging</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Shared workspaces</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Department messaging</h3>   
 
<p>Department-specific signage should be displayed only
within the applicable department.</p>
<p>
These should <strong>not</strong> be displayed in
meeting rooms, shared workspaces
or public spaces.</p>
<p>When possible, monitors should be
used to display this content.</p>

<p><strong>Framing is the preferred display
method for these. See <a href="display-specs.aspx">here</a>.</strong></p>

<h4>Department example</h4>
<p><img src="images/signage1.PNG" class="img-responsive" /></p>

<p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4967827" target="_blank">Mission Template</a></small></p>

</div>


    <div class="pane" id="tabs-2">
 <h3>Shared workspaces</h3>
 
 <p>When possible, monitors should be utilized in
shared workspaces to display changing or
updateable messages, such as:</p>
<ul><li>Announcements</li>
<li>Metrics</li>
<li>Recognition</li>
<li>Corporate messaging</li>
<li>Patient stories</li>
</ul>
  
  
<h4>Monitor examples</h4>
<p><img src="images/signage2.PNG" class="img-responsive" /></p>


</div>




</div>

</div>
 </div>


    <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    

  </div>
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
