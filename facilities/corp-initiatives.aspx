﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body>
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



 <div id="corp-initiatives" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Edwards corporate initiatives</li>
  <li>Speak up, SAFE, Diversity</li>
  </ol>
  
  
  
  <h2>Edwards corporate initiatives<a href="assets/pdf/initiatives.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>


<p>These may include:</p>
<ul>
<li>Speak Up</li>
<li>SAFE</li>
<li>Diversity</li></ul>
<p>These should be displayed in
hallways and entrances where
employee traffic is maximized.</p>



<p><strong>Framing is the preferred display
method for Quality and EHS policies. See <a href="display-specs.aspx">here</a>.</strong></p>

  


<div class="row">
<div class="col-md-4" id="speakup">
<h3>Speak up</h3>
<img src="images/corp-init1.PNG" width="274" height="341" class="img-responsive" />
</div>

<div class="col-md-4" id="safe">
<h3>SAFE</h3>
<img src="images/corp-init2.PNG" width="273" height="339" class="img-responsive" />
</div>

<div class="col-md-4" id="diversity">
<h3>Diversity</h3>
<img src="images/corp-init3.PNG" width="274" height="341" class="img-responsive" />
</div>

</div>

<p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4964786" target="_blank">Corporate Initiatives</a></small></p>


</div>




    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
