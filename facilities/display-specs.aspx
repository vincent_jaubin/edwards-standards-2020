﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



  <div id="display-specs" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> 
    
    <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Display Specifications</li>
  <li>Framing, Acrylic, Document holders</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Display specifications<a href="assets/pdf/display.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>


 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab active"><a href="#tabs-1" class="match-height"><span>Framing</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Acrylic</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Document holders</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">

 <h3 style="padding-top:0">Framing</h3>   
<p>The following visuals will help you to select the
materials recommended for framed posters and
how to properly install for best visual effect.</p>

<div class="row"><div class="col-md-4">
<p><strong>Installation:</strong>
Framed posters should be hung at
<strong>eye level</strong> with the center point of frame 60
inches (152.4 cm)</p>
<p><strong>Components:</strong>
<strong>Poster output:</strong> This should be a high-quality
photograde output in lustre finish. UV inks are
highly recommended for longevity.</p>
<p><strong>Frame moulding:</strong><br>
All Edwards moulding is in <strong>Matt Black</strong><br>
Sizes:<br>
1.5” Face (Studio Moulding 27501)<br>
1“ Face (Studio Moulding 22201)</p>
<p><strong>Inside mat and filet:</strong><br>
2” Silver outer mat<br>
Brushed Silver (Bainbridge 8006)<br>
1/4” Red Filet for accent (optional)<br>
Code Red (Crescent 9610)</p>
<p><strong>Protection:</strong><br>
1/8” (0.3175 cm) Acrylic - UV protected</p>
<p><strong>Hardware:</strong><br>
Security hanging hardware
is recommended</p>
</div>
<div class="col-md-8">
<p><img src="images/framing-new-aio.PNG" width="858" height="850" class="img-responsive" /></p>
</div>

</div>





</div>


    <div class="pane" id="tabs-2">
<h3 style="padding-top:0">Acrylic</h3>
 
<p>The following visuals will help you to select
the materials recommended for acrylic
displays and how to properly install for best
visual effect.</p>

    
    <div class="row"><div class="col-md-4">
<p><strong>Installation:</strong><br>
Framed posters should be hung at
<strong>eye level</strong> with the center point of frame 60
inches (152.4 cm)</p>
<p><strong>Components:</strong><br>
<strong>Poster output:</strong> This should be a
high-quality photograde output in lustre
finish. UV Inks are highly recommended for
longevity.</p>
<p><strong>Mounting:</strong><br>
Graphics are face-mounted on the back of
the acrylic</p>
<p><strong>Acrylic specifications:</strong><br>
1/4” (.635 cm)recommended” up to 1/2“<br>
(1.27 cm)<br>
Polished Edges</p>
<p><strong>Drill holes:</strong><br>
3/4” (1.905 cm) on center</p>
<p><strong>Hardware:</strong><br>
Stand-off hardware<br>
<a href="https://standoffsystems.com" target="_blank">https://standoffsystems.com</a></p></div>
<div class="col-md-8">
<p><img src="images/acrylic-aio.PNG" width="948" height="896" class="img-responsive" /></p>
</div>

</div>

</div>

<div class="pane" id="tabs-3">
 <h3>Document holders</h3>
 
<p>The following visuals will help you to select the materials
recommended for signage that requires updating, such as:</p>


<ul>
<li style="padding:0">Conference rooms</li>
<li style="padding:0">Workstations</li>
<li style="padding:0">Directories</li>
<li style="padding:0">Wayfinding</li>
<li style="padding:0">Evacutaion</li>
<li style="padding:0">Business license/permits</li>
<li>Door/wall mount
</li></ul>

 <div class="row">
 <div class="col-md-5">
<p>
<strong>Conference room, workstation and cubicle:</strong><br>
Drop lens signage features a drop-in, 3/8"-thick simulated
glass lens and an anodized, aluminum housing with minimum
reveal at bottom and two sides. Remove or replace the
graphic insert by sliding the lens up.</p>
<p>
<strong>Directory and wayfinding:</strong><br>
Modular signs feature multiple-piece construction with
tiles that are reconfigurable or replaceable and allow for
changeable inserts, so signs can be updated easily.</p>
<p>
<strong>Business license on counter:</strong><br>
These holders are 4 x 10 panoramic clear acrylic picture
frames with magnetic closure for countertop display.</p>
<p>
<strong>License holder on wall:</strong><br>
These are 8.5 x 11 clear acrylic sign holders with magnetic
closures and stand-o hardware that mount to wall surfaces.</p>
<p>
<strong>Emergency information holder on door:</strong><br>
These are standard 8.5 x 11 clear acrylic sign holders for doors
or walls are installed using adhesive tape backing.</p>
</div>
<div class="col-md-7"><img src="images/document-holder-aio.PNG" class="img-responsive"></div>
</div>

</div>

</div>
</div>
</div>
    <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
      </div>

  </div>
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
