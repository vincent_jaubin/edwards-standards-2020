﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body>
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



 <div id="contents-one-edwards" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="index.aspx">Facilities standards</a></li>
  <li>Contents</li>
  <li>One Edwards</li>
  </ol>
  
  
  
  <h2>One Edwards
  <a href="assets/pdf/oneedwards.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
  
  
  
  <div class="well intro">
  <p>Edwards Lifesciences is the global leader in patient-focused
medical innovations for structural heart disease, as well as
critical care and surgical monitoring. When people hear the
Edwards name, they should feel an inherent sense of trust,
knowing that their products came from a company with a
heritage of innovative, leading products working in close
partnership with the clinical community. That trust is reinforced
anytime a customer, patient, or current or prospective
employee comes in contact with a person, product or facility
that represents Edwards.</p>
<p>We have adopted a strategy of “One Edwards.” We believe in
reinforcing our corporate brand both internally and externally to
build equity and awareness. We endeavor to portray one
company, one brand and one consistent image.</p>
<p><strong>To help deliver on the “One Edwards” strategy, we have developed these
Facilities Standards, which will help you execute a consistent look for
Edwards facilities and offices throughout the world</strong></p>
</div>

<p>For more information or clarification about the use of these standards, please contact:</p>

<strong>Global Communications & Branding<br>
Nolan Taira</strong><br>
<a href="mailto:nolan_taira@edwards.com">nolan_taira@edwards.com</a><br>
	  949-250-6870</p>

<p>For access to any of the templates mentioned in this document, artwork or photos,
please explore this website, or contact: <br>
<strong>Jade Jourdan</strong><br>
<a href="mailto:jade_jourdan@edwards.com">jade_jourdan@edwards.com</a><br>
	949–250–3560</p></div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
