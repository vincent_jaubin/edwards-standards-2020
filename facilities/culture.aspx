﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body>
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



 <div id="culture" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Edwards culture</li>
  <li>Credo, Secret Sauce & Aspirations</li>
  </ol>
  
  
  
  <h2>Edwards culture<a href="assets/pdf/culture.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>

 <div class="well intro">
<p>Edwards is committed to fostering a
culture focused on innovation and
humanity. This culture is demonstrated
in our Credo, Secret Sauce, and
Aspirations.</p>
</div>


<p><strong>Acrylic display is preferred,
  <a href="display-specs.aspx">click here</a>.</strong></p>
  
  <p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4965123" target="_blank">Credo & Secret Sauce & Aspirations</a></small></p>
<p><small>These can be scaled for hallway areas
and materials are specified.</small></p>
  


<div class="row">


<div class="col-md-4" id="credo">
<h3>Credo</h3>
<p><img src="images/culture2.PNG" width="651" height="1171" class="img-responsive" /></p></div>

<div class="col-md-4" id="sauce">
<h3>Secret Sauce</h3>
<p><img src="images/culture1.PNG" width="651" height="1171" class="img-responsive" /></p>
</div>



<div class="col-md-4" id="aspirations">
<h3>Aspirations</h3>
<p><img src="images/culture3.PNG" width="651" height="1171" class="img-responsive" /></p></div>

</div>

</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
