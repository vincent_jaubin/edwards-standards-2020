﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-foundation" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>Brand Foundation</li>
  </ol>
  
  
  
   <div id="tabslider">
  <h2>Brand Foundation<a href="pdf/p-brand-foundation.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  
  
 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Overview</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Brand promise</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Brand positioning</span></a></li>
  <li><a href="#tabs-4" class="match-height"><span>Brand personality</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
  
  <div class="well intro">
  <p>Our company began more than 60 years ago when engineer
Miles “Lowell” Edwards partnered with cardiac surgeon
Dr. Albert Starr to help patients in need. Their innovation,
the first successful artificial heart valve, transformed patient
care and established the foundation of our company.</p>
</div>

<p>While Edwards Lifesciences has continued to grow and transform over the
years, our foundation has remained consistent. Our company and our brand
are rooted in these guiding truths. They are the constants that define who
we are as an organization today and in the future.</p>
<ol>
<li><strong>Patient focus.</strong> Edwards Lifesciences is driven by a passion to help patients
live longer, healthier and more productive lives. We provide innovative
technologies and solutions that address significant, unmet clinical needs.
Helping patients is our life’s work and life is now.</li>
<li><strong>Humanity.</strong> At the heart of Edwards is a culture built on developing lasting
and trusted partnerships with all our stakeholders, including clinicians,
patients and employees. We pride ourselves on maintaining a high
standard of integrity and character, and most of all, of passion and caring
for all that we do.</li>
<li><strong>Innovation.</strong> Edwards innovates with purpose. We are focused on
transformative solutions and will invest in ideas that have the power for
big change. To do so, we dream big, take risks and think outside the norm.
When we look for one thing, but discover another, we grab that surprise
and try to do something valuable with what we’ve learned.</li>
<li><strong>Leadership.</strong> We are constantly defining the future of medicine and
patient care. This frame of mind enables us to achieve and maintain the
global leadership position for 95% of the products we sell.</li>
</ol>
</div>

    <div class="pane" id="tabs-2">

<h3>Brand promise</h3>
<p>Our brand promise is the “big” idea that unifies our brand. It is the overriding
assurance we deliver to clinicians, to patients, to investors and to all of our
constituents. And it is the measuring stick against which we benchmark all
communications for consistency – does what we are communicating support
our brand promise?</p>
<div class="well">
<p><em>Edwards' brand promise is:</em></p>
<p class="edw-red h2">Innovation + Humanity</p>
</div>

</div>


    <div class="pane" id="tabs-3">

<h3>Brand positioning</h3>
<p>Our brand positioning is a concise, compelling summary that describes the
core idea behind our business and identifies our point of differentiation. In
essence, it is our view of the company and how we want customers, employees,
investors – and all constituents – to think of us.</p>



<div class="well">
<p><em>Edwards’ brand positioning is:</em></p>
<p class="edw-red h4">Edwards Lifesciences is committed to bringing forth transformational
technologies and therapies that elevate patient care. Actionable
discovery and excellence have been our guiding beacons for
more than 60 years. We are deeply invested in developing quality
solutions that inspire trust because it’s personal—for us, for
clinicians and most especially, for patients.</p>
</div>

</div>


    <div class="pane" id="tabs-4">

<h3>Brand personality</h3>

<a name="personality"></a>
<p>Our brand has a personality. It is made up of human traits that define our
verbal and visual expression. While our brand positioning helps define what
we say, our brand personality defines how we say it through our tone of voice,
color palette and photography selection.</p>
<p><em>The Edwards brand personality is comprised of five attributes:</em></p>

<div class="row text-center">
<div class="col-md-15"><div class="well match-height"><p class="h3 edw-red">Visionary</p>
<p>Committed to the discovery and development of big ideas that will transform medicine, today and tomorrow</p>
</div></div>
<div class="col-md-15"><div class="well match-height"><p class="h3 edw-red">Driven</p>
<p>Powered by an intrinsic certainty that there are always better ways to care for patients - and we need to discover them</p>
</div></div>
<div class="col-md-15"><div class="well match-height"><p class="h3 edw-red">Dedicated</p>
<p>Leaders by example through our actions, beliefs and accomplishments</p>
</div></div>
<div class="col-md-15"><div class="well match-height"><p class="h3 edw-red">Passionate</p>
<p>Inspired by a personal commitment to helping patients live longer and healthier lives</p>
</div></div>
<div class="col-md-15"><div class="well match-height"><p class="h3 edw-red">Collaborative</p>
<p>Trusted partners and advocates of clinicians, patients and hospitals</p>

</div></div>



</div>

</div>

  
    </div>
    </div>
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
