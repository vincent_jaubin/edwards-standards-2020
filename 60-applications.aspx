﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->

<div id="edwards-wrapper">

<!-- #include file="header.html" -->

 <div id="60-applications" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>60th Anniversary logo lockup standards</li>
  <li>Applications</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Applications<a href="pdf/60-applications.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>

  <p><a href="https://ewdigstrategy.blob.core.windows.net/standards/templates/60th-lockup-ppt.zip" class="btn btn-info" role="button"><i class="icon-download"></i> Download 60th Anniversary powerpoint templates</a></p>
    <p><a href="https://ewdigstrategy.blob.core.windows.net/standards/templates/convention-graphics-files.zip" class="btn btn-info" role="button"><i class="icon-download"></i> Download 60th Anniversary convention graphics</a></p>
    <p><a href="https://ewdigstrategy.blob.core.windows.net/standards/templates/60th_application-ads.zip" class="btn btn-info" role="button"><i class="icon-download"></i> Download 60th Anniversary ads</a></p>

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Print</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Digital</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Environments</span></a></li>
 
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
    <div class="well intro"><p>On brochures, ads and other print
materials, swap out Version 1 of the
Edwards logo with the 60th logo
lockup.</p></div>
 <h3>Print</h3>   

<div class="row">
<div class="col-md-8 col-md-push-4"><p><img src="img/applications-print.png" class="img-responsive img-margin"></p></div>
<div class="col-md-4 col-md-pull-8">
<p><strong>Scaling</strong></p>
<p>The logo lockup should be scaled so that the
overall height matches the height of the stand
alone Edwards logo on each print piece.</p>
<p>Since the 60th logo lockup is wider than the
stand alone Edwards logo, text to the left
of the lockup may have to shift to the left,
maintaining the minimum lockup clear area.</p>
<p><strong>Background colors and photos</strong></p>
<p>The 60th logo lockup should not be anchored
to a background color field or photo edge
the way a stand alone Edwards logo typically
appears. Instead, shift the edge of the
background color or photo area up or down,
and outside the lockup clear area.</p>


</div>

</div>

<p><strong>Separating the lockup</strong></p>
<p>If an application is too narrow or two-sided,
the lockup may be broken up so that one
graphic appears on each side. This includes
premium items or environment applications.
Be sure to maintain equal proportions
between the two graphics and clear area
when separating the elements.</p>


    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Digital</h3>   
<p>In some applications, including
digital communications, the
<em>60 Years of Discovery</em> treatment may
be separated from the Edwards logo.</p>
<p>If separating the Edwards logo and the
<em>60 Years of Discovery</em> treatment, the
proportions of the two should remain the
same since they will be in close proximity to
one another. In limited instances, such as a
feature image on a website, the <em>60 Years of
Discovery</em> treatment may be scaled up to make
a statement, while the Edwards logo is in a
fixed position at the top of the page.</p>
<div class="row">
<div class="col-md-8 col-md-push-4"><p><img src="img/applications-digital-1.png" class="img-responsive img-margin"></p></div>
<div class="col-md-4 col-md-pull-8">

<p><strong>Presentations</strong></p>
<p>A special PowerPoint template has been
developed that includes the commemorative
artwork. On title slides, the Edwards logo
remains by itself in the bottom right corner,
but the <em>60 Years of Discovery</em> treatment has
been added within the secondary graphic.
Do not alter this placement.</p>
<p>Three photo title slides have also been added
to the template to commemorate this special
anniversary.</p>
<p>The logo lockup has also been added to the
transition slide and end slides.</p>
</div>
</div>

<div class="row">
  <div class="col-md-8 col-md-push-4"><p><img src="img/applications-digital-2.png" class="img-responsive img-margin"></p></div>
  <div class="col-md-4 col-md-pull-8"><p><strong>Websites</strong></p>
<p>Use the <em>60 Years of Discovery</em> treatment in
the photo area as needed. Do not replace the
Edwards logo in the top right corner with the
lockup as it would hang from the gray bar.</p>
<p><strong>Digital ads</strong></p>
<p>As space is limited on digital ads, the lockup
does not need to be used. However, if space
allows, scale the logo lockup so that the
overall height matches the height of the stand
alone Edwards logo.</p></div>
  
</div>

    
   </div>
    <div class="pane" id="tabs-3">
    
    <h3>Environments</h3>   

<div class="row">
<div class="col-md-8 col-md-push-4"><p><img src="img/applications-environment.png" class="img-responsive"></p></div>
<div class="col-md-4 col-md-pull-8">
<p><strong>Banners</strong></p>
<p>On banners, the <em></em>>60 Years of Discovery</em>
treatment may be placed in the bottom right
corner of the larger shape in the secondary
graphic, or in the photo area directly below
the Edwards logo. Whenever possible, try to
scale the <em>60 Years of Discovery</em> treatment so
that it is similar in size to the Edwards logo.
Otherwise, scale it so that it is clearly visible.</p>
<p><strong>In-booth graphics</strong></p>
<p>The <em>60 Years of Discovery</em> treatment may
be used by itself if the graphic application
is within an Edwards booth environment
where the Edwards logo already appears in a
prominent position.</p>
</div>

</div>




    </div>

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
