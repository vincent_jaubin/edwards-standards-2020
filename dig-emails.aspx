﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="dig-emails" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Digital media</li>
  <li>Emails</li>
  </ol>
  
  
  
  <h2>Emails <span class="edw-titan">Desktop and mobile</span>
  <a href="pdf/p-emails.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>


  <div class="well intro"><p>For desktop emails that are 700 px wide, the
    logo is 50 px (based on the height of the E
    symbol); the area surrounding the logo is
    102 x 102 px. For mobile, the logo area scales
    down proportionately to 92 x 92 px, so that
    the E symbol is approximately 45 px high.</p></div>
<div class="row">
<div class="col-md-5">
<p><strong>Desktop</strong></p>
<p>The larger red shape in the secondary graphic
holds the title, but it may also include a photo
HTML can be used for fluid width e-mails to
ensure the red graphic extends the width of
the template. Images should be used when the
width of each e-mail is fixed for mobile and
desktop. The height of the larger square may
increase to hold longer titles or images, but for
brevity, avoid titles longer than two lines.</p>

<p>Align the content on desktop emails to the
  right edge of the logo – this margin is equal
  to 78 px. Use the same measurement for the
  right margin.</p>
<p>Use Arial Regular for content. Use Arial Bold
for subheads or to add emphasis.</p>
</div>
<div class="col-md-7">
<img src="img/emails-1.png" class="img-responsive">


</div>

</div>

<div class="row">
<div class="col-md-5">

<p><strong>Mobile</strong></p>
<p>HTML can be used for fluid width e-mails to
ensure the red graphic extends the width of
the template. Images should be used when
the width of each e-mail is fixed for mobile
and desktop. The height of the larger square
may increase to hold longer titles or images.</p>
<p>Align the content on mobile layouts to the
  left edge of the Edwards logo which is equal
  to 25 px. The right margin is also 25 px.</p>


<p><strong>Images</strong></p>
<p>Images, charts or diagrams should fall within
the main content area. Images may span the
width of the content area or appear side-by-side
with copy, but they should appear within
the left and right margins.</p>

</div>
<div class="col-md-7">
<img src="img/emails-2.png" class="img-responsive">


</div>

</div>





  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
