﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="pat-typography" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Patient awareness</li>
  <li>Support typography</li>
  </ol>
  
  
  
  <h2>Support typogaphy
  <a href="pdf/p-patient-awareness.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
  <div class="well intro"><p>Materials directed toward patients may incorporate a secondary typeface to allow more flexibility</p></div>
 
<div class="row">
<div class="col-md-4">
<p><strong>Support typography</strong></p>

<p>
    <em>
        The Serif typeface may only be used for direct-topatient
        materials.
    </em>
</p>

<p>
    Although communications should always lead with
    Vesta Std, The Serif has been selected as a support
    typeface. The Serif is a contemporary font that has
    clean, simple shapes. Like Vesta Std, the characters
    are open, balanced and upright. Because both
    fonts have more even stroke weights, the letters
    are easier to read.
</p>

<p>
    The Serif is available in a variety of weights
    and styles, everything from The Serif Light to
    The Serif Bold.
</p>



</div>
<div class="col-md-8">
    <p><img src="img/pat-typography-image.png" class="img-responsive"></p>
</div>

</div>
<div class="row">
    <div class="col-md-12">
        <p>
            Both Vesta Std and The Serif may be used together,
            but it is preferred that communications lead with
            Vesta Std. When using the typefaces, especially
            when used together, keep the number of sizes,
            weights and styles to a minimum to avoid visual
            clutter. At maximum, use three to four sizes and
            two to three weights or styles.
        </p>
        
        <p><strong>Legibility</strong></p>
        
        <p>
            On patient materials it is important to consider
            both font size and color. Body copy and captions
            should be increased 2–4 points as appropriate.
        </p>
        
        <p>
            Font color is also important. Always ensure there is
            sufficient contrast between the typography color
            and its background. Adjust images as needed to
            ensure messages are legible. Do not place white or
            light-colored typography over a light or mid-toned
            color or image; and do not place black or dark copy
            over a dark or mid-toned color or image.
        </p>
        
        <p>
            For more information, visit: 
            <a href="https://www.w3.org/WAI/WCAG21/quickref/#contrast-minimum" target="_blank">https://www.w3.org/WAI/WCAG21/quickref/#contrast-minimum</a>
        </p>
    </div>
</div>



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
