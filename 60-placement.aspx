﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->

<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="60-placement" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg banner-anniversary">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>60th Anniversary logo lockup standards</li>
  <li>Lockup placement</li>
  </ol>
  
  
  
  <h2>Lockup placement
  <a href="pdf/lockup-placement.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
  <div class="well intro"><p>In most applications, the stand-alone Commemorative type treatment
Edwards logo may be replaced with
the 60th logo lockup.</p></div>
<p>Scale and align the logo lockup on each
application so that the Edwards logo in the
lockup matches the existing Edwards logo.
Do not simply add the <em>60 Years of Discovery</em>
treatment next to an existing Edwards logo –
use the full lockup artwork.</p>
 
<div class="row">
  <div class="col-md-8 col-md-push-4">
<p><img src="img/lockup-placement.png" class="img-responsive"></p>
</div>
<div class="col-md-4 col-md-pull-8">
<p><strong>Clear area</strong></p>
<p>The Edwards 60th logo lockup has greater
impact when it is free from other text or
graphics competing for attention. Although
more space is always recommended, use the
height of the <em>Edwards</em> name (X) to measure
the minimum required clear area around all
sides of the logo lockup.</p>
<p><strong>Minimum size</strong></p>
<p>To ensure the logo lockup is clearly visible on
print and digital applications, a minimum size
has been determined. The minimum size is
based on the overall height of the logo lockup.</p>
<p><strong>Position</strong></p>

<p>On print pieces, the logo lockup is placed in
the bottom right corner, scaled and aligned to
the grid as the Edwards logo typically appears.</p>

</div>



</div>
<p>In some applications, such as PowerPoint and
banners, the Edwards logo and <em>60 Years of
Discovery</em> treatment may be separated. See
the <em>Applications</em> pages for more information.</p>
<p>Do not anchor the logo lockup to a field of
color or photo the way the Edwards logo
normally appears. Instead, a background
field of color or photo should end outside the
lockup clear area.</p>
  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
