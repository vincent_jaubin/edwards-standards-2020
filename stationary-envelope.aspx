﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="stationary-envelope" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Stationary</li>
  <li>Envelope</li>
  </ol>
  
  
  
  <h2>Envelope
  <a href="pdf/p-patient-awareness.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
<div class="row">
<div class="col-md-4">
<p><strong>Template</strong></p>
<p>
    Refer to the mechanical file for measurements
    and for font sizes and styles. Use the approved
    template at 100% scale.
</p>

<p>
    An additional address line may be added to
    highlight specific actions or other important
    information, for example, “ATTN: Special
    Initiative”.
</p>

<p><strong>Window envelopes</strong></p>

<p>
    If using a window envelope the logo may
be scaled smaller so that the height of the E
symbol is equal to 5/8" (0.625"). Maintain the
left and top margins around the logo as shown
here. Also maintain the same space between
the logo and the address information – which
means the address should shift to the left.
</p>
</div>
<div class="col-md-8">
    <p><img src="img/stationary-envelope-image.png" class="img-responsive"></p>
</div>

</div>



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
