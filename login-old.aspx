﻿<%@ Page Language="C#" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    public void Login_OnClick(object sender, EventArgs args) {
        if (FormsAuthentication.Authenticate(UsernameTextbox.Text, PasswordTextbox.Text))
            FormsAuthentication.RedirectFromLoginPage(UsernameTextbox.Text, true);
        else
            Msg.Text = "Login failed. Please check your user name and password and try again.";
    }
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (AllowAnonymousAccess())
                FormsAuthentication.RedirectFromLoginPage("Edwards", true);
        }
    }
    private bool AllowAnonymousAccess() {

        string clientIPString = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "";
        string allowedUserIPList = "";
        if (ConfigurationManager.AppSettings["allowedIpList"] != null) {
            allowedUserIPList = ConfigurationManager.AppSettings["allowedIpList"];
        }
        if (allowedUserIPList == "")
            return false;

        IPAddress clientIPAddr;
        if (IPAddress.TryParse(clientIPString.Trim(), out clientIPAddr)) {
            string[] ipAddresArray = allowedUserIPList.Split(',');
            List<string> ipAddresses = new List<string>();

            foreach (string ipAddress in ipAddresArray) {
                if (ipAddress.Contains("/"))
                    ipAddresses.AddRange(IpAddressRange(ipAddress));

                else
                    ipAddresses.Add(ipAddress.Trim());
            }

            if (ipAddresses.Contains(clientIPString))
                return true;
        }

        return false;
    }

    private List<string> IpAddressRange(string ipAddress) {
        try {
            string[] ipParts = ipAddress.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string[] startIP = ipParts[0].Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            int ipCount = Convert.ToInt32(ipParts[1]);
            int ipSuffix = Convert.ToInt32(startIP[3]);

            List<string> ipList = new List<string>();
            for (int i = 0; i < ipCount; i++)
                ipList.Add(string.Format("{0}.{1}.{2}.{3}", startIP[0], startIP[1], startIP[2], (ipSuffix + i).ToString()));

            return ipList;
        }
        catch (Exception) { }

        return new List<string>();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Login</title>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:300);

        .login-page {
            width: 360px;
            padding: 8% 0 0;
            margin: auto;
        }

        .form {
            text-align:left;
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 360px;
            margin: 0 auto 100px;
            padding: 45px;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

            .form input {
                font-family: "Roboto", sans-serif;
                outline: 0;
                background: #f2f2f2;
                width: 100%;
                border: 0;
                margin: 0 0 15px;
                padding: 15px;
                box-sizing: border-box;
                font-size: 14px;
            }

        .form-button {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #c8102e !important;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
        }


        .form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

            .form .message a {
                color: #4CAF50;
                text-decoration: none;
            }

        .form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

            .container:before, .container:after {
                content: "";
                display: block;
                clear: both;
            }

            .container .info {
                margin: 50px auto;
                text-align: center;
            }

                .container .info h1 {
                    margin: 0 0 15px;
                    padding: 0;
                    font-size: 36px;
                    font-weight: 300;
                    color: #1a1a1a;
                }

                .container .info span {
                    color: #4d4d4d;
                    font-size: 12px;
                }

                    .container .info span a {
                        color: #000000;
                        text-decoration: none;
                    }

                    .container .info span .fa {
                        color: #EF3B3A;
                    }

        body {
            background: #505759; /* fallback for old browsers */
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .chk-public {
            display: inline;
            float: left;
            width: 36px;
        }
    </style>
</head>
<body>
    <div class="login-page">
        <div class="form">
            <form class="login-form" id="form1" runat="server">
                <asp:Label ID="Msg" ForeColor="maroon" runat="server" /><br />
                <div style="text-align: left;">
                    Username:
        <asp:TextBox ID="UsernameTextbox" runat="server" /><br />
                    Password:
        <asp:TextBox ID="PasswordTextbox" runat="server" TextMode="Password" /><br />
                    <asp:Button ID="LoginButton" Text="Login" OnClick="Login_OnClick" runat="server" CssClass="form-button" />
                </div>

            </form>
        </div>
    </div>
</body>
</html>
