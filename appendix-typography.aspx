﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="appendix-typography" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Appendix</li>
  <li>Typography specimens</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Typography specimens<a href="pdf/p-digital-ads.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/edwards-dig-ads.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the templates"><i class="icon-download"></i></a>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Vesta Std</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Merriweather Sans</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Arial</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Vesta Std</h3>   
<div class="row">
<div class="col-md-5">
    <p><strong>Designed by</strong><br>Gerard Unger</p>
    <p><strong>Foundry</strong><br>Linotype</p>
    <p><strong>Format</strong><br>OpenType® font</p>
    <p><strong>Font folder</strong><br>Vesta Std 1 Volume</p>
    <p><strong>Font files</strong><br>Linotype - VestaStd-Bold.otf<br>Linotype - VestaStd-BoldItalic.otf<br>Linotype - VestaStd-Italic.otf<br>Linotype - VestaStd-Regular.otf<p>
</div>
<div class="col-md-7"><img src="img/appendix-typography-vesta.png" class="img-responsive img-margin"></div>
</div>
    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Merriweather Sans</h3>   

    <div class="row">
        <div class="col-md-5">
            <p><strong>Designed by</strong><br>Eben Sorkin<br>Principal design</p>
            <p><strong>Format</strong><br>TrueType® font</p>
            <p><strong>Font folder</strong><br>Merriweather_Sans</p>
            <p><strong>Font files</strong><br>MerriweatherSans-Regular.ttf<br>
                MerriweatherSans-Italic.ttf<br>
                MerriweatherSans-Bold.ttf<br>
                MerriweatherSans-BoldItalic.ttf<p>
        </div>
        <div class="col-md-7"><img src="img/appendix-typography-merriweather.png" class="img-responsive img-margin"></div>
        </div>

    
    
   </div>
   
   <div class="pane" id="tabs-3">
    <h3>Arial</h3>   

    <div class="row">
        <div class="col-md-5">
            <p><strong>Designed by</strong><br>Robin Nicholas<br>Patricia Saunders</p>
            <p><strong>Foundry</strong><br>Monotype</p>
            <p><strong>Format</strong><br>TrueType® font</p>
            <p><strong>Font folder</strong><br>Merriweather_Sans</p>
            <p><strong>Font files</strong><br>Arial<br>
                Arial Italic.ttf<br>
                Arial Bold.ttf<br>
                Arial Bold Italic.ttf<p>
        </div>
        <div class="col-md-7"><img src="img/appendix-typography-arial.png" class="img-responsive img-margin"></div>
        </div>

    
    
   </div>
   
   
   
    
    

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
