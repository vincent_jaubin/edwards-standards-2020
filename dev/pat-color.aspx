﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="pat-color" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Patient awareness</li>
  <li>Color and typography</li>
  </ol>
  
  
  
  <h2>Color and typography
  <a href="pdf/p-patient-awareness.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
  <div class="well intro"><p>Materials directed toward patients
may incorporate additional visual
identity tools. Although not required,
an extended color palette and a
secondary typeface will allow for
creative flexibility.</p></div>
 
<div class="row">
<div class="col-md-12">
<p><strong>Extended palette</strong></p>
<p>These colors may only be used for direct-to-patient
materials.</p>
<p>Although red and gray will always be Edwards’
colors, this palette was chosen to integrate with
the primary, secondary and neutral palettes.</p>
<p>Use only two to three colors on a
communication, including a neutral color or
gray. The extended palette may be used solid
(at 100%) or tinted for more flexibility.</p>
<p><img src="img/pat-aw-color.png" class="img-responsive"></p>
<p><strong>Support typography</strong></p>
<p>The Serif typeface may only be used for direct-to-
patient materials.</p>
<p>To complement our primary typeface, Vesta Std,
The Serif has been selected as the support
typeface. The Serif is a contemporary serif
face that has clean, simple shapes. The
characters are open, balanced and upright,
similar to Vesta Std.</p>
<p>The Serif is available in a variety of weights
and styles, everything from The Serif Light to
The Serif Bold.</p>
<p>Both Vesta Std and The Serif may be
used together, but it is preferred that
communications lead with Vesta Std. When
using the typefaces, especially when used
together, keep the number of sizes, weights
and styles to a minimum to avoid visual
clutter. Use three to four sizes and two to
three weights or styles.</p>


<p><img src="img/pat-aw-type.png" class="img-responsive"></p>
</div>

</div>



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
