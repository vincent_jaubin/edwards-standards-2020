﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="header.html" -->
            <div id="elements-image-library" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Brand identity standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="contents.aspx">Brand identity standards</a></li>
                                <li>Basic elements</li>
                                <li>Image library</li>
                            </ol>
                            <h2>Image library
 
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/image-library.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download all images"><i class="icon-download"></i></a></h2>
                            <div class="clearfix"></div>
                            <p>These lifestyle images are available for use in Edwards digital and print collateral.</p>
                            <p><a href="templates/image-library.zip" class="btn btn-info" role="button"><i class="icon-download"></i> Download all images</a></p>
                            <div id="tabslider">
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>HCP</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>HCP &amp; patient</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Patient</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>HCP</h3>
                                        <div id="hcp-repeat" class="row imagethumbs text-center">
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>HCP &amp; patient</h3>
                                        <div id="hcp-patient-repeat" class="row imagethumbs text-center">
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Patient</h3>
                                        <div id="patient-repeat" class="row imagethumbs text-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="sidemenu-branding.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="meta-bottom.html" -->
        <script>

        // HCP Section

        var imagesHCP = ['hcp-1.jpg','hcp-2.jpg','hcp-3.jpg','hcp-4.jpg','hcp-5.jpg','hcp-6.jpg','hcp-7.jpg','hcp-8.jpg','hcp-9.jpg','hcp-10.jpg','hcp-11.jpg','hcp-12.jpg','hcp-13.jpg','hcp-14.jpg','hcp-15.jpg','hcp-16.jpg','hcp-17.jpg','hcp-18.jpg','hcp-19.jpg','hcp-20.jpg','hcp-21.jpg','hcp-22.jpg','hcp-23.jpg','hcp-24.jpg','hcp-25.jpg','hcp-26.jpg','hcp-27.jpg','hcp-28.jpg','hcp-29.jpg','hcp-30.jpg','hcp-31.jpg','hcp-32.jpg','hcp-33.jpg','hcp-34.jpg','hcp-35.jpg','hcp-36.jpg','hcp-37.jpg','hcp-38.jpg','hcp-39.jpg','hcp-40.jpg'];

        var counterHCP = 0;

        while (counterHCP < imagesHCP.length) {
            $('#hcp-repeat').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="templates/image-library/hcp/thumb/' + imagesHCP[counterHCP] + '" > <div class="caption"><p><a href="templates/image-library/hcp/' + imagesHCP[counterHCP] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterHCP++;
        }

        // HCP - Patient Section

        var imagesHCPpatient = ['hcp-patient-1.jpg','hcp-patient-2.jpg','hcp-patient-3.jpg','hcp-patient-4.jpg','hcp-patient-5.jpg','hcp-patient-6.jpg','hcp-patient-7.jpg','hcp-patient-8.jpg','hcp-patient-9.jpg','hcp-patient-10.jpg','hcp-patient-11.jpg','hcp-patient-12.jpg','hcp-patient-13.jpg','hcp-patient-14.jpg','hcp-patient-15.jpg','hcp-patient-16.jpg','hcp-patient-17.jpg','hcp-patient-18.jpg','hcp-patient-19.jpg','hcp-patient-20.jpg','hcp-patient-21.jpg','hcp-patient-22.jpg','hcp-patient-23.jpg','hcp-patient-24.jpg','hcp-patient-25.jpg','hcp-patient-26.jpg','hcp-patient-27.jpg','hcp-patient-28.jpg','hcp-patient-29.jpg','hcp-patient-30.jpg','hcp-patient-31.jpg','hcp-patient-32.jpg','hcp-patient-33.jpg','hcp-patient-34.jpg','hcp-patient-35.jpg','hcp-patient-36.jpg','hcp-patient-37.jpg'];

        var counterHCPpatient = 0;

        while (counterHCPpatient < imagesHCPpatient.length) {
            $('#hcp-patient-repeat').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="templates/image-library/hcp-patient/thumb/' + imagesHCPpatient[counterHCPpatient] + '" > <div class="caption"><p><a href="templates/image-library/hcp-patient/' + imagesHCPpatient[counterHCPpatient] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterHCPpatient++;
        }


        // Patient Section

        var imagesPatient = ['patient-1.jpg','patient-2.jpg','patient-3.jpg','patient-4.jpg','patient-5.jpg','patient-6.jpg','patient-7.jpg','patient-8.jpg','patient-9.jpg','patient-10.jpg','patient-11.jpg','patient-12.jpg','patient-13.jpg','patient-14.jpg','patient-15.jpg','patient-16.jpg','patient-17.jpg','patient-18.jpg','patient-19.jpg','patient-20.jpg','patient-21.jpg','patient-22.jpg','patient-23.jpg','patient-24.jpg','patient-25.jpg','patient-26.jpg','patient-27.jpg','patient-28.jpg','patient-29.jpg','patient-30.jpg','patient-31.jpg','patient-32.jpg','patient-33.jpg','patient-34.jpg','patient-35.jpg','patient-36.jpg','patient-37.jpg','patient-38.jpg','patient-39.jpg','patient-40.jpg','patient-41.jpg','patient-42.jpg','patient-43.jpg','patient-44.jpg','patient-45.jpg','patient-46.jpg','patient-47.jpg','patient-48.jpg','patient-49.jpg','patient-50.jpg','patient-51.jpg','patient-52.jpg','patient-53.jpg','patient-54.jpg','patient-55.jpg','patient-56.jpg','patient-57.jpg','patient-58.jpg','patient-59.jpg','patient-60.jpg','patient-61.jpg','patient-62.jpg','patient-63.jpg','patient-64.jpg','patient-65.jpg','patient-66.jpg','patient-67.jpg','patient-68.jpg','patient-69.jpg','patient-70.jpg','patient-71.jpg','patient-72.jpg','patient-73.jpg','patient-74.jpg','patient-75.jpg','patient-76.jpg'];

        var counterPatient = 0;

        while (counterPatient < imagesPatient.length) {
            $('#patient-repeat').append('<div class="col-md-2 col-xs-4"><div class="thumbnail match-height"><img class="img-responsive" src="templates/image-library/patient/thumb/' + imagesPatient[counterPatient] + '" > <div class="caption"><p><a href="templates/image-library/patient/' + imagesPatient[counterPatient] + '" class="btn btn-info btn-sm" role="button" target="_blank"><i class="icon-download"></i> Download</a></p></div></div></div>');
            counterPatient++;
        }

        </script>
    </body>

    </html>