﻿<%@ Page Language="C#" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    public void Login_OnClick(object sender, EventArgs args)
    {
        string verified = Request.Form["__EVENTARGUMENT"];
        if (verified.Equals("true",StringComparison.OrdinalIgnoreCase))
        {
            FormsAuthentication.RedirectFromLoginPage(UsernameTextbox.Text, true);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (AllowAnonymousAccess())
                FormsAuthentication.RedirectFromLoginPage("Edwards", true);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ClientScript.GetPostBackEventReference(UsernameTextbox, string.Empty);
    }

    private bool AllowAnonymousAccess()
    {

        string clientIPString = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "";
        string allowedUserIPList = "";
        if (ConfigurationManager.AppSettings["allowedIpList"] != null)
        {
            allowedUserIPList = ConfigurationManager.AppSettings["allowedIpList"];
        }
        if (allowedUserIPList == "")
            return false;

        IPAddress clientIPAddr;
        if (IPAddress.TryParse(clientIPString.Trim(), out clientIPAddr))
        {
            string[] ipAddresArray = allowedUserIPList.Split(',');
            List<string> ipAddresses = new List<string>();

            foreach (string ipAddress in ipAddresArray)
            {
                if (ipAddress.Contains("/"))
                    ipAddresses.AddRange(IpAddressRange(ipAddress));

                else
                    ipAddresses.Add(ipAddress.Trim());
            }

            if (ipAddresses.Contains(clientIPString))
                return true;
        }

        return false;
    }

    private List<string> IpAddressRange(string ipAddress)
    {
        try
        {
            string[] ipParts = ipAddress.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string[] startIP = ipParts[0].Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            int ipCount = Convert.ToInt32(ipParts[1]);
            int ipSuffix = Convert.ToInt32(startIP[3]);

            List<string> ipList = new List<string>();
            for (int i = 0; i < ipCount; i++)
                ipList.Add(string.Format("{0}.{1}.{2}.{3}", startIP[0], startIP[1], startIP[2], (ipSuffix + i).ToString()));

            return ipList;
        }
        catch (Exception) { }

        return new List<string>();
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>


    <script src="assets/config.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/lr-raas.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/customize.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/custom-social.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/social-icons.css" />
    <link rel="stylesheet" href="assets/css/jquery-ui.css" />

    <!-- Customizing -->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/userregistration.js" type="text/javascript"></script>

    <script src="lib/LoginRadiusSDK.2.0.1.js" type="text/javascript"></script>
    <script src="assets/js/LoginRadius.js" type="text/javascript"></script>
    <script src="assets/js/LoginRadiusRaaS.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/lr-theme-full.js"></script>

    <!-- Production -->
    <script type="text/html" id="loginradiuscustom_tmpl">
        <span class="lr-provider-label lr-sl-shaded-brick-button lr-flat-<#=Name.toLowerCase()#>"
            onclick="return $SL.util.openWindow('<#= Endpoint #>&is_access_token=true&callback=<#= window.location #>');"
            title="Sign up with <#= Name #>" alt="Sign in with <#=Name#>">
            <span class="lr-sl-icon lr-sl-icon-<#=Name.toLowerCase()#>"></span>
            Sign up with <# =Name#>
        </span>
    </script>
    <script type="text/html" id="loginradiuscustom_tmpl_IOS">
        <span class="lr-provider-label lr-sl-shaded-brick-button lr-flat-<#=Name.toLowerCase()#>"
            onclick="return $SL.util.openWindow('<#= Endpoint #>&is_access_token=true&callback=<#= window.location #>&callbacktype=hash');"
            title="Sign up with <#= Name #> IOS" alt="Sign in with <#=Name#> IOS">
            <span class="lr-sl-icon lr-sl-icon-<#=Name.toLowerCase()#>"></span>
            Sign up with <# =Name#> IOS
        </span>
    </script>
        <style type="text/css">

        body {
            background: #505759; /* fallback for old browsers */
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        </style>
</head>
<body>
<div class="login-modal">
    <div style="display: none;">
        <a id="login-link" class="lr-raas-button lr-raas-theme-login">Login</a>
    </div>
    <footer>
        <div class="overlay" id="fade" style="display: none;">
            <div class="circle">
                <div id="imganimation">
                    <img src="//cdn.loginradius.com/demo/common/loading_spinner.gif" alt="LoginRadius Processing"
                         style="margin-top: -66px;margin-left: -73px;width: 338px;height: 338px;">
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </footer>
    <div style="display: none;">
        <form class="login-form" id="form1" runat="server">
        <asp:TextBox ID="UsernameTextbox" runat="server" />
        <asp:Button ID="LoginButton" Text="Login" OnClick="Login_OnClick" runat="server" CssClass="form-button" />
        <asp:HiddenField ID="CommandArg" runat="server" />
        </form>
    </div> 
</div>
<script>
if (window.location.href.indexOf("vtype") > -1){
    $('.login-modal').remove();
}
</script>
</body>
</html>
