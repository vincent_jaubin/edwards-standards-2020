﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="elements-color" class="page">
  

    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> 
  
  <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Basic elements</li>
  <li>Color</li>
  </ol>
  
  
  
  <h2>Color
  <a href="pdf/p-color.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
  </h2>
  <div class="clearfix"></div>
  
  
  
  <div class="well intro">
  <p>Edwards is a red and titanium brand. Red communicates
our passion. Titanium symbolizes our reputation. Because
of this, our palette focuses on a family of strong reds;
all other colors are secondary. Refer to the specifications
below for accurate formulas.</p>
</div>

<div class="row">
<div class="col-md-6">
<p><strong>Primary</strong><br>
From the red secondary graphic to the titanium logo, these two colors
are our primary colors and should dominate our materials. Use red for
titles and headlines, charts and diagrams. Do not tint the red. The gray
may be tinted to 75%, 50%, 25% or lighter when used for information
graphics. It may also be used as a field of color behind the secondary
graphic, but tinted to 50% or lighter.</p>
</div>
<div class="col-md-6"><img src="img/colors-primary.png" alt="colors" class="img-responsive img-margin" /></div>
</div>
<div class="row">
<div class="col-md-6">
<p><strong>Secondary</strong><br>
The secondary palette includes a family of
reds and a dark gray. Use the reds for titles or
headlines, infographics or in limited digital
applications (see <a href="dig-apps.aspx" target="_blank">apps</a>, <a href="dig-emails.aspx" target="_blank">emails</a>, and <a href="dig-video.aspx" target="_blank">video & animation</a> pages). Never tint the
reds. The dark gray may be used for text and
infographics and may be tinted lighter.</p>
</div>
<div class="col-md-6"><img src="img/colors-secondary.png" alt="colors" class="img-responsive img-margin" /></div>
</div>
<div class="row">
<div class="col-md-6">
<p><strong>Neutrals</strong><br>
The neutral palette includes a range of hues with softer tones to
avoid competing with the red and grays. These colors may be used
for charts or information graphics, but in small amounts. When
neutrals are used as a background fill, tint the color to 50%, 25% or
lighter; never darker than 50% (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>).</p></div>
<div class="col-md-6"><img src="img/colors-neutral.png" alt="colors" class="img-responsive" /></div>
</div>



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
