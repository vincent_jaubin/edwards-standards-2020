﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="print-covers" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Print collateral</li>
  <li>Covers</li>
  </ol>
  
  
  <div id="tabslider">
  
  <h2>Covers
  <a href="pdf/p-covers.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/edwards-brochures.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the templates"><i class="icon-download"></i></a></h2>
  <div class="clearfix"></div>
  
  
 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Small secondary graphic</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Large secondary graphic</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 
 <h3>Small secondary graphic</h3>     
      


<div class="row">
<div class="col-md-4">
<p><strong>Page layout</strong></p>
<p>The grid determines the placement for each
visual element on this 8.5"W x 11"H format.</p>
<p>The logo is scaled to 0.80", based on the
height of the E symbol. It aligns to the right
and bottom margins.</p>
<p>The secondary graphic is a fixed size. In this
layout, the secondary graphic equals 10
square units high and wide.</p>
<p>Do not move the graphic left or right. However,
the graphic may be moved up or down, always
maintaining the minimum specified distances
from the top and bottom of the background.</p>
</div>

<div class="col-md-8">
<p><img src="img/covers-small-secondary-img-1.png" alt="Small secondary graphic" class="img-responsive center-block" /></p>
</div>
</div>

<div class="row">
<div class="col-md-8">

<p><strong>Photo background</strong></p>
<p>Photos in the approved corporate style may
appear behind the secondary graphic. Photos
bleed off the top, left and right edges, while
the bottom of the photo aligns to the top
horizontal line of the E symbol.</p>


</div>

<div class="col-md-4">
<p><img src="img/covers-small-secondary-img-2.png" alt="Small secondary graphic" class="img-responsive center-block" /></p>
</div>
</div>

<div class="row">
<div class="col-md-8">
<p><strong>Other backgrounds</strong></p>
<p>If a photo is not being used, the secondary
graphic may be placed over white, the
approved background pattern, or a tinted
neutral color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>). The bottom edge of the background
should align to the top horizontal line of the E
symbol. The small secondary graphic should
always be 100% primary red, no tints. When
placed over a neutral color, the secondary
graphic is 100% opaque; not translucent.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>
</div>

<div class="col-md-4">
<p><img src="img/covers-small-secondary-img-3.png" alt="Small secondary graphic" class="img-responsive center-block" /></p>
</div>
</div>
    
    
    </div>
    <div class="pane" id="tabs-2">
    
  <h3>Large secondary graphic</h3> 
  


<div class="row">
<div class="col-md-4">
<p><strong>Page layout</strong></p>
<p>The grid determines the placement for each
visual element on this 8.5"W x 11"H format.</p>
<p>The logo is scaled to 0.80", based on the
height of the E symbol It aligns to the right
and bottom margins.</p>
<p>On brochure covers, the secondary graphic is
a fixed size and in a fixed position. The right
edge of the graphic aligns to the right edge of
the page. Do not shift the graphic up or down,
left or right. In this layout, the secondary
graphic equals 15 square units high and wide.</p>
<p>When the larger square appears primary red
(PANTONE 186), the title and subtitle may be
white or a tinted neutral color, provided there
is sufficient contrast with the red background.</p>



</div>
<div class="col-md-8">
<p><img src="img/covers-large-secondary-img-1.png" alt="Large secondary graphic" class="img-responsive center-block" /></p>
</div>

</div>

<div class="row">
<div class="col-md-8"><p><strong>Silhouette photo square</strong></p>
<p>Silhouetted photos may appear in the
larger square of the secondary graphic. The
background of the photo should be an
approved tinted color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>) to help
define the edges of the secondary graphic. In
this application the title should be a solid red
from the approved palette, no tints.</p>
</div>
<div class="col-md-4">
<p><img src="img/covers-large-secondary-img-2.png" alt="Large secondary graphic" class="img-responsive center-block" /></p>
</div>

</div>
<div class="row">
<div class="col-md-8">
<p><strong>Photo square</strong></p>
<p>The larger square in the secondary graphic
may be used to place full-color photos. In this
application, titles should be a solid red from
the approved palette, no tints Subtitles may
be gray or black.</p>
<p>The secondary graphic may be placed over
white, the approved background pattern, or
a tinted neutral color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>). The
bottom edge of the background should align
to the top horizontal line of the E symbol.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p></div>
<div class="col-md-4">
<p><img src="img/covers-large-secondary-img-3.png" alt="Large secondary graphic" class="img-responsive center-block" /></p>
</div>

</div>


</div>     
    
    
    </div>
    
    
    
  </div>
  
  
  
  

    </div>
    
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
