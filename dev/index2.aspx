<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->

 
  
  <div class="jumbotron banner-edw banner-home">
    <div class="container">
      <div class="redboxes">
      <div class="redbox-sm"></div>
      <div class="redbox-lg">
        <h1>Edwards Global Experience Language</h1>
      </div>
      </div>
    </div>
  </div><!-- /.banner-edw --> 
  
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-3 col-sm-6">
  		<div class="thumbnail grid-edw"> <a href="#"><img src="img/whatisegel.png" class="img-responsive" alt="What is EGEL?"></a>
            <div class="caption">
        	<h3>What is EGEL?</h3>
            <p>Learn about our EGEL and how to use this site for your Edwards projects.</p>
            <p><a href="#" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
 			</div>
        </div>
  </div>
  <div class="col-md-3 col-sm-6">
  		<div class="thumbnail grid-edw"> <a href="#"><img src="img/personality.png" class="img-responsive" alt="Our Brand Personality"></a>
            <div class="caption">
        	<h3>Our brand personality</h3>
            <p>Learn the five attributes of the Edwards brand personality</p>
            <p><a href="#" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
 			</div>
        </div>
  </div>
  <div class="col-md-3 col-sm-6">
  		<div class="thumbnail grid-edw"> <a href="#"><img src="img/brand-elements.png" class="img-responsive" alt="Basic Brand Elements"></a>
            <div class="caption">
        	<h3>Basic brand elements</h3>
            <p>Find out the latest basic design requirements of our Edwards brand</p>
            <p><a href="#" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
 			</div>
        </div>  
      </div>
  <div class="col-md-3 col-sm-6">
  		<div class="thumbnail grid-edw"> <a href="#"><img src="img/design-elements.png" class="img-responsive" alt="Design Elements"></a>
            <div class="caption">
        	<h3>Digital design elements</h3>
            <p>Get the simple components that make up our digital designs</p>
            <p><a href="#" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
 			</div>
        </div>
  </div>
  </div> <!-- /row -->
  </div>
 
 <div class="container-fluid edw-gray-bg">
 <div class="container">
 <div class="row edw-media">
<div class="col-md-5">
      <img src="img/about.png" alt="Edwards Boilerplate" class="img-responsive">
  </div>
  <div class="col-md-7">
    <h3>Edwards' boilerplate</h3>
 <p>Edwards Lifesciences, based in Irvine, Calif., is the global
leader in patient-focused medical innovations for
structural heart disease, as well as critical care and surgical
monitoring. Driven by a passion to help patients, the
company collaborates with the world’s leading clinicians
and researchers to address unmet healthcare needs,
working to improve patient outcomes and enhance lives.
For more information, visit <a href="http://edwards.com" target="_blank">Edwards.com</a> and follow us on
Twitter <a href="https://twitter.com/edwardslifesci" target="_blank">@EdwardsLifesci</a>.</p>
<p><a href="#" class="btn btn-success block-xs" role="button"><span class="icon-circle-right"></span> Get the Edwards' Credo</a></p>
  </div>
</div> <!-- /row -->
  </div> 
  </div>
  
   <div class="container">
   <h2>Branding quick links</h2>
 <div class="row dot-borders">
 		<div class="col-md-4 col-sm-4">
        <div class="row">
        	<div class="col-md-5"><img src="img/thumbnail-print.png" alt="Print Collateral" class="img-responsive">
            </div>
            <div class="col-md-7">
            <h4>Print collateral</h4>
            <p><a href="#" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
            </div>
        </div>
        </div>
 		<div class="col-md-4 col-sm-4">
         <div class="row">
        	<div class="col-md-5"><img src="img/thumbnail-ad.png" alt="Advertising" class="img-responsive">
            </div>
            <div class="col-md-7">
            <h4>Advertising</h4>
            <p><a href="#" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
            </div>
        </div>
        </div>
 		<div class="col-md-4 col-sm-4">
         <div class="row">
        	<div class="col-md-5"><img src="img/thumbnail-ppt.png" alt="PowerPoint" class="img-responsive">
            </div>
            <div class="col-md-7">
            <h4>PowerPoint</h4>
            <p><a href="#" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
            </div>
        </div>
        </div>
 
 </div> <!-- /row -->
 </div>
 
 <div class="container-fluid edw-gray-bg">
 	<div class="container">
    <div class="row">
    	<div class="col-md-6 col-sm-6">
        	<div class="row">
            	<div class="col-md-2"><span class="edw-circle-icon"><span class="icon-display"></span></span></div>
                <div class="col-md-9"><h3>Web page templates</h3>
                <p>Web templates and building blocks to help you design your next Edwards web projects.</p>
                <p><a href="#" role="button" class="btn btn-block btn-success"><span class="icon-circle-right"></span> Visit web component section</a></p>
                <p><a href="#" role="button" class="btn btn-block btn-success"><span class="icon-circle-right"></span> Visit page template section</a></p>
                </div>
            </div>
        </div>
    	<div class="col-md-6 col-sm-6">
        	<div class="row">
            	<div class="col-md-2"><span class="edw-circle-icon"><span class="icon-download"></span></span></div>
                <div class="col-md-9"><h3>Digital downloads</h3>
                <p>Get the latest assets to help you design or code your next Edwards graphics projects.</p>
                <p><a href="#" role="button" class="btn btn-block btn-success"><span class="icon-file-zip"></span> Download web templates</a></p>
                <p><a href="#" role="button" class="btn btn-block btn-success"><span class="icon-file-pdf"></span> Download brand guidelines</a></p>
                </div>
            </div>        
        </div>
    </div> <!-- /row -->
    </div>
 </div>
 
 
<!-- #include file="footer2.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
