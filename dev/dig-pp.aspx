﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="dig-pp" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Digital media</li>
  <li>PowerPoint presentations</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>PowerPoint presentations<a href="pdf/p-powerpoint.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/edwards-pp.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the templates"><i class="icon-download"></i></a>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Cover slides</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Content slides</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Section header, disclosure and end slides</span></a></li>
  <li><a href="#tabs-4" class="match-height"><span>Supporting visual elements</span></a></li>
  <li><a href="#tabs-5" class="match-height"><span>Font usage</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Cover slides</h3>   

<div class="row">
<div class="col-md-4">
<p>The PowerPoint presentation templates
contain approved layouts and sample slides
for cover, content (including text, chart,
graphic and table options), section header,
disclosure and end slides.</p>
<p>The templates have been carefully constructed.
Do not change the size or position of master
slide elements. Read the sample slides for tips
on how to use and customize the template.</p>
<p><strong>Proportion</strong></p>
<p>There are templates for 16:9 (10" x 5.63",
shown here), 4:3 (10" x 7.5") and vertical
(7.5" x 10") proportions. The same master
and sample slides are in all templates.</p>
<p><strong>Colors</strong></p>
<p>The built-in colors are based on the approved
RGB color formulas, with a few adjustments
to enhance chart legibility. See the “Our color
palette” slide in the template for details and
specifications.</p>
</div>
<div class="col-md-8"><img src="img/pp-cover.png" class="img-responsive"></div>
</div>

<div class="row">
<div class="col-md-12">

<p><strong>Fonts</strong></p>
<p>For optimal compatibility, the PowerPoint
templates use Arial exclusively for all live text.</p>
<p><strong>Cover slides</strong></p>
<p>The first eight sample slides in the templates
(shown here) demonstrate how the small
and large secondary graphics can be used on
cover slides.</p>
<p>Cover slides may contain a photo – either
behind the small secondary graphic or within
the large secondary graphic. When used
behind the small secondary graphic, the
image partially shows though the graphic.</p>
<p>Alternatively, cover slides may have solid
color – 25% or 50% tints of select colors – or
pattern backgrounds – dark gray or white.</p>
<p>See the sample cover slides and the color
palette slide within the templates for details.</p>
</div>
</div>



    
 </div>
 
    <div class="pane" id="tabs-2">
    <h3>Content slides</h3>   

<div class="row">
<div class="col-md-4">
<p><strong>Content slides</strong></p>
<p>The sample content slides in the templates
show how text, chart, graphic and table
slides can be created in one- or two-column
formats. Read the content on these sample
slides as it contains helpful formatting tips.</p>
<p><strong>Text</strong></p>
<p>For consistency, use the built-in text levels.
By default, level one text comes with a square
bullet but may be used without the bullet.
Use the “Decrease List Level” and “Increase
List Level” buttons on the “Home” tab to
change text levels. Use bold selectively.</p>
<p><strong>Charts and graphics</strong></p>
<p>Keep charts and graphics simple. Use approved
colors and avoid distracting effects. Do not let
graphic effects distract from the data.</p>
</div>
<div class="col-md-8"><img src="img/pp-content.png" class="img-responsive"></div>
</div>

<div class="row">
<div class="col-md-12">


<p>SmartArt can be a quick and effective way
to create graphics such as process and
organizational charts. Avoid rounded corners
on square and rectangular shapes. Drop
shadows, if used at all, should be subtle.</p>
<p><strong>Tables</strong></p>
<p>The built-in “Edwards” table style is a good
starting point for tables. Use the “Header
Row”, “Total Row”, “Banded Rows” and other
table options to quickly and consistently
format tables. Turning all table options off
creates a simple table with horizontal lines.</p>
</div>

</div>

    
    
   </div>
   
   <div class="pane" id="tabs-3">
    <h3>Section header, disclosure and end slides</h3>   

<div class="row">
<div class="col-md-7">
<p><strong>Section header slides</strong></p>
<p>Use section header slides to organize
content into topics or chapters. For small
presentations, section headers may not
be necessary.</p>
</div>
<div class="col-md-5"><img src="img/pp-section-1.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-4">
<p><strong>Disclosure (or legal) slides</strong></p>
<p>Disclosure slides must meet all regulatory
and legal requirements. Text is anchored to
the bottom of the slide and grows up as text
is added. Remember to update the copyright
year (if needed) and code number.</p>

</div>
<div class="col-md-8"><img src="img/pp-section-2.png" class="img-responsive img-margin"></div>
</div>


<div class="row">
<div class="col-md-4">

<p><strong>End (or sign-off) slides</strong></p>
<p>The background of the end slides may be a
25% or 50% tint of select colors, or white
(with or without the pattern).</p>
<p>End slides with the “Helping Patients is Our
Life’s Work…” line are for internal use only.</p>
</div>
<div class="col-md-8"><img src="img/pp-section-3.png" class="img-responsive"></div>
</div>



    
    
   </div>
   
   <div class="pane" id="tabs-4">
    <h3>Supporting visual elements</h3>   

<div class="row">
<div class="col-md-4">
<p>For sustaining corporate internal programs
and initiatives (such as BEST, Total Wellness,
etc.) there may be a need to support specific
messaging The following is allowed:</p>
<ul>
<li>Incorporate a supporting visual element
within the template</li>
<li>The element should appear visually and
hierarchically secondary to the corporate
brand</li>
<li>The element must meet Edwards’ brand
standards (i e , color palette, font styles)</li>
<li>The element may appear large and/or
animated, following the title slide</li>
<li>Support usage with remaining placement
of the element consistently in the top or
bottom right corner of interior slides</li>
<li>Must be approved by Corporate Branding</li>
</ul>
</div>
<div class="col-md-8"><img src="img/pp-supp-visual.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-12">
<div class="well intro"><p>These specifications apply only to internal
programs and initiatives</p></div>
</div>


</div>



    
    
   </div>
   
   <div class="pane" id="tabs-5">
    <h3>Font usage</h3>   

<div class="row">
<div class="col-md-12">
<p>For optimal compatibility, the PowerPoint templates use Arial exclusively for
all live text.</p>
<p>Vesta Std, however, can be used as artwork (vector/outlined or as a rasterized
image) within graphics and animations. Vesta Std should never be used as live
text since most users—both internally and externally—will not have the font.</p>
<p>Ensure that all Vesta Std is artwork, not live text.</p>
</div>
</div>

    
    
   </div>
    
    

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
