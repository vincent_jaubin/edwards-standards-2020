<%@ Page Language="C#" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register</title>

    <script src="assets/config.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/lr-raas.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/customize.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/custom-social.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/social-icons.css" />
    <link rel="stylesheet" href="assets/css/jquery-ui.css" />

    <!-- Customizing -->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/userregistration.js" type="text/javascript"></script>

    <script src="lib/LoginRadiusSDK.2.0.1.js" type="text/javascript"></script>
    <script src="assets/js/LoginRadius.js" type="text/javascript"></script>
    <script src="assets/js/LoginRadiusRaaS.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/lr-theme-full.js"></script>

    <!-- Production -->
    <script type="text/html" id="loginradiuscustom_tmpl">
        <span class="lr-provider-label lr-sl-shaded-brick-button lr-flat-<#=Name.toLowerCase()#>"
            onclick="return $SL.util.openWindow('<#= Endpoint #>&is_access_token=true&callback=<#= window.location #>');"
            title="Sign up with <#= Name #>" alt="Sign in with <#=Name#>">
            <span class="lr-sl-icon lr-sl-icon-<#=Name.toLowerCase()#>"></span>
            Sign up with <# =Name#>
        </span>
    </script>
    <script type="text/html" id="loginradiuscustom_tmpl_IOS">
        <span class="lr-provider-label lr-sl-shaded-brick-button lr-flat-<#=Name.toLowerCase()#>"
            onclick="return $SL.util.openWindow('<#= Endpoint #>&is_access_token=true&callback=<#= window.location #>&callbacktype=hash');"
            title="Sign up with <#= Name #> IOS" alt="Sign in with <#=Name#> IOS">
            <span class="lr-sl-icon lr-sl-icon-<#=Name.toLowerCase()#>"></span>
            Sign up with <# =Name#> IOS
        </span>
    </script>
        <style type="text/css">

        body {
            background: #505759; /* fallback for old browsers */
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        </style>
</head>
<body>
<div class="register-modal">
    <div style="display: none;">
        <a id="login-link" class="lr-raas-button lr-raas-theme-register">Register</a>
    </div>
    <footer>
        <div class="overlay" id="fade" style="display: none;">
            <div class="circle">
                <div id="imganimation">
                    <img src="//cdn.loginradius.com/demo/common/loading_spinner.gif" alt="LoginRadius Processing"
                         style="margin-top: -66px;margin-left: -73px;width: 338px;height: 338px;">
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </footer>
</div>

</body>
</html>
