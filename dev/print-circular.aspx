﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="print-circular" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Print collateral</li>
  <li>Circular formats</li>
  </ol>
  
  
  
  <h2>Circular formats
  <a href="pdf/p-circular.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
  
<p>The small secondary graphic may be used on
circular applications, such as a DVD label.
The secondary graphic should appear to the
left of the center opening in the disc – do not
crop the secondary graphic on the opening.
Be sure the small red square has space around
it – do not bleed or crop it.</p>
<p>The gray areas shown here may be used for
a photo, the approved pattern, a tinted
neutral color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>), or white.
The bottom edge of the background should
align to the logo when content below allows.</p>
  
 
<div class="row">
<div class="col-md-4">
<p><strong>DVD labels</strong></p>
<p>On DVD labels the logo is scaled to 0.50",
based on the height of the E symbol. The
photo or background area may align to the
top horizontal line of the E symbol, otherwise
it must end outside the logo clear area.
To ensure a well organized layout, align all
levels of typography to one another and to
the logo. While the curved edges might limit
the use of some space, the edges of support
copy (not title or headlines) may follow a
curved edge.</p>

</div>

<div class="col-md-8">

<img src="img/print-circular-1.png" alt="Circular layouts" class="img-responsive img-margin" />
</div>
</div>

<div class="row">
<div class="col-md-4">

<p><strong>Other circular applications</strong></p>
<p>Other larger circular formats — such as
table top graphics or clings – should follow
the same guidance described above, only
at a larger scale. While the logo and type
sizes may vary, the same principles apply
for maintaining as few alignment points as
possible for a cleaner layout.</p>
</div>

<div class="col-md-8">

<img src="img/print-circular-2.PNG" alt="Circular layouts" class="img-responsive" />
</div>
</div>


  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
