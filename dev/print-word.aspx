﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="print-word" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Print collateral</li>
  <li>Collateral in Microsoft Word</li>
  </ol>
  
  
  
  <h2>Collateral in Microsoft Word
  <a href="pdf/p-mword.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
  
 
<div class="row">
<div class="col-md-12">
<p><strong>Page layout</strong></p>
<p>Collateral pieces created in Microsoft Word should mimic the layouts of other
print collateral. Logo placement, secondary graphic, typographic grids and
hierarchy for content sheets or other pieces should match the specifications
detailed here in the print collateral section of these standards.</p>
<p>Printing from a Word template does not typically allow a full bleed, but the
system was designed to accommodate this. For example, the top and right
edges of the secondary graphic on content sheets will be cropped, which is
acceptable. Copy within the secondary graphic may move down as necessary.
Photos may also be cropped, so their placement may need to be adjusted.</p>
<p><strong>Typography</strong></p>
<p>Vesta Std will always be the preferred Edwards typeface. However, most users—
both internally and externally—will not have Vesta Std which is why Arial is
the default typeface for Word templates. Use Arial in comparable weights
and styles as Vesta Std in the examples illustrated here in the print collateral
section of these standards.</p>
<p>Although it is preferred that either Vesta Std or Arial be used independently,
both typefaces may appear on the same communication in a limited manner.
For example, Vesta Std Regular may be used for the title within the red
secondary graphic as artwork (vector/outlined or as a rasterized image), while
Arial is used for live type everywhere else. Never mix the two typefaces within
the same sentence or paragraph.</p>
</div>

</div>


  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
