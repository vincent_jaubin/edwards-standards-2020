﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-awareness" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>Awareness programs</li>
  </ol>
  
  
  
  <h2>Awareness programs<a href="pdf/p-awareness.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  
<h3>Patient awareness</h3>
<p>Patient awareness communications, defined as any communication directed
to the patient, or other non-healthcare professional (HCP) audience, whether
individually or as a portfolio, have some additional guidelines that support
their development. These communications:</p>
<ul>
<li>Employ the same visual standards as product promotion materials.</li>
<li>Include an additional option for color choices (see <a href="pat-color.aspx" target="_blank">patient awareness page</a>).</li>
<li>Include an option for one (1) additional typeface (see <a href="pat-color.aspx" target="_blank">patient awareness page</a>).</li>
<li>May be fully branded, or less branded.
<ul>
<li>Less branded means the logo may be omitted on the front of a
communication and appear only on the back. Additionally, the hierarchy
of the logo placement on digital materials may be moved down.</li></ul></li>
</ul>

<h3>Therapy awareness</h3>
<p>Therapy awareness communications have three defining qualities. They are
1) informational or educational, and related to procedures, approaches or
options, 2) Edwards products are used for representative purposes only, and
3) communications do not contain product-specific claims, features or
benefits. Therapy awareness communications also have some additional
guidelines that support their development. These communications:</p>
<ul>
<li>Employ the same visual standards as product promotion materials.</li>
<li>In small format materials (example, digital banner ads), the Edwards logo
may be omitted, but the material must include reference to being provided
by Edwards Lifesciences.</li>
<li>May be fully branded, or less branded.
<ul>
<li>Less branded means the logo may be omitted on the front of a
communication and appear only on the back. Additionally, the hierarchy
of the logo placement on digital materials may be moved down.</li></ul></li>
</ul>

<h3>Disease and protocol awareness</h3>
<p>Disease and protocol awareness communications are more educational
or informational in nature; they may discuss protocol implementation and effectiveness,
or disease states and related treatment options without mention or
regard for Edwards or it products. Additional guidelines that govern these
communications include:</p>
<ul>
<li>Employ a separate design than is used for product promotion so as not to
lead the viewer back to Edwards’ products.</li>
<li>The Edwards logo may be used or omitted, as needed, for any given material.
<ul>
<li>If the logo is omitted, the materials must clearly state they are provided
by Edwards Lifesciences.</li></ul></li>
</ul>


</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
