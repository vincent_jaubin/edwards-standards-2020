﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="ads-print" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Advertising</li>
  <li>Print ad spreads</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Print ad spreads<a href="pdf/p-print-ad-spreads.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Small secondary graphic</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Large secondary graphic</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>References, regulatory<br> & legal information</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Small secondary graphic</h3>   

<div class="row">
<div class="col-md-4">
<p><strong>Page layout</strong></p>
<p>In this 11"H x 17"W full-spread layout, the
bottom edge of the photo background may
move up or down to allow for more legal,
regulatory or body copy, or for a larger photo. If the photo height is adjusted, the secondary
graphic and its contents may also move up or
down to work with the photo background.</p>
<p>The secondary graphic is a fixed size and
should not be scaled. Do not move the
graphic left or right. The secondary graphic
equals 10 square units high and wide.</p>
<p>The logo is scaled to 1.0", based on the height
of the E symbol. It aligns to the right and
bottom margins.</p>

<p>Legal and address copy also align to the bottom
margin and are typeset in Vesta Std Regular,
7pt on 9pt line spacing – <em>Edwards Lifesciences</em>
is Bold.</p>


</div>
<div class="col-md-8"><img src="img/ads-spread-small-sec-1.png" class="img-responsive"></div>
</div>
<div class="row">
<div class="col-md-4">



<p><strong>Sample layouts</strong></p>
<p>Photos should bleed off the top, left and right
sides. When adjusting the height of the photo
background, the bottom edge should be at
least 0.30" above the logo.</p>

<p>When used on photos, the larger of the two
squares in the secondary graphic may have
a multiply effect as described <a href="elements-secondary.aspx" target="_blank">here</a>.
Otherwise, it may remain opaque.</p>
<p>The photo area should be no less than half
of the page height. When adjusting the
height of the photo, it is preferred that the
secondary graphic and its contents center
align (vertically) with the background.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>
</div>
<div class="col-md-8"><img src="img/ads-spread-small-sec-2.png" class="img-responsive"></div>
</div>



</div>



    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Large secondary graphic</h3>   

<div class="row">
<div class="col-md-4">
<p><strong>Page layout</strong></p>
<p>In this 11"H x 17"W full-spread layout, the
larger shape in the secondary graphic may shift
up or down to allow for more legal, regulatory
or body copy. Headlines and subtitles may shift
up or down within the shape as necessary, but
should remain aligned to the left margin. The
smaller square should remain a square – do not
stretch the smaller square.</p>
<p>The width of the secondary graphic in this
layout equals 30 square units wide. Do
not adjust the width of the graphic, only
the height.</p>
<p>The logo is scaled to 1.0", based on the height
of the E symbol. It aligns to the right and
bottom margins.</p>

<p>Legal and address copy also align to the
bottom margin and are typeset in Vesta Std
Regular, 7pt on 9pt line spacing – <em>Edwards
Lifesciences</em> is Bold.</p>
</div>
<div class="col-md-8"><img src="img/ads-spread-large-sec-1.png" class="img-responsive"></div>
</div>

<div class="row">
<div class="col-md-4">


<p><strong>Sample layouts</strong></p>
<p>The bottom edge of the photo may extend
down to 0.30" above the logo. In this layout,
headlines should be red from the primary
palette.</p>

<p>Photos in the larger square may extend down
to align with the top horizontal line within
the E symbol.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>
</div>
<div class="col-md-8"><img src="img/ads-spread-large-sec-2.png" class="img-responsive"></div>
</div>

    
    
   </div>
    <div class="pane" id="tabs-3">
    
    <h3>References, regulatory & legal information</h3>   

<div class="row">
<div class="col-md-4">
<p><strong>Page layout</strong></p>
<p>The logo is scaled to 1.0", based on the height
of the E symbol. It aligns to the right and
bottom margins.</p>
<p>Depending on the amount of content, the
main disclosure/legal content can be either
Vesta Std Regular 7pt on 9pt line spacing, or
9pt on 12pt line spacing. Paragraphs may be
numbered. Use paragraph styles built into the
templates to format text.</p>
<p>The address copy block aligns to the bottom
margin and is typeset in Vesta Std Regular,
7pt on 9pt line spacing – <em>Edwards Lifesciences</em>
is Bold.</p>
<p><strong>Color</strong></p>
<p>The disclosure/legal content spread can be
printed either in color or grayscale. When
printed in grayscale, the secondary graphic
and logo are both 55% black.</p>

<p>If needed, both color and grayscale versions
can be converted to non-bleed layouts by
clipping the secondary graphic 0.125" from
all edges.</p>
<p>The page layout is the same for both color
and grayscale versions.</p>
</div>
<div class="col-md-8"><img src="img/ads-spread-ref-reg-leg.png" class="img-responsive"></div>
</div>


     </div>
    

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
