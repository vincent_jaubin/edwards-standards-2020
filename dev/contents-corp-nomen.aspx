﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-corp-nomen" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>Corporate nomenclature</li>
  </ol>
  
  
  
  <h2>Corporate nomenclature<a href="pdf/p-corporate.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  
<h3>Holding company</h3>
<p>The name of our holding company is Edwards Lifesciences Corporation. This
is our NYSE listed publicly traded company. This name is used only on select
corporate materials, such as legal documents for the holding company, stock
certificates and our annual report.</p>
<h3>Principal United States operating subsidiary</h3>
<p>The principal United States operating subsidiary name, Edwards Lifesciences
LLC, should be used on U.S. external communications, such as stationery
and business cards, forms, collateral, etc.</p>
<p>Edwards Lifesciences must be used initially when referencing the company in
body copy. It is preferred that Edwards be used in all subsequent references.</p>
<p>The full word “Corporation” is the only correct form for the holding company name,
while the abbreviation “LLC” is the only correct form for the U.S. operating company
name. (Edwards Lifesciences LLC – without a comma between “Lifesciences” and “LLC”).</p>
<h3>Principal international subsidiaries</h3>
<p>Edwards conducts business through subsidiaries in many countries. A detailed
list of global subsidiary names is posted on the company intranet site within
the Legal section.</p>
<p>In some countries, it is required to include the full name of the company
specific to that country. Please consult with local legal and regulatory counsel
regarding these specific requirements. When required, the full name of the
country subsidiary should be used on external communications, such as
stationery and business cards, forms, advertising, collateral, etc.</p>
<h3>Proper spelling and capitalization of our name</h3>
<p>Please note that "Lifesciences" is treated as a single word, with only the “L” capitalized.
This is consistent with all of our legal documentation and registration of the
Edwards identity throughout the world. No other letters should be capitalized
when referencing “Edwards Lifesciences.” When using the company name in the
possessive, it should be written as either Edwards’ or Edwards Lifesciences’. For
example, Edwards’ passion or Edwards Lifesciences’ emphasis on patient care.</p>
  
 <h3>Proper abbreviations for our name</h3>
<p>Unless the full legal name is required, there are a number of proper
abbreviations available:</p>
<ul>
<li><strong>Edwards Lifesciences</strong> (the full name should be cited in the first usage in
body copy in all external documents)</li>
<li><strong>Edwards</strong> (appropriate for subsequent usages, if desired; acceptable on both
internal and external communications)</li>
<li><strong>EW</strong> (our trading symbol on the New York Stock Exchange)
</li>
</ul>
  


</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
