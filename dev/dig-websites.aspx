﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="dig-websites" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Digital media</li>
  <li>Websites</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Websites<a href="pdf/p-websites.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Desktop headers</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Responsive layouts</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Desktop headers</h3>   
 
 <div class="well intro">
 <p>Use our identity elements – logo,
color, typography, photography and
secondary graphic – together to
create a consistent and recognizable
digital presence.</p>
 </div>

<div class="row">
<div class="col-md-4">

<p>Our websites – whether viewed on desktop or
mobile devices – can vary widely in intended
use, target audience, functionality and design.
Regardless, they should all be recognizably
ours. This is achieved by consistent use of our
identity elements.</p>
<p>The samples shown illustrate how the identity
elements could be used together to create
the top portion of a webpage intended to be
viewed on a desktop computer. Consistency
across sites – for example, in navigation – can
further enhance the user experience.</p>
<p><strong>Using the secondary graphics</strong></p>
<p>Quite often, the small and large secondary
graphics can be used online the same way
they are for tradition print media.</p>
</div>
<div class="col-md-8"><img src="img/websites-desktop.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-12">

<p>Start by following the guidelines for print (see <a href="elements-secondary.aspx" target="_blank">secondary graphic page</a>). Due to differing screen sizes
and proportions, however, some additional
flexibility is allowed. For example, the height
of larger shape in the secondary graphic may
be adjusted to accommodate content above
the fold/scroll.</p>
<p>Websites should be built on a fluid grid.
However, the size and padding around the
logo and small secondary graphic should
remain fixed. Full-width images may be
cropped as the browser is resized.</p>
<p><em>Note: This document is not indented to provide
comprehensive guidelines for online media.</em></p>
</div>
</div>



    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Responsive layouts</h3>   
<div class="well intro">
<p>A responsive website can control the
design and content of your site, as it
scales up and down with the browser
or device.</p>

<p>The samples illustrate how the website layout
can adapt across a variety of viewing widths.</p>
</div>
<div class="row">
<div class="col-md-5">
<p><strong>Extended</strong></p>
<p>The background color of the masthead and
footer may extend beyond 1400px, the max
width of the content area. The background
color in all other extended areas should
remain white.</p>
</div>
<div class="col-md-7"><img src="img/websites-responsive-1.png" class="img-responsive img-margin" alt="Extended" /></div>
</div>

<div class="row">
<div class="col-md-5">
<p><strong>Desktop and tablet</strong></p>
<p>Websites should be built on a fluid grid.
However, the size and padding around the
logo and small secondary graphic should
remain fixed. Full-width images may be
cropped as the browser is resized.</p>
</div>
<div class="col-md-7"><img src="img/websites-responsive-2.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-5">

<p><strong>Mobile</strong></p>
<p>Websites should have at least one breakpoint
to transition from desktop to mobile.
Although, both layouts may differ, the
content’s hierarchical structure should remain
the same. This consistent representation of
content across all widths strengthens the user
experience.</p>

</div>

<div class="col-md-7"><img src="img/websites-responsive-3.png" class="img-responsive"></div>
</div>

    
    
   </div>
   
  
   
   
   
    
    

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
