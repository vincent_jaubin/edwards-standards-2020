﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="print-interior" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Print collateral</li>
  <li>Interior layouts</li>
  </ol>
  
  
  
  <h2>Interior layouts
  <a href="pdf/p-interior-layouts.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/Metric/edwards-interior.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the metric templates"><i class="icon-download"></i> <span class="share-txt">Metric</span></a><a href="templates/US-standard/edwards-interior.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the U.S. templates"><i class="icon-download"></i> <span class="share-txt">U.S.</span></a></h2>
  <div class="clearfix"></div>
  
  
 
<div class="row">
<div class="col-md-4">
<p><strong>Page layout</strong></p>
<p>The grids determine the placement for each
element on this 8.5"W x 11"H format. Although
the top margin is 0.50", it is recommended
that the copy begin 1.50" from the top.</p>
<p>This layout shows effective typographic
hierarchies, from titles and pull-quotes to
body copy. Pages may be divided into 2- or
3-column grids as shown here. On a 3-column
page, text areas can be either one wide and
one narrow column (see left page), or three
narrow columns. On a 2-column grid,
copy may be divided into two equal column
widths. Avoid creating a single, wide column
across the page at it will be difficult to read.</p>
</div>

<div class="col-md-8">

<img src="img/print-interior-layout-1.png" alt="Interior layouts" class="img-responsive" />
</div>
</div>

<p>Interior pages do not need to include the
secondary graphic. Sidebars and call-outs
may appear on a 25% or lighter tint of primary
gray or other neutral color.</p>
<div class="row">
<div class="col-md-12">

<p>Colors from the approved palette may be
used on interior and back cover layouts for
headlines and pull-quotes. However, only use
one or two colors per layout to avoid visual
clutter. Body copy should be black or gray.
<p>Photos should be cropped square or in a
rectangular shape. When placing photos,
they should align to the grid. Use a “padding”
of 0.30" around all sides of an image – a
measurement equal to the gutter width.</p>
<p><strong>Charts & diagrams</strong></p>
<p>Charts and diagrams should use colors from
the approved palette. Do not tint the reds;
all other colors may be tinted as needed. Use
Vesta Std Bold for headers and Regular for
support, preferably in the same point size.</p>
<p>
<img src="img/print-interior-layout-2.png" alt="Interior layouts" class="img-responsive" /></p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>
</div>
</div>

  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
