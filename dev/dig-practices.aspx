﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="dig-practices" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Digital media</li>
  <li>SEO / HTML best practices</li>
  </ol>
  
  
  
  <h2>SEO / HTML best practices
  <!-- <a href="pdf/p-emails.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a> -->
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>

<div class="well intro"><p>This document is intended for agencies that are developing HTML for Edwards Lifesciences.
These are list of best practices and basic SEO that should be incorporated in the HTML.</p></div>

<h3>SEO / HTML best practices</h3>

<p><strong>Keywords in title tag</strong></p>
<ul>
  <li>Descriptive <code>&lt;title&gt;</code> tag that contains product, therapy, device, or page name followed by Edwards Lifesciences</li>
  <li>e.g. Mitral Heart Valve Repair | Edwards Lifesciences</li>
</ul>
<p><strong>Keywords in meta description</strong></p>
<ul>
  <li>Meta Description should contain a vsummary about the product - about 160 characters</li>
  <li>e.g. <code>&lt;meta name="description" content="Meta Description should contain a summary about the product and keywords incorporated in 160 characters. This is text is what usually appears in SERP."&gt;</code></li>
</ul>
<p><strong>Structured data in JSON-LD format</strong></p>
<ul>
  <li>Minimum data should contain product name, product description, and page URL</li>
  <li>Structured data adhering to <a href="http://schema.org/" target="_blank">schema.org</a> and tested using <a href="https://search.google.com/structured-data/testing-tool/u/0/?hl=EN" target="_blank">Google's Structured Data Testing Tool</a></li>
</ul>
<p><strong>Include alt tag that describes the image with Edwards branding</strong></p>
<ul>
  <li>e.g. <code>&lt;img src="/images/product1.jpg" alt="product image 1 by Edwards Lifesciences"&gt;</code></li>
</ul>
<p><strong>NoFollowed tag in &lt;a href ...&gt; links pointing to third party sites</strong></p>
<ul>
  <li>e.g. <code>&lt;a href="http://www.thirdpartysite.com/section/page1.html" rel="nofollow"&gt;Keyword in Anchor&lt;/a&gt;</code></li>
</ul>

<p><strong>Text link with keywords in anchor text</strong></p>
<ul>
  <li>e.g. <code>&lt;a href="http://www.edwards.com"&gt;keyword in anchor&lt;/a&gt;</code></li>
</ul>
<p><strong>Add canonical tag, if applicable</strong></p>
<ul>
  <li>e.g. <code>&lt;link rel="canonical" href="http://www.edwards.com/devices/productname" /&gt;</code></li>
</ul>
<p><strong><span class="edw-red">Do not</span> use of inline style tag</strong></p>
<ul>
  <li>e.g. <code>&lt;div style="..."&gt;</code></li>
</ul>
<p><strong><span class="edw-red">Do not</span> use !important tags in style sheet</strong></p>
<ul>
  <li>e.g. <code>h1 {font-size:15px !mportant;}</code></li>
</ul>
<p><strong><span class="edw-red">Do not</span> use social metadata tags; this is not approved by legal</strong></p>
<ul>
  <li>e.g. open graph, twitter card, etc</li>
</ul>
<p>Use Bootstrap framework; responsive design in desktop, tablet and mobile phone</p>
<p>Test all pages, images, and links are working as expected in IE 10+, Safari, Chrome and Firefox on PC, Mac, tablet, and mobile devices</p>

<h3>Content optimized for SEO</h3>
<ul>
<li>Product name in <code>&lt;h1&gt;</code> tag</li>
<li>Product description with keywords written in a paragraph format (avoid using bullet points in the first paragraph)</li>
<li>Use text whenever possible rather than text embedded in images</li>
<li>Keyword research and related keywords to be incorporated into the body content</li>
<li>Video(s) in the page should have transcripts in the page</li>
</ul>
<h3>Supported web browsers</h3>
<p>Web page or web application delivered must work as expected on these web browsers.</p>
<ul>
  <li>Chrome</li>
  <li>Internet Explorer
    <ul>
      <li>Two most popular IE versions are IE 7 and IE 11</li>
      <li>IE 8, 9, 10 have very little usage</li>
    </ul>
  </li>
  <li>Safari (Mac)</li>
  <li>Firefox</li>
</ul>

<h3>Most used desktop screen sizes on Edwards.com</h3>
<ul>
  <li>1920 x 1080</li>
  <li>1366 x 768</li>
  <li>1440 x 900</li>
</ul>
<h3>Most used mobile screen sizes on Edwards.com:</h3>
<ul>
  <li>375 x 667 (mobile)</li>
  <li>360 x 650 (mobile)</li>
</ul>
<h3>Sample HTML code</h3>
<pre>
  <code>
&lt;!DOCTYPE html&gt;
&lt;html lang="en" xml:lang="en"&gt;
&lt;head&gt;
    &lt;title&gt;Page Title | Edwards Lifesciences&lt;/title&gt;
    &lt;meta name="title" content="Page Title | Edwards Lifesciences"/&gt;
    &lt;meta name="description" content="Meta Description should contain a summary about the product and keywords incorporated in 160 characters. This is text is what usually appears in SERP."/&gt;
  
    &lt;!-- No Open Directory Project --&gt;
    &lt;meta name="robots" content="noodp" /&gt;
  
    &lt;link rel="canonical" href="http://www.edwards.com/productpage1" /&gt;
    &lt;link rel="alternate" hreflang="en-us" href="http://www.edwards.com/productpage1" /&gt;
    &lt;link rel="alternate" hreflang="en" href="http://www.edwards.com/es/productpage1" /&gt;
    &lt;link rel="alternate" hreflang="pt" href="http://www.edwards.com/pt/productpage1" /&gt;
    &lt;link rel="alternate" hreflang="pt-BR" href="http://www.edwards.com/br/productpage1" /&gt;
    &lt;link rel="alternate" href="http://www.edwards.com/productpage1" hreflang="x-default"&gt;
 
    &lt;script&gt;
    {
      "@context": "http://www.schema.org",
      "@type": "product",
      "brand": "Edwards Lifesciences",
      "name": "name of the product",
      "image": "http://www.edwards.com/imgages/primary-product-image",
      "description": "This is a short description of the product and around 160 characters",
    }  
    &lt;/script&gt;
&lt;/head&gt;
  
&lt;body&gt;
    &lt;h1&gt;Product Name&lt;/h1&gt;
    &lt;div&gt;This is main product description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lacinia finibus ipsum, nec cursus velit pulvinar nec.&lt;/div&gt;
    &lt;div&gt;This is secondary product description.&lt;/div&gt;
    &lt;div&gt;
        &lt;div class="bullets"&gt;Lorem ipsum dolor sit amet&lt;/div&gt;
        &lt;div class="bullets"&gt;Lorem ipsum dolor sit amet&lt;/div&gt;
    &lt;/div&gt;
    &lt;div&gt;
        &lt;a href="/sapien3"&gt;&lt;img src="/image/sapien-3-transcatheter-heart-valve.jpg" alt="SAPIEN 3 TRANSCATHETER HEART VALVE by Edwards Lifesciences" /&gt;&lt;/a&gt;
    &lt;/div&gt;
    &lt;div&gt;
        &lt;img src="/image/keyword.jpg" alt="keyword in alt tag by Edwards Lifesciences" width="200" height="100"&gt;
        &lt;a href="http://www.edwards.com/page1.html"&gt;Keyword in Anchor Text&lt;/a&gt;&lt;br/&gt;
        &lt;a href="http://www.thirdpartysite.com/section/page1.html" rel="nofollow"&gt;Keyword in Anchor&lt;/a&gt;
    &lt;/div&gt;
    ...
&lt;/body&gt;
 
&lt;/html&gt;
  </code>
</pre>
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
