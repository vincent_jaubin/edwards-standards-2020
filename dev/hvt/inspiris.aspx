﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-inspiris" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>HVT brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/hvt/intro.aspx">HVT brand standards</a></li>
                                <li>Tier 3 brand books</li>
                                <li>INSPIRIS RESILIA aortic valve</li>
                            </ol>
                            <div id="tabslider">
                                <h2>INSPIRIS RESILIA aortic valve<a href="../downloads/print-inspiris.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
 <a href="../downloads/INSPIRIS-RESILIA-aortic valve-Brand-Book-23Jun2017.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="" data-original-title="Download PDF"><i class="icon-download"></i></a>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Brand story</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Brand components</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Communications</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Brand story</h3>
                                        <div class="panel-group" id="accordion-story">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse1-story" class="collapsed" aria-expanded="false">
        Unmet need<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-story" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Unmet need</h3>
                                                        <div class="well intro">
                                                            <p>Patients are expected to live longer than ever before – and they want to maintain their active lifestyles following surgical AVR. As a result, some patients continue to face limitations in current tissue valve therapy. This creates a need for improved technologies that will enable surgeons to provide tissue valve therapy to more patients in the future.</p>
                                                        </div>
                                                        <p>For additional information on current limitations, visit the <a href='/hvt/resilia-tissue.aspx#collapse1-story'>RESILIA tissue brand book</a>.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse2-story" class="collapsed" aria-expanded="false">
        Product introduction<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-story" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Product introduction</h3>
                                                        <div class="well">
                                                            <div class="row marg20">
                                                                <div class="col-sm-4">
                                                                    <p><img class="img-responsive " src="../img/inspiris-aortic-valve.png" alt="INSPIRIS aortic valve"></p>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <p><strong>The INSPIRIS RESILIA aortic valve</strong> – provides an ideal
foundation for your patient’s future through:</p>
<ol>
    <li>RESILIA tissue</li>
    <li>Trusted design and features</li>
    <li>VFit technology <sup>&yen;</sup></li>
</ol>
<div class="small">
    &yen; Refer to device instructions for use for important warnings related to VFit technology. These features have not been observed in clinical studies to establish the safety and effectiveness of the model 11500A for use in valve-in-valve procedures. VFit technology is available on sizes 19-25 mm.
</div><!-- /.small -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row marg20">
                                                            <div class="col-sm-4">
                                                                <p class="text-center"><strong>RESILIA tissue</strong></p>
                                                                <p>Edwards Lifesciences’ integrity preservation technology transforms bovine pericardial tissue into RESILIA tissue, effectively eliminating free aldehydes, while protecting and preserving the tissue.</p>
                                                                <p>RESILIA tissue is the first to deliver the combination of:</p>
                                                                <div class="row">
                                                                    <div class="col-sm-3"><img class="img-responsive center-block" src="../img/improved-anti-calcification.png" alt="INSPIRIS aortic valve"></div>
                                                                    <div class="col-sm-9">
                                                                        <p>Improved anti-calcification properties<sup>*</sup></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3"><img class="img-responsive center-block" src="../img/improved-hemodynamic-option.png" alt="INSPIRIS aortic valve"></div>
                                                                    <div class="col-sm-9">
                                                                        <p>Improved sustained hemodynamic performance<sup>*</sup></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3"><img class="img-responsive center-block" src="../img/dry-conditions.png" alt="INSPIRIS aortic valve"></div>
                                                                    <div class="col-sm-9">
                                                                        <p>Stored dry and ready to use<sup>&para;</sup></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p class="text-center"><strong>Trusted design and features</strong></p>
                                                                <p>The INSPIRIS valve leverages features of the trusted Carpentier-Edwards PERIMOUNT Magna Ease valve and is built on the proven performance of the Carpentier-Edwards PERIMOUNT valve design:</p>
                                                                <div class="row">
                                                                    <div class="col-sm-3"><img class="img-responsive center-block" src="../img/published-clinical-durability.png" alt="INSPIRIS aortic valve"></div>
                                                                    <div class="col-sm-9">
                                                                        <p>Published clinical durability of over 20 years (PERIMOUNT)<sup>2,3,4</sup></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3"><img class="img-responsive center-block" src="../img/mathematically-modeled-option.png" alt="INSPIRIS aortic valve"></div>
                                                                    <div class="col-sm-9">
                                                                        <p>Mathematically modeled, bioengineered design</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3"><img class="img-responsive center-block" src="../img/three-leaflets.png" alt="INSPIRIS aortic valve"></div>
                                                                    <div class="col-sm-9">
                                                                        <p>Three independent leaflets matched for thickness and elasticity</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3"><img class="img-responsive center-block" src="../img/flexible.png" alt="INSPIRIS aortic valve"></div>
                                                                    <div class="col-sm-9">
                                                                        <p>Flexible, radiopaque CoCr alloy wireform</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p class="text-center"><strong>VFit technology<strong>**</strong></strong>
                                                                </p>
                                                                <p>VFit technology incorporates two novel features designed for potential future valve-in-valve procedures:</p>
                                                                <div class="row">
                                                                    <div class="col-sm-3"><img class="img-responsive center-block" src="../img/fluoroscopically-visible.png" alt="INSPIRIS aortic valve"></div>
                                                                    <div class="col-sm-9">
                                                                        <p>Fluoroscopically visible size markers</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3"><img class="img-responsive center-block" src="../img/expansion-zone.png" alt="INSPIRIS aortic valve"></div>
                                                                    <div class="col-sm-9">
                                                                        <p>Expansion zone<sup>**</sup></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <p>For more detailed product messaging, please visit Vault to access up-to-date claims matrices, or contact your INSPIRIS valve marketing or brand team.</p>
                                                        <p>For more detailed RESILIA tissue messaging, visit the <a href="/hvt/resilia-tissue.aspx#collapse1-components">RESILIA tissue brand book</a>.</p>
                                                        <p class="small">
                                                            <sup>*</sup> RESILIA tissue tested against commercially available bovine pericardial tissue from Edwards in a juvenile sheep model.<sup>1</sup> No clinical data are available that evaluate the long-term impact of RESILIA tissue in patients.
                                                            <br>
                                                            <sup>&para;</sup> No rinse required.
                                                            <br>
                                                            <sup>**</sup> Refer to device instructions for use for important warnings related to VFit technology. These features have not been observed in clinical studies to establish the safety and effectiveness of the model 11500A for use in valve-in-valve procedures. VFit technology is available on sizes 19–25 mm.
                                                            <br>1. Flameng W, et al. A randomized assessment of an advanced tissue preservation technology in the juvenile sheep model. J Thorac Cardiovasc Surg. 2015;149:340–5.
                                                            <br>2. Bourguignon T, et al. Very long-term outcomes of the Carpentier-Edwards Perimount valve in aortic position. Ann Thorac Surg. 2015;99:831–7.
                                                            <br>3. Johnston DR, et al. Long-term durability of bioprosthetic aortic valves: implications from 12,569 implants. Ann Thorac Surg. 2015;99:1239–47.
                                                            <br>4. Forcillo J, et al. Carpentier-Edwards pericardial valve in the aortic position: 25-years experience. Ann Thorac Surg. 2013;96:486–93.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse3-story" class="collapsed" aria-expanded="false">
        Portfolio positioning<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-story" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Portfolio positioning</h3>
                                                        <p><strong>Edwards believes that RESILIA tissue will create a new class of bovine pericardial resilient heart valves.</strong></p>
                                                        <p>The INSPIRIS RESILIA aortic valve is the first offering in this new class of resilient valves.</p>
                                                        <p>For additional information on RESILIA tissue portfolio positioning, visit <a href="/hvt/resilia-tissue.aspx#collapse2-story">RESILIA tissue brand book</a>.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse4-story" class="collapsed" aria-expanded="false">
        Target audience<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse4-story" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Target audience</h3>
                                                        <p><strong>The target audience for the RESILIA tissue product portfolio, including the INSPIRIS RESILIA aortic valve, is cardiac and cardiothoracic surgeons.</strong></p>
                                                        <p>Note: the content herein has been designed and tested to be most effective with this audience. As such, it may not be an appropriate guide when creating collateral for other audiences.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse5-story" class="collapsed" aria-expanded="false">
        Brand identity map<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse5-story" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Brand identity map</h3>
                                                        <p><strong>The brand identity expresses the unique set of values and positive associations with the INSPIRIS RESILIA aortic valve that we aspire to create and maintain.</strong></p>
                                                        <p>It is how we want our customers to perceive the brand within the framework of Edwards as a whole: the set of expectations, memories, stories and relationships that inform decisions to choose the INSPIRIS valve. As such, all components of the INSPIRIS valve and Edwards’ corporate values work together to build this brand identity.</p>
                                                        <p><img src="../img/inspiris-brand-map.png" alt="INSPIRIS brand identity map" class="img-responsive"></p>
                                                        <p class="small"><sup>*</sup>Benefit cannot be explicitly stated as such; intended to be conveyed emotionally only.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Brand components</h3>
                                        <div class="panel-group" id="accordion-brand-components">
                                            <h4>Brand messaging</h4>
                                            <div class="panel-group" id="accordion-components1">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components1" href="#collapse1-components1" class="collapsed" aria-expanded="false">
        Brand positioning<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse1-components1" class="panel-collapse collapse" aria-expanded="false">
                                                        <div class="panel-body">
                                                            <h3>Brand positioning</h3>
                                                            <div class="well intro">
                                                                <p>The essence of the positioning for the INSPIRIS RESILIA aortic valve is summarized via the following:</p>
                                                            </div>
                                                            <p><strong>An ideal foundation for your patient’s future</strong></p>
                                                            <p><strong>Positioning statements</strong></p>
                                                            <ul>
                                                                <li><strong>Market</strong>. In the heart valve therapy market</li>
                                                                <li><strong>Target audience</strong>. For cardiothoracic surgeons</li>
                                                                <li><strong>Production description</strong>. The INSPIRIS RESILIA aortic valve is the first RESILIA tissue surgical valve</li>
                                                                <li><strong>Proposition</strong>. It provides “an ideal foundation for your patient’s future.”</li>
                                                                <li><strong>Reasons to believe</strong>. Because it contains RESILIA tissue and VFit technology, while offering a familiar implant procedure and proven valve platform</li>
                                                                <li><strong>Higher-order benefit</strong>. So that you can establish a new therapeutic standard for your patients.</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components1" href="#collapse2-components1" class="collapsed" aria-expanded="false">
        Key messages<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse2-components1" class="panel-collapse collapse" aria-expanded="false">
                                                        <div class="panel-body">
                                                            <h3>Key messages</h3>
                                                            <p><strong>For key product messaging, please visit the <a href="#collapse2-story">Product Introduction</a> page.</strong></p>
                                                            <p>For more detailed product messaging, please visit Vault to access up-to-date claims matrices, or contact your INSPIRIS valve marketing or brand team.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components1" href="#collapse3-components1" class="collapsed" aria-expanded="false">
        Message weighting<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse3-components1" class="panel-collapse collapse" aria-expanded="false">
                                                        <div class="panel-body">
                                                            <h3>Message weighting</h3>
                                                            <p>The INSPIRIS RESILIA aortic valve is part of the resilient valve class enabled by RESILIA tissue. As such, INSPIRIS valve collateral can be created one of two ways.</p>
                                                            <p>Depending on which format is used, the amount of weight devoted to RESILIA tissue (and the resilient tissue valve class) versus the valve itself and valve features should differ.</p>
                                                            <p>That said, in order to reinforce positioning of the INSPIRIS valve as the first product offering within the RESILIA tissue umbrella brand, <strong>it is preferred that INSPIRIS valve collateral be created in accordance with option 1 below.</strong></p>
                                                            <p><strong>1. INSPIRIS valve piece nested within a RESILIA tissue piece</strong></p>
                                                            <p>A smaller percentage of overall weight should be given to information and visuals referencing RESILIA tissue and the resilient tissue valve class.</p>
                                                            <div class="row marg20">
                                                                <div class="col-sm-6">
                                                                    <p>Being nested within supportive RESILIA tissue information, less relative weight is devoted to RESILIA tissue (see circled elements: resilient tissue valve class, tissue features).</p>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <p><strong>Example 1: INSPIRIS valve insert within RESILIA tissue brochure</strong></p>
                                                                    <p><img src="../img/message-weighting-1.png" alt="Message weighting" class="img-responsive edw-border"></p>
                                                                </div>
                                                            </div>
                                                            <p><strong>2. INSPIRIS valve standalone piece</strong></p>
                                                            <p>A greater percentage of overall weight should be given to information and visuals referencing RESILIA tissue and the resilient tissue valve class.</p>
                                                            <div class="row marg20">
                                                                <div class="col-sm-6">
                                                                    <p>Without a foundation of RESILIA tissue information, greater relative weight is devoted to RESILIA tissue (see circled elements: resilient tissue valve class, patient resiliency, patient imagery).</p>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <p><strong>Example 2: standalone INSPIRIS valve print ad</strong></p>
                                                                    <p><img src="../img/message-weighting-2.png" alt="Message weighting" class="img-responsive edw-border"></p>
                                                                </div>
                                                            </div>
                                                            <p><strong>A word about message weighting</strong></p>
                                                            <p>When it comes to message weighting, there are no precise percentages to adhere to. Instead, the two examples depicted should serve as loose guides, keeping in mind that both text and visual components count towards the weighting of an element.</p>
                                                            <p>When considering how much weight to give each component of an INSPIRIS valve piece, it is also important to note that message weighting does not refer to the actual depth of information, but rather to the weight (or prevalence) of information relative to the total amount of information contained in the piece. For this reason, the INSPIRIS valve insert depicted may appear to devote more detail to RESILIA tissue than the standalone print ad. Yet the print ad contains a greater percentage of RESILIA tissue text and visuals <em>relative to the total amount of information</em>.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4>Brand guidelines</h4>
                                            <div class="panel-group" id="accordion-components2">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components2" href="#collapse1-components2">
        Guidelines overview<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse1-components2" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3>Guidelines overview</h3>
                                                            <div class="well intro">
                                                                <p>The visual and verbal guidelines for the INSPIRIS RESILIA aortic valve abide by all stipulated parameters within the Edwards corporate Brand Identity Standards.</p>
                                                            </div>
                                                            <p>These guidelines are created to give structure and consistency to all INSPIRIS valve campaign collateral, while complementing, supporting and co-existing within the framework of the wider corporate standards as well as aligning with the <a href="/hvt/resilia-tissue.aspx#collapse1-components2">RESILIA tissue guidelines</a>.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4>Key visuals</h4>
                                            <div class="panel-group" id="accordion-components3">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components3" href="#collapse1-components3">
        Watermark<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse1-components3" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3>Watermark</h3>
                                                            <p>As part of the RESILIA tissue valve class, INSPIRIS RESILIA aortic valve collateral should abide by all RESILIA tissue guidelines with regards to the use of the watermark.</p>
                                                            <p>For illustration of watermark usage in INSPIRIS valve collateral, see examples depicted.</p>
                                                            <div class="row marg20">
                                                                <div class="col-sm-6">
                                                                    <p><strong>Correct:</strong></p>
                                                                    <p>
                                                                        <img src="../img/inspiris-watermark-c1.png" class="img-responsive" alt="Watermark example">
                                                                    </p>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <p><strong>Incorrect:</strong></p>
                                                                    <div id="watermark-incorrect" class="carousel slide" data-ride="carousel" data-interval="false">
                                                                        <!-- Wrapper for slides -->
                                                                        <div class="carousel-inner" role="listbox">
                                                                            <div class="item active">
                                                                                <img src="../img/inspiris-watermark-i1.png" alt="incorrect watermark">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="../img/inspiris-watermark-i2.png" alt="incorrect watermark">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="../img/inspiris-watermark-i3.png" alt="incorrect watermark">
                                                                            </div>
                                                                        </div>
                                                                        <!-- Controls -->
                                                                        <a class="left carousel-control" href="#watermark-incorrect" role="button" data-slide="prev">
                                                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                                            <span class="sr-only">Previous</span>
                                                                        </a>
                                                                        <a class="right carousel-control" href="#watermark-incorrect" role="button" data-slide="next">
                                                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                            <span class="sr-only">Next</span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <p><strong>Note: to access Adobe Illustrator files of the watermark, visit the INSPIRIS RESILIA aortic valve folder within the WebDam.</strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components3" href="#collapse2-components3">
        Valve image<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse2-components3" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3>Valve image</h3>
                                                            <p><strong>Approved imagery depicting the INSPIRIS RESILIA aortic valve should be displayed on all INSPIRIS valve campaign collateral. This will help the audience easily identify product communications, as well as provide a visual point of reference for valve benefits.</strong></p>
                                                            <p>INSPIRIS valve guidelines</p>
                                                            <p><strong>For cover/primary image:</strong></p>
                                                            <div class="row marg20">
                                                                <div class="col-sm-6">
                                                                    <ul>
                                                                        <li><strong>The cover image (or primary image for single-page collateral) must feature the full, closed-valve campaign hero shot depicted here</strong> (accessible by visiting the INSPIRIS RESILIA aortic valve folder within the WebDam)</li>
                                                                        <li>The valve image should be placed entirely within the secondary box graphic (box cannot cut off any part of the valve image)</li>
                                                                        <li>The valve size should scale relative to patient imagery to emphasize the valve; a wider shot of patient imagery (versus RESILIA tissue collateral) should be used so that the valve appears slightly larger than the patient to avoid competition</li>
                                                                        <li>The patient’s head should never align directly with the valve image</li>
                                                                        <li>The valve image should not be colorized (use true color)</li>
                                                                    </ul>
                                                                    <p><strong>For internal/secondary images:</strong></p>
                                                                    <ul>
                                                                        <li>The valve cutaway image (or other approved valve images) may be used on internal pages (or as secondary images) only</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <p><strong>Correct:</strong></p>
                                                                    <div id="valve-correct" class="carousel slide marg20" data-ride="carousel" data-interval="false">
                                                                        <!-- Wrapper for slides -->
                                                                        <div class="carousel-inner" role="listbox">
                                                                            <div class="item active">
                                                                                <img src="../img/inspiris-valve-c1.png" alt="INSPIRIS valve image" class="img-responsive">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="../img/inspiris-valve-c2.png" alt="INSPIRIS valve image" class="img-responsive">
                                                                            </div>
                                                                        </div>
                                                                        <!-- Controls -->
                                                                        <a class="left carousel-control" href="#valve-correct" role="button" data-slide="prev">
                                                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                                            <span class="sr-only">Previous</span>
                                                                        </a>
                                                                        <a class="right carousel-control" href="#valve-correct" role="button" data-slide="next">
                                                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                            <span class="sr-only">Next</span>
                                                                        </a>
                                                                    </div>
                                                                    <p><strong>Incorrect:</strong></p>
                                                                    <p><img src="../img/inspiris-valve-i1.png" alt="INSPIRIS valve" class="img-responsive"></p>
                                                                </div>
                                                            </div>
                                                            <p><strong>Note: to access native files, visit the INSPIRIS RESILIA aortic valve folder within the WebDam.</strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components3" href="#collapse3-components3">
        Photography<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse3-components3" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3>Photography</h3>
                                                            <p>As part of the RESILIA tissue valve class, INSPIRIS RESILIA aortic valve collateral should abide by all <a href="/hvt/resilia-tissue.aspx#collapse3-components3">RESILIA tissue guidelines</a> with regards to the use of photography.</p>
                                                            <div class="row marg20">
                                                                <div class="col-sm-6"><img src="../img/swimmer.jpg" alt="Swimmer" class="img-responsive"></div>
                                                                <div class="col-sm-6"><img src="../img/mechanic.png" alt="Mechanic" class="img-responsive"></div>
                                                            </div>
                                                            <p><strong>Note: to access native files, visit the INSPIRIS RESILIA aortic valve folder within the WebDam.</strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4>Key verbal</h4>
                                            <div class="panel-group" id="accordion-components4">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components4" href="#collapse1-components4">
        Campaign spirit<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse1-components4" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3>Campaign spirit</h3>
                                                            <p>As part of the RESILIA tissue valve class, INSPIRIS RESILIA aortic valve collateral should align with the campaign spirit of <a href="/hvt/resilia-tissue.aspx#collapse1-components4">RESILIA tissue</a>.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components4" href="#collapse2-components4">
        Language<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse2-components4" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3>Language</h3>
                                                            <p>The following is the approved headline/subhead for INSPIRIS RESILIA aortic valve campaign collateral – <strong>it must be used for all covers and as the primary headline/subhead for single-page pieces:</strong></p>
                                                            <p><strong>INSPIRIS RESILIA Aortic Valve an ideal foundation for your patient’s future</strong></p>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <ol>
                                                                        <li>Headlines/subheads should create brand awareness and communicate core campaign message (INSPIRIS valve is designed to help surgeons meet the needs of today’s resilient patients)
                                                                            <br>
                                                                            <br>Headlines should be attention-grabbing but not inappropriately emphatic (e.g. avoid exclamation points and intensifiers)</li>
                                                                        <li>Subheads may be used to support a headline or complete a thought</li>
                                                                        <li>Copy should reference campaign spirit of resilience and surgeon/patient relationship
                                                                            <br>
                                                                            <br>Body copy should be used sparingly, avoiding dense “paragraphs” in favor of quick bits or bullet points</li>
                                                                        <li>CTAs should be understated versus overtly promotional (e.g. avoid “Order today!”)</li>
                                                                        <li>Tone should convey a passionate, realistically optimistic perspective that is neither dark/somber nor flippant
                                                                            <br>
                                                                            <br>Voice should convey confidence without arrogance</li>
                                                                    </ol>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <p><img src="../img/inspiris-ad.jpg" alt="INSPIRIS brochure" class="img-responsive edw-border"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4>Brand architecture</h4>
                                            <div class="panel-group" id="accordion-components5">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components5" href="#collapse1-components5">
        Brand architecture<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse1-components5" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3>Brand architecture</h3>
                                                            <p>The INSPIRIS RESILIA aortic valve is the first product offering from the RESILIA tissue portfolio.</p>
                                                            <p>Since RESILIA tissue is both its own brand and an umbrella brand encompassing the various RESILIA tissue valves, it is important to clearly communicate brand architecture by visually differentiating between:</p>
                                                            <ol>
                                                                <li>RESILIA tissue collateral</li>
                                                                <li>RESILIA tissue portfolio-level collateral</li>
                                                                <li><strong>RESILIA tissue product-level collateral (INSPIRIS valve)</strong></li>
                                                            </ol>
                                                            <p><strong>INSPIRIS valve collateral guidelines</strong></p>
                                                            <div class="row marg20">
                                                                <div class="col-sm-6">
                                                                    <ul>
                                                                        <li>INSPIRIS valve collateral should place visual emphasis on the product</li>
                                                                        <li>Collateral should use a wider shot of lifestyle imagery to emphasize the product</li>
                                                                        <li>Collateral should use the “full-bleed” photo layout (see depicted example)</li>
                                                                        <li>Secondary graphic should be placed within the full-bleed photo</li>
                                                                        <li>Headline and subhead should be placed within the secondary graphic</li>
                                                                        <li>A silhouetted product photo should be placed within the secondary graphic as well</li>
                                                                        <li>The white space below should be used for any additional subheads, body copy and graphic elements</li>
                                                                        <li>Collateral should include the watermark</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <p><strong>Correct:</strong></p>
                                                                    <p><img src="../img/inspiris-brand-c1.png" alt="INSPIRIS correct" class="img-responsive"></p>
                                                                    <p><strong>Incorrect:</strong></p>
                                                                    <div id="brand-incorrect" class="carousel slide marg20" data-ride="carousel" data-interval="false">
                                                                        <!-- Wrapper for slides -->
                                                                        <div class="carousel-inner" role="listbox">
                                                                            <div class="item active">
                                                                                <img src="../img/inspiris-brand-i1.png" alt="INSPIRIS valve image" class="img-responsive">
                                                                            </div>
                                                                            <div class="item">
                                                                                <img src="../img/inspiris-brand-i2.png" alt="INSPIRIS valve image" class="img-responsive">
                                                                            </div>
                                                                        </div>
                                                                        <!-- Controls -->
                                                                        <a class="left carousel-control" href="#brand-incorrect" role="button" data-slide="prev">
                                                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                                            <span class="sr-only">Previous</span>
                                                                        </a>
                                                                        <a class="right carousel-control" href="#brand-incorrect" role="button" data-slide="next">
                                                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                            <span class="sr-only">Next</span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components5" href="#collapse2-components5">
        Campaign samples<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse2-components5" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3>Campaign samples</h3>
                                                            <p>The INSPIRIS valve campaign includes the following key collateral, accessible via the INSPIRIS RESILIA aortic valve folder within the WebDam:</p>
                                                            <h4>INSPIRIS valve promotional brochure</h4>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <p class="marg10"><img src="../img/inspiris-1314.png" alt="PP--US-1314" class="img-responsive edw-border" /></p>
                                                                    <p class="small">PP--US-1314</p><!-- /.small -->
                                                                </div><!-- /.col-sm-4 -->
                                                            </div><!-- /.row -->
                                                            
                                                            <h4>INSPIRIS valve print ads</h4>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <p class="marg10"><img src="../img/inspiris-1332.png" alt="PP--US-1332" class="img-responsive edw-border" /></p>
                                                                    <p class="small">PP--US-1332</p><!-- /.small -->
                                                                </div><!-- /.col-sm-4 -->
                                                                <div class="col-sm-4">
                                                                    <p class="marg10"><img src="../img/inspiris-1333.png" alt="PP--US-1333" class="img-responsive edw-border" /></p>
                                                                    <p class="small">PP--US-1333</p><!-- /.small -->
                                                                </div><!-- /.col-sm-4 -->
                                                                <div class="col-sm-4">
                                                                    <p class="marg10"><img src="../img/inspiris-1334.png" alt="PP--US-1334" class="img-responsive edw-border" /></p>
                                                                    <p class="small">PP--US-1334</p><!-- /.small -->
                                                                </div><!-- /.col-sm-4 -->
                                                            </div><!-- /.row -->
                                                            <h4>INSPIRIS valve animation</h4>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <p class="marg10"><img src="../img/inspiris-1268.png" alt="PP--US-1268" class="img-responsive edw-border" /></p>
                                                                    <p class="small">PP--US-1268</p><!-- /.small -->
                                                                </div><!-- /.col-sm-4 -->
                                                            </div><!-- /.row -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components5" href="#collapse3-components5">
        Name trademark and treatment<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                    </div>
                                                    <div id="collapse3-components5" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <h3>Name trademark and treatment</h3>
                                                            <p><strong>General rules: INSPIRIS RESILIA aortic valve and features</strong></p>
                                                            <ul>
                                                                <li>“INSPIRIS” is a brand, trademarked by Edwards – a ™ should be inserted after the first instance (note: if a complete trademark statement appears on the piece, the ™ may be omitted)</li>
                                                                <li>“INSPIRIS” must always be coupled with the word “valve” (see list of acceptable uses below)</li>
                                                                <li>Every letter of “INSPIRIS” should be capitalized, but “aortic” and/or “valve” should be lowercased – unless they are used in a title or label written using title case (i.e. all other significant words are capitalized)</li>
                                                                <li>INSPIRIS and other valves with RESILIA tissue are part of the “resilient tissue valve class”</li>
                                                                <li>Acceptable uses of product name include:
                                                                    <ul>
                                                                        <li><strong>INSPIRIS RESILIA aortic valve</strong></li>
                                                                        <li><strong>INSPIRIS aortic valve</strong></li>
                                                                        <li><strong>INSPIRIS valve</strong></li>
                                                                    </ul>
                                                                </li>
                                                                <li>“VFit” is a brand, trademarked by Edwards – a ™ should be inserted after the first instance (note: if a complete trademark statement appears on the piece, the ™ may be omitted)</li>
                                                                <li>“VFit” should always be coupled with the word “technology” (no other words may be used in place of “technology”)</li>
                                                                <li>The first two letters of “VFit” should be capitalized, but “technology” should be lowercased – unless it is used in a headline written using title case (i.e. all other significant words are capitalized)</li>
                                                            </ul>
                                                            <p><strong>General rules: other Edwards’ valves</strong></p>
                                                            <p>When referencing Edwards’ non-RESILIA valves in INSPIRIS valve collateral, the guidelines below apply.</p>
                                                            <ul>
                                                                <li>“PERIMOUNT” and “Magna Ease” are brands, trademarked by Edwards – a ™ should be inserted after the first instance of each (note: if a complete trademark statement appears on the piece, the ™ for each may be omitted)</li>
                                                                <li>Every letter of “PERIMOUNT” should be capitalized, while only the first letters of “Magna Ease” should be capitalized</li>
                                                                <li>Valve names must always be coupled with the word “valve”; “aortic” and/or “valve” should be lowercased – unless they are used in a headline written using title case (i.e. all other significant words are capitalized)</li>
                                                                <li>Complete product names must be used in the first/only mention:</li>
                                                            </ul>
                                                            <p><strong>Carpentier-Edwards PERIMOUNT valve</strong></p>
                                                            <p><strong>Carpentier-Edwards PERIMOUNT Magna Ease valve</strong></p>
                                                            <ul>
                                                                <li>In subsequent mentions, acceptable uses include:
                                                                    <ul>
                                                                        <li>PERIMOUNT aortic valve</li>
                                                                        <li>PERIMOUNT valve</li>
                                                                        <li>PERIMOUNT valve design</li>
                                                                        <li>PERIMOUNT Magna Ease valve</li>
                                                                        <li>Magna Ease aortic valve</li>
                                                                        <li>Magna Ease valve</li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Communications</h3>
                                        <h4>Claims</h4>
                                        <p>For a copy of approved product claims, please visit Vault to access up-to-date claims matrices, or contact your INSPIRIS valve marketing or brand team.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
