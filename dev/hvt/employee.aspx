<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="employee" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>HVT brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/hvt/intro.aspx">HVT brand standards</a></li>
                                <li>Tier 2 brand books</li>
                                <li>Employee communication initiative</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Employee communication initiative<a href="../downloads/print-employee.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
<a href="../downloads/employee-communication-initiative.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Download PDF"><i class="icon-download"></i></a>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>HVT employee communication initiative</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Campaign design assets</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Campaign in action</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>Communication templates</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>HVT employee communication initiative</h3>
                                        <h4>Campaign objectives</h4>
                                        <ul>
                                            <li>Create awareness for HVT's 2017 strategy and business goals</li>
                                            <li>Boost employee engagement</li>
                                            <li>Build employee trust and confidence</li>
                                            <li>Foster a culture of accountability</li>
                                        </ul>
                                        <div class="panel-group" id="accordion-communications">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-communications" href="#collapse1-communications">
        Core messaging<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-communications" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Core messaging</h3>
                                                        <ul>
                                                            <li>Campaign headline
                                                                <ul>
                                                                    <li>Lead. Advance. Transform</li>
                                                                </ul>
                                                            </li>
                                                            <li>Pillar specific headlines:
                                                                <ul>
                                                                    <li>Lead the charge</li>
                                                                    <li>Advance the core</li>
                                                                    <li>Transform the business</li>
                                                                </ul>
                                                            </li>
                                                            <li>Supporting message
                                                                <ul>
                                                                    <li>Our work today will shape the future of surgical innovations that improve patients' lives.</li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-communications" href="#collapse2-communications">
        Campaign platform<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-communications" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Campaign platform</h3>
                                                        <p><img src="../img/lead-advance-transform.png" alt="" class="img-responsive"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Campaign design assets</h3>
                                        <h4>Core design elements: photography</h4>
                                        <p>Leverage a blend of Edwards employee and patient event photos with stock photos that represent team work and collaboration.</p>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <p><img src="../img/core-design-photo-1.png" alt="" class="img-responsive"></p>
                                            </div>
                                            <div class="col-sm-3">
                                                <p><img src="../img/core-design-photo-2.png" alt="" class="img-responsive"></p>
                                            </div>
                                            <div class="col-sm-3">
                                                <p><img src="../img/core-design-photo-3.png" alt="" class="img-responsive"></p>
                                            </div>
                                            <div class="col-sm-3">
                                                <p><img src="../img/core-design-photo-4.png" alt="" class="img-responsive"></p>
                                            </div>
                                        </div>
                                        <p><a href="../downloads/employee-photography.zip" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Photography</a></p>
                                        <h4>Resources</h4>
                                        <p>For access to all images, please visit <a href="https://edwards.webdamdb.com/">WebDam</a>, or contact your global brand team.</p>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Campaign in action</h3>
                                        <h4>Employee starter kit</h4>
                                        <div class="row">
                                            <div class="col-sm-2"><img src="../img/employee-starter-1.png" alt="" class="img-responsive"></div>
                                            <div class="col-sm-3"><img src="../img/employee-starter-2.png" alt="" class="img-responsive"></div>
                                            <div class="col-sm-3"><img src="../img/employee-starter-3.png" alt="" class="img-responsive"></div>
                                        </div>
                                        <h4>Office signage</h4>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <p><img src="../img/office-signage-1.png" alt="" class="img-responsive"></p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p><img src="../img/office-signage-2.png" alt="" class="img-responsive"></p>
                                            </div>
                                        </div>
                                        <h4>Resources</h4>
                                        <p>For access to working files, please visit <a href="https://edwards.webdamdb.com/">WebDam</a>, or contact your global brand team.</p>
                                    </div>
                                    <div class="pane" id="tabs-4">
                                        <h3>Communication templates</h3>
                                        <h4>Powerpoint template</h4>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <p><img src="../img/lead-powerpoint.png" alt="" class="img-responsive"></p>
                                                 <p><a href="../downloads/presentation-guide.pptx" class="btn btn-info" role="button"><span class="icon-file-zip"></span> PPT template</a></p>
                                            </div><!-- /.col-sm-4 -->
                                        </div><!-- /.row -->
                                        <h4>Newsletter or memo template</h4>
                                        <div class="row">
                                             <div class="col-sm-4">
                                                <p><img src="../img/newsletter-memo-2.png" alt="" class="img-responsive edw-border"></p>
                                                 <p><a href="../downloads/employee-word-templates.zip" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Word templates</a></p>
                                            </div><!-- /.col-sm-4 -->
                                            <div class="col-sm-4">
                                                <p><img src="../img/newsletter-memo-1.png" alt="" class="img-responsive edw-border"></p>
                                            </div><!-- /.col-sm-4 -->
                                        </div><!-- /.row -->
                                        <h4>Resources</h4>
                                        <p>For access to templates, please visit <a href="https://edwards.webdamdb.com/">WebDam</a>, or contact your global brand team.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
