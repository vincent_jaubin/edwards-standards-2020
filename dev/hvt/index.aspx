﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body>
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-overview" class="page">
                <div class="jumbotron banner-edw banner-overview banner-home">
                    <div class="container">
                        <div class="redboxes">
                            <div class="redbox-sm"></div>
                            <div class="redbox-lg">
                                <h1>Heart Valve Therapy<br>Brand Standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.banner-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 main-content">
                            <div class="clearfix"></div>
                            <div class="row dot-borders">
                                <div class="col-md-4 col-sm-4">
                                    <div class="row">
                                        <div class="col-md-5"><img src="../img/tier-1.jpg" alt="Logo usage" class="img-responsive">
                                        </div>
                                        <div class="col-md-7">
                                            <h4 class="marg0">Tier 1</h4>
                                            
                                            <p>Edwards Initiatives</p>
                                            <p><a href="/hvt/pro-edu.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> All Tier 1 Books</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="row">
                                        <div class="col-md-5"><img src="../img/tier-2.jpg" alt="Image library" class="img-responsive">
                                        </div>
                                        <div class="col-md-7">
                                            <h4 class="marg0">Tier 2</h4>
                                            
                                            <p>Campaign level</p>
                                            <p><a href="/hvt/transforming-tomorrow.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> All Tier 2 Books</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="row">
                                        <div class="col-md-5"><img src="../img/tier-3.jpg" alt="Digital standards" class="img-responsive">
                                        </div>
                                        <div class="col-md-7">
                                            <h4 class="marg0">Tier 3</h4>
                                            
                                            <p>Product and procedure level</p>
                                            <p><a href="/hvt/inspiris.aspx" class="btn btn-info block-xs" role="button"><span class="icon-circle-right"></span> All Tier 3 Books</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /row -->
                        </div>
                    </div>
                </div>
                <div class="container-fluid edw-gray-bg">
                    <div class="row dot-borders nomargin">
                        <div class="col-md-6 match-height">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="../img/home-deskcomp.jpg" alt="Edwards Overview" class="img-responsive"></div>
                                <div class="col-md-7">
                                    <h3>Overview</h3>
                                    <p>Welcome to the Edwards HVT global brand standards website. This website is a useful resource for all departments and functional units, as well as select outside vendors, to develop high impact communications and collateral, and to present a consistent worldwide presence for Edwards Lifesciences to the cardiac surgical audience.</p>
                                    <p>Follow the link below to learn more about our brand architecture, tiered communications strategy and how to access your resources.</p>
                                    <p><a href="/hvt/intro.aspx" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Learn more</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 match-height">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-1">
                                    <h3>Our HVT Brand Vision</h3>
                                    <p>Our vision in this rapidly evolving surgical heart valve therapy market is to create consistent, differentiating and relevant brand communications to our customers and stakeholders.</p>
                                    <p>By creating and demonstrating strong and memorable brand value for our products and services, Edwards creates lasting brand loyalty and maximizes promotional spend across geographies.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
        <!-- end page -->
        <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
