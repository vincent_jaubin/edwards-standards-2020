﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-resilia-tissue" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>HVT brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/hvt/intro.aspx">HVT brand standards</a></li>
                                <li>Tier 2 brand books</li>
                                <li>RESILIA tissue</li>
                            </ol>
                            <div id="tabslider">
                                <h2>RESILIA tissue<a href="../downloads/print-resilia-tissue.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
 <a href="../downloads/RESILIA-tissue-Brand-Book-23Jun2017.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Download PDF"><i class="icon-download"></i></a>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Brand story</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Communications strategy</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Brand components</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>Lexicon</span></a></li>
                                    <li><a href="#tabs-5" class="match-height"><span>Communications</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Brand story</h3>
                                        <div class="panel-group" id="accordion-story">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse1-story">
        Unmet need<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-story" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Unmet need</h3>
                                                        <div class="well intro">
                                                            <p>Because patients will live longer and want to maintain their active lifestyles following surgical AVR, and for patients for whom limitations remain in current tissue valve therapy, there is a need for improved technologies that enable surgeons to provide tissue valve therapy to more patients in the future.</p>
                                                        </div>
                                                        <div class="row marg20">
                                                            <div class="col-sm-6">
                                                                <p><img class="img-responsive" src="../img/life-expectancy.png" alt="Life expectancy"></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img class="img-responsive" src="../img/stable-projections.png" alt="Stable projections"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse2-story">
        Brand vision<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-story" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Brand vision</h3>
                                                        <p><strong>What is RESILIA tissue?</strong></p>
                                                        <p>RESILIA tissue is bovine pericardial tissue transformed by a novel integrity preservation technology. This technology incorporates two new proprietary features that effectively eliminate free aldehydes while protecting and preserving the tissue.</p>
                                                        <p><strong>Edwards believes that RESILIA tissue will create a new class of resilient heart valves.</strong></p>
                                                        <p>This resilient tissue valve class has three common characteristics:</p>
                                                        <ul>
                                                            <li>Improved anti-calcification properties<sup>*</sup></li>
                                                            <li>Improved sustained hemodynamic performance<sup>*</sup></li>
                                                            <li>Capability for dry storage</li>
                                                        </ul>
                                                        <p><img class="img-responsive" src="../img/tissue-balls.png" alt="RESILIA tissue chart"></p>
                                                        <p>This figure represents the step changes in surgical heart valve technology, ending with Edwards’ vision for RESILIA tissue – a new class of resilient bovine pericardial tissue valves.</p>
                                                        <p>RESILIA tissue has been rigorously evaluated, and builds on more than 30 years of Edwards’ experience in heart valves. This new class of resilient tissue valves will greatly benefit patients and physicians.</p>
                                                        <p class="small"><sup>*</sup>RESILIA tissue tested against tissue from commercially available pericardial valves from Edwards in a juvenile sheep model1. No clinical data are available that evaluate the long-term impact of RESILIA tissue in patients.
                                                            <br> 1. Flameng W, et al. A randomized assessment of an advanced tissue preservation technology in the juvenile sheep model. J Thorac Cardiovasc Surg. 2015;149:340–345.
                                                            <br> For additional scientific explanation of the technology, please refer to the <a href="/hvt/inspiris.aspx#collapse2-components4">RESILIA tissue lexicon</a>.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse3-story">
        Brand objective<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-story" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Brand objective</h3>
                                                        <p><strong>The overall brand objective is to create a RESILIA tissue umbrella brand for all products that use this tissue platform, such that we connect the durability benefits to all of these products and, ultimately, create a new resilient tissue valve class.</strong></p>
                                                        <ul>
                                                            <li>It is important to exit the pericardial tissue valve category in order to leapfrog out of this very crowded space – thus the need for the resilient valve class</li>
                                                            <li>The first of these resilient tissue valves is the INSPIRIS RESILIA aortic valve</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Communications strategy</h3>
                                        <div class="panel-group" id="accordion-strategy">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-strategy" href="#collapse1-strategy">
        Target audience<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-strategy" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Target audience</h3>
                                                        <p><strong>The target audience for the RESILIA tissue product portfolio is cardiac and cardiothoracic surgeons.</strong></p>
                                                        <p>Note: the content herein has been designed and tested to be most effective with this audience. As such, it may not be an appropriate guide when creating collateral for other audiences.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-strategy" href="#collapse2-strategy">
        Existing mindset<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-strategy" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Existing mindset</h3>
                                                        <p><strong>The following perspectives provide context for RESILIA tissue:</strong></p>
                                                        <ul>
                                                            <li><strong>Edwards’ perspective on product performance:</strong> while Edwards PERIMOUNT valves have demonstrated excellent long-term performance, at Edwards Lifesciences, we believe we still need to improve the integrity of the pericardial tissue. This is becoming critically important as more patients are expected to live longer and want to maintain their active lifestyles. Therefore, we must provide even more resilient tissue solutions</li>
                                                            <li><strong>Cardiothoracic (CT) surgeons’ mindset:</strong> CT surgeons are keenly aware of the gap in product offerings for their patients with active lifestyles, especially those who have a life expectancy of more than 30 years. The product gap is particularly concerning moving forward as CT surgeons predict that a higher percentage of their caseload will be younger patients with active lifestyles – and these patients will need a more resilient tissue valve solution</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-strategy" href="#collapse3-strategy">
        Rollout strategy<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-strategy" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Rollout strategy</h3>
                                                        <p><strong>By using RESILIA tissue across the Edwards portfolio, we believe resilient tissue valves will form a new class of pericardial tissue valves.</strong></p>
                                                        <ul>
                                                            <li>The first product to feature RESILIA tissue is the INSPIRIS RESILIA aortic valve</li>
                                                            <li>The second product in the resilient tissue valve class is the KONECT RESILIA aortic valve conduit (AVC)</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Brand components</h3>
                                        <h4>Brand messaging</h4>
                                        <div class="panel-group" id="accordion-components1">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components1" href="#collapse1-components">
        Brand positioning and key messages<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-components" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Brand positioning and key messages</h3>
                                                        <div class="well intro">
                                                            <p>One of the keys to successful marketing is adherence to a common language and brand identity. The following messages are approved for use in all RESILIA tissue promotional materials.</p>
                                                        </div>
                                                        <p><strong>Value proposition</strong></p>
                                                        <p>Through RESILIA tissue, Edwards introduces a new class of resilient pericardial surgical tissue valves that gives surgeons more confidence to extend tissue valve therapy to more of their patients, especially those with an active lifestyle.</p>
                                                        <p> <strong>Key messages</strong></p>
                                                        <p>For cardiac surgeons who strive to provide the best option for their active patients, RESILIA tissue is the first and only tissue that provides the combination of improved anti-calcification properties1, improved sustained hemodynamic performance1 and capability for storage in dry conditions through its novel integrity preservation technology, which has shown superior durability compared to the proven PERIMOUNT pericardial tissue.</p>
                                                        <ul>
                                                            <li><strong>RESILIA tissue:</strong> treated pericardial tissue that is <strong>transformed</strong> by Edwards’ novel integrity preservation technology</li>
                                                            <li><strong>Edwards’ novel integrity preservation technology: technology</strong> that virtually eliminates free aldehydes while preserving and protecting the tissue, via the combination of two proprietary features:
                                                                <ul>
                                                                    <li><strong>Stable-capping – permanently blocks</strong> free aldehydes</li>
                                                                    <li><strong>Glycerolization – preserves the tissue</strong> to prevent further exposure to aldehydes, and enables <strong>dry storage</strong>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li><strong>Resilient tissue valve class:</strong> an entirely <strong>new class of bovine pericardial valves</strong>, with characteristics that improve upon those of current pericardial tissue valves. The defining characteristics of a resilient tissue valve are:
                                                                <ul>
                                                                    <li><strong>Improved anti-calcification properties</strong> over current pericardial tissue valves</li>
                                                                    <li><strong>Improved sustained hemodynamic performance</strong> over current pericardial tissue valves</li>
                                                                    <li>Capability to be <strong>stored in dry conditions</strong></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        <p>For more detailed messaging, please visit VAULT to access up-to-date claims matrices, or contact your INSPIRIS RESILIA valve marketing or brand team.</p>
                                                        <p class="small">
                                                            1. Flameng W, et al. A randomized assessment of an advanced tissue preservation technology in the juvenile sheep model. J Thorac Cardiovasc Surg. 2015;149:340–345.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>Brand guidelines</h4>
                                        <div class="panel-group" id="accordion-components2">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components2" href="#collapse1-components2">
        Guidelines overview<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-components2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Guidelines overview</h3>
                                                        <div class="well intro">
                                                            <p>The RESILIA tissue visual and verbal guidelines abide by all stipulated parameters within the Edwards corporate Brand Identity Standards.</p>
                                                        </div>
                                                        <p>These guidelines are created to give structure and consistency to all RESILIA tissue campaign collateral, while complementing, supporting and coexisting within the framework of the wider corporate standards.</p>
                                                        <p>The guidelines herein are for use with surgical heart valve therapy products only. As such, transcatheter heart valve (THV) products are not required to adhere to these brand guidelines. For additional details, please contact your THV marketing or brand team.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>Key visuals</h4>
                                        <div class="panel-group" id="accordion-components3">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components3" href="#collapse1-components3">
        Watermark<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-components3" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Watermark</h3>
                                                        <p>
                                                            <strong>The RESILIA tissue watermark is a key component of the visual identity of RESILIA tissue. As such, it should be used in all RESILIA tissue campaign communications.</strong>
                                                        </p>
                                                        <p>
                                                            The watermark functions as an identifying image and visual through-line connecting RESILIA tissue collateral with RESILIA tissue product collateral.
                                                        </p>
                                                        <div class="row marg20">
                                                            <div class="col-sm-4">
                                                                <p><img class="img-responsive" src="../img/watermark-black.png" alt=""></p>
                                                                <strong>On black</strong>
                                                                <p>Watermark for use on fully black background. 100% opacity</p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p><img class="img-responsive" src="../img/watermark-dgrey.png" alt=""></p>
                                                                <strong>On gray</strong>
                                                                <p>Watermark for use on gray backgrounds; opacity percentage should be adjusted so 7th layer of band is barely visible</p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p><img class="img-responsive" src="../img/watermark-red.png" alt=""></p>
                                                                <strong>On color/photography</strong>
                                                                <p>Watermark for use on color or photography backgrounds; opacity percentage should be adjusted so 7th layer of band is barely visible</p>
                                                            </div>
                                                        </div>
                                                        <p><strong>Watermark guidelines</strong></p>
                                                        <ul>
                                                            <li>The watermark is comprised of seven outer bands, gradually diminishing in opacity and culminating in the last band being barely visible</li>
                                                            <li>The watermark should be angled at 45 degrees</li>
                                                            <li>The watermark should be placed within the background lifestyle imagery, in the upper-left corner</li>
                                                            <li>The watermark should never be placed on a white background (see depicted examples for use on other solid backgrounds)</li>
                                                            <li>The center of the watermark should sit on the corner of the document (on the trim for print collateral), allowing the bands to radiate outward</li>
                                                            <li>The background lifestyle imagery should complement the watermark (i.e. not be overly distracting)</li>
                                                            <li>The background lifestyle photo can be imperceptibly retouched to make the watermark stand out; however, the watermark itself should not be colorized or changed in terms of opacity</li>
                                                            <li>The size of the watermark should scale to that of the contained area beneath it (to avoid overpowering the primary visual); as a rule of thumb, the watermark should occupy no more than 15% of the area containing the background image or color</li>
                                                        </ul>
                                                        <p><strong>Watermark photography examples:</strong></p>
                                                        <div class="row marg20">
                                                            <div class="col-sm-6">
                                                                <p><strong>Correct:</strong></p>
                                                                <div id="watermark-correct" class="carousel slide" data-ride="carousel" data-interval="false">
                                                                    <!-- Wrapper for slides -->
                                                                    <div class="carousel-inner" role="listbox">
                                                                        <div class="item active">
                                                                            <img src="../img/watermark-c1.png" alt="correct watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/watermark-c2.png" alt="correct watermark">
                                                                        </div>
                                                                    </div>
                                                                    <!-- Controls -->
                                                                    <a class="left carousel-control" href="#watermark-correct" role="button" data-slide="prev">
                                                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                                        <span class="sr-only">Previous</span>
                                                                    </a>
                                                                    <a class="right carousel-control" href="#watermark-correct" role="button" data-slide="next">
                                                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                        <span class="sr-only">Next</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><strong>Incorrect:</strong></p>
                                                                <div id="watermark-incorrect" class="carousel slide" data-ride="carousel" data-interval="false">
                                                                    <!-- Wrapper for slides -->
                                                                    <div class="carousel-inner" role="listbox">
                                                                        <div class="item active">
                                                                            <img src="../img/watermark-i1.png" alt="incorrect watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/watermark-i2.png" alt="incorrect watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/watermark-i3.png" alt="incorrect watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/watermark-i4.png" alt="incorrect watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/watermark-i5.png" alt="incorrect watermark">
                                                                        </div>
                                                                    </div>
                                                                    <!-- Controls -->
                                                                    <a class="left carousel-control" href="#watermark-incorrect" role="button" data-slide="prev">
                                                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                                        <span class="sr-only">Previous</span>
                                                                    </a>
                                                                    <a class="right carousel-control" href="#watermark-incorrect" role="button" data-slide="next">
                                                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                        <span class="sr-only">Next</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <p>Watermark on-product guidelines</p>
                                                        <p>Watermark on-product imagery may not be subject to all guidelines noted. <strong>Please contact your global marketing partner for details regarding watermark usage on packaging.</strong></p>
                                                        <p><strong>Note: to access Adobe Illustrator files of the watermark, visit the RESILIA tissue folder within the WebDam.</strong></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components3" href="#collapse2-components3">
        Step diagram<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-components3" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Step diagram</h3>
                                                        <p><strong>The step diagram serves as a visual reminder of the new class of resilient bovine pericardial heart valves enabled by RESILIA tissue. It also functions as an infographic, in that it condenses complex information in an illustrative way.</strong></p>
                                                        <p>When visually appropriate, collateral focusing on the new class of valves should include the step diagram.</p>
                                                        <p><strong>Step diagram guidelines</strong></p>
                                                        <ul>
                                                            <li>Diagram “steps” should lead up, toward the resilient tissue valves icon (never down or horizontally)</li>
                                                            <li>The step diagram should be created using the depicted examples as a guide</li>
                                                            <li>The step diagram should align with the approved Edwards color scheme (see corporate Brand Identity Standards)</li>
                                                        </ul>
                                                        <div class="row marg20">
                                                            <div class="col-sm-6">
                                                                <p><strong>Correct:</strong></p>
                                                                <div id="stepdiagram-correct" class="carousel slide" data-ride="carousel" data-interval="false">
                                                                    <!-- Wrapper for slides -->
                                                                    <div class="carousel-inner" role="listbox">
                                                                        <div class="item active">
                                                                            <img src="../img/stepdiagram-c1.png" alt="correct watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/stepdiagram-c2.png" alt="correct watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/stepdiagram-c3.png" alt="correct watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/stepdiagram-c4.png" alt="correct watermark">
                                                                        </div>
                                                                    </div>
                                                                    <!-- Controls -->
                                                                    <a class="left carousel-control" href="#stepdiagram-correct" role="button" data-slide="prev">
                                                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                                        <span class="sr-only">Previous</span>
                                                                    </a>
                                                                    <a class="right carousel-control" href="#stepdiagram-correct" role="button" data-slide="next">
                                                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                        <span class="sr-only">Next</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><strong>Incorrect:</strong></p>
                                                                <div id="stepdiagram-incorrect" class="carousel slide" data-ride="carousel" data-interval="false">
                                                                    <!-- Wrapper for slides -->
                                                                    <div class="carousel-inner" role="listbox">
                                                                        <div class="item active">
                                                                            <img src="../img/stepdiagram-i1.png" alt="incorrect watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/stepdiagram-i2.png" alt="incorrect watermark">
                                                                        </div>
                                                                    </div>
                                                                    <!-- Controls -->
                                                                    <a class="left carousel-control" href="#stepdiagram-incorrect" role="button" data-slide="prev">
                                                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                                        <span class="sr-only">Previous</span>
                                                                    </a>
                                                                    <a class="right carousel-control" href="#stepdiagram-incorrect" role="button" data-slide="next">
                                                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                        <span class="sr-only">Next</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components3" href="#collapse3-components3">
        Photography<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-components3" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Photography</h3>
                                                        <p><strong>To connect with the target audience and reference valve resilience, lifestyle images evoking patient resilience were chosen as core campaign photography.</strong></p>
                                                        <p>RESILIA tissue collateral is encouraged to feature approved swimmer or mechanic photography, selected for its representation of the resilient patient (based on market research and testing). If alternative photography is selected instead, it must meet the approved guidelines.</p>
                                                        <div class="row marg20">
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/swimmer.jpg" alt="swimmer" class="img-responsive"></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/mechanic.png" alt="mechanic" class="img-responsive"></p>
                                                            </div>
                                                        </div>
                                                        <p><strong>Photography guidelines</strong></p>
                                                        <ul>
                                                            <li>Lifestyle subjects should represent the spectrum of “active lifestyle” patients, ranging from 40 to 70 years of age</li>
                                                            <li>Subjects should be shown engaging in an activity or vocation contingent on the health of the patient:
                                                                <ul>
                                                                    <li>Physical activities with a greater chance of injury, connoting the absence of anticoagulants</li>
                                                                    <li>Strenuous physical activities requiring endurance, connoting strong hemodynamics</li>
                                                                    <li>Physically demanding vocations, conveying the need for solutions that allow longer-living patients to continue these vocations (“active lifestyle” is about more than activity, but the ability to continue demanding vocations)
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>Photography should never portray stereotypically “elderly” patients in sedentary or overly sentimental scenes (see depicted example)</li>
                                                            <li>Subjects should showcase expressions and posture that capture the campaign spirit of resilience</li>
                                                            <li>Photography should align with the Edwards corporate Brand Identity Standards, conveying a consistent feel across various communications through the use of light, bright backgrounds</li>
                                                        </ul>
                                                        <div class="row marg20">
                                                            <div class="col-sm-6">
                                                                <p><strong>Correct:</strong></p>
                                                                <div id="phography-incorrect" class="carousel slide" data-ride="carousel" data-interval="false">
                                                                    <!-- Wrapper for slides -->
                                                                    <div class="carousel-inner" role="listbox">
                                                                        <div class="item active">
                                                                            <img src="../img/photography-c1.png" alt="incorrect watermark">
                                                                        </div>
                                                                        <div class="item">
                                                                            <img src="../img/photography-c2.png" alt="incorrect watermark">
                                                                        </div>
                                                                    </div>
                                                                    <!-- Controls -->
                                                                    <a class="left carousel-control" href="#phography-incorrect" role="button" data-slide="prev">
                                                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                                        <span class="sr-only">Previous</span>
                                                                    </a>
                                                                    <a class="right carousel-control" href="#phography-incorrect" role="button" data-slide="next">
                                                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                                        <span class="sr-only">Next</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><strong>Incorrect:</strong></p>
                                                                <p><img src="../img/birthday.jpg" alt="Man eating birthday cake" class="img-responsive"></p>
                                                            </div>
                                                        </div>
                                                        <p><strong>Note: to access native files, visit the RESILIA tissue folder within the WebDam.</strong></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>Key verbal</h4>
                                        <div class="panel-group" id="accordion-components4">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components4" href="#collapse1-components4">
        Campaign spirit<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-components4" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Campaign spirit</h3>
                                                        <div class="well intro">
                                                            <p>The spirit of the RESILIA tissue campaign is embodied by the word resilience. Today’s heart valve replacement patients are resilient – both in terms of their increased life expectancy and their motivation to maintain an active lifestyle post surgery.</p>
                                                        </div>
                                                        <p>With RESILIA tissue, Edwards is enabling a new class of resilient heart valves; consequently, campaign materials must encapsulate this same resilient spirit.</p>
                                                        <p>A secondary theme is the relationship between surgeon and patient. The campaign conveys an emotional investment on the part of the surgeon in the health and well-being of patients. This is communicated through the consistent use of language such as “you” and “your patients.”</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components4" href="#collapse2-components4">
        Language<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-components4" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Language</h3>
                                                        <p>The following is the approved headline/subhead for RESILIA tissue campaign collateral:</p>
                                                        <p>Your patients are resilient. Their valves should be too.</p>
                                                        <div class="row marg20">
                                                            <div class="col-sm-6">
                                                                <ol>
                                                                    <li>Headlines/subheads should together communicate core campaign message – Edwards’ passion for supporting today’s active, resilient patients
                                                                        <br>
                                                                        <br>Headlines should be attention-grabbing but not inappropriately emphatic (e.g. avoid exclamation points and intensifiers)
                                                                    </li>
                                                                    <li>Subheads may be used to support a headline or complete a thought</li>
                                                                    <li>Copy should reference campaign spirit of resilience and surgeon/patient relationship
                                                                        <br>
                                                                        <br>Body copy should be used sparingly, avoiding dense “paragraphs” in favor of quick bits or bullet points
                                                                    </li>
                                                                    <li>CTAs should be understated versus overtly promotional (e.g. avoid “Order today!”)</li>
                                                                    <li>Tone should convey a passionate, realistically optimistic perspective that is neither dark/somber nor flippant
                                                                        <br>
                                                                        <br>Voice should convey confidence without arrogance
                                                                        <br>
                                                                        <br>Copy should align with the <a href="#tabs-4">RESILIA tissue lexicon</a> and stick as closely as possible to approved language in claims matrices<sup>*</sup></li>
                                                                </ol>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img class="img-responsive" src="../img/resilia-tissue-ad.jpg" alt="RESILIA brochure"></p>
                                                            </div>
                                                        </div>
                                                        <p class="small">
                                                            <sup>*</sup>All statements are required to be approved through your regional advertising approval SOP such as the Vault system. Some statements may require accompanying disclaimers and references.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>Brand architecture</h4>
                                        <div class="panel-group" id="accordion-components5">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components5" href="#collapse1-components5">
        Brand architecture<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-components5" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Brand architecture</h3>
                                                        <div class="well intro"><p>RESILIA tissue is both its own brand and an umbrella brand encompassing the various RESILIA tissue valves, the first of which is the INSPIRIS RESILIA aortic valve.</p></div>
                                                        <p><strong>To clearly communicate brand architecture, it is important to visually differentiate between:</strong></p>
<ol>
    <li>RESILIA tissue collateral</li>
    <li>RESILIA tissue portfolio-level collateral</li>
    <li>RESILIA tissue product-level collateral</li>
</ol>
<p><strong>RESILIA tissue collateral guidelines</strong></p>
<div class="row">
    <div class="col-sm-8">
        <ul>
            <li>RESILIA tissue collateral should place visual emphasis on the patient</li>
            <li>Lifestyle imagery should be placed within the large secondary graphic (see depicted example)</li>
            <li>The white space below the secondary graphic should be used for any additional subheads, body copy and graphic elements (i.e. step diagram)</li>
            <li>RESILIA tissue collateral should not display product visuals (product visuals are displayed in portfolio-level collateral)</li>
            <li>RESILIA tissue collateral should include the watermark</li>
        </ul>
    </div>
    <!-- /.col-sm-8 -->
    <div class="col-sm-4">
        <img src="../img/resilia-ba-1.png" alt="RESILIA tissue collateral guidelines" class="img-responsive" />
    </div>
    <!-- /.col-sm-4 -->
</div>
<!-- /.row -->
<p><strong>RESILIA tissue portfolio-level collateral guidelines</strong></p>
<div class="row">
    <div class="col-sm-8">
        <ul>
            <li>RESILIA tissue portfolio collateral should place visual emphasis on multiple products</li>
            <li>Portfolio collateral should use a wider shot of lifestyle imagery to emphasize the product</li>
            <li>Portfolio collateral should use the almost “full-bleed” photo layout (see depicted example), with bleed on top and right side only</li>
            <li>The white space below the secondary graphic should be used for any additional subheads, body copy and graphic elements (i.e. silhouetted product photos or step diagram)</li>
            <li>Portfolio collateral should include the watermark</li>
        </ul>
    </div>
    <!-- /.col-sm-8 -->
    <div class="col-sm-4">
        <img src="../img/resilia-ba-2.png" alt="RESILIA tissue portfolio-level collateral guidelines" class="img-responsive" />
    </div>
    <!-- /.col-sm-4 -->
</div>
<!-- /.row -->
<p><strong>RESILIA tissue product-level collateral guidelines</strong></p>
<div class="row">
    <div class="col-sm-8">
        <ul>
            <li>RESILIA tissue product collateral should place visual emphasis on the product</li>
            <li>Product collateral should use a wider shot of lifestyle imagery to emphasize the product</li>
            <li>Product collateral should use the “full-bleed” photo layout (see depicted example)</li>
            <li>Secondary graphic should be placed within the full-bleed photo</li>
            <li>Headline and subhead should be placed within the secondary graphic</li>
            <li>A silhouetted product photo should be placed within the secondary graphic as well</li>
            <li>The white space below should be used for any additional subheads, body copy and graphic elements (e.g. step diagram)</li>
            <li>Product collateral should include the watermark</li>
        </ul>
    </div>
    <!-- /.col-sm-8 -->
    <div class="col-sm-4">
        <img src="../img/resilia-ba-3.png" alt="RESILIA tissue product-level collateral guidelines" class="img-responsive" />
    </div>
    <!-- /.col-sm-4 -->
</div>
<!-- /.row -->

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components5" href="#collapse2-components5">
        Campaign samples<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-components5" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Campaign samples</h3>
                                                        <div class="well intro">
                                                            <p>The RESILIA tissue campaign includes the following key collateral, accessible via the RESILIA tissue folder within the WebDam:</p>
                                                        </div>
                                                        <h4>RESILIA tissue brochure (sales presentation piece)</h4>
                                                                <p class="marg10">
                                                                    <img src="../img/resilia-1323.png" alt="PP--US-1323" class="img-responsive" />
                                                                </p>
                                                                <p class="small">PP--US-1323</p><!-- /.small -->
                                                                <h4>RESILIA tissue animation</h4>
                                                                <p class="marg10">
                                                                    <img src="../img/resilia-1258.png" alt="PP--US-1258" class="img-responsive" />
                                                                </p>
                                                                <p class="small">PP--US-1258</p><!-- /.small -->
                                                       
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-components5" href="#collapse3-components5">
        Name trademark and treatment<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-components5" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Name trademark and treatment</h3>
                                                        <p><strong>General rules</strong></p>
                                                        <ul>
                                                            <li>“RESILIA” is a brand, trademarked by Edwards – a ™ should be inserted after the first instance (note: if a complete trademark statement appears on the piece, the ™ may be omitted)</li>
                                                            <li>“RESILIA” must always be coupled with the word “tissue,” except when used in a product name</li>
                                                            <li>When referring to a specific valve, “RESILIA” should only be included if the entire name is used (e.g. INSPIRIS RESILIA aortic valve)</li>
                                                            <li>“RESILIA” will replace “pericardial” in product names and tissue descriptions (e.g. “RESILIA tissue” instead of “pericardial tissue”)
                                                            </li>
                                                            <li>Every letter of “RESILIA” should be capitalized, but “tissue” should be lowercased – unless it is used in a title or label written using title case (i.e. all other significant words are capitalized)</li>
                                                            <li>The term “transformed” should be used when describing the creation of RESILIA tissue</li>
                                                            <li>RESILIA tissue should not be referred to as “treated tissue” – this description is reserved for non-RESILIA tissues</li>
                                                            <li>“RESILIA tissue” is the trademarked Edwards brand of tissue – valves with RESILIA tissue are part of the “resilient tissue valve class”</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-4">
                                        <h3>Lexicon</h3>
                                        <div class="panel-group" id="accordion-lexicon">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-lexicon" href="#collapse1-lexicon" class="collapsed" aria-expanded="false">
          New tissue heart valve class<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-lexicon" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>New tissue heart valve class</h3>
                                                        <p><strong>Resilient valves will allow clinicians to choose the approach or clinical scenario needed for each patient. RESILIA tissue will create a new tissue heart valve class that is driven by Edwards’ integrity preservation technology.</strong></p>
                                                        <p><img src="../img/arrowed-balls.png" alt="RESILIA chart" class="img-responsive"></p>
                                                        <ul>
                                                            <li>The creation of traditional bovine pericardial tissue valves involves a <strong>fixation process (1)</strong> and the application of an <strong>anti-calcification tissue treatment (2)</strong></li>
                                                            <li><strong>Resilient tissue valves (4)</strong> also undergo these early processes (1, 2), but are transformed by the addition of a novel and proprietary <strong>integrity preservation technology (3)</strong></li>
                                                        </ul>
                                                        <p class="small"><sup>*</sup>CAUTION: Products are under development and not available for sale in the United States. Do not discuss these products with any external audiences</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion-lexicon" href="#collapse2-lexicon" class="collapsed" aria-expanded="false">
            Brand nomenclature<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
          </h4>
                                                </div>
                                                <div id="collapse2-lexicon" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Brand nomenclature</h3>
                                                        <p><strong>Edwards’ integrity preservation technology transforms bovine pericardium into RESILIA tissue. It is more than an anti-calcification technology; a visual manifestation of this is the tissue’s capability to be stored in dry conditions.</strong></p>
                                                        <p>This novel technology incorporates two new proprietary features, stable-capping and glycerolization, that virtually eliminate free aldehydes while preserving and protecting the tissue.</p>
                                                        <p>The RESILIA tissue Brand Lexicon provides a complete vocabulary for terminology related to RESILIA tissue. This includes key nomenclature associated with the progression from a traditional bovine pericardial tissue valve to a resilient tissue valve.</p>
                                                        <p>To obtain a complete copy of the RESILIA tissue Brand Lexicon, please contact your marketing or brand team.</p>
                                                        <p><img src="../img/labeled-balls.png" alt="RESILIA chart" class="img-responsive"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-5">
                                        <h3>Communications</h3>
                                        <div class="panel-group" id="accordion-presentation-templates">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-presentation-templates" href="#collapse1-presentation-templates" class="collapsed" aria-expanded="false">
          RESILIA tissue presentation templates<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-presentation-templates" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>RESILIA tissue presentation templates</h3>
                                                        <div class="well intro">
                                                            <p>This presentation template is approved for use with all RESILIA tissue campaign internal and external communications.</p>
                                                        </div>
                                                        <p>The RESILIA tissue presentation template can be accessed here:</p>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <p><img class="img-responsive" src="../img/presentation-template.jpg" alt="Presentation template"></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><a href="../downloads/resilia-16x9.pptx" class="btn btn-info" role="button"><i class="icon-circle-right"></i> Edwards RESILIA tissue PPT template 16×9</a></p>
                                                                <p><a href="../downloads/resilia-4x3.pptx" class="btn btn-info" role="button"><i class="icon-circle-right"></i> Edwards RESILIA tissue PPT template 4×3</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>COMMENCE trial templates &amp; information</h4>
                                        <div class="panel-group" id="accordion-communications">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-communications" href="#collapse1-communications" class="collapsed" aria-expanded="false">
        Presentation templates<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-communications" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Presentation templates</h3>
                                                        <p><strong>Available for download are the COMMENCE templates for the Aortic, Mitral and Pulmonary arms of the clinical trial. These templates should be used for building slide presentations for this trial.</strong></p>
                                                        <p>Please note the following:</p>
                                                        <ul>
                                                            <li>All templates include an organizational chart outlining the three arms. This slide should remain in all presentations so the visual identity system outlining the arms of the trial is clear to the surgical audience – Aortic, Mitral and Pulmonary</li>
                                                            <li>The templates also include sample text, chart and graph template slides to assist in developing content</li>
                                                        </ul>
                                                        <div class="row marg20">
                                                            <div class="col-sm-6">
                                                                <p><img class="img-responsive" src="../img/commence-template.png" alt="COMMENCE template"></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><a href="../downloads/commence-aortic.pptx" class="btn btn-info" role="button"><i class="icon-circle-right"></i> COMMENCE Aortic presentation template</a></p>
                                                                <p><a href="../downloads/commence-mitral.pptx" class="btn btn-info" role="button"><i class="icon-circle-right"></i> COMMENCE Mitral presentation template</a></p>
                                                                <p><a href="../downloads/commence-pulmonary.pptx" class="btn btn-info" role="button"><i class="icon-circle-right"></i> COMMENCE Pulmonary presentation template</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-communications" href="#collapse2-communications" class="collapsed" aria-expanded="false">
        Study framework<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-communications" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Study framework</h3>
                                                        <p><strong>What is the design of the COMMENCE Trial?</strong></p>
                                                        <p>It is a single arm IDE trial to study the GLX next-generation tissue treatment platform applied to surgical bovine pericardial heart valves. The GLX technology will initially be studied on the Magna Ease aortic valve, and the study may be broadened to include other devices in the future.</p>
                                                        <p>The clinical trial will enroll between 500 and 700 patients with primary endpoints of safety and effectiveness using historical controls and standard heart valve guidance. The anticipated start of the trial enrollment is fourth quarter of 2012. More information is available on the trial at <a href="www.clinicaltrials.gov">www.clinicaltrials.gov</a>.</p>
                                                        <p><strong>What does COMMENCE stand for?</strong></p>
                                                        <p>COMMENCE is a trademarked acronym primarily used to communicate the essence, the objective and promise of the trial and/or the technology it involves. This acronym – and in general any study acronym – is not a commercial brand and, as such, it is not supposed to be used in commercial context. Study acronyms are scientific brands supporting clinical experience and data.</p>
                                                        <p>The acronym COMMENCE stands for Prospe<strong>C</strong>tive, n<strong>O</strong>n‐rando<strong>M</strong>ized, <strong>M</strong>ultic<strong>EN</strong>ter <strong>C</strong>linical evaluation of <strong>E</strong>dwards Pericardial Bioprostheses with a new tissue treatment platform.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-communications" href="#collapse3-communications" class="collapsed" aria-expanded="false">
        Trademark symbols<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-communications" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Trademark symbols</h3>
                                                        <p><strong>In order to visually depict a brand identity system for the clinical activities, each activity will have the same font style, size and visual icon identifier. These icons will have a consistent visual “curve” that represents the characteristics of the brand.</strong></p>
                                                        <p>Trademark symbols will not be used. Exceptions to this are on materials where the trademark statement cannot be reasonably included.</p>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <ol>
                                                                    <li>US IDE Trial</li>
                                                                </ol>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/glx-trade.png" alt="trademark symbols" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-communications" href="#collapse4-communications" class="collapsed" aria-expanded="false">
        Trade name acronyms<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse4-communications" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Trade name acronyms</h3>
                                                        <p><strong>COMMENCE is a trademarked acronym and primarily used to communicate the essence, the objective and promise of the trial and/or the technology it involves.</strong></p>
                                                        <p>This acronym – and in general any study acronym – is not a commercial brand and, as such, it is not supposed to be used in a commercial context. Study acronyms are scientific brands supporting clinical experience and data.</p>
                                                        <p>The acronym COMMENCE stands for Prospe<strong>C</strong>tive, n<strong>O</strong>n‐rando<strong>M</strong>ized, <strong>M</strong>ultic<strong>EN</strong>ter <strong>C</strong>linical evaluation of Edwards Pericardial Bioprostheses with a new tissue treatment platform.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>Claims</h4>
                                        <p>For a copy of approved brand claims, please visit Vault to access up-to-date claims matrices, or contact your RESILIA tissue marketing or brand team.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
