﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-mc3" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>HVT brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/hvt/intro.aspx">HVT brand standards</a></li>
                                <li>Tier 3 brand books</li>
                                <li>MC3 ring</li>
                            </ol>
                            <div id="tabslider">
                                <h2>MC3 ring<a href="../downloads/print-mc3.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Brand story</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Brand components</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Communications</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Brand story</h3>
                                        <h4>Brand strategy</h4>
                                        <p>The brand strategy of MC3 tricuspid annuloplasty rings is to drive awareness of the first 3D tricuspid-specific ring.</p>
                                        <h4>Brand identity map</h4>
                                        <p><img src="../img/mc3-identity.png" alt="Brand identity map" class="img-responsive"></p>
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Brand components</h3>
                                        <div class="panel-group" id="accordion-brand-components">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-brand-components" href="#collapse1-brand-components">
                                                Brand messaging<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                              </h4>
                                                </div>
                                                <div id="collapse1-brand-components" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Product positioning</h3>
                                                        <p>MC3 is the clinically proven choice for treating tricuspid insufficiency by consistently delivering predictable results that lead to a lasting repair.</p>
                                                        <p>From the innovative leader in valvular reconstruction comes the first 3D tricuspid-specific ring.</p>
                                                        <p>With its unique design features, clinically proven efficacy, and more than 50,000 patient-years of experience worldwide, Edwards MC3 tricuspid annuloplasty ring delivers a lasting repair with predictable results.</p>
                                                        <p><strong>Message weighting</strong></p>
                                                        <div class="row">
                                                            
                                                            <div class="col-sm-6">
                                                                <p><strong>Predictable and lasting (60%)</strong> <span class="side-box side-box-red"></span></p>
                                                                <p>Clinical experience is a driving benefit and a key competitive differentiator, as no other tricuspid specific ring has more published evidence.</p>
                                                                <p><strong>Clinically proven design (40%)</strong> <span class="side-box side-box-gray"></span></p>
                                                                <p>While it is the first tricuspid specific 3D shaped ring, it is not enough on its own.</p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/mc3-weighting.png" alt="Message weighting" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                        <h3>Body copy and building copy tone</h3>
                                                        <p><strong>Note: </strong> All statements are approved in vault and some statements may require an accompanying disclaimer.</p>
                                                        <p>For more detailed product messaging, please visit Vault to access up-to-date claims matrices, or contact your local marketing or brand team.</p>
                                                        <p><strong>Approved statements</strong></p>
                                                        <ul>
                                                            <li>The world's first 3D tricuspid specific ring</li>
                                                            <li>Rigid ring for remodeling the tricuspid annulus</li>
                                                            <li>Constructed of titanium alloy</li>
                                                            <li>3D shape mimics the tricuspid annulus, corresponds to the anatomical contour</li>
                                                            <li>Corrects annular dilation, increases leaflet coaptation, reinforces annular suture lines, and prevents further dilation of annulus</li>
                                                        </ul>
                                                        <h3>Campaign story</h3>
                                                        <p>The brand campaign for the MC3 annuloplasty ring helps customers and prospects understand the product’s value. It articulates the brand promise and stimulates a desire to trial or adopt the product.</p>
                                                       
                                                        <p><strong>Headline</strong></p>
                                                        <p>The worlds first 3D tricuspid specific ring</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-brand-components" href="#collapse2-brand-components">
                                                Digital images and illustrations<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                              </h4>
                                                </div>
                                                <div id="collapse2-brand-components" class="panel-collapse collapse">
                                                    <div class="panel-body">  
                                                    <h3>Digital images - primary</h3>
                                                    
                                                    <div class="row">
                                                      <div class="col-sm-6"> 
                                                      <p>The MC3 ring core visuals adhere to the same color and typographic standards of the Edwards corporate brand globally.</p><p>If this product is used along with other Heart Valve Therapy products in the same communications, please use the primary photo of the product on a background consisting of the corporate color palette, unless it is part of a localized campaign.</p>
                                                      <p><a href="https://edwards.webdamdb.com/" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Access Webdam</a></p></div>
                                                      <div class="col-sm-6"><p><img src="../img/mc3-digital.png" alt="Digital images - primary" class="center-block img-responsive"></p></div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-brand-components" href="#collapse3-brand-components">
                                                Trademarks<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                              </h4>
                                                </div>
                                                <div id="collapse3-brand-components" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                    <h3>Trademarks</h3>
                                                        
                                                        
                                                        <p><strong>Trademark / legal statement</strong></p>
                                                        <p>Edwards, Edwards Lifesciences, the stylized E logo, and MC3 are trademarks of Edwards Lifesciences Corporation. All other trademarks are the property of their respective owners.</p>
                                                        <p>&copy; 2015 Edwards Lifesciences Corporation. All rights reserved. PP-US-XXXXX</p>

<p><strong>Regulatory statements (as determined by Regional Regulatory Affairs) must be included as needed.</strong></p>
                                                        <p>For further trademark information, please refer to <a href="http://standards.edwards.com/pdf/Trademark_021616.pdf" target="_blank">Trademarks Style Guide</a>. Note: Exceptions or questions should be addressed to <a href="mailto:debbie_sklar@edwards.com">Debbie Sklar</a> or Edwards Intellectual Property Counsel.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Communications</h3>
                                 <div class="well intro">
                                          <p>Commercialization materials such as product images, animations, procedural images and collateral pdf files for regional and cross-functional team use can be accessed via <a href="https://edwards.webdamdb.com/"><strong><u>Webdam</u></strong></a>. As new executions and tools are created, they will be uploaded to this site. You may also contact your local marketing or brand team.</p>
                                          <p><a href="https://edwards.webdamdb.com/" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Access Webdam</a></p>
                                        </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
