﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="dig-apps" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Digital media</li>
  <li>Apps</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Apps<a href="pdf/p-apps.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Icons & splash screens</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Mobile title screens</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Tablet title screens</span></a></li>
  <li><a href="#tabs-4" class="match-height"><span>Content screens</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Icons & splash screens</h3>   
 
 <div class="well intro">
 <p>An app icon should be simple, by
displaying the preferred Edwards logo.
And even though splash screens are
only visible for a brief moment, it is a
good opportunity to introduce and
reinforce our identity through color.</p>
 </div>

<div class="row">
<div class="col-md-4">
<p><strong>App icon</strong></p>
<p>The app icon should feature logo version 1 –
titanium/gray and black – on a white
background. The size of the logo within the
icon is determined by the clear area: the
space above and below the logo is equal to
the height of the <em>Edwards</em> name (X).</p>
</div>
<div class="col-md-8"><img src="img/apps-icon-splash-1.jpg" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-4">
<p><strong>Splash screen background color</strong></p>
<p>The background may be 100% red, 50% Gray
423 or 100% of any of our secondary colors.</p>
</div>
<div class="col-md-8"><img src="img/apps-icon-splash-2.jpg" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-4">

<p><strong>Drop shadow</strong></p>
<p>To help the E symbol stand apart from the
background color, a subtle drop shadow
may be added behind the E symbol on splash
screens. Do not add a drop shadow to the
Edwards name below the E symbol.</p>
</div>
<div class="col-md-8"><img src="img/apps-icon-splash-3.jpg" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-12">

<p><strong>Logo color and size</strong></p>
<p>The E symbol should always be gray and the
Edwards name should always be white.</p>
<p>For 375 x 667px screens, the logo is 175px
based on the height of the E symbol.</p>
<p>For 1024 x 768px screens, the logo is 200px
based on the height of the E symbol.</p>
<p><strong>Different screen sizes</strong></p>
<p>Base other size screens on the samples
shown here, maintaining the same general
proportions. The logo should be prominent
but not overwhelming, and be visually
centered horizontally and vertically.</p>
</div>
</div>



    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Mobile title screens</h3>   

<div class="row">
<div class="col-md-3">
<p><strong>Small secondary graphic</strong></p>
<p>The small secondary graphic may be used
against a photo background, the approved
background pattern, a tinted neutral color
(see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>), or white.</p>
</div>
<div class="col-md-9"><img src="img/apps-mobile-1.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-3">
<p><strong>Large secondary graphic</strong></p>
<p>A photo must entirely fill the larger shape in
the secondary graphic. The large secondary
graphic may be used against the approved
background pattern, a tinted neutral color
(see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>), or white.</p>


</div>

<div class="col-md-9"><img src="img/apps-mobile-2.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-12">

<p><strong>Logo size and background alignment</strong></p>
<p>For 375 x 667px screens, the logo is 50px
based on the height of the E symbol. Align the
background color or photo precisely with the
bottom horizontal line of the E symbol.</p>
<p><strong>Fonts</strong></p>
<p>Use Merriweather Sans Regular for apps. Use
Bold or Italic to add emphasis when needed.</p>

<p><strong>UI elements</strong></p>
<p>For input forms, buttons, drop-down menus
and other UI elements, use as few visual
effects as possible to maintain a clean and
simple look. Do not use drop shadows,
outlines, gradients, inner glows or other
treatments that create clutter.</p>
<p>For example, input fields should be simple
rectangles with gray or black typography;
buttons may be rounded red or gray
rectangles to differentiate them from the
secondary graphic; and drop-down menus
may be gray or another tinted neutral color
with subtle lines separating each choice.</p>
<p><strong>Different screen sizes</strong></p>
<p>Base other size screens on the samples
shown here, maintaining the same general
proportions.</p>

</div>

</div>

    
    
   </div>
   
   <div class="pane" id="tabs-3">
    <h3>Tablet title screens</h3>   

<div class="row">
<div class="col-md-4">
<p><strong>Small secondary graphic</strong></p>
<p>The small secondary graphic may be used
against a photo background, the approved
background pattern, a tinted neutral color
(see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>), or white.</p>
</div>

<div class="col-md-8"><img src="img/apps-tablet-1.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-4">
<p><strong>Large secondary graphic</strong></p>
<p>A photo must entirely fill the larger shape in
the secondary graphic. The large secondary
graphic must be used against a white
background.</p>
</div>
<div class="col-md-8"><img src="img/apps-tablet-2.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-12">

<p><strong>Logo size and background alignment</strong></p>
<p>For 1024 x 768px screens, the logo is 80px
based on the height of the E symbol. Align the
background photo precisely with the bottom
horizontal line of the E symbol.</p>
<p><strong>Fonts</strong></p>
<p>Use Merriweather Sans Regular for apps. Use
Bold or Italic to add emphasis when needed.</p>
<p><strong>Different screen sizes</strong></p>
<p>Base other size screens on the samples
shown here, maintaining the same general
proportions.</p>
</div>
</div>

    
    
   </div>
   
   
   
    <div class="pane" id="tabs-4">
    <h3>Content screens</h3>   
    
    <div class="well intro">
    <p>Content screens can vary widely.
Use our graphic elements – including
colors and fonts – consistently
across apps to create a recognizable
Edwards look and feel.</p>
    </div>

<div class="row">
<div class="col-md-4">

<p><strong>Fonts</strong></p>
<p>Use Merriweather Sans Regular for apps. Use
Bold or Italic to add emphasis when needed.</p>
<p><strong>Photos</strong></p>
<p>Photos areas (indicated by gray rectangles
with white crossed lines) may appear side-by-side with copy, or may span the width of
the display screen. A photo area may be used
for product imagery, lifestyle imagery, charts,
diagrams or other visuals.</p>
<p><strong>Colors</strong></p>
<p>Colors from the approved palette may be
used for clickable areas and for infographics.
Only use the secondary reds (at 100% values)
when the secondary graphic is not being used.</p>
</div>
<div class="col-md-8"><img src="img/apps-content-screens.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-12">

<p><strong>UI elements</strong></p>
<p>For input forms, buttons, drop-down menus
and other UI elements, use as few visual
effects as possible to maintain a clean and
simple look. Do not use drop shadows,
outlines, gradients, inner glows or other
treatments that create clutter.</p>
<p>For example, input fields should be simple
rectangles with gray or black typography;
buttons may be rounded red or gray
rectangles to differentiate them from the
secondary graphic; and drop-down menus
may be gray or another tinted neutral color
with subtle lines separating each choice.</p>
<p><strong>Different screen sizes</strong></p>
<p>Base other size screens on the samples shown
here, maintaining the same general proportions.</p>
</div>
</div>

    
    
   </div>
    

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
