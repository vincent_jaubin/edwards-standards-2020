﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->

<div id="edwards-wrapper">

<!-- #include file="header.html" -->

 <div id="ads-single" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Advertising</li>
  <li>Single page print ads</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Single page print ads<a href="pdf/p-single-page-print-ads.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/Metric/edwards-printad-metric.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the metric templates"><i class="icon-download"></i> <span class="share-txt">Metric</span></a><a href="templates/US-standard/edwards-ads.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the U.S. templates"><i class="icon-download"></i> <span class="share-txt">U.S.</span></a>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Small, fixed<br>
 secondary graphic</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Small, flexible<br>
 secondary graphic</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Large, fixed<br>
 secondary graphic</span></a></li>
  <li><a href="#tabs-4" class="match-height"><span>Large, flexible<br>
 secondary graphic</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
    <div class="well intro"><p>There are multiple layout options for
print ads, offering a range of flexibility.</p></div>
 <h3>Small, fixed secondary graphic</h3>   

<div class="row">
<div class="col-md-6">
<p><strong>Page layout</strong></p>
<p>The grid determines the placement for each
visual element on this 8.5"W x 11"H format.</p>
<p>On print ads, the logo is scaled to 0.80", based
on the height of the E symbol. It aligns to the
right and bottom margins.</p>
<p>Legal and address copy also align to the bottom
margin and are typeset Vesta Std Regular, 7pt
on 9pt line spacing. <em>Edwards Lifesciences</em> is Bold
Other typography specifications, including
suggested headline and subhead sizes, are
shown in the samples here.</p>
<p>The secondary graphic is a fixed size. It may
shift up or down to work with the photo and
content. Do not shift the graphic left or right.
However, if the ad is to appear on a rightfacing
page and there is concern that the left
margin would cause the secondary graphic to
fall into the gutter, the secondary graphic and
all text may shift to the right as a unit. Do not
move the logo. For left-facing pages, move the
logo to the left.</p>

<p>In this layout, the secondary graphic equals 10
square units high and wide, scaled as shown.</p>

</div>
<div class="col-md-6"><img src="img/ads-single-small-fixed-1.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-8">

<p><strong>Photo background</strong></p>
<p>Photos should bleed off the top, left and right;
the bottom of the photo should align to the
top horizontal line of the E symbol.</p>
<p>The larger square may use a multiply effect
as described <a href="elements-secondary.aspx" target="_blank">here</a> when placed over
a photo.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>

</div>

<div class="col-md-4"><img src="img/ads-single-small-fixed-2.png" class="img-responsive"></div>
</div>



    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Small, flexible secondary graphic</h3>   

<div class="row">
<div class="col-md-6">
<p><strong>Page layout</strong></p>
<p>In this 8.5"W x 11"H layout, the area for the
background photo may move up to allow
for more legal, regulatory or body copy. If
the background photo is made shorter, the
secondary graphic and its contents also move up
to center align (vertically) with the background.</p>
<p>The secondary graphic is a fixed size and
should not be scaled. Do not move the
graphic left or right. However, if the ad is to
appear on a right-facing page and there is
concern that the left margin would cause the
secondary graphic to fall into the gutter, the
secondary graphic and text may shift to the
right as a unit. Do not move the logo. For leftfacing
pages, nudge the logo to the left.</p>
<p>The secondary graphic is 10 square units high
and wide, scaled as shown. The logo is scaled
to 0.80", based on the height of the E symbol
It aligns to the right and bottom margins.</p>

<p>Legal and address copy also align to the bottom
margin and are typeset Vesta Std Regular, 7pt
on 9pt line spacing Edwards Lifesciences is Bold.</p>
</div>
<div class="col-md-6"><img src="img/ads-single-small-flexible-1.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-8">


<p><strong>Photo background</strong></p>
<p>Photos should bleed off the top, left and
right sides. When shortening the photo
background, the bottom of the photo should
be at least 0.30" above the logo. The larger red
square may use a multiply effect as described
<a href="elements-secondary.aspx" target="_blank">here</a> when placed over a photo.</p>
</div>

<div class="col-md-4"><img src="img/ads-single-small-flexible-2.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-8">
<p><strong>Minimum photo height</strong></p>
<p>The photo area should be no less than half
of the page height. When shortening the
background photo, the secondary graphic
and its contents should move to center align
(vertically) with the background.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>
</div>

<div class="col-md-4"><img src="img/ads-single-small-flexible-3.png" class="img-responsive"></div>
</div>

    
    
   </div>
    <div class="pane" id="tabs-3">
    
    <h3>Large, fixed secondary graphic</h3>   

<div class="row">
<div class="col-md-6">
<p><strong>Page layout</strong></p>
<p>The grid determines the placement for each
visual element on this 8.5"W x 11"H format.</p>
<p>On print ads, the logo is scaled to 0.80", based
on the height of the E symbol. It aligns to the
right and bottom margins.</p>
<p>Legal and address copy also align to the bottom
margin and are typeset Vesta Std Regular, 7pt
on 9pt line spacing <em>Edwards Lifesciences</em> is Bold.</p>
<p>The large secondary graphic is a fixed size
and in a fixed position. The right edge of the
graphic aligns to the right edge of the page.
Do not shift the graphic up or down, left or
right. However, if the ad is to appear on a
right-facing page and there is concern that
the left margin would cause the secondary
graphic to fall into the gutter, the secondary
graphic and text may shift to the right as a
unit. Do not move the logo.</p>


<p>In this application, the secondary graphic equals
15 square units high and wide, scaled as shown.</p>
<p>The secondary graphic holds the headline,
and subhead (optional). Body copy may be
placed in the secondary graphic or below it.</p>
</div>
<div class="col-md-6"><img src="img/ads-single-large-fixed-1.png" class="img-responsive"></div>
</div>

<div class="row">
<div class="col-md-8">

<p><strong>Photo background</strong></p>
<p>The larger square in the secondary graphic
may be used for a full-color photo. In this
application, titles must be a solid red from
the approved palette. The subhead and body
copy may be gray or black.</p>
<p>The background behind the secondary graphic
may be white, the approved pattern or a
tinted neutral color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>). The
bottom edge of the background should align
to the top horizontal line of the E symbol.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>

</div>

<div class="col-md-4"><img src="img/ads-single-large-fixed-2.png" class="img-responsive"></div>
</div>

     </div>
    <div class="pane" id="tabs-4">
    
    <h3>Large, flexible secondary graphic</h3>   

<div class="row">
<div class="col-md-6">
<p><strong>Page layout</strong></p>
<p>In this 8.5"W x 11"H ad layout, the larger
shape in the secondary graphic can be made
taller to allow for a larger photo. Headlines,
subheads and body copy may shift up or
down within the shape as necessary, but
should remain aligned to the left margin. The
smaller square should remain a square – do
not stretch or scale the smaller square.</p>
<p>The width of the secondary graphic in this
layout equals 15 square units wide. Do not
adjust the width of the graphic, only the
height of the larger shape.</p>
<p>If the ad is to appear on a right-facing page
and there is concern that the left margin
would cause the secondary graphic to fall into
the gutter, the secondary graphic and all text
may move to the right as a unit. Do not move
the logo.</p>

<p>The logo is scaled to 0.80", based on the
height of the E symbol. It aligns to the right
and bottom margins.</p>
<p>Legal and address copy also align to the
bottom margin and are typeset in Vesta Std
Regular, 7pt on 9pt line spacing – <em>Edwards
Lifesciences</em> is Bold.</p>
</div>
<div class="col-md-6"><img src="img/ads-single-large-flexible-1.png" class="img-responsive"></div>
</div>

<div class="row">
<div class="col-md-8">

<p><strong>Small photo</strong></p>
<p>Photos in the larger square may bleed off
the top of the page, and the bottom edge
may extend to 0.30" above the logo. In this
application, titles should be a solid red from
the approved palette, no tints.</p>
</div>
<div class="col-md-4"><img src="img/ads-single-large-flexible-2.png" class="img-responsive"></div>
</div>

<div class="row">
<div class="col-md-8">
<p><strong>Large photo</strong></p>
<p>Photos in the larger square may extend
from the top of the page, down to the top
horizontal line within the E symbol.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>

</div>

<div class="col-md-4"><img src="img/ads-single-large-flexible-3.png" class="img-responsive"></div>
</div>

    </div>

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
