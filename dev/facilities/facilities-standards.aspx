﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



 <div id="facilities-standards" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Color</li>
  <li>Graphics color palette and Paint palette</li>
  </ol>
  
  <div id="tabslider">
  
  <h2>Color<a href="assets/pdf/facilities.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab active"><a href="#tabs-1" class="match-height"><span>Graphics color palette</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Paint palette</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
  
<h3>Graphics color palette</h3>
 <div class="well intro">
<p>
Edwards is a red and titanium brand. Red

communicates our passion. Titanium symbolizes

our reputation. Because of this, our palette focuses

on a family of strong reds; all other colors are

secondary. Refer to the specifications for accurate

formulas.</p></div>
<p>
This palette should be used for all graphics:</p>
<ul><li>Acrylic displays</li>
<li>Posters</li>
<li>Internal signage</li>
<li>Monitor graphics</li></ul>

<h4>Primary colors for printed graphics</h4>
<p>
From the red secondary graphic to the titanium logo, these two colors
are our primary colors and should dominate our materials. Use red for
titles and headlines, charts and diagrams. Do not tint the red. The gray
may be tinted to 75%, 50%, 25% or lighter when used for information
graphics.</p>

<p><img src="images/color-1.PNG" width="469" height="340" class="img-responsive" /></p>
<p><small>Refer to <a href="../elements-color.aspx">Edwards Brand Identity Standards</a> for full color usage detail</small></p>
</div>


    <div class="pane" id="tabs-2">
<h3>Paint palette</h3>

<p>Edwards has developed a paint palette for wall surfaces 
throughout the campus. Please refer to this palette for
primary and secondary usage.</p>

<p>Including:</p>
<ul>
<li>Exterior</li>
<li>Lobby areas</li>
<li>Accent walls</li>
<li>Manufacturing areas</li>
<li>Gowning/dressing rooms</li>
<li>Locker rooms</li>
<li>Lunch rooms</li>
<li>Break rooms</li>
</ul>


<h4>Primary paints for facility surfaces*</h4>
<p><img src="images/color-2.PNG" width="177" height="150" class="img-responsive" /></p>
<h4>Accent paints for facility surfaces*</h4>
<p><img src="images/color-3.PNG" width="636" height="258" class="img-responsive" /></p>
<p><small>Refer to <a href="assets/Edwards Lifesciences Standards Manual 8-16-18.xlsx">Facilities resource guide</a> for full paint
and fixture detail</small></p>


</div>




</div>

</div>
</div>

    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
 </div>
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
