﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



  <div id="internal-comm" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Internal communication</li>
  <li>Templates</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Templates<a href="assets/pdf/internal-comm.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>


 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab active"><a href="#tabs-1" class="match-height"><span>Internal communications</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Internal signage</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Internal communications</h3>   
 
 <p>As part of delivering a consistent
look, specific interior signage
and templates have been
designed to identify messaging
where employee-specific
information is needed.</p>
<p>
These should be used for:</p>
<ul>

<li>Manufacturing</li>
<li>Clean rooms</li>
<li>Labs</li>
<li>Other locations as applicable</li>
</ul>

<h4>Emergency contact information</h4>
<p><img src="images/internal-comm-1.PNG" class="img-responsive" /></p>

<h4>8 1/2” x 11” Vertical insert</h4>

<p><img src="images/internal-comm-2.PNG" width="523" height="336" class="img-responsive" /></p>
<h4>8 1/2” x 11” Horizontal insert</h4>
<p><img src="images/internal-comm-3.PNG" width="737" height="265" class="img-responsive" /></p>

<h4>Optional Templates - No red bar or logo</h4>
<p><img src="images/internal-comm-3c.PNG" width="379" height="173" class="img-responsive" /></p>



<p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4964716" target="_blank">Communications Templates</a></small></p>
</div>


    <div class="pane" id="tabs-2">
 <h3>Internal signage</h3>
 
 <p>Templates have been designed to aid
in preparing signage for locations such
as warehouse, clean room, lab,
machine shop, and other similar
locations.</p>
  
  
<h4>Ceiling headers</h4>
<p><img src="images/internal-comm-4.PNG" width="791" height="157" class="img-responsive" /></p>

<h4>Miscellaneous sizes</h4>  
    <p><img src="images/internal-comm-5.PNG" width="723" height="422" class="img-responsive" /></p>
    
    
<h4>Optional Template - No red bar or logo</h4>  
    <p><img src="images/internal-comm-3b.PNG" width="429" height="174" class="img-responsive" /></p>


<p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4964716" target="_blank">Communications Templates</a></small></p>    
</div>




</div>

</div>
 </div>


    <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    

  </div>
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
