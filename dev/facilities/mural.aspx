﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body>
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



 <div id="mural" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Life is Now</li>
  </ol>
  
  
  
  <h2>Life is Now<a href="assets/pdf/lifeisnow.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  

 <div class="well intro">
<p>Our commitment as a company,
as articulated in our Credo, is
Helping Patients is Our Life’s
Work, and Life&nbsp;is&nbsp;Now.</p>
</div>
<p>The complete tagline should
always be used. Do not use
'Life is Now' alone.</p>
<p>
The Life is Now graphic is a
specific, stylized font (see files)
and can be displayed in:</p>
<ul>
<li>Manufacturing areas</li>
<li>Gowning/dressing rooms</li>
<li>Locker rooms</li>
<li>Lunch rooms</li>
<li>Break rooms</li>
</ul>
<p><strong>Vinyl lettering or 3-D acrylic
display is preferred.</strong></p>

<h4>Life is Now Option - Arial</h4>

<p><img src="images/mural-1.PNG" class="img-responsive" /></p>


<h4>Life is Now Option - Vesta</h4>

<p><img src="images/mural-2.PNG" class="img-responsive" /></p>




<p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4965062" target="_blank">Life is Now</a></small></p>
<p><small>These can be scaled for hallway areas and materials are specified</small></p>


</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
