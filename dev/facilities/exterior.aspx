﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body>
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



  <div id="exterior" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Exterior</li>
  <li>Entry signage, Building top, Eyebrow, Monument, Flags</li>
  </ol>
  
  

  <h2>Exterior<a href="assets/pdf/exterior.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>

<div class="well intro">
<p>These guidelines provide a general framework
within which exterior signage is to be specified
and positioned for installation.</p>
</div>


<p>Signage usually comes in three types:</p>

<div class="row"><div class="col-md-6" id="btop">
<p><strong>Building top</strong>, as the name implies, is at the
top of the building</p>
</div>
<div class="col-md-6"><img src="images/exterior-1.PNG" width="678" height="329" class="img-responsive" /></div>
</div>

<div class="row"><div class="col-md-6"  id="eyebrow">
<p><strong>Eyebrow</strong> signs are those found above the
main entrance to the building, or at the first
or second floor level. These are situated so
someone walking or driving down the street
will take notice.</p>
</div>
<div class="col-md-6"><img src="images/exterior-2.PNG" width="631" height="307" class="img-responsive" /></div>
</div>

<div class="row"><div class="col-md-6"  id="monument">
<p><strong>Monument</strong> signage is at ground/floor level
and can typically be found next to the building’s
driveway entrance, main entrance and in the
lobby.</p></div>
<div class="col-md-6"><img src="images/exterior-3.PNG" width="664" height="308" class="img-responsive" /></div>
</div>

<p  id="flags"><strong>Edwards flag for entrance:</strong><br>
Art files and specifications available<br>
36” x 60” (91.44 cm x 152.4 cm)<br>
48” x 72” (121.92 cm x 182.88 cm)</p>
<p>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4967936" target="_blank">Edwards Flag</a><br>
Included:</p>
<ul>
<li>Protocol</li>
<li>Order information</li>
<li>Art files</li>
</ul>
<p><img src="images/exterior-4.PNG" width="435" height="570" class="img-responsive" /></p>


 </div>
    <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    

  </div>
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
