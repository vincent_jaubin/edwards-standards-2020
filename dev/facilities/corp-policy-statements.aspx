﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body>
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



 <div id="corp-policy-statements" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Policy statements</li>
  <li>Quality and Environmental Health & Safety</li>
  </ol>
  
  
  
  <h2>Policy statements<a href="assets/pdf/policy.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>

 <div class="well intro">
<p>Edwards must fulfill a number of
regulatory and/or legal requirements
regarding statements of specific
operational policies. These include
standard statements relating to our
<strong>Quality Policy</strong> (globally required)
and our <strong>Environmental Health and
Safety Policy</strong> (US required).</p>
</div>

<p>The files provided will enable you to
produce translated versions of these
signs, or additional document(s)
required for your respective location.</p>


<p><strong>Framing is the preferred display
method for Quality and EHS policies. See <a href="display-specs.aspx">here</a>.</strong></p>

  


<div class="row">
<div class="col-md-4" id="quality">
<h3>Quality</h3>
<img src="images/corppolicy1.PNG"  class="img-responsive" />
</div>


<div class="col-md-8" id="ehs">
<h3>Environmental Health & Safety</h3>
<img src="images/corppolicy2.PNG" class="img-responsive" /></div>
</div>

<p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4965118" target="_blank">Policy Statements</a></small></p>


</div>




    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
