﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



  <div id="interior" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Facilities standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Facilities standards</a></li>
  <li>Interior</li>
  <li>Identity & messaging hierarchy, Lobby logo sign, Monitor</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Interior<a href="assets/pdf/interior.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>


 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab active"><a href="#tabs-1" class="match-height"><span>Identity & messaging hierarchy</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Lobby logo sign</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Monitor</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Identity & messaging hierarchy</h3>   
 <p>This illustrates the primary and secondary
communication tools within a facility’s public
and departmental areas.</p>

<div class="row marg30">
<div class="col-md-4"> 
  <p><strong class="text-primary">Public spaces</strong><br>
    These areas include, but may not
    be limited to: building entrances
    and exits, lobbies, hallways and
    conference rooms that a visitor
    may have access to.
  </p>
</div>

<div class="col-md-4"> 
  <p><strong class="text-primary">Shared workspaces</strong><br>
    These areas include, but may
    not be limited to: non-public
    hallways, break and lunch
    rooms, labs, and clean rooms.</p>
</div>

<div class="col-md-4"> 
  <p><strong class="text-primary">Personal workspaces</strong><br>
    These areas include:
    your assigned office,
    cubicle, or desk</p>
</div>
</div>

<div class="row marg30">
<div class="col-md-12">
<h6>Corporate messaging - Lobby sign</h6>
<p><img src="images/interior-3.PNG" width="218" height="314" class="img-responsive" /></p>

<h6>Edwards Culture Messaging in Open Spaces</h6>
<p><img src="images/interior-4.PNG" width="743" height="484" class="img-responsive" /></p>
</div>
</div>

<div class="row marg30">
<div class="col-md-6"> 
<h6>Updatable Messaging<br>
  55” Monitor<br>
  Corporate Powerpoint - Lobby</h6>
<p><img src="images/interior-5.PNG" width="447" height="310" class="img-responsive" /></p>
</div>
<div class="col-md-6"> 
<h6>Updatable Messaging<br>
  55” Monitor<br>
  Internal Powerpoint</h6>
<p><img src="images/interior-6.PNG" width="437" height="263" class="img-responsive" /></p>
</div>
</div>


<div class="row marg30">

<div class="col-md-3">
<h6>Edwards Quality Policy<br>
  Hallways / Conference Rooms</h6>
<p><img src="images/interior-7.PNG" width="216" height="278" class="img-responsive" /></p>
</div>

<div class="col-md-6">
<h6>Entrance/Exits of Buildings</h6>
<p><img src="images/interior-8.PNG" width="416" height="236" class="img-responsive" /></p>
</div>

<div class="col-md-3">
<h6>Department Signage<br>
  Specific to Group</h6>
<p><img src="images/interior-9.PNG" width="202" height="235" class="img-responsive" /></p>
</div>

</div>

<div class="row marg30">
<div class="col-md-12">
<h6>Edwards Corporate Messaging - Humanity</h6>
<p><img src="images/interior-10.PNG" width="723" height="324" class="img-responsive" /></p>
</div>
</div>


<div class="row marg30">
<div class="col-md-12">
<h6>Sample Elevator Wrap - 2 Door</h6>
<p><img src="images/interior-12.PNG" width="327" height="590" class="img-responsive" /></p>
</div>
</div>

 

</div>


    <div class="pane" id="tabs-2">
 <h3>Lobby logo sign</h3>
 <p>The Edwards logo should be
prominently displayed in the lobby. It should
always appear on a neutral colored wall.</p>
<p>Please refer to <a href="assets/Edwards Lifesciences Standards Manual 8-16-18.xlsx">Facilities resource guide</a> for paint and
material options.</p>

<div class="row">
<div class="col-md-12">
<h4>Lobby sign example - preferred</h4>
<p><img src="images/interior-1.PNG" width="546" height="422" class="img-responsive" /></p>

<p><small>See folder: <a href="https://edwards.webdamdb.com/cloud/#folder/4965075" target="_blank">Lobby Signage & Schematic</a></small></p>

<p><small>These can be scaled. Materials are specified, including
schematic and installation recommendations</small></p>
</div>
<div class="col-md-12 marg30">
<h4>Example of neutral background paint and accent wall finish</h4>
<p><img src="images/interior-2.PNG" width="261" height="124" class="img-responsive" /></p>
</div>
</div>




</div>

<div class="pane" id="tabs-3">
<div class="row">
<div class="col-md-7">

 <h3>Public areas - monitor</h3>
<p>Monitors should be utilized in public and shared
workspaces to display changing or updateable
messages, such as:</p>
<p>Public monitor usage:</p>
<ul>
<li>Welcome guests</li>
<li>Corporate messaging</li>
<li>Facility overview</li>
<li>Innovation</li>
<li>Announcements</li>
</ul>
</div>

<div class="col-md-5">

<h4>Monitor example</h4>
<p><img src="images/interior-11.PNG" width="714" height="665" class="img-responsive" /></p>
</div>
</div>

</div>


</div>

</div>
 </div>


    <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-facilities.html" -->
  
  
  </div>
    
    

  </div>
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
