﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="print-pamphlets" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Print collateral</li>
  <li>Pamphlets</li>
  </ol>
  
  
  
  <h2>Pamphlets <span class="edw-titan">Vertical formats</span>
  <a href="pdf/p-pamphlets.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/edwards-brochures.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the templates"><i class="icon-download"></i></a></h2>
  <div class="clearfix"></div>
  
  
<div class="well intro">
<p>Pamphlets include a small or large
secondary graphic and have the same
flexibility as larger collateral pieces.</p></div>
 
<p>The grids determine the placement for each
element on this 4"W x 9"H format.</p>
<p>The logo is scaled to 0.60", based on the
height of the E symbol, and aligns to the right
and bottom margins.</p>
<div class="row">
<div class="col-md-4">
<p><strong>Small secondary graphic</strong></p>
<p>The small secondary graphic is a fixed size and
in a fixed position. Do not shift the graphic
left or right. In this layout, the secondary
graphic equals 10 square units high and wide,
scaled as shown.</p>
<p>When placed over a photo, the larger of the
two squares may have a multiply effect as
described <a href="elements-secondary.aspx#multiply" target="_blank">here</a>. If a photo is not being
used, the secondary graphic may be placed
over white, the approved background pattern,
or a tinted neutral color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>).
The bottom edge of the color field or pattern
should align to the top horizontal line of the
E symbol.</p>
</div>

<div class="col-md-4 col-md-offset-1">

<img src="img/print-pamphlets-vert-1.png" alt="Pamphlets" class="img-responsive img-margin" />
</div>
</div>

<div class="row">
<div class="col-md-4">


<p><strong>Large secondary graphic</strong></p>
<p>The large secondary graphic is a fixed size and
in a fixed position. Do not shift the graphic
up or down, left or right. In this layout, the
secondary graphic totals 20 square units high,
however, the right edge is cropped.</p>
<p>The larger square in the secondary graphic
may include a photo, in which case titles are a
solid red from the approved palette, no tints.
Silhouetted photos may also be used with a
100% primary red or an approved background
color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>).</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>
</div>

<div class="col-md-4 col-md-offset-1">

<img src="img/print-pamphlets-vert-2.png" alt="Pamphlets" class="img-responsive" />
</div>
</div>

  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
