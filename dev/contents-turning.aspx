﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-turning" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>Turning the inside out</li>
  </ol>
  
  
  
  <h2>Turning the inside out<a href="pdf/p-turning.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  
  
  <p>Our brand platform is an internal tool meant to influence how we
communicate externally. The components we’ve described to this point
are not intended to be used as external copy or for marketing promotions.
Rather, they are intended to drive the content, tone and design of those
communications. Here, we talk about the tone of content for external
communications.</p>



<h3>Language and tonality</h3>
<p>We defined our brand personality as visionary, driven, dedicated, passionate
and collaborative. We should infuse our writing with these attributes through
tone, language and content.</p>
<p>Our five personality attributes coexist and everything we write should remain
true to all of them. That doesn’t mean we always exhibit them all equally.
Numerous factors affect which personality attributes take the lead in any
given piece of communication – from audience to subject matter to medium
to timing.</p>
<p>For example, our annual report might primarily reflect our visionary and
driven characteristics. The other three personality attributes aren’t absent
from the communication — they simply play a supporting role in a report of
this nature.</p>
<p>A video about the impact of aortic stenosis on patient lives might stress the
dedicated, passionate and collaborative aspects of our personality. The other
two attributes still can come across, however, in our choice of voiceover talent,
imagery, word choice, music and tone.</p>
<p>Always keep in mind that we endeavor to be perceived as we are – humble
and helpful.</p>

<h3>Edwards’ boilerplate</h3>
<p>As part of maintaining consistent brand language, we have developed a
“boilerplate” company description that is intended to translate our brand
platform for external usage. This boilerplate is to be used in company press
releases, investor and recruiting materials and company summaries.</p>



<div class="well">
<p><em>Our boilerplate is:</em></p>
<p class="edw-red h4">Edwards Lifesciences, based in Irvine, Calif., is the global
leader in patient-focused medical innovations for
structural heart disease, as well as critical care and surgical
monitoring. Driven by a passion to help patients, the
company collaborates with the world’s leading clinicians
and researchers to address unmet healthcare needs,
working to improve patient outcomes and enhance lives.
For more information, visit Edwards.com and follow us on
Twitter <a href="https://twitter.com/edwardslifesci" target="_blank">@EdwardsLifesci</a>.</p>
</div>

<h3 id="credo">Edwards’ credo</h3>
<p>Our credo will always be our credo. But it’s an internal tool. A rallying cry.
An inspiration. And a north star.</p>
<p>To the contrary, our brand drives communications – internally and externally.
It helps form how we look and what we say to all stakeholders.</p>
<p><em>Life is now</em> and other elements of the credo should only be used <strong>internally</strong>, and
on documents that will be used for internal purposes. However, the credo can
be used on Recruitment communications to enable prospective employees to
better understand the Edwards culture.</p>

<div class="well">
<p><em>Our credo is:</em></p>
<p class="edw-red h4">At Edwards Lifesciences, we are dedicated to providing
innovative solutions for people fighting cardiovascular
disease.<br><br>
Through our actions, we will become trusted partners with
customers, colleagues and patients – creating a community
unified in its mission to improve the quality of life around
the world. Our results will benefit customers, patients,
employees and shareholders.<br><br>
We will celebrate our successes, thrive on discovery and
continually expand our boundaries. We will act boldly,
decisively and with determination on behalf of people
fighting cardiovascular disease.<br>
<br>

<img src="img/lifeisnow-credo.jpg" width="669" height="149" alt="Life is Now" class="img-responsive" /></p>
</div>




</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
