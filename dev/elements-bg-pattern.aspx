﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="elements-bg-pattern" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Basic elements</li>
  <li>Backgrounds</li>
  </ol>
  
  
     <div id="tabslider">
  <h2>Backgrounds
  <a href="pdf/p-bg-pattern.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
  
   <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Pattern</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Colors</span></a></li>
</ul>
   <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
    
    <h3>Pattern</h3>
    
  <div class="well intro">
  <p>Comprised of stylized “Es”, our
background pattern adds vibrancy
and texture to materials. It
communicates the collaborative
nature of elements coming together.</p>
</div>

<p>This specially drawn pattern consists of
repeating groups of three lines, carefully
spaced, staggered and aligned to fill the
background area. When used on a cover, it
should align with the Edwards logo as shown.</p>
<div class="row">
<div class="col-md-4">
<p><strong>Color</strong></p>
<p>To keep the effect subtle – i.e., a background
texture, not a bold graphic effect – the pattern
may only be used in the following colors:</p>
<ul>
<li>25% tint of primary gray 423 against white;</li>
<li>White pattern against a 25% or 50% tint of
gray or other neutral color;</li>
<li>A linear or radial gradient, where the
darkest part is a 25% or 50% tint of primary
gray 423 against white;</li>
<li>White pattern against a linear or radial
gradient, where the darkest part is a 25% or
50% tint of gray or a neutral color</li></ul>
</div>

<div class="col-md-8">
<img src="img/background-pattern.png" alt="background-pattern" class="img-responsive" />
</div>
</div>

<div class="row">
<div class="col-md-12">


<p><strong>Scale</strong></p>
<p>The pattern should not be so small that it’s
unrecognizable, and not so large that it
distracts or competes with the logo. For most
print pieces, there should be 3 or 4 columns
of the pattern across the width of the logo.
When the logo is used larger in relation to the
page, more columns may be appropriate (for
example, 7 columns on PowerPoint title slides).</p>
<p><strong>Other patterns</strong></p>
<p>This is the only approved graphic pattern or
texture. Other subtle patterns or textures must
be integrated with a photo in the photo area,
and not compete with the secondary graphic.</p>
</div>
</div>

</div>

    <div class="pane" id="tabs-2">
    
      <h3>Colors</h3>
      
<p>In order to maintain the focus and strength of
the secondary graphic and our signature red
and titanium palette, backgrounds and large
fields of color need to be tinted back. Primary
gray, the neutral palette and white are the
only approved background colors.</p>
  <div class="row">
<div class="col-md-4">
<p><strong>Approved tints</strong></p>
<p>Background colors may be a 50% or 25% tint of
the primary gray (PANTONE 423), any neutral
color or white. Colors may be tinted lighter to
work with a photo, but colors may not appear
darker than a 50% tint.</p>
<p><strong>Samples</strong></p>
<p>The print and banner samples shown here
demonstrate how to use the background
colors correctly with the secondary graphic.</p>
<ul>
<li>Behind a small secondary graphic</li>
<li>Behind a silhouetted photo within a large
secondary graphic</li>
<li>Behind a large secondary graphic (with or
without a photo)</li>
</ul>

</div>

<div class="col-md-8">

<img src="img/background-color.png" alt="colors" class="img-responsive img-margin" />
</div>
</div>

<div class="row">
<div class="col-md-12">


<p>In each case, the primary gray or neutral color
background should be a 50% (or lighter) tint.</p>
<p>The bottom or top edge of a background color
field should anchor to the Edwards logo as
described on <a href="elements.aspx" target="_blank">this page</a> in "Logo placement" tab.</p>
</div>
</div>  
    </div>

  
    </div>
    </div>
    </div>
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
