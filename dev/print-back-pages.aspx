﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="print-back-pages" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Print collateral</li>
  <li>Back page</li>
  </ol>
  
  
  
  <h2>Back page
  <a href="pdf/p-back-page.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/edwards-interior.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the templates"><i class="icon-download"></i></a></h2>
  <div class="clearfix"></div>
  
  
<div class="well intro">
<p>The legal, regulatory and address
information on the back of brochures
and on content sheets should have a
standard look.</p></div>
 
<p>These specifications apply to the last page
of multi-page documents. For single-sided
pieces, such as ads, refer to those standards.</p>
<div class="row">
<div class="col-md-5">
<p><strong>Page layout</strong></p>
<p>The grids determine the placement for each
element on this 8.5"W x 11"H format. The
content area may be 2–3 columns. Although
the top margin is 0.50", it is preferred the copy
begin 1.50" from the top.</p>
<p>The logo is scaled to 0.80", based on the
height of the E symbol. It aligns to the right
and bottom margins.</p>

<p>The regulatory, legal/trademark, copyright
and address information align to the bottom
margin with the logo and “grow” upward as
copy is added. This copy is Vesta Std Regular,
7pt on 9pt line spacing, with an additional
4.5pt space after each level of information
(no extra spacing between references). The
caution statement and Edwards Lifesciences
are Vesta Std Bold. Use a single space followed
by a bullet and another space ( • ) before the
physical address and before the URL.</p>

<p>A 0.5pt black line should appear above the
legal copy, matching the spacing as shown.</p>
</div>

<div class="col-md-7">

<img src="img/print-backpage-1.png" alt="Back page" class="img-responsive img-margin" />
</div>
</div>

<div class="row">
<div class="col-md-8">

<p><strong>Additional references</strong></p>
<p>Move the black line up when more references
are needed. The spacing below the line remains
the same. Remove the line if space is limited.</p>
</div>

<div class="col-md-4">

<img src="img/print-backpage-2.png" alt="Back page" class="img-responsive img-margin" />
</div>
</div>
<div class="row">
<div class="col-md-8">

<p><strong>No references</strong></p>
<p>If there are no references or regulatory copy
does not go beyond the top of the Edwards
logo, do not use a black line.</p>
</div>

<div class="col-md-4">

<img src="img/print-backpage-3.png" alt="Back page" class="img-responsive" />
</div>
</div>

  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
