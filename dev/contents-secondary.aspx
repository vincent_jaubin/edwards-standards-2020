﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-secondary" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>Secondary campaigns</li>
  </ol>
  
  
  
  <h2>Secondary campaigns<a href="pdf/p-secondary-campaigns.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  

<p>Overall brand standards apply to all secondary campaigns whether internal
or external – product, product portfolio, professional education, recruitment,
employee – or investor-focused communications.</p>
<p>To provide for differentiation around these campaigns, the following guidelines
have been established.</p>

<ul>
<li>To help unify communications, campaigns may use a common photograph,
provided it meets overall corporate style standards.</li>
<li>Dedicated messaging and headlines may be created for a campaign, but
must be used within the context of a headline only; no taglines can be
developed for individual campaigns or other use.
<ul>
<li>Campaign messaging should ladder up to or be a natural extension from
the corporate brand platform. Global Communications and Branding can
provide further guidance and approval.</li></ul>
</li>
</ul>

<p>For external campaigns:</p>
<ul>
<li>To reinforce Edwards as the master brand and to elevate the primacy of
the Edwards logo, no other logo, lockup, or type treatment may be created
for external use.
<ul>
<li>See exception for Clinical Trial Branding on <a href="contents-cobranding.aspx">this page</a>.</li></ul>
</li>
<li>A logo or lockup is defined as a combination of characters, graphics, and/or
design elements creating a single unique symbol used to identify a company,
product, service, event or entity.</li>
<li>A type treatment is defined as use of colored text, font size, bolding, and/or
extended kerning.</li>
</ul>

<p>For internal initiatives:</p>
<ul>
<li>The development or use of a logo, lockup, or type treatment for internal use
is discouraged, but may be considered only for broad enduring corporate
initiatives or programs (i.e., BEST program, engagement survey, Total
Wellness program, etc.). The development or use of a logo, lockup, or type
treatment for teams, departments, groups, etc. is not permitted.</li>
<li>Those considering a logo, lockup, or type treatment for internal use must
obtain approval from Global Communications and Branding.</li>
</ul>


</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
