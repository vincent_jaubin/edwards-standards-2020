﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="elements-logo" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Basic elements</li>
  <li>Logo usage</li>
  </ol>
  
   <div id="tabslider">
  <h2>Logo usage
  <a href="pdf/p-pref-logo.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/logos.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download all logos"><i class="icon-download"></i></a>
 </h2>

<p>These logos are available for use in Edwards digital and print collateral.</p>
                            <p><a href="templates/logos.zip" class="btn btn-info" role="button"><i class="icon-download"></i> Download all images</a></p>

  <div class="clearfix"></div>
  
  
 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Preferred logo</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Alternate logos</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Logo placement</span></a></li>
  <li><a href="#tabs-4" class="match-height"><span>Incorrect logo uses</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
  <h3>Preferred logo</h3>
  <div class="well intro">
  <p>The Edwards logo is our signature. It symbolizes innovation and
humanity. This unique graphic unifies
our communications and helps to
distinguish us from the competition.</p>
</div>

<div class="row">
<div class="col-md-5">
<p><strong>Logo version 1</strong><br>

This logo is preferred for all communications,
regardless of vertical or horizontal
applications. It includes the E symbol with
the <em>Edwards</em> name below. <em>Edwards</em> has been carefully
drawn and is not a font or typeface – it is
artwork. The proportions
and arrangement of these elements have
been carefully designed and should not be
altered, separated or recreated.</p>
</div>
<div class="col-md-7">

<img src="img/logo-usage-1.png" alt="Logos" class="img-responsive" />
</div>
</div>

<div class="row">
<div class="col-md-5">

<p><strong>Positive logos</strong><br>

Titanium (PANTONE 8421) is the preferred
logo color and should be used in print
applications whenever possible. If metallic ink
is not available, print in PANTONE 423, either
spot color, process or as grayscale (55% black).
The 100% black logo should only be used on
premium items, for etching or when limited
to one color without tints.
In all positive logo color versions, the <em>Edwards</em>
name is 100% black, never a tint, and the
da Vinci lines and ‘E’ within the symbol appear
white, not transparent.</p>

</div>
<div class="col-md-7">

<img src="img/logo-usage-2.png" alt="Logos" class="img-responsive" />
</div>
</div>

<div class="row">
<div class="col-md-5">

<p><strong>Reverse logos</strong><br>

Reverse color logos are available in the same
colors as the positive logos, however, the
<em>Edwards</em> name, the da Vinci lines and ‘E’ within
the symbol appear white.</p>
</div>
<div class="col-md-7">

<img src="img/logo-usage-3.png" alt="Logos" class="img-responsive" />
</div>
</div>


</div>


    
    
  
    <div class="pane" id="tabs-2">  
    
    <h3>Alternate logos</h3>
  
  <div class="well intro">
  <p>Logo versions 2 and 3 are available in the same
positive and reverse color options as version 1.
Refer to <a href="elements.aspx">preferred logo page</a> for specifications.</p>
</div>

<div class="row">
<div class="col-md-5">
<p><strong>Logo version 2</strong><br>

This version of the logo includes the E symbol
and the full <em>Edwards Lifesciences</em> name to the
right of the symbol. <em>Edwards Lifesciences</em> has been
carefully drawn and is not a font or typeface – it
is artwork. The proportions and arrangement of
these elements have been carefully designed and
should not be altered, separated or recreated.</p>
<p>This logo version should only be used for
horizontal applications when logo version 1
will not fit.</p>

</div>
<div class="col-md-7">

<img src="img/logo-usage-4.png" alt="Logos" class="img-responsive" />
</div>
</div>

<div class="row">
<div class="col-md-5">

<p><strong>Logo version 3</strong><br>

Version 3 includes the E symbol with the <em>Edwards</em>
name to the right. <em>Edwards</em> has been carefully
drawn and is not a font or typeface – it is artwork.
The proportions and arrangement of these
elements have been carefully designed and
should not be altered, separated or recreated.</p>
<p>This logo version should only be used for
horizontal applications when logo version 1
or 2 will not fit.</p>
</div>
<div class="col-md-7">

<img src="img/logo-usage-5.png" alt="Logos" class="img-responsive" />
</div>
</div>

<div class="row">
<div class="col-md-5">

<p><strong>Specialty use logos</strong><br>

These logos include a simplified version of
the E symbol where the da Vinci lines have
been removed. These versions should only
be used when production processes such as
screen printing or pad printing do not allow
for such fine detail.</p>
<p>The specialty use logos are available in all
three logo versions.</p>


<p>For embroidery, a thread color has been
determined to best match the titanium logo.
The gray thread color is <em>Cloud</em> 2483∕6983, the
‘E’ in the symbol is white, and the <em>Edwards</em>
logotype is black, white or gray.</p>
</div>

<div class="col-md-7">

<img src="img/logo-usage-6.png" alt="Logos" class="img-responsive" />
</div>
</div>

    </div>
    
       <div class="pane" id="tabs-3">  
       
   <h3>Logo placement</h3>    
  
  <div class="well intro">
  <p>Placing the logo in a consistent size
and area will help unify our
communications and ensure that
our name is clearly visible.</p>
</div>

<div class="row">
<div class="col-md-5">
<p><strong>Clear area</strong><br>

The Edwards logo has greater impact when it
is free from other text or graphics competing
for attention. Although more space is always
recommended, use the height of the <em>Edwards</em>
name (X) to measure the minimum required
clear area around the logo. The clear area is
the same for every logo version.</p>

</div>

<div class="col-md-7">

<img src="img/logo-usage-7.png" alt="Logos" class="img-responsive img-margin" />
</div>
</div>

<div class="row">
<div class="col-md-5">
<p><strong>Minimum size</strong><br>

To ensure the logo is clearly visible on print
and digital applications, minimum sizes
have been determined. The minimum size
is based on the overall height of each logo
configuration.</p>

</div>

<div class="col-md-7">

<img src="img/logo-usage-8.png" alt="Logos" class="img-responsive img-margin" />
</div>
</div>

<div class="row">
<div class="col-md-5">
<p><strong>Position</strong><br>

On print pieces, the logo is placed in the
bottom right corner and aligns to the grid as
shown on the following pages. The logo may
be “anchored” to a field of color or photo
by aligning the top horizontal line of the E
symbol with the background.</p>

<p>For web and app communications, place the
logo in the top left corner. It may be anchored
to the bottom horizontal line of the E symbol.</p>
<p>For banners and other sign applications, the
logo should be placed in the top right corner.
In this placement, the logo may be anchored
to the bottom horizontal line of the E symbol.</p>
<p>When anchoring the logo to a photo or
field of color, be sure the logo has sufficient
contrast with the background. If necessary,
lighten or darken that area of the photo or
select a different background color or tint.</p>


</div>

<div class="col-md-7">

<img src="img/logo-usage-9.png" alt="Logos" class="img-responsive img-margin" />
</div>
</div>


    </div>
    
       <div class="pane" id="tabs-4">  
       
       <h3>Incorrect logo uses</h3>
       
  
  <div class="well intro">
  <p>The Edwards logo is our signature and an important part
of our visual identity. Always follow these brand standards
and use approved artwork.</p>
</div>

<div class="row">
<div class="col-md-12">
<ol>
<li><strong>Don’t change the logo colors</strong>
Use only the approved logo color versions as described in "Preferred logo" tab of this page.<br><img class="img-responsive img-margin" src="img/logousage_01.png"></li><li><strong>Don’t change the Edwards logotype</strong>
<em>Edwards</em> and <em>Edwards Lifesciences</em> have been carefully drawn and
should not be replaced with a typeface or message.<br><img class="img-responsive img-margin" src="img/logousage_02.png">
</li><li><strong>Don’t use a gradient in the symbol</strong>
While the preferred logo is titanium, do not mimic the metallic color
with a gradient. Only use the approved colors seen in "Preferred logo" tab of this page.<br><img class="img-responsive img-margin" src="img/logousage_03.png">
</li><li><strong>Don’t modify the logo elements</strong>
Never remove lines or alter the Edwards logo in any way.<br><img class="img-responsive img-margin" src="img/logousage_05.png">
</li><li><strong>Don’t change the proportions</strong>
Do not alter the proportions of the logo elements – on any logo version.<br><img class="img-responsive img-margin" src="img/logousage_06.png">
</li><li><strong>Don’t change the lines or ‘E’ color</strong>
The ‘E’ and the lines within the symbol should appear white, never
black or another color.<br><img class="img-responsive img-margin" src="img/logousage_07.png">
</li><li><strong>Don’t place the logo on a background that has insufficient contrast</strong>
Always be sure the logo is clearly visible, whether placing it on a solid
or split colored background, or over a photo.<br><img class="img-responsive img-margin" src="img/logousage_08.png">
</li><li><strong>Don’t use the vertical lines as an anchor</strong>
Only use the horizontal lines in the symbol to anchor the logo to a color
or photo background.<br><img class="img-responsive img-margin" src="img/logousage_09.png">
</li><li><strong>Don’t center the symbol over a background</strong>
The logo may only be anchored to horizontal lines within the symbol.<br><img class="img-responsive img-margin" src="img/logousage_10.png">
</li><li><strong>Don’t reverse the logo</strong>
Only use the approved color versions shown in "Preferred logo" tab of this page.<br><img class="img-responsive img-margin" src="img/logousage_11.png">
</li><li><strong>Don’t use the E symbol without the logotype</strong>
The E symbol and the logotype are a lockup—always use them together.<br><img class="img-responsive img-margin" src="img/logousage_12.png">
</li><li><strong>Don’t distort the logo</strong>
Do not stretch, skew, rotate or otherwise distort the logo.<br><img class="img-responsive img-margin" src="img/logousage_13.png">
</li>
</ol>

</div>


</div>

    </div>
    
    
    
    </div>
    </div>
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
