﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-cobranding" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>Co-branded & unbranded materials</li>
  </ol>
  
  
  
  <h2>Co-branded & unbranded materials<a href="pdf/p-cobranded-unbranded.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>


<h3>Clinical Trial branding</h3>
<p>To maintain clinical credibility/data integrity and where a unique and durable
trial brand is deemed necessary to differentiate from competing trials, the
nature of clinical trials may require the creation of an identity that remains
separate from our promotional materials; as such clinical trial branding is
exempt from the Brand Identity Standards.</p>
<p>When dictated by business needs, clinical trial branding and related logos
may be implemented along with Edwards branding and logo per co-branding
guidelines.</p>
<h3>Co-branding for clinical trial materials</h3>
<p>Where a product is still under investigation, materials may keep the pure
clinical trial branding; or may include the Edwards brand elements as needed.</p>
<p>Co-branding guidance should be employed when utilizing both clinical trial
branding/logo along with Edwards branding.</p>



<ul><img src="img/cobranded-1.png" width="328" height="426" class="img-responsive img-left" />
<li>Edwards branding may be
implemented on front and back,
or just on the back, based on the
specific business need.</li>
<li>When utilizing a clinical trial logo
together with the Edwards logo,
the logos should be similar scale to
establish visual equality.</li>
</ul>
<p>Where a product is commercial,
Edwards branding must be included
on collateral that references the
clinical trial.</p>

<div class="clearfix"></div>

<h3>Co-branding treatment and usage of other logos</h3>
<p>When the Edwards logo is used in conjunction with another logo, i e , a logo
from an Edwards-sponsored clinical trial or from another company, unless
stipulated by contractual agreements, the following rules apply:</p>


<ul>
<li>The Edwards logo should always larger or at least the same size as the other logo.</li>
<li>Placement of the other logo should always complement the Edwards logo 
and should not be dominant.</li>
<li>The secondary logo should be separate from the Edwards brand elements.</li>
</ul>
<p><img src="img/cobranded-2.png" width="635" height="301" class="img-responsive" /></p>



<h3>Unbranded materials</h3>
<p>Some materials may require the Edwards brand be eliminated or unbranded, in
which case the following rules apply:</p>
<ul>
<li>No Edwards logo, no secondary graphic.</li>
<li>However, even without the logo, the materials must clearly state they are
provided by Edwards Lifesciences.</li>
</ul>
<p>Other materials will require a more subtle Edwards look, in which case the
following rules apply:</p>
<ul>
<li>The secondary graphic, typography, colors and photo style may be used.</li>
<li>The logo may appear on the back of print materials, or at the bottom of
digital materials.</li>
</ul>


</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
