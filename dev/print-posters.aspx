﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="print-posters" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Print collateral</li>
  <li>Posters</li>
  </ol>
  
  
  <div id="tabslider">
  
  <h2>Posters
  <a href="pdf/p-posters.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/Metric/edwards-posters-metric.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the metric templates"><i class="icon-download"></i> <span class="share-txt">Metric</span></a><a href="templates/US-standard/edwards-poster.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the U.S. templates"><i class="icon-download"></i> <span class="share-txt">U.S.</span></a></h2>
  <div class="clearfix"></div>
  
  
 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>16" x 20"</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>24" x 36"</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 
 <h3>16" x 20"</h3>     
      
<p>The grid determines the placement for each
element on these 16"W x 20"H posters.</p>
<p>The logo is scaled to 1.60", based on the
height of the E symbol. Legal and address
information aligns to the bottom margin,
typeset in Vesta Std Regular, 10pt on 13pt line
spacing; Edwards Lifesciences is Bold.</p>


<div class="row">
<div class="col-md-8">
<p><strong>Small secondary graphic</strong></p>
<p>The small secondary graphic is a fixed size. The
graphic may shift up or down as needed to
work with the photo; do not shift the graphic
left or right. In this layout, the secondary
graphic equals 10 square units high and wide,
scaled as shown.</p>

<p>The headline and subtitle appear in the larger
red square. The background may be white,
the approved pattern or a tinted neutral color
(see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>). The bottom edge of the
background should align to the logo.</p>

</div>

<div class="col-md-4">
<p><img src="img/print-posters-16-20-1.png" alt="Small formats vertical" class="img-responsive center-block" /></p>
</div>
</div>



<div class="row">
<div class="col-md-8">

<p><strong>Large secondary graphic</strong></p>
<p>The large secondary graphic is a fixed size
and in a fixed position. The right edge of the
graphic aligns to the right edge of the poster.
Do not move the graphic. In this application,
the secondary graphic equals 15 square units
high and wide, scaled as shown.</p>
<p>The larger square in the secondary graphic may
be used for photos. In this instance, titles should
be a solid red from the approved palette. The
background may be white, the approved pattern,
or a tinted neutral color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>).
The bottom edge of the background should
align to the top horizontal line of the E symbol.</p>

<p><strong>Horizontal formats</strong></p>
<p>Refer to the separate InDesign template for
horizontal formatted poster layouts.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>

</div>

<div class="col-md-4">
<p><img src="img/print-posters-16-20-2.png" alt="Small formats vertical" class="img-responsive center-block" /></p>
</div>
</div>


    
    </div>
    <div class="pane" id="tabs-2">
    
  <h3>24" x 36"</h3> 
 
<p>The grid determines the placement for each
element on these 24"W x 36"H posters.</p>
<p>The logo is scaled to 2.50", based on the
height of the E symbol. Legal and address
information aligns to the bottom margin,
typeset in Vesta Std Regular, 16pt on 20pt line
spacing; Edwards Lifesciences is Bold.</p>
<div class="row">
<div class="col-md-8">
<p><strong>Small secondary graphic</strong></p>
<p>The small secondary graphic is a fixed size. The
graphic may shift up or down as needed to
work with the photo; do not shift the graphic
left or right. In this layout, the secondary
graphic equals 10 square units high and wide,
scaled as shown.</p>

<p>The background may be white,
the approved pattern or a tinted neutral color
(see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>). The bottom edge of the
background should align to the logo.</p>
</div>

<div class="col-md-4">
<p><img src="img/print-posters-24-36-1.png" alt="Small formats horizontal" class="img-responsive center-block" /></p>
</div>
</div>

<div class="row">
<div class="col-md-8">


<p><strong>Large secondary graphic</strong></p>
<p>The large secondary graphic is a fixed size
and in a fixed position. The right edge of the
graphic aligns to the right edge of the poster.
Do not move the graphic. In this application,
the secondary graphic equals 15 square units
high and wide, scaled as shown.</p>
<p>The larger square in the secondary graphic may
be used for photos. In this instance, titles should
be a solid red from the approved palette. The
background may be white, the approved pattern,
or a tinted neutral color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>).
The bottom edge of the background should
align to the top horizontal line of the E symbol.</p>

<p><strong>Horizontal formats</strong></p>
<p>Refer to the separate InDesign template for
horizontal formatted poster layouts.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>

</div>

<div class="col-md-4">
<p><img src="img/print-posters-24-36-2.png" alt="Small formats horizontal" class="img-responsive center-block" /></p>
</div>
</div>



    
    </div>
    
    
    
  </div>
  
  
  
  

    </div>

  
    </div>
    
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
