﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-one-edwards" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>One Edwards</li>
  </ol>
  
  
  
  <h2>One Edwards
  <a href="pdf/p-1edwards.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
  
  
  
  <div class="well intro">
  <p>We have adopted a strategy of &ldquo;One Edwards.&rdquo;
We believe that by reinforcing our corporate
brand externally to build equity and awareness,
we will envelop and boost all of our business
functions and business units individually.
We endeavor to portray one company, one
brand and one consistent image to the outside
world. As such, we have streamlined our
communications approach, creating one set
of standards and templates across the entire
company - every business function, every
business unit and every region.</p>
<p><strong>These brand standards apply to most internal and
external materials.</strong></p>
</div>

<p>For more information or clarification about the use of these standards,
please contact:</p>

<div class="row">
<div class="col-md-5">
<p><strong>Becky Kibbey</strong><br>
<a href="mailto:Becky_Kibbey@edwards.com"><span class="icon-envelop"></span> Becky_Kibbey@edwards.com</a><br>
<span class="icon-phone"></span> 949-250-3455</p>

</div>

<div class="col-md-5">
<p><strong>Erin Brankov</strong><br>
<a href="mailto:Erin_Brankov@edwards.com"><span class="icon-envelop"></span> Erin_Brankov@edwards.com</a><br>
<span class="icon-phone"></span> 949-250-2436</p>

</div>
</div>

<div class="row">
 <div class="col-md-12">
<p>For questions about the templates, artwork, or image library, contact:</p></div>
<div class="col-md-5">
<p><strong>Jade Jourdan</strong><br>
<a href="mailto:Jade_Jourdan@edwards.com"><span class="icon-envelop"></span> Jade_Jourdan@edwards.com</a><br>
<span class="icon-phone"></span> 949-250-3560</p>
</div>
</div>
  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
