﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->

<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="60-logo" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg banner-anniversary">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>60th Anniversary logo lockup standards</li>
  <li>Logo lockup</li>
  </ol>
  
  
  
  <h2>Logo lockup
  <a href="pdf/logo-lockup.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="https://ewdigstrategy.blob.core.windows.net/standards/templates/60th-lockup-logos.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download all logos"><i class="icon-download"></i></a></h2>
 
   <p><a href="https://ewdigstrategy.blob.core.windows.net/standards/templates/60th-lockup-logos.zip" class="btn btn-info" role="button"><i class="icon-download"></i> Download all 60th Anniversary logos</a></p>
   
  <div class="clearfix"></div>
 
  <div class="well intro"><p>This custom drawn lockup may be
used with the Edwards logo for a
limited time during the 2018 calendar
year on most communications that
will either be used exclusively in
2018, or can easily be updated to
remove the <em>60 Years of Discovery</em> type
treatment for continued use, such
as journal ads, exhibit graphics, web
banners, etc.</p>
</div>
<p>The lockup includes the <em>60 Years of Discovery</em>
treatment to the left of the Edwards logo
(Version 1). The custom type treatment
should only be used with the Version 1
vertical logo, and never with the Version 2 or
Version 3 horizontal logo.</p>
 
<div class="row">
  <div class="col-md-8 col-md-push-4">
<p><img src="img/logo-lockup-1.png" class="img-responsive"></p>
</div>
<div class="col-md-4 col-md-pull-8">

<p><strong>Positive logos</strong></p>
<p>The positive logo may print in the following
colors:</p>
<ul>
  <li>Spot color (PANTONE 423 and 186)</li>
  <li>Process color (CMYK)</li>
  <li>Titanium and Primary Red (PANTONE 8421 and 186)</li>
  <li>Grayscale</li>
  <li>100% black</li>
</ul>
<p>The 100% black logo should only be used on
premium items, for etching or when limited
to one color without tints.</p>

</div>


</div>
<div class="row">
  <div class="col-md-8 col-md-push-4">
<p><img src="img/logo-lockup-2.png" class="img-responsive"></p>
</div>
<div class="col-md-4 col-md-pull-8">
<p><strong>Reverse logos</strong></p>
<p>Reverse color logos are available in spot color,
process color, titanium and grayscale. In all
reverse versions, the <em>Years of Discovery</em>
message, horizontal lines and Edwards name
appear white.</p>

</div>



</div>



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
