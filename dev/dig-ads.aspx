﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="dig-ads" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Digital media</li>
  <li>Digital ads</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Digital ads<a href="pdf/p-digital-ads.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/edwards-dig-ads.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the templates"><i class="icon-download"></i></a>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Square</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Horizontal</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Vertical</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Square</h3>   

<p>Digital ads may be static or animated. For
example, an animated image could appear
followed by a message, ultimately revealing the
headline, secondary graphic, body copy and logo.
The elements should be placed as shown here.</p>
<div class="row">
<div class="col-md-4">
<p><strong>Small secondary graphic</strong></p>
<p>The small secondary graphic may be 15 or 25
units wide with a photo behind it. The photo
must fill the placeholder area. The height of the
cropped secondary graphic may be adjusted
to work with different amounts of text.</p>
</div>


<div class="col-md-8"><img src="img/dig-ads-sq-1.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-4">
<p><strong>Large secondary graphic</strong></p>
<p>The width of the large secondary graphic in this
layout equals 25 square units wide. The height
of the larger shape in the secondary graphic
may be adjusted to work with various amounts
of text. Ensure that text is always legible. The
smaller square should remain a solid red square.</p>
</div>
<div class="col-md-8"><img src="img/dig-ads-sq-2.png" class="img-responsive"></div>
</div>

<div class="row">
<div class="col-md-12">

<p><strong>Text</strong></p>
<p>Digital ads should be written to communicate
with a limited amount of copy. Use text sizes
that work with the amount of content and
the ad size. Text should be easily legible. Ads
that make product claims may need to have
important safety information scrolling within
the ad. Three lines of copy must be visible
within the ad, then the remaining text may
scroll (it may not be a click away).</p>
<p><strong>Logo</strong></p>
<p>For 336 x 280px ads, the logo is 38px based on
the height of the E symbol.</p>
<p><strong>Different ad sizes</strong></p>
<p>There are many different ad sizes. Base other
layouts on the samples shown here. Scale
elements proportionally, making adjustments
to work with the desired dimensions.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>
</div>



</div>



    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Horizontal</h3>   

<div class="row">
<div class="col-md-3">
<p><strong>Small secondary graphic</strong></p>
<p>In horizontal digital ads, the small secondary
graphic may be 20 or 30 square units wide
with a photo in the approved corporate style
appearing behind it. The photo must fill the
entire placeholder area. The height of the
larger shape in the secondary graphic may
be adjusted to work with different amounts
of text.</p>
</div>

<div class="col-md-9"><img src="img/dig-ads-horz-1.png" class="img-responsive center-block"></div>
</div>
<div class="row">
<div class="col-md-3">
<p><strong>Large secondary graphic</strong></p>
<p>The width of the large secondary graphic in
this layout equals 40 square units wide. A
photo must entirely fill the larger shape in the
secondary graphic, or it should remain red.
Ensure that text is always legible. The smaller
square should remain a solid red square.</p>
</div>
<div class="col-md-9"><img src="img/dig-ads-horz-2.png" class="img-responsive center-block"></div>
</div>

<div class="row">
<div class="col-md-12">


<p><strong>Text</strong></p>
<p>Digital ads should be written to communicate
with a limited amount of copy. Use text sizes
that work with the amount of content and
the ad size. Text should be easily legible. Ads
that make product claims may need to have
important safety information scrolling within
the ad. Three lines of copy must be visible
within the ad, then the remaining text may
scroll (it may not be a click away).</p>
<p><strong>Logo</strong></p>
<p>For 728 x 90px ads, the logo is 50px based on
the height of the E symbol.</p>
<p><strong>Different ad sizes</strong></p>
<p>There are many different digital ad sizes.
Base other layouts on the samples shown
here. Scale elements proportionally, making
adjustments horizontally and/or vertically to
work with the desired dimensions.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>

</div>

</div>

    
    
   </div>
   
   <div class="pane" id="tabs-3">
    <h3>Vertical</h3>   

<div class="row">
<div class="col-md-7">

<p><strong>Small secondary graphic</strong></p>
<p>In vertical digital ads, the small secondary
graphic is 15 square units wide with a photo
in the approved corporate style appearing
behind it. The photo must fill the entire
placeholder area. The vertical position of the
secondary graphic may be adjusted to work
with the underlying photo.</p></div>

<div class="col-md-5"><img src="img/dig-ads-vert-1.jpg" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-7">

<p><strong>Large secondary graphic</strong></p>
<p>The large secondary graphic is also 15 square
units wide. A photo must entirely fill the
larger shape in the secondary graphic. Ensure
that text is always legible. The smaller square
should remain a solid red square.</p>
</div>
<div class="col-md-5"><img src="img/dig-ads-vert-2.jpg" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-12">

<p><strong>Text</strong></p>
<p>Digital ads should be written to communicate
with a limited amount of copy. Use text sizes
that work with the amount of content and
the ad size. Text should be easily legible Ads
that make product claims may need to have
important safety information scrolling within
the ad. Three lines of copy must be visible
within the ad, then the remaining text may
scroll (it may not be a click away).</p>
<p><strong>Logo</strong></p>
<p>For 160 x 600px ads, the logo is 38px based
on the height of the E symbol. The photo may
extend from the top of the page down as far
as the top horizontal line within the E symbol.
The logo may shift up or down based on the
amount of content or to work with the photo.</p>
<p><strong>Different ad sizes</strong></p>
<p>There are many different digital ad sizes.
Base other layouts on the samples shown
here. Scale elements proportionally, making
adjustments horizontally and/or vertically to
work with the desired dimensions.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>
</div>

</div>

    
    
   </div>
   
   
   
    
    

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
