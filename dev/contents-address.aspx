﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="contents-address" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Contents</li>
  <li>Address lines</li>
  </ol>
  
  
  
  <h2>Address lines<a href="pdf/p-address.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  
<p>The following three address lines are the only acceptable forms for displaying
the Edwards Lifesciences name on applicable materials. Address lines are
limited to three (3) lines of text.</p>

<p>In printed materials, type should be Vesta Std Bold and Regular, 7/9pt.</p>
  
  

<div class="well">
<p class="h3"><em>Option 1 (preferred):</em></p>
<p><strong>Edwards Lifesciences</strong> • One Edwards Way, Irvine CA 92614 USA • edwards.com</p>
<p><strong>Edwards Lifesciences</strong> • Route de l’Etraz 70, 1260 Nyon, Switzerland • edwards.com</p>
</div>

<div class="well">

<p class="h3"><em>Option 2 (for narrow formats):</em></p>

<p>
<strong>Edwards Lifesciences</strong><br>
One Edwards Way, Irvine CA 92614 USA<br>
edwards.com
</p>
<p>
<strong>Edwards Lifesciences</strong><br>
Route de l’Etraz 70, 1260 Nyon, Switzerland<br>
edwards.com
</p>
</div>


<div class="well">

<p class="h3"><em>Option 3 (two-line):</em></p>
<p>
<strong>Edwards Lifesciences</strong> • edwards.com<br>
One Edwards Way, Irvine CA 92614 USA</p>
<p>
<strong>Edwards Lifesciences</strong> • edwards.com<br>
Route de l’Etraz 70, 1260 Nyon, Switzerland</p>

</div>






<p><em>Note:</em> In some countries, it is required to include the company’s full operating
name specific to the area where materials are to be distributed. Please consult
with local legal and/or regulatory regarding specific requirements.</p>


</div>



    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
