﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="elements-photo" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Basic elements</li>
  <li>Photography</li>
  </ol>
  
  
  
  <h2>Photography
  <a href="pdf/p-photography.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
  
  
  
  <div class="well intro">
  <p>Our photo style helps unify our
communications, whether a
corporate brochure or a product ad.
The subjects in photos will differ,
but they will have a common feel
in their light, bright backgrounds.</p>
</div>

<div class="row">
<div class="col-md-4">
<p><strong>Lifestyle</strong><br>

Portraits and lifestyle photography
reinforces our dedication to patients and our
collaboration with clinicians as a primary pillar
of our brand. Whether it’s a corporate, product
or patient/people focused communication,
lifestyle photos should look consistent.</p>
<p>When selecting stock photos or shooting
new ones – interior or exterior environments
– photos should have a light background to
reflect a clean, modern look. They should
appear airy and open, not dark or heavy.
Backgrounds should appear white or
light. Refer to the neutral color palette for
suggested hues.</p>

<p>When possible, photos should highlight
our red palette through the use of neutral
backgrounds, but it should not feel forced.
In essence, do not allow other colors – such
as blues, greens or blacks – to become the
dominant element. Our secondary graphic
should remain the dominant element.</p>
</div>
<div class="col-md-8">

<img src="img/photography-1.png" alt="photography" class="img-responsive" /><br>
<br>
<a href="elements-image-library.aspx" class="btn btn-info" role="button"><i class="icon-circle-right"></i> Go to image library</a>
</div>
</div>

<div class="row">
<div class="col-md-4">
<p><strong>Products</strong><br>

When products are photographed they
should appear on a white, light gray or
titanium-looking background. Backgrounds
can have subtle shifts in tones, but avoid
dramatic lighting or flat colors. The
background hues should mimic the colors
seen in the neutral palette; not bright,
saturated colors.</p>

</div>

<div class="col-md-8">

<img src="img/photography-2.png" alt="photography" class="img-responsive" />
</div>
</div>

  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
