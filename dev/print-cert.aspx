﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="print-cert" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Print collateral</li>
  <li>Certificates</li>
  </ol>
  
  
  
  <h2>Certificates
  <a href="pdf/p-cert.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/edwards-certs.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the templates"><i class="icon-download"></i></a></h2>
  <div class="clearfix"></div>
  
  
 
<div class="row">
<div class="col-md-5">
<p>Two certificate layouts have been designed
and are available in a PowerPoint template.
Both horizontal layouts include a large
secondary graphic where the larger shape is a
tint of gray and all content is placed within it.
One layout includes a white background, and
the other includes a background pattern to
give the certificate a finessed look.</p>
<p>Because most office printers cannot print
full-bleed, these layouts have been designed
to include a 0.30" white border on all sides
when printed.</p>
<p>Each design element has been carefully sized
and placed within the template. Do not scale,
distort or rearrange these elements.</p>
<p><strong>Typography</strong></p>
<p>All typography – including the certificate title,
recipient’s name and description – is editable.
The template was designed using Arial for all
levels of content. Always use the text boxes
built into the template.</p>
<p>The certificate title and date are 100% primary
red; all other messages are dark gray.</p>
</div>

<div class="col-md-7">

<img src="img/print-certificates.png" alt="Certificates" class="img-responsive" />
</div>
</div>

<div class="row">
<div class="col-md-12">

</div>
</div>

  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->


<!-- #include file="contact-form.html" -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
