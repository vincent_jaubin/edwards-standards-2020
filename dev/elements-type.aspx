﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="elements-type" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Basic elements</li>
  <li>Typography</li>
  </ol>
  
  
  
  <h2>Typography
  <a href="pdf/p-type.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
  
  
  
  <div class="well intro">
  <p>Typography isn’t just what we say, it’s how we say it.
Vesta Std is a humanist typeface that balances the simple
lines of a sans-serif with the legibility of a serif. In essence,
it’s modern yet approachable.</p>
</div>

<div class="row">
<div class="col-md-4">
<p><strong>Primary</strong><br>

<p>Vesta Std is our preferred typeface and should be used for print,
promotional and environmental applications. It is available in four
weights and styles, however, keep the number of styles and sizes to a
minimum. Use Regular weight for titles, headlines, subtitles and body
copy. Use Bold for subheads or lead-in text. Italics may be used for pull quotes
or to add emphasis to body copy.</p>
</div>

<div class="col-md-8">

<img src="img/typography-1.png" alt="typography" class="img-responsive left-red-border" />
</div>
</div>

<p>Our typographic style is simple: instead of relying on multiple typefaces
to create hierarchy, use either a size or weight shift, not both. For
example, titles and subtitles are both Vesta Std Regular, but titles are
larger to distinguish them from subtitles. Subheads and body copy are
the same size, but subheads are Bold. In general, maintain as few type
sizes as possible and use available templates.</p>
<p>Typography should be flush-left/ragged-right, not centered or justified.</p>
<div class="row">
<div class="col-md-4">
<p><strong>Web, mobile & apps</strong></p>
<p>For websites, apps or other digital applications, Merriweather Sans has
been selected as the preferred typeface because it has characteristics
similar to Vesta Std. Use Merriweather Sans as you would Vesta Std –
Regular weight for titles, subtitles and body copy, Bold for subheads or
lead-in text, and Italics to add emphasis.</p>
</div>

<div class="col-md-8">

<img src="img/typography-2.png" alt="typography" class="img-responsive left-red-border" />
</div>
</div>
<div class="row">
<div class="col-md-4">
<p><strong>Email & office applications</strong></p>
For emails and office applications, such as PowerPoint, use Arial Regular,
Arial Italic, Arial Bold or Arial Bold Italic.</p>

</div>

<div class="col-md-8">

<img src="img/typography-3.png" alt="typography" class="img-responsive left-red-border" />
</div>
</div>

<p><strong>Capitalization</strong></p>
<p>Only capitalize the first word and proper names in titles or headlines.
Title Case May Also Be Used. AVOID ALL CAPS.</p>
<p><strong>Color</strong></p>
<p>Color plays an important role with typography. Titles and headlines
may be primary red, a secondary red, or white on a primary red
background. Subtitles may be a red, gray, black, or white on a primary
red background. All other copy may be a gray or black. Do not mix or
alternate colors within headlines, sentences or paragraphs.</p>
  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
