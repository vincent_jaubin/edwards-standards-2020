﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="env-trade" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Environmental Graphics</li>
  <li>Trade shows</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Trade shows<a href="pdf/p-tradeshows.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab active"><a href="#tabs-1" class="match-height"><span>Overview</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Skyline graphics</span></a></li>
  <li><a href="#tabs-3" class="match-height"><span>Primary digital displays</span></a></li>
  <li><a href="#tabs-4" class="match-height"><span>Eye-level graphics</span></a></li>
  <li><a href="#tabs-5" class="match-height"><span>Identifiers</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Overview</h3>   

<div class="well intro"><p>Trade show graphics must attract,
engage, inform and direct clients in
a busy, competitive environment.</p></div>

<div class="row">
<div class="col-md-4">

<p><strong>Different purposes</strong></p>
<p>Skyline graphics clearly identify the Edwards
booth location on an often chaotic trade
show floor. Primary digital displays combine
video, imagery, color and messages to engage
clients on multiple topics as they approach
and explore the booth. Eye-level graphics
further inform clients as they dig into specific
topics. And identifiers provide direction and
organization within the booth.</p>
<p><strong>Different content</strong></p>
<p>Each level of graphics uses a different mix of
our brand elements – logo, secondary graphic,
corporate imagery/messages/colors, and
product imagery/messages. The chart on the
right identifies which elements should be
used on each level.</p>
</div>
<div class="col-md-8"><img src="img/tradeshows-1.png" class="img-responsive"></div>
</div>





    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Skyline graphics</h3>   

<div class="well intro"><p>Skyline graphics clearly identify the
Edwards booth location on an often
chaotic trade show floor.</p></div>
<div class="row">
<div class="col-md-4">
<p><strong>Outside panels</strong></p>
<p>The outside panels of skyline graphics – the
large signs that hang above a trade show
booth – must display the Edwards logo against
a white background.</p>
<p>The secondary graphic is recommended. The
examples here use secondary graphics that are
either 25 or 15 units wide, top cropped. See
  <a href="elements-secondary.aspx" target="_blank">secondary graphic</a> page for more information about sizing and
cropping secondary graphics.</p>
<p>Since clients may approach from all sides,
the same graphics should appear on all
outside panels.</p>
<p>Do not include imagery, messages or colors
on outside panels.</p>
</div>
<div class="col-md-8"><img src="img/tradeshows-2.png" class="img-responsive"></div>
</div>

<div class="row">
<div class="col-md-12">


<p><strong>Inside panels</strong></p>
<p>As the inside panels are not always visible
from a distance, their purpose is to reinforce
the Edwards identity as a client approaches,
enters and explores the booth.</p>
<p>The inside panels of skyline graphics can
contain supporting corporate photos,
messages and/or fields of approved colors.
Keep inside panels simple.</p>
<p>Do not use the Edwards logo or product
imagery/messages on the inside panels.</p>
<p>The skyline graphic is the primary location
of the Edwards logo. Other static graphics
within the booth environment do not require
the logo, but animations should include it at
least once.</p>

</div>

</div>

    
    
   </div>
   
   <div class="pane" id="tabs-3">
    <h3>Primary digital displays</h3>   

<div class="well intro"><p>Primary digital displays combine
video, imagery, color and messages
to engage clients on multiple topics
as they approach and explore the
trade show booth.</p></div>
<div class="row">
<div class="col-md-4">
<p>Animation loops should use corporate
imagery and messages (or product imagery
and messages as appropriate) along with
approved corporate colors and typography.</p>
<p>The Edwards logo does not have to (and
should not) appear on every screen, but it
must appear as a sign-off at the end of a loop
or as a transition between loops.</p>
<p>Likewise, the secondary graphic does not
have to appear on every screen, but should be
used somewhere in each loop.</p>
</div>
<div class="col-md-8"><img src="img/tradeshows-3.png" class="img-responsive"></div>
</div>



    
    
   </div>
   
   <div class="pane" id="tabs-4">
    <h3>Eye-level graphics</h3>   

<div class="well intro"><p>Eye-level graphics inform clients as
they dig into specific topics within
the trade show booth.</p></div>
<div class="row">
<div class="col-md-4">
<p>Eye-level graphics may be static or animated.
Use corporate imagery and messages
(or product imagery and messages as
appropriate) along with approved corporate
colors and typography.</p>
<p>The Edwards logo does not need to appear on
every eye-level graphic since it appears on the
skyline graphic. However, use the logo as a
transition between animation loops.</p>
<p>The secondary graphic is also recommended
but not required.</p>
<p>When several eye-level graphics are used
together as a series, consistent formatting is
recommended.</p>
</div>
<div class="col-md-8"><img src="img/tradeshows-4.png" class="img-responsive img-margin"></div>
</div>

<div class="row">
<div class="col-md-4">
<p>Eye-level murals may include corporate or
product images, messages, approved colors
and typography.</p>
<p>Use either the small or large secondary graphic
for the headline. Other support graphics –
such as call-outs, charts or diagrams – may
also be incorporated as long as their design
does not compete with the secondary
graphic. To help organize such information,
use vertical or horizontal rules, or translucent
linear gradients (not solid colors) behind the
information to help organize the content.</p>
<p>The Edwards logo does not need to appear on
murals when the graphic is used in a booth
environment with a skyline graphic. However,
if used as a free standing graphic outside of a
booth environment where the Edwards logo
is not visible, the logo must be used. See other
applications – such as advertising layouts – for
placing the logo, but scale up and adjust the
elements as necessary.</p>
</div>


<div class="col-md-8"><img src="img/tradeshows-5.png" class="img-responsive"></div>

</div>



    
    
   </div>
   
   <div class="pane" id="tabs-5">
    <h3>Identifiers</h3>   

<div class="well intro"><p>Identifiers provide direction
and organization within the trade
show booth.</p></div>
<div class="row">
<div class="col-md-4">
<p>Miscellaneous signs can be used to identify
business units or initiatives, and to provide
product-specific information.</p>
<p>Typography, color and imagery are the main
components of these functional identifiers.</p>
<p>Use Vesta Std for all levels of messaging.
Keep the number of styles and weights to
a minimum. As a general rule, make only
one change to a particular body of copy to
add emphasis: either change the size, or the
weight, not both. Keep layouts clean with
plenty of white space.</p>
</div>
<div class="col-md-8"><img src="img/tradeshows-6.png" class="img-responsive"></div>
</div>

    
    
   </div>
    
    

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
