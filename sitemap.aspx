﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="meta-top.html" -->
        <style>
        .main-content ol li:not(:last-child),
        .main-content ul li:not(:last-child) {
            padding-bottom: 12px;
        }
        </style>
    </head>

    <body>
        <!-- #include file="contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="header.html" -->
            <div id="elements-altlogo" class="page">
                <div class="jumbotron banner-edw banner-branding">
                    <div class="container">
                        <div class="redboxes">
                            <div class="redbox-sm"></div>
                            <div class="redbox-lg">
                                <h1>Site map</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.banner-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 main-content">
                            <ol class="breadcrumb">
                                <li><a href="index.aspx">Home</a></li>
                                <li>Site map</li>
                            </ol>
                            <h2>Site map</h2>
                            <h3>Corporate Brand Identity Standards</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <p><strong>General</strong></p>
                                    <ul>
                                        <li><a href="index.aspx">Home</a></li>
                                        <li><a href="http://www.edwards.com/legal/privacypolicy" target="_blank">Privacy policy</a></li>
                                        <li><a href="http://www.edwards.com/legal/legalterms" target="_blank">Legal terms</a></li>
                                        <li><a href="sitemap.aspx">Site map</a></li>
                                    </ul>
                                    <p><strong>Contents</strong></p>
                                    <ul>
                                        <li><a href="contents.aspx" class="contents-one-edwards">One Edwards</a></li>
                                        <li><a href="contents-foundation.aspx" class="contents-foundation">Brand foundation</a></li>
                                        <li><a href="contents-turning.aspx" class="contents-turning">Turning the inside out</a></li>
                                        <li><a href="contents-corp-nomen.aspx" class="contents-corp-nomen">Corporate nomenclature</a></li>
                                        <li><a href="contents-dig.aspx" class="contents-dig">Digital content</a></li>
                                        <li><a href="contents-secondary.aspx" class="contents-secondary">Secondary campaigns</a></li>
                                        <li><a href="contents-awareness.aspx" class="contents-awareness">Awareness branding</a></li>
                                        <li><a href="contents-cobranding.aspx" class="contents-cobranding">Co-branded &amp; unbranded materials</a></li>
                                        <li><a href="contents-reg.aspx" class="contents-reg">Regulatory &amp; legal requirements</a></li>
                                        <li><a href="contents-address.aspx" class="contents-address">Address lines</a></li>
                                        <li><a href="contents-visual.aspx" class="contents-visual">Visual representation</a></li>
                                    </ul>
                                    <p><strong>Elements</strong></p>
                                    <ul>
                                        <li><a href="elements.aspx" class="elements-logo">Logo usage</a></li>
                                        <li><a href="elements-color.aspx" class="elements-color">Color</a></li>
                                        <li><a href="elements-type.aspx" class="elements-type">Typography</a></li>
                                        <li><a href="elements-photo.aspx" class="elements-photo">Photography</a></li>
                                        <li><a href="elements-secondary.aspx" class="elements-secondary">Secondary graphic</a></li>
                                        <li><a href="elements-bg-pattern.aspx" class="elements-bg-pattern">Backgrounds</a></li>
                                    </ul>
                                    <p><strong>Environmental graphics</strong></p>
                                    <ul>
                                        <li><a href="env-banners.aspx" class="env-banners">Banners</a></li>
                                        <li><a href="env-trade.aspx" class="env-trade">Trade shows</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <p><strong>Print Collateral</strong></p>
                                    <ul>
                                        <li><a href="print-covers.aspx" class="print-covers">Covers</a></li>
                                        <li><a href="print-content-sheets.aspx" class="print-content-sheets">Content sheets</a></li>
                                        <li><a href="print-interior.aspx" class="print-interior">Interior layouts</a></li>
                                        <li><a href="print-back-pages.aspx" class="print-back-pages">Back page</a></li>
                                        <li><a href="print-pamphlets.aspx" class="print-pamphlets">Pamphlets</a></li>
                                        <li><a href="print-small-formats.aspx" class="print-small-formats">Small formats</a></li>
                                        <li><a href="print-postcards.aspx" class="print-postcards">Postcards</a></li>
                                        <li><a href="print-posters.aspx" class="print-posters">Posters</a></li>
                                        <li><a href="print-cert.aspx" class="print-cert">Certificates</a></li>
                                        <li><a href="print-circular.aspx" class="print-circular">Circular formats</a></li>
                                        <li><a href="print-other-sizes.aspx" class="print-other-sizes">Other sizes</a></li>
                                        <li><a href="print-word.aspx" class="print-word">Collateral in Microsoft Word</a></li>
                                    </ul>
                                    <p><strong>Advertising</strong></p>
                                    <ul>
                                        <li><a href="ads-single.aspx" class="ads-single">Single page print ads</a></li>
                                        <li><a href="ads-print.aspx" class="ads-print">Print ad spreads</a></li>
                                        <li><a href="ads-lg-format.aspx" class="ads-lg-format">Large format ads</a></li>
                                    </ul>
                                    <p><strong>Digital media</strong></p>
                                    <ul>
                                        <li><a href="dig-ads.aspx" class="dig-ads">Digital ads</a></li>
                                        <li><a href="dig-websites.aspx" class="dig-websites">Websites</a></li>
                                        <li><a href="dig-practices.aspx" class="dig-practices">SEO / HTML best practices</a></li>
                                        <li><a href="dig-apps.aspx" class="dig-apps">Apps</a></li>
                                        <li><a href="dig-emails.aspx" class="dig-emails">Emails</a></li>
                                        <li><a href="dig-video.aspx" class="dig-video">Videos &amp; animations</a></li>
                                        <li><a href="dig-pp.aspx" class="dig-pp">PowerPoint presentations</a></li>
                                    </ul>
                                    
                                    <p><strong>Patient awareness</strong></p>
                                    <ul>
                                        <li><a href="pat-color.aspx" class="pat-color">Color &amp; typography</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /row -->
                            <h3>Business Unit Brand Standards</h3>
                            <h4>Heart valve therapy</h4>
                            <div class="row">
                              <div class="col-md-6">
                                <p><strong>Contents</strong></p>
                                    <ul>
                                        <li><a href="/hvt/intro.aspx" class="hvt-intro">Introduction</a></li>
                                        <li><a href="/hvt/tiered.aspx" class="hvt-tiered">Tiered communication strategy</a></li>
                                    </ul>
                                 <p><strong>Tier 1 brand books</strong></p>
                                    <ul>
                                        <li><a href="/hvt/pro-edu.aspx" class="hvt-pro-edu">Professional education</a></li>
                                        <li><a href="/hvt/clinical.aspx" class="hvt-clinical">Clinical trials</a></li>
                                    </ul>
                                <p><strong>Tier 2 brand books</strong></p>
                                    <ul>
                                        <li><a href="/hvt/transforming-tomorrow.aspx" class="transforming-tomorrow">Transforming tomorrow campaign</a></li>
                                        <li><a href="/hvt/employee.aspx" class="employee">Employee communication initiative</a></li>
                                        <li><a href="/hvt/resilia-tissue.aspx" class="hvt-resilia-tissue">RESILIA tissue</a></li>
                                    </ul>
                              </div>
                              <div class="col-md-6">
                              <p><strong>Tier 3 brand books</strong></p>
                                    <ul>
                                        <li><a href="/hvt/inspiris.aspx" class="hvt-inspiris">INSPIRIS RESILIA aortic valve</a></li>
                                        <li><a href="/hvt/intuity-elite.aspx" class="hvt-intuity-elite">EDWARDS INTUITY Elite valve system</a></li>
                                        <li><a href="/hvt/magna-ease.aspx" class="hvt-magna-ease">Magna Ease valve</a></li>
                                        <li><a href="/hvt/magna-mitral.aspx" class="hvt-magna-mitral">Magna Mitral Ease valve</a></li>
                                        <li><a href="/hvt/perimount-aortic.aspx" class="hvt-perimount-aortic">PERIMOUNT aortic valve</a></li>
                                        <li><a href="/hvt/perimount-mitral.aspx" class="hvt-perimount-mitral">PERIMOUNT mitral valve</a></li>
                                        <li><a href="/hvt/ce-physio-II.aspx" class="hvt-ce-physio-II">Carpentier-Edwards Physio II ring</a></li>
                                        <li><a href="/hvt/physio-tricuspid.aspx" class="hvt-physio-tricuspid">Physio tricuspid ring</a></li>
                                        <li><a href="/hvt/mc3.aspx" class="hvt-mc3">MC3 ring</a></li>
                                        <li><a href="/hvt/annuloplasty.aspx" class="hvt-annuloplasty">General annuloplasty rings</a></li>
                                    </ul>
                                </div>
                            </div><!-- /row --> 
                                <h3>Digital Assets</h3>
                            <div class="row">
                              <div class="col-md-6">
                                    <ul>
                                        <li><a href="/elements-image-library.aspx" class="elements-image-library">Corporate image library</a></li>
                                        <li><a href="https://edwards.webdamdb.com">WebDam</a></li>
                                    </ul>
                              </div>
                                <div class="col-md-6">
                                    <!-- Future content -->
                                </div>
                            </div><!-- /row --> 

                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="meta-bottom.html" -->
    </body>

    </html>