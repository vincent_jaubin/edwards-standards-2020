// JavaScript Document

jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 300,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

});


//top menu functionality
$(window).load(function() {
    $('body').show();
	$('.first-tab').addClass('active');
	
//resize fixed edw tabs after 992px width
// remove 15 pixels for computed window width

$(window).resize(function(){
   console.log('resize called');
   var width = $(window).width();
	var getPaneWidth = $('.pane').css('width');
   if(width >= 977){
       $('.edw-tabs').addClass('resizeFixedBar');
	   var newFixedClass = $('.resizeFixedBar');
	   $(newFixedClass).css('width',getPaneWidth);
   }
   else{
        $('.edw-tabs').removeClass('resizeFixedBar');
		$('.edw-tabs').removeAttr('style');
   }
})
.resize();//trigger the resize event on page load.


$('a').click(function() {

    var hRef = $(this).attr("href");

    if (hRef == "#") {
        alert('That content is coming soon!');
    }

});

  $('[data-toggle="tooltip"]').tooltip(); 

//smooth anchor scroll
$(function() {
  $('.nav li a[href*="#"]:not([href="#"],a[href*="submenu"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 300);
        return false;
      }
    }
  });
});



	
});


$('.edw-menu .dropthis').hover(function() {
    if (!($(this).children('a').hasClass('selected'))) {
        $(this).toggleClass('active-link');
        $(this).find('.edw-submenu').stop(true, false, true).slideToggle(300);

    };

});






// match height

$(function() {
    $('.match-height').matchHeight();
	var theHeight = $('.match-height').outerHeight();
    var lineHeight = theHeight - 24 + 'px';
	$('.match-height').css('line-height',lineHeight);
});

$(function() {
    $('.imagethumbs .match-height').matchHeight({
        target: $('.thumbnail')
    });
});


//script to show active page on side menu
$('#submenu-accordion .list-group-item a').each(function() {
    if ($(this).attr("class") == $('.page').attr("id")) {
        $(this).parent().parent().addClass('in');
        $(this).addClass('activeside');
    }
});

/*$('#submenu-accordion .list-group-item a').each(function() {
	
    if (($(this).attr('class') == $('.page').attr('id')) && (($(this).attr('class').indexOf('+') <= -1))) {
        $(this).parent().parent().addClass('in');
        $(this).addClass('activeside');
    } 
	else if (($(this).attr('class').indexOf('+') > -1)) {
        
    } 
});*/



// Javascript to enable link to tab
var url = document.location.toString();
console.log(url);
if (url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
} 

// Change hash for page-reload
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
});

// Accordian color change

  $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });

// Accordion Arrow

$('.collapse').on('show.bs.collapse', function(){
$(this).parent().find(".glyphicon-triangle-bottom").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-top");
}).on('hide.bs.collapse', function(){
$(this).parent().find(".glyphicon-triangle-top").removeClass("glyphicon-triangle-top").addClass("glyphicon-triangle-bottom");
});


// Link to other accordions on same page

$('.panel-body a').not('a.carousel-control').click(function(){

var link = $(this).attr('href').replace('#','');

$('#' + link).collapse('show');

$('html,body').animate({
        scrollTop: $('#' + link).offset().top - 30},
        'slow');

});

// Link to other accordions if they are on a different page

$(window).load(function(){
var type = window.location.hash.substr(1);

if(type.length) {

$('#' + type).collapse('show');

$('html,body').animate({
        scrollTop: $('#' + type).offset().top - 20},
        'slow');
}
});
                                                        

