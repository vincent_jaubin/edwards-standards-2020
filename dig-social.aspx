﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="dig-social" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Digital media</li>
  <li>Social media</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Social media<a href="pdf/p-powerpoint.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
 
 <a href="https://ewdigstrategy.blob.core.windows.net/standards/templates/Metric-PPT-Templates.zip" class="btn-share" role="button" data-toggle="tooltip" title="" data-original-title="Download the metric templates"><i class="icon-download"></i> <span class="share-txt">Metric</span></a>
 
 <a href="https://ewdigstrategy.blob.core.windows.net/standards/templates/US-PPT-Templates.zip" class="btn-share" role="button" data-toggle="tooltip" title="" data-original-title="Download the U.S. templates"><i class="icon-download"></i> <span class="share-txt">U.S.</span></a>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Profile and header images</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Ads</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Profile and header images</h3>   


 <div class="well intro">
    <p>Profile and header image sizes vary
        from site to site. For cover images,
        use the secondary graphic if possible,
        but a simple image is also acceptable.
    </p>
</div>
<div class="row">
<div class="col-md-4">
<p><strong>Facebook</strong></p>

<p>
    A Facebook business page profile image must
    be at least 180 x 180 px. Use version 1 of the
    Edwards logo on a white background.
</p>

<p>
    A header image will look different in a browser
    than on mobile. On mobile apps, the left and
    right sides of the image will be cropped. If the
    secondary graphic or text is used, make sure it
    fits within the mobile area. Keep messages brief
    and avoid using text elsewhere in the image.
</p>

<p><strong>Twitter</strong></p>

<p>
    The Twitter profile image is 400 x 400 px. Use
    version 1 of the logo on a white background.
</p>





    
</div>
<div class="col-md-8"><p><img src="img/dig-social-image-1.png" class="img-responsive"></p></div>
</div>
<div class="row">
    <div class="col-md-12">
        <p>
            The Twitter image size is fixed at 1500 x 500
            px. When viewed on a desktop, the top and
            bottom of the header image may be cropped
            depending on the scale of the browser window.
            If using the secondary graphic, only use a large
            version along the top with a brief message.
        </p>
        <p><strong>LinkedIn</strong></p>

<p>
    The LinkedIn company logo image is 400 x
    400 px. Use version 1 of the logo on white.
</p>

<p>
    The header image will be covered by the
    company logo and other information on
    desktops. To avoid cropping the secondary
    graphic and text, only use an image, the
    background texture, or a solid red. Never use
    text within a LinkedIn header image.
</p>

<p>
    Refer to specific social media sites or apps
    for exact specifications, as they will vary. As
    information changes, contact the Corporate
    Brand team for additional guidance.
</p>
    </div>
</div>

    
 </div>
 
    <div class="pane" id="tabs-2">
    <h3>Ads</h3>   

    <div class="well intro">
        <p>Social media sites are typically filled
        with content, which is why a single
        image must have a huge impact.
        </p>
    </div>
    <div class="row">
    <div class="col-md-4">
        <p>
            Images can be of patients, clinicians, products
            or an infographic. In any case, messages
            within the image should be brief, if used at
            all. Messages typically appear above and
            sometimes below the image. Use the Edwards
            logo as the profile picture and don’t repeat it
            within the image.
        </p>
    
        <p><strong>Secondary graphic</strong></p>
    
        <p>
            Use the small or large secondary graphic with a
            brief message. Although the secondary graphic
            is always preferred, if the image does not allow
            space or if it competes with the secondary
            graphic, just use the image. Don’t force the
            secondary graphic to work with the image in such
            a small space. However, headlines must be red.
        </p>
    
        
    </div>
    <div class="col-md-8"><p><img src="img/dig-social-image-2.png" class="img-responsive"></p></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>
                Image sizes vary from app to app. They can
                be horizontal, vertical or square. However,
                use the graphic elements in a similar fashion.
                Refer to specific social media sites or apps for
                exact specifications, as they will vary.
            </p>
        
            <p><strong>Carousel ads</strong></p>
        
            <p>
                These ads can include many images, arranged
                one after another. Don’t use the secondary
                graphic more than once and use images to help
                tell a story, such as a combination of product
                and lifestyle images. The live text area above and
                below the images will help complete the story.
            </p>
            <p><strong>Important safety information (ISI)</strong></p>
            <p>
                Using ISI copy will vary between social media
                apps. Some platforms, such as Facebook, allow
                scrolling text. When scrolling is not an option
                at least 3 lines of copy must be visible before
                continuing onto another screen or page. In
                either case, Vesta Std or Merriweather Sans are
                preferred.
            </p>
        </div>
    </div>

    
    
   </div>
    
    

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
