﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-magna-mitral" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Surgical Structural Heart brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
                                <li>Tier 3 brand books</li>
                                <li>Magna Mitral Ease valve</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Magna Mitral Ease valve<a href="../downloads/print-magna-mitral.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Brand story</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Brand components</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Communications</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Brand story</h3>
                                        <h4>Platform goals</h4>
                                        <p>The Platform goals for the Magna Mitral Ease valve are to further optimize implantation to meet the challenges of the mitral position.</p>
                                        <h4>Brand strategy</h4>
                                        <p>The brand strategy for the Magna Mitral Ease valve is to drive awareness of the mitral heart valve that was designed specifically for use in the mitral position.</p>
                                        <h4>Brand identity map</h4>
                                        <p><img src="../img/mitral-identity.png" alt="Brand identity map" class="img-responsive"></p>
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Brand components</h3>
                                        <div class="panel-group" id="accordion-brand-components">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-brand-components" href="#collapse1-brand-components">
        Brand messaging<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-brand-components" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Product positioning</h3>
                                                        <p><strong>Message weighting</strong></p>


                                                        <div class="row"><div class="col-sm-6">
                                                                                                        <ul>
                                                                                                            <li>Enhanced Mitral implantability (60%) <span class="side-box side-box-red"></span></li>
                                                                                                            
                                                                                                            <li>Built on proven performance (40%) <span class="side-box side-box-gray"></span></li>
                                                                                                            
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                     <div class="col-sm-6">
                                                                                                        <p><img src="../img/mitral-weighting.png" alt="Message weighting" class="img-responsive center-block"></p>
                                                                                                    </div></div>
                                                        


                                                        
                                                        <h3>Body copy and building copy tone</h3>
                                                        <p><strong>Note:</strong> All statements are approved in the VAULT and some statements may require an accompanying disclaimer. For more detailed product messaging, please visit Vault to access up-to-date claims matrices, or contact your local marketing or brand team.</p>
                                                        <p><strong>Supportive statements</strong></p>
                                                        <p>With the Magna Mitral Ease valve, you can choose with confidence, knowing you are getting a valve, from Edwards Lifesciences, the worldwide leader in heart valve therapy.</p>
                                                        <p>Built upon the unique and proven PERIMOUNT valve design, the Magna Mitral Ease valve gives you and your patients:</p>
                                                        <ul>
                                                          <li>Ultra-low profile, with reduced ventricular projection by up to 40%<sup>*</sup></li>
                                                          <li>Exceptional long-term durability</li>
                                                          <li>Optimized for ease of implant</li>
                                                        </ul>
                                                        <p>Supra-annular position and asymmetric design, help to reduce ventricular projection<sup>*</sup></p>
                                                        <p>Built on the proven performance of the PERIMOUNT valve design, with published clinical durability of up to 20 years</p>
                                                        <p class="small"><sup>*</sup>As compared to the Carpentier-Edwards PERIMOUNT Theon mitral valve.</p>
                                                        <h3>Campaign story</h3>
                                                        <p>The brand campaign for the Magna Mitral Ease valve helps customers and prospects understand the product’s value. It articulates the brand promise and stimulates a desire to trial or adopt the product. Typically, the brand messaging is led by a compelling headline and closes with a tagline that combines the product values, consequences and attributes, differentiating it from other product brands.</p>
                                                        <p><strong>Headline</strong></p>
                                                        <p>When the moment matters: make the choice you would make for yourself</p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-brand-components" href="#collapse2-brand-components">
        Digital images and illustrations<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-brand-components" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Digital images - core visual</h3>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <p>The Magna Mitral Ease valve core visuals adhere to the same color and typographic standards of the Edwards corporate brand globally.</p>
                                                        <p>If this product is used along with other Heart Valve Therapy products in the same communications, please use the primary photo of the product on a background consisting of the corporate color palette, unless it is part of a localized campaign.</p>
                                                      <p><a href="https://edwards.webdamdb.com/" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Access Webdam</a></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <img src="../img/mitral-digital.png" alt="Digital images core visual" class="img-responsive center-block">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-brand-components" href="#collapse3-brand-components">
        Trademarks<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-brand-components" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p><strong>Trademark / legal statement</strong></p>
                                                        <p>Edwards, Edwards Lifesciences, the stylized E logo, Carpentier-Edwards, Magna, Magna Mitral Ease, PERIMOUNT, and ThermaFix are trademarks of Edwards Lifesciences Corporation.</p>
                                                        <p>&copy; Edwards Lifesciences Corporation. All rights reserved. PPXXXXX</p>
                                                        <p>For further trademark information, please refer to <a href="http://standards.edwards.com/pdf/Trademark_021616.pdf">Trademarks Style Guide</a>. Note: Exceptions or questions should be addressed to <a href="mailto:debbie_sklar@edwards.com">Debbie Sklar</a> or Edwards Intellectual Property Counsel.
</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Communications</h3>
                                        <div class="well intro">
                                          <p>Commercialization materials such as product images, animations, procedural images and collateral pdf files for regional and cross-functional team use can be accessed via <a href="https://edwards.webdamdb.com/"><strong><u>Webdam</u></strong></a>. As new executions and tools are created, they will be uploaded to this site. You may also contact your local marketing or brand team.</p>
                                          <p><a href="https://edwards.webdamdb.com/" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Access Webdam</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
