<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-tiered" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Surgical Structural Heart brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
                                <li>Contents</li>
                                <li>Tiered communication strategy</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Tiered communication strategy<a href="../downloads/print-tiered.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Brand architecture to the surgical audience</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Tiered communication strategy</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Tier 1 - Edwards initiatives</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>Tier 2 - Campaign level</span>
                                    <li><a href="#tabs-5" class="match-height"><span>Tier 3 - Product and procedure level</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                    <h3>Brand architecture to the surgical audience</h3>
                                        <img class="img-responsive" src="../img/brand-architecture.png" alt="Brand architecture">
                                    </div>
                                    <div class="pane" id="tabs-2">
                                    <h3>Tiered communication strategy</h3>
                                        <div class="well intro">
                                            <p>All initiatives, educational offerings, clinical brands, campaigns and product promotions are aligned with the Surgical Structural Heart 3-tier communication strategy. The following chart shows that the <em>Transforming Tomorrow</em> campaign is directed at tier 2, focusing specifically on cardiac surgeons.</p>
                                        </div>
                                        <p><img class="img-responsive" src="../img/hvt-chart-innovation.png" alt="Edwards corporate branding innovation and humanity"></p>                                        
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Tier 1 - Edwards initiatives</h3>
                                        <p><a href="/contents.aspx" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Corporate brand identity standards</a></p>
                                        <p><a href="/surg/pro-edu.aspx" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Tier 1 brand books</a></p>
                                    </div>
                                    <div class="pane" id="tabs-4">
                                        <h3>Tier 2 - Campaign level</h3>
                                        <p>The Transforming Tomorrow surgeon campaign is an example of a tier 2 level campaign.  This campaign his concisely represents the value that we bring to cardiac surgeons. Surgeons can feel confident and motivated to achieve the next level with Edwards Lifesciences specialized expertise and experience enabling their peak performance.</p>
                                        <p><img class="img-responsive" src="../img/brand-foundation.png" alt=""></p>
                                        <p><a href="/surg/transforming-tomorrow.aspx" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Tier 2 brand books</a></p>
                                    </div>
                                    <div class="pane" id="tabs-5">
                                        <h3>Tier 3 - Product and procedure level</h3>
                                        <p><a href="/surg/inspiris.aspx" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Tier 3 brand books</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
