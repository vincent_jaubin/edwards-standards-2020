﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- #include file="../meta-top.html" -->
   </head>
   <body data-spy="scroll" data-target="#tabslider">
      <!-- #include file="../contact-form.html" -->
      <div id="edwards-wrapper">
         <!-- #include file="../header.html" -->
         <div id="hvt-intuity-elite" class="page">
            <div class="container-fluid">
               <div class="row">
                  <div class="redboxes-horz">
                     <div class="redbox-horz-sm"></div>
                     <div class="redbox-horz-lg">
                        <h1>Surgical Structural Heart brand standards</h1>
                     </div>
                  </div>
               </div>
            </div>
            <!-- /.basic-header-edw -->
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-9 col-md-push-3 main-content">
                     <ol class="breadcrumb">
                        <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
                        <li>Tier 3 brand books</li>
                        <li>EDWARDS INTUITY Elite valve system</li>
                     </ol>
                     <div id="tabslider">
                        <h2>
                           EDWARDS INTUITY Elite valve system<a href="../downloads/print-intuity-elite.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
                           <!-- Go to www.addthis.com/dashboard to customize your tools -->
                           <div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
                            <a href="../downloads/EDWARDS-INTUITY-Elite-Brand-Book-13Jun2017.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Download PDF"><i class="icon-download"></i></a>
                        </h2>
                        <div class="clearfix"></div>
                        <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                           <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Contents</span></a></li>
                           <li><a href="#tabs-2" class="match-height"><span>Product introduction</span></a></li>
                           <li><a href="#tabs-3" class="match-height"><span>Brand story</span></a></li>
                           <li><a href="#tabs-4" class="match-height"><span>Brand components</span></a></li>
                        </ul>
                        <div class="edw-tabslider">
                           <div class="pane" id="tabs-1">
                              <h3>Contents</h3>
                              <h4>Brand book benefits</h4>
                              <div class="well intro">
                                 <p>The brand book is the essential guide for developing all brand-related communications and collateral in accordance with the EDWARDS INTUITY Elite valve system global brand construct.</p>
                              </div>
                              <p>This guide provides the specific set of visual standards, messaging, design elements, and instructions for use in all product brand development, planning and execution. It also serves as a guide to understanding:</p>
                              <ul>
                                 <li>How the brand was constructed: strategy, essence, brand position launch campaign and how this must be articulated.</li>
                                 <li>Specific mandatory elements including messaging, copy tone, and visual elements to be used in the design of promotional and advertising materials.</li>
                                 <li>Correct use of the brand trademark, legal and approved advertising statements.</li>
                                 <li>Currently available combinations of visual elements, that are permissible and appropriate in several applications.</li>
                              </ul>
                              <p><img src="../img/brand-book-benefits.png" alt="Brand Book Benefits" class="img-responsive center-block"></p>
                           </div>
                           <div class="pane" id="tabs-2">
                              <h3>Product introduction</h3>
                              <div class="panel-group" id="accordion-intro">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-intro" href="#collapse1-intro" class="collapsed" aria-expanded="false">
                                          The EDWARDS INTUITY Elite valve system<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse1-intro" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>The EDWARDS INTUITY Elite valve system</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>The EDWARDS INTUITY Elite valve system represents our commitment to continued innovation for surgeons and patients in heart valve therapy.</p>
                                                <p>The surgical heart valve market is evolving. <strong>Concomitant
                                                   procedures</strong> are becoming a larger percent of the surgical mix, and <strong>MIS</strong> is gaining in importance. To enable surgeons to address these trends, we have developed the EDWARDS INTUITY Elite valve system.
                                                </p>
                                                <p>We have combined our <strong>proven pericardial valve
                                                   technology</strong> with our <strong>innovations in transcatheter
                                                   heart valves</strong> to create a new category of surgical valves designed to streamline procedures and facilitate smaller incision surgery. We believe more efficient, less invasive procedures can provide significant benefits, both during the procedure and after.
                                                </p>
                                                <p>This is the next evolution of surgical aortic valves.</p>
                                                <p><strong>This is the EDWARDS INTUITY Elite valve system.</strong></p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-valve-system.png" alt="Edwards INTUITY Elite valve system" class="img-responsive center-block"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-intro" href="#collapse2-intro" class="collapsed" aria-expanded="false">
                                          EDWARDS INTUITY Elite valve<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse2-intro" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>EDWARDS INTUITY Elite valve</h3>
                                          <p><img src="../img/intuity-elite-valve.png" alt="Edwards INUTITY Elite valve" class="img-responsive"></p>
                                          <p class="small"><sup>*</sup> Carpentier-Edwards PERIMOUNT Magna Ease pericardial aortic bioprosthesis.
                                             <br>
                                             <sup>&dagger;</sup> No clinical data are available that evaluate the long-term impact of the Carpentier-Edwards ThermaFix tissue process in patients.
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-intro" href="#collapse3-intro" class="collapsed" aria-expanded="false">
                                          EDWARDS INTUITY Elite delivery system<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse3-intro" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>EDWARDS INTUITY Elite delivery system</h3>
                                          <p><img src="../img/intuity-elite-delivery-system.png" alt="EDWARDS INTUITY Elite delivery system" class="img-responsive"></p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pane" id="tabs-3">
                              <h3>Brand story</h3>
                              <div class="panel-group" id="accordion-story">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse1-story" class="collapsed" aria-expanded="false">
                                          Strategic platform goals<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse1-story" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Strategic platform goals</h3>
                                          <h4>Target market - MIS and complex concomitant AVRs</h4>
                                          <p>The <strong>EDWARDS INTUITY Elite valve system</strong> represents our commitment to continued innovation for surgeons and patients in heart valve therapy. In partnering with our surgeon customers, we aspire to continue elevating and transforming the standard of surgical care for aortic valve replacement in MIS and complex concomitant cases. The EDWARDS INTUITY Elite valve system was uniquely designed for performance without compromise by achieving three important goals simultaneously.</p>
                                          <ul>
                                             <li>Activates unrivaled surgical portfolio opening up new “High Premium” market tier</li>
                                             <li>Improves patient outcomes</li>
                                             <li>Expands MIS AVR market</li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse2-story" class="collapsed" aria-expanded="false">
                                          Campaign strategy<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse2-story" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Campaign strategy</h3>
                                          <p>The goals for this campaign are to:</p>
                                          <div class="row text-center">
                                             <div class="col-md-4">
                                                <div class="well match-height b-white">
                                                   <p class="h3 edw-red">1</p>
                                                   <p>Continue the bold and innovative product focused messaging</p>
                                                </div>
                                             </div>
                                             <div class="col-md-4">
                                                <div class="well match-height b-white">
                                                   <p class="h3 edw-red">2</p>
                                                   <p>Drive adoption for the EDWARDS INTUITY Elite valve system platform (rapid deployment category)</p>
                                                </div>
                                             </div>
                                             <div class="col-md-4">
                                                <div class="well match-height b-white">
                                                   <p class="h3 edw-red">3</p>
                                                   <p>Further reinforce the platform’s three key messages
                                                   </p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse3-story" class="collapsed" aria-expanded="false">
                                          Brand strategy<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse3-story" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Brand strategy</h3>
                                          <div class="well intro">
                                             <p>Our strategic goal is to create an entirely new
                                                market category of rapid deployment valves
                                                through the EDWARDS INTUITY valve system
                                                platform.
                                             </p>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>This process began with early adopters (1), in this case,
                                                   surgeons currently performing less invasive procedures or
                                                   very interested in doing so.
                                                </p>
                                                <p>The second and larger customer demographic will be
                                                   mainstream surgeons (2) who are interested in advancing
                                                   their practice by expanding treatment options to include
                                                   less invasive procedures, but currently do not because
                                                   these cases are more labor/time intensive. Additionally,
                                                   as TAVI procedures continue to surge and absorb some of
                                                   the simple cases, mainstream surgeons are treating more
                                                   complex concomitant cases. These customers will see the
                                                   value proposition of the EDWARDS INTUITY Elite valve
                                                   system in streamlining these cases.
                                                </p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-brand-strategy.png" alt="Brand strategy" class="img-responsive"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-story" href="#collapse4-story" class="collapsed" aria-expanded="false">
                                          Brand identity map<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse4-story" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Brand identity map</h3>
                                          <div class="well intro">
                                             <p>The brand identity drives the look and feel of
                                                our marketing and communications collateral.
                                                This map is based largely on the market research
                                                done by Equibrand and Wallace Church,
                                                spanning many customer interviews in the U.S.
                                                and EU.
                                             </p>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-4">
                                                <p>Brand identity starts with the core essence of <strong>Freedom
                                                   from convention</strong>, and from there extends to emotional
                                                   and functional attributes, such as confidence and control,
                                                   respectively.
                                                </p>
                                                <p>Brand imagery for the EDWARDS INTUITY Elite valve
                                                   system should evoke a sense of streamlined, vibrancy
                                                   and innovation to tie back to the attributes and original
                                                   core essence.
                                                </p>
                                             </div>
                                             <div class="col-sm-8">
                                                <p><img src="../img/intuity-elite-brand-identity-map.png" alt="Brand identity map" class="img-responsive center-block"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pane" id="tabs-4">
                              <h3>Brand components</h3>
                              <div class="panel-group" id="accordion-brand-components1">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components1" href="#collapse1-brand-components1" class="collapsed" aria-expanded="false">
                                          Positioning statement<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse1-brand-components1" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Positioning statement</h3>
                                          <div class="row">
                                             <div class="col-md-15">
                                                <div class="well  match-height">
                                                   <p class="h3 edw-red">For...</p>
                                                   <p>… surgeons conducting surgical aortic valve replacements
                                                   </p>
                                                </div>
                                             </div>
                                             <div class="col-md-15">
                                                <div class="well  match-height">
                                                   <p class="h3 edw-red">Who...</p>
                                                   <p>… recognize the need for smaller incision approaches and streamlined complex concomitant procedures
                                                   </p>
                                                </div>
                                             </div>
                                             <div class="col-md-15">
                                                <div class="well  match-height">
                                                   <p class="h3 edw-red">EDWARDS INTUITY Elite valve system</p>
                                                   <p>… offers further design and procedural refinements on a trusted, proven Edwards valve platform</p>
                                                </div>
                                             </div>
                                             <div class="col-md-15">
                                                <div class="well  match-height">
                                                   <p class="h3 edw-red">That...</p>
                                                   <p>… further enhances
                                                      small incision surgical
                                                      approaches and further
                                                      streamlines complex
                                                      concomitant cases while
                                                      building on a trusted,
                                                      proven valve platform
                                                   </p>
                                                </div>
                                             </div>
                                             <div class="col-md-15">
                                                <div class="well  match-height">
                                                   <p class="h3 edw-red">Unlike...</p>
                                                   <p>… the cumbersome
                                                      procedural steps
                                                      required to implant a
                                                      conventional valve
                                                   </p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components1" href="#collapse2-brand-components1" class="collapsed" aria-expanded="false">
                                          Value proposition<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse2-brand-components1" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Value proposition</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>Value proposition features three core elements:</p>
                                                <ol>
                                                   <li>Proven PERIMOUNT platform</li>
                                                   <li>Streamlines concomitant procedures</li>
                                                   <li>Facilitates small incision surgery</li>
                                                </ol>
                                                <p>This is conveyed through our Mobius triangle
                                                   graphic.
                                                </p>
                                                <p>The EDWARDS INTUITY Elite valve system was uniquely
                                                   designed for performance without compromise
                                                   by achieving three important goals simultaneously:
                                                </p>
                                                <ul>
                                                   <li>Built on a trusted, proven valve platform</li>
                                                   <li>Streamlines concomitant procedures</li>
                                                   <li>Facilitates small incision surgery</li>
                                                </ul>
                                                <p>Some competitor (sutureless) valves may offer reduced
                                                   procedure times, but do not utilize a proven valve Only
                                                   the EDWARDS INTUITY Elite valve system provides
                                                   the combination of all three key attributes for total
                                                   confidence.
                                                </p>
                                                <p>For key product messaging, please visit the <i><a href="#collapse1-intro">Product introduction</a> pages.</i>
                                                </p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-value-proposition.png" alt="Value proposition" class="img-responsive"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <h4>Key messages</h4>
                              <div class="panel-group" id="accordion-brand-components2">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components2" href="#collapse1-brand-components2" class="collapsed" aria-expanded="false">
                                          Trusted valve platform and durability statements<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse1-brand-components2" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Trusted valve platform and durability statements</h3>
                                          <div class="row">
    <div class="col-sm-6">
        <table class="table table-primary">
            <thead>
                <tr>
                    <th>Key messages</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><strong>Build on a Trusted, Proven Valve Platform</strong> <i>or</i> <strong>Trusted Valve Platform</strong></td>
                </tr>
                <tr>
                    <td><strong>Designed for Durability. Created to Last.</strong></td>
                </tr>
                <tr>
                    <td>The EDWARDS INTUITY Elite valve system combines our proven pericardial valve technology with our innovations in transcatheter heart valves.</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-6">
        <table class="table table-primary">
            <thead>
                <tr>
                    <th>Supportive statements</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><strong>Excellent 3-Year Hemodynamics</strong>
                        <br>Single digit mean gradients (8.7 mmHg overall n = 59) demonstrated in the prospective multicenter TRITON trial of 287 patients.
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Low Profile for Maximum Options</strong>
                        <br><i>or</i>
                        <br><strong>Low Profile Valve Design</strong>
                        <br>Low supra-annular profile facilitates use with any aortotomy and is designed to provide good clearance from the coronary ostia.
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Built on the PERIMOUNT Valve Performance</strong> The EDWARDS INTUITY Elite valve system is built upon the proven performance and durability of the PERIMOUNT heart valve design. By mounting matched leaflets under the flexible stent, commissural stress points are minimized.
                    </td>
                </tr>
                <tr>
                    <td><strong>Bovine Pericardium</strong>
                        <br>Provides proven durability with three independent leaflets matched for thickness and elasticity.
                    </td>
                </tr>
                <tr>
                    <td><strong>Flexible Alloy Wireform</strong>
                        <br>Absorbs energy during the cardiac cycle for long-term performance.
                    </td>
                </tr>
                <tr>
                    <td><strong>Textured Sealing Cloth</strong>
                        <br>Provides a secure fit in the annulus to aid sealing.
                    </td>
                </tr>
                <tr>
                    <td><strong>Stainless Steel Frame</strong>
                        <br>Maintains high radial strength and short sub-annular height for maximum clearance from underlying structures.
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components2" href="#collapse2-brand-components2" class="collapsed" aria-expanded="false">
                                          Rapid deployment and potential time savings statements<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse2-brand-components2" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Rapid deployment and potential time savings statements</h3>
                                          <div class="row">
    <div class="col-sm-6">
        <table class="table table-primary">
            <thead>
                <tr>
                    <th>Key messages</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><strong>Provide Rapid Deployment* for Faster Procedures</strong>
                        <br><i>or</i>
                        <br>
                        <strong>Rapid Deployment<sup>*</sup></strong>
                        <br>
                        <br>
                        <sup>*</sup> Simplified implantation through reduced suture steps
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-6">
        <table class="table table-primary">
            <thead>
                <tr>
                    <th>Supportive statements</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><strong>Streamlined Implantation. Because Time is Precious.</strong>
                        <br>Implantation of the EDWARDS INTUITY Elite valve system is streamlined to help reduce crossclamp and bypass times.
                    </td>
                </tr>
                <tr>
                    <td><strong>Secure Assembly</strong>
                        <br>Engineered to ensure only the correct size valve and delivery system are connected for procedural confidence.
                    </td>
                </tr>
                <tr>
                    <td><strong>Balloon Expanded Delivery for Efficient Procedures</strong> The EDWARDS INTUITY Elite valve system utilizes three guiding sutures in conjunction with the expanded frame for secure annular placement, helping reduce procedural steps and time.</td>
                </tr>
                <tr>
                    <td><strong>Rapid Valve Preparation</strong> No collapsing or folding of the valve leaflets during preparation or implantation.</td>
                </tr>
                <tr>
                    <td><strong>Innovative Balloon Design</strong> Incorporated within the delivery system for reliable balloon positioning and inflation, as well as simplified device preparation.</td>
                </tr>
                <tr>
                    <td><strong>Meaningful Time Savings</strong> Short cross-clamp time demonstrated in isolated AVR procedures in the prospective, multicenter TRITON trial.</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components2" href="#collapse3-brand-components2" class="collapsed" aria-expanded="false">
                                          Small incisions statements<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse3-brand-components2" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Small incisions statements</h3>
                                          <div class="row">
    <div class="col-sm-6">
        <table class="table table-primary">
            <thead>
                <tr>
                    <th>Key messages</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><strong>Well Combined With Small Incisions</strong>
                        <br><i>or</i>
                        <br>
                        <strong>Smaller Incisions</strong>
                    </td>
                </tr>
                <tr>
                    <td>Empowering Multiple Approaches. Progress Through Access. The EDWARDS INTUITY Elite valve system is designed to enhance the ease of implantation through small incisions by using 3 guiding sutures.</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-6">
        <table class="table table-primary">
            <thead>
                <tr>
                    <th>Supportive statements</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><strong>Streamlined Delivery</strong> Utilizes a balloon expanded frame and 3 guiding sutures to provide ease of implantation and excellent visualization.</td>
                </tr>
                <tr>
                    <td><strong>Traditional Surgical Valves</strong> Require 12–15 sutures, making implantation difficult through smaller incisions.
                    </td>
                </tr>
                <tr>
                    <td><strong>Flexibility for Optimal Access</strong> Facilitates access through small incisions with a long, flexible delivery system shaft.</td>
                </tr>
                <tr>
                    <td><strong>High Use of Small Incision Approaches</strong> The TRITON Trial showed high rates of small incision usage in isolated AVR. 55% (n = 87/158).</td>
                </tr>
                <tr>
                    <td><strong>Building the Clinical Evidence</strong> Driving Outcomes. Investing in the Future. The EDWARDS INTUITY Elite valve system platform is being studied through a robust series of trials and in commercial sites with clinicians across the globe.</td>
                </tr>
      
            </tbody>
        </table>
    </div>
</div>

                                          <p>For more detailed product messaging, please visit VAULT to access up-to-date claims matrices, or contact your EDWARDS INTUITY Elite valve marketing or brand team.</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="panel-group" id="accordion-brand-components3">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components3" href="#collapse1-brand-components3" class="collapsed" aria-expanded="false">
                                          Campaign messages<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse1-brand-components3" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Campaign messages</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p><strong>Headline</strong></p>
                                                <p>Our headline, “Leading the Evolution”, implies that we are actually surpassing surgeons’ current gold standard procedure and bringing them a product that delivers results beyond their expectations It implies a leap forward to a new platform of device that can expand their treatment options, and potentially, their practice.</p>
                                                <p><strong>Subhead</strong></p>
                                                <p>“Advanced approach on a proven platform”, conveys that the EDWARDS INTUITY Elite valve system is the only product on the market that can provide a Trusted Platform while providing the innovation of Rapid Deployment.
                                                </p>
                                                <p>Whenever possible, the headline and subhead should be used with the hero product image, signing off with the product name and descriptor.</p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-campaign-messages.png" alt="Campaign messages" class="img-responsive"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components3" href="#collapse2-brand-components3" class="collapsed" aria-expanded="false">
                                          Core image<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse2-brand-components3" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Core image</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>Our core image features the valve placed on
                                                   the delivery system. Together, they hover
                                                   dynamically over a reflective titanium surface.
                                                   Behind the valve is a glowing white background
                                                   meant to draw attention to the valve. This white,
                                                   halo-like glow helps to define the horizon line
                                                   and gives depth to the visual.
                                                </p>
                                                <p>Showing the valve and delivery system together is
                                                   important as it helps communicate a unique offering and
                                                   differentiates EDWARDS INTUITY Elite valve system from
                                                   traditional valves.
                                                </p>
                                                <p>The image may be sized and cropped in many ways (see
                                                   image placement below). However, when restricted to small spaces, the
                                                   valve should always be the primary focus. Do not crowd
                                                   the valve or delivery system with copy.
                                                </p>
                                                <p><strong>Resources</strong></p>
                                                <p>High-resolution files are available for download through
                                                   <a href="https://edwards.webdamdb.com" target="_blank">WebDam</a>.
                                                </p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-core-image.png" alt="Core image" class="img-responsive"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components3" href="#collapse3-brand-components3" class="collapsed" aria-expanded="false">
                                          Image placement<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse3-brand-components3" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Image placement</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>The core image may be placed on a variety of media and
                                                   cropped for both horizontal and vertical spaces Keep in
                                                   mind that the horizon line and white background glow
                                                   may need to be adjusted, depending how the image is
                                                   cropped
                                                </p>
                                                <p><strong>Horizon line</strong></p>
                                                <p>The left and right edges of the horizon line should be
                                                   blurred or softened so that they do not intersect with
                                                   the edge of the image area or intrude upon key messages
                                                   As shown in these examples, the blurred area will vary,
                                                   depending on how narrow or wide the image area is
                                                </p>
                                                <p><strong>Background glow</strong></p>
                                                <p>In some applications, the glowing white background
                                                   behind the valve may need to be adjusted The intent
                                                   is that the area behind the valve is the brightest The
                                                   highlight is also important to help defi ne the edge of the
                                                   horizon line, so it should have sufficient contrast with
                                                   the refl ective surface below the combined valve and
                                                   delivery system.
                                                </p>
                                                
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-image-placement.png" alt="Image placement" class="img-responsive"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <h4>Image assets</h4>
                              <div class="panel-group" id="accordion-brand-components4">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components4" href="#collapse1-brand-components4" class="collapsed" aria-expanded="false">
                                          Primary images<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse1-brand-components4" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Primary images</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>In addition to the core image, there are other product
                                                   renderings and graphics that may be used on materials.
                                                </p>
                                                <p>The core image should always be the lead visual –
                                                   appearing on the cover of a piece, or when only one
                                                   image may be featured, such as on a banner.
                                                </p>
                                                <p>Other primary images may appear in a prominent
                                                   position on interior pages of print materials; they may
                                                   also appear on second level screens, on an animated
                                                   digital ad, for example.
                                                </p>
                                                
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-primary-images1.png" alt="Primary images" class="img-responsive"></p>
                                             </div>
                                          </div>
                                          <p><strong>Resources</strong></p>
                                                <p>For access to all EDWARDS INTUITY Elite valve system related
                                                   procedural images, please visit <a href="https://edwards.webdamdb.com" target="_blank">WebDam</a>, or
                                                   contact your marketing or brand team.
                                                </p>
                                          <div class="row">
                                             <div class="col-sm-4">
                                                <p><img src="../img/intuity-elite-primary-images2.png" alt="Primary images" class="img-responsive center-block"></p>
                                             </div>
                                             <div class="col-sm-4">
                                                <p><img src="../img/intuity-elite-primary-images3.png" alt="Primary images" class="img-responsive center-block"></p>
                                             </div>
                                             <div class="col-sm-4">
                                                <p><img src="../img/intuity-elite-primary-images4.png" alt="Primary images" class="img-responsive center-block"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components4" href="#collapse2-brand-components4" class="collapsed" aria-expanded="false">
                                          Support images<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse2-brand-components4" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Support images</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>A collection of preparation and procedural images are available to help support communications These images are secondary, so they should not appear on covers or as the primary visual in digital media.</p>
                                                <p>Support images should be scaled smaller, and placed in a subordinate position to primary images.</p>
                                                <p><strong>Resources</strong></p>
                                                <p>For access to all EDWARDS INTUITY Elite valve system related procedural images, please visit <a href="https://edwards.webdamdb.com" target="_blank">WebDam</a>, or contact your marketing or brand team.</p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-support-images.png" alt="Support images" class="img-responsive"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components4" href="#collapse3-brand-components4" class="collapsed" aria-expanded="false">
                                          Patient images<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse3-brand-components4" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Patient images</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>A number of images that are directed towards patients and their experience are available for use This includes lifestyle images of the patient in the recovery room, with family and in everyday situations, but also simple illustrations to help patients understand the process.</p>
                                                <p><strong>Resources</strong></p>
                                                <p>For access to all EDWARDS INTUITY Elite valve system-related patient-directed images, please visit <a href="https://edwards.webdamdb.com" target="_blank">WebDam</a>, or contact your marketing or brand team.</p>
                                                <p><img src="../img/intuity-elite-patient-images1.png" alt="Patient images" class="img-responsive center-block"></p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-patient-images2.png" alt="Patient images" class="img-responsive"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="panel-group" id="accordion-brand-components5">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components5" href="#collapse1-brand-components5" class="collapsed" aria-expanded="false">
                                          Color<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse1-brand-components5" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Color</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>The red and titanium color combination is Edwards’ signature Because red is our brightest color, it should be used for headlines and titles, and to draw attention to key fi gures and shapes on infographics Primary gray may be used for support messages, charts, and as a tinted gradient behind infographics.</p>
                                                <p>The secondary, dark gray may be used for charts and diagrams and for captions The two blues in our neutral palette may be used for infographics, but should be used in small amounts as to not compete with the red and titanium palette.</p>
                                                <p>Titanium (metallic) ink may only be used for the Edwards logo on the cover and back cover of prominent print materials.
                                                </p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-color.png" alt="Color" class="img-responsive"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components5" href="#collapse2-brand-components5" class="collapsed" aria-expanded="false">
                                          Advertising<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse2-brand-components5" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Advertising</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p>Print and digital ads should include the following elements:</p>
                                                <div class="row">
                                                   <div class="col-sm-6">
                                                      <ul>
                                                         <li>Core visual</li>
                                                         <li>Headline</li>
                                                         <li>Subhead</li>
                                                      </ul>
                                                   </div>
                                                   <div class="col-sm-6">
                                                      <ul>
                                                         <li>Product name</li>
                                                         <li>Support messages</li>
                                                         <li>Regulatory and copyright information</li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <p>Each element is placed according to the Edwards Brand
                                                   Standards, and will also include the red secondary graphic
                                                   and Edwards logo.
                                                </p>
                                                <p>The amount of support copy and/or additional graphics
                                                   will vary depending on the size On print ads – full-page or
                                                   spread – a call-to-action and required legal copy, such as
                                                   a brief summary for the U S , should be included Digital
                                                   ads may be a single frame with limited content, or an
                                                   animated GIF (multi-frame) to present more content,
                                                   depending on the host site requirements.
                                                </p>
                                                <p>When placing the core image, crop and adjust the
                                                   horizon line and preserve the key elements: the valve,
                                                   delivery system, the glowing white halo and horizon line
                                                   If cropped into a small space, the valve should be the
                                                   primary focus Do not crowd the valve or delivery system
                                                   with copy.
                                                </p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-advertising.png" alt="Advertising" class="img-responsive center-block"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components5" href="#collapse3-brand-components5" class="collapsed" aria-expanded="false">
                                          Print finishes<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse3-brand-components5" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Print finishes</h3>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <p><strong>Spot vs. process colors</strong></p>
                                                <p>Edwards primary red (PANTONE 186) was chosen because the color matches nicely to its 4-color process (CMYK) equivalent Edwards primary gray may also print as CMYK The titanium logo (PANTONE 8421) should only appear on the core brochure, reference sheets and quick reference guide Unless produced as an insert, print ads should print CMYK.
                                                </p>
                                                <p><strong>Spot varnish</strong></p>
                                                <p>Our product brochure includes spot varnishes over silhouetted products, rectangular-cropped secondary images, and the evolution triangle graphic Other graphic elements, such as bar charts and non-product imagery, should not be spot varnished.</p>
                                                <p><strong>Coatings</strong></p>
                                                <p>Each piece should have an overall satin aqueous varnish to give communications a subtle luster.</p>
                                             </div>
                                             <div class="col-sm-6">
                                                <p><img src="../img/intuity-elite-print-finishes.png" alt="Print finishes" class="img-responsive center-block"></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion-brand-components5" href="#collapse4-brand-components5" class="collapsed" aria-expanded="false">
                                          Trademark usage<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                       </h4>
                                    </div>
                                    <div id="collapse4-brand-components5" class="panel-collapse collapse" aria-expanded="false">
                                       <div class="panel-body">
                                          <h3>Trademark usage</h3>
                                          <p><strong>Brand trademark</strong></p>
                                          <p>The INTUITY name must be preceded by EDWARDS
                                             and both words must always be capitalized. Following
                                             EDWARDS INTUITY, Elite will be used to refer to this
                                             specific generation within the EDWARDS INTUITY valve
                                             system platform. EDWARDS INTUITY Elite must be in the
                                             same line and carry equal weight (font size, bold, etc.).
                                          </p>
                                          <p><strong>Trademark / Legal Statement</strong></p>
                                          <p>EDWARDS INTUITY Elite is a trademark of the Edwards
                                             Lifesciences Corporation.
                                          </p>
                                          <p>The trademark statement should be included in no
                                             smaller than 7 point type at the end of all executions
                                             using the product name.
                                          </p>
                                          <p>If this is not possible, please use EDWARDS INTUITY Elite™
                                             in the first use within the piece.
                                          </p>
                                          <p><strong>Full Brand Name in Body Copy</strong></p>
                                          <p>The full brand name, “EDWARDS INTUITY Elite valve
                                             system”, should appear in the first usage of the product
                                             name in body copy. Both EDWARDS and INTUITY must
                                             always be in all capital letters and should have the same
                                             font treatment (bold, etc.). Elite should use an initial
                                             capital.
                                          </p>
                                          <p>The full brand name should appear in this order only,
                                             unless regional grammar requires otherwise.
                                          </p>
                                          <p><strong>Shortened Brand Name in Body Copy</strong></p>
                                          <p>After the first instance use of the full brand name, the
                                             shortened product name can appear thereafter in body
                                             copy. Examples of such names would include “EDWARDS
                                             INTUITY Elite valve” or “EDWARDS INTUITY Elite delivery
                                             system”.
                                          </p>
                                          <p>As always, both EDWARDS and INTUITY must be in all
                                             capital letters and have the same font treatment and Elite
                                             should use an initial capital. The descriptor (“valve”,
                                             “valve system”, “delivery system”) should not start with
                                             a capital letter.
                                          </p>
                                          <p><strong>Translations and Descriptors</strong></p>
                                          <p>The brand name “EDWARDS INTUITY Elite” should always
                                             be in English and never translated to help maintain brand
                                             uniformity.
                                          </p>
                                          <p>The descriptor (“valve system”, “valve”, “delivery system”)
                                             may be translated within the body copy of materials,
                                             and should follow regional vernacular and grammar.
                                             The treatment of descriptor translations must remain
                                             consistent across executions (sales aid, presentations, etc.).
                                          </p>
                                          <p>For any clinical trial or registry of the EDWARDS INTUITY
                                             valve system platform, the study name and descriptor
                                             should always be in English and never translated.
                                          </p>
                                          <p>For further trademark information, please refer to
                                             <i><a href="http://oneedwards/spaces/102/intellectual-property/files/detail/3176" target="_blank">Trademarks Style Guide</a></i>.
                                          </p>
                                          <p>Note: Exceptions or questions should be addressed to
                                             Debbie Sklar (<a href="mailto:debbie_sklar@edwards.com">debbie_sklar@edwards.com</a>) or Edwards
                                             Intellectual Property Counsel.
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3 col-md-pull-9">
                     <!-- #include file="../sidemenu-hvt.html" -->
                  </div>
               </div>
            </div>
         </div>
         <!-- end page -->
         <!-- #include file="../footer.html" -->
      </div>
      <!-- /wrapper -->
      <!-- #include file="../meta-bottom.html" -->
   </body>
</html>