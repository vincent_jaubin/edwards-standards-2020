<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-pro-edu" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Surgical Structural Heart brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
                                <li>Tier 1 brand books</li>
                                <li>Professional education</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Professional education<a href="../downloads/print-pro-edu.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Professional educational brand</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Educational portfolio</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Target audiences</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>Brand strategy</span></a></li>
                                    <li><a href="#tabs-5" class="match-height"><span>Communication examples</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Professional educational brand</h3>
                                        <p><strong>Mission</strong></p>
                                        <p>Edwards Lifesciences is dedicated to providing the surgical community with world-class education through the use of faculty who are internationally recognized experts.</p>
                                        <p>Our goal is to enrich and expand knowledge in current and emerging technologies.</p>
                                        <p><strong>Educational brand goals</strong></p>
                                        <ul>
                                            <li>Be recognized globally as the leading source of educational content, across the spectrum of live programming to digital tools.</li>
                                            <li>Partner with the cardiac surgical community to deliver relevant educational content that exceeds expectations.</li>
                                            <li>Maximize resources to drive awareness, education, adoption and proficiency.</li>
                                            <li>Facilitate the "transfer of knowledge" in a meaningful and enduring way.</li>
                                            <li>Empower the cardiac surgical team to deliver superior patient care.</li>
                                        </ul>
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Educational portfolio</h3>
                                        <p><strong>Edwards corporate headquarter visits</strong></p>
                                        <p>This program includes a visit to and tour of the Edwards Lifesciences corporate headquarters, led by professional education and the sales representative. Participants will have an opportunity to gain an in-depth view of heart valve design, the manufacturing process, the current Edwards Lifesciences product portfolio and future technologies.</p>
                                        <p><strong>Edwards cardiac fellows program</strong></p>
                                        <p>This program provides the cardiothoracic surgical fellow a unique and interactive forum in which to participate actively and respond to questions similar to clinical practice and examinations. The fellows program is organized around practical case presentations addressing the surgical management of valve disease, aortic root pathology and aneurysms, mitral and tricuspid valve disease, CABG, cardiac reoperations, atrial fibrillation, minimally-invasive surgical approaches and transcatheter aortic valve technology. The course is taught by cardiothoracic surgeons and cardiologists who are experts in their fields and are dedicated to enhancing medical education.</p>
                                        <p><strong>Edwards centers of excellence</strong></p>
                                        <p>The objective of this series of courses is to review repair and replacement techniques for the treatment of valvular disease. The typical agenda includes didactic lectures, case studies, and live surgical observation. In addition, participants will discuss the latest trends in the assessment of valvular disease using echocardiography with the world class faculty at selected education teaching facilities. (Mount Sinai School of Medicine, Cleveland Clinic Foundation, University of Michigan, Northwestern Memorial Hospital, Florida Hospital)</p>
                                        <p><strong>MIVS preceptorship</strong></p>
                                        <p>Throughout the course, participants will gain a more thorough understanding of minimally invasive surgical techniques for valve replacement and/or repair. The attendee will observe live minimally invasive valve surgical procedures and gain knowledge of the minimally invasive technologies required.</p>
                                        <p><strong>In-service wet lab support</strong></p>
                                        <p>These programs are pre-arranged by the local Edwards sales representative. The objective of this course is to offer didactic lectures and an intensive hands-on pig heart workshop with a particular focus on step-by-step methodology for valve repair and replacement. Surgical techniques are discussed by pre-arranged faculty.</p>
                                        <p><strong>Regional speaker events</strong></p>
                                        <p>These product training programs are pre-arranged by the local Edwards sales representative. A clinician guest speaker provides a dinner presentation to educate local surgeons about patient selection and surgical treatment and techniques for valvular disease.</p>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Target audiences</h3> 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li>Cardiac surgeons (70%) <span class="side-box side-box-red"></span></li>
                                                    <li>Cardiologists &amp; anesthesiologists (10%) <span class="side-box side-box-brown"></span></li>
                                                    <li>Cardiac team (perfusionists, PA) (20%) <span class="side-box side-box-gray"></span></li>
                                                </ul>
                                            </div>
                                             <div class="col-sm-6">
                                                <p><img class="img-responsive" src="../img/target-audiences-chart.png" alt="target audciences"/></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-4">
                                        <h3>Brand strategy</h3>
                                        <div class="well intro">
                                            <p>Our brand strategy for professional education is to create a global umbrella of education, with consistency across all programs with high-caliber content and execution.</p>
                                        </div>
                                        <ul>
                                            <li>Continue to work with cardiac surgery thought leaders to develop new and innovative courses.</li>
                                            <li>Support programs and goals with content-rich materials that enhance the learning experience.</li>
                                            <li>Establish leading digital tools to support sales and marketing in their customer interactions.</li>
                                            <li>Expand awareness of Edwards' brands through events, symposia and customer tours and visits.</li>
                                            <li>Explore new opportunities for partnership across BUs as customers and technologies cross over.</li>
                                        </ul>
                                    </div>
                                    <div class="pane" id="tabs-5">
                                        <h3>Communication examples</h3>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                
                                        <p>The following examples illustrate approved global/regional artwork for promotional use.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <img class="img-responsive center-block edw-border" src="../img/pro-edu-ce1.jpg" alt="communication example">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
