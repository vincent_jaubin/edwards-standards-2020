﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="../meta-top.html" -->
</head>

<body>
<!-- #include file="../contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="../header.html" -->



 <div id="hvt-clinical" class="page">      <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Surgical Structural Heart brand standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --><div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
  <li>Tier 1 brand books</li>
  <li>Clinical trials</li>
  </ol>
  
  
  
  <h2>Clinical Trials
  <a href="../downloads/print-clinical.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
  
<p><strong>Clinical trial branding</strong></p>
<p>To maintain clinical credibility/data integrity and where a unique and durable trial brand is deemed necessary to differentiate from competing trials, the nature of clinical trials may require the creation of an identity that remains separate from our promotional materials; as such clinical trial branding is exempt from the branding guidelines.</p>
<p>When dictated by business needs, clinical trial branding and related logos may be implemented along with Edwards branding and logo per co-branding guidelines.</p>
<p><strong>Co-branding for clinical trial materials</strong></p>
<p>Where a product is still under investigation, materials may keep the pure clinical trial branding; or may include the Edwards brand elements as needed. Co-branding guidance should be employed when utilizing both clinical trial branding/logo along with Edwards branding.</p>
<ul>
  <li>Edwards branding may be implemented on front and back, or just on the back, based on the specific business need.</li>
  <li>When utilizing a clinical trial logo together with the Edwards logo on the front, include circle halo/red line/logo as part of the basis for determining “visual equality”.</li>
</ul>
<p>Where a product is commercial, Edwards branding must be included on collateral that references the clinical trial.</p>
<p>Edwards clinical trials are considered a tier 1 communication. For further information about Edwards Surgical Structural Heart brand architecture, please refer to the tiered communication strategy section in the general guidelines.</p>
<p>
  For more information about clinical trial branding, please visit our <a href="http://standards.edwards.com/contents-cobranding.aspx" target="_blank">corporate standards</a>.
</p>
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="../sidemenu-hvt.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="../footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="../meta-bottom.html" --> 
</body>
</html>
