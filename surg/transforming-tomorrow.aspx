﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="transforming-tomorrow" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Surgical Structural Heart brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
                                <li>Tier 2 brand books</li>
                                <li>Transforming tomorrow campaign</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Transforming tomorrow campaign<a href="../downloads/print-transforming-tomorrow.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
 <a href="../downloads/campaign-transforming-tomorrow.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Download PDF"><i class="icon-download"></i></a>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Brand story</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Brand components</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Campaign in action</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>Launch strategies</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Brand story</h3>
                                        <!-- accordion-brand-story1 -->
                                        <div class="panel-group" id="accordion-brand-story1">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-brand-story1" href="#collapse1-brand-story1" class="collapsed" aria-expanded="false">
          Campaign objectives<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-brand-story1" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Campaign objectives</h3>
                                                        <div class="row dot-borders">
                                                            <div class="col-sm-4 match-height">
                                                                <p><strong>Create an aspirational campaign that connects and inspires our target audience:<br><span class="edw-red">cardiac surgeons</span></strong></p>
                                                            </div>
                                                            <div class="col-sm-4 match-height">
                                                                <p><strong>Develop an <span class="edw-red">umbrella campaign for new products</span> that communicates:</strong></p>
                                                                <ul>
                                                                    <li>Surgical Structural Heart’s continued commitment to its cardiac surgeon partners</li>
                                                                    <li>The development of new surgical innovations for patients</li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-4 match-height">
                                                                <p><strong>Build a <span class="edw-red">brand platform &amp; identity</span> that:</strong></p>
                                                                <ul>
                                                                    <li>Resonates with our cardiac surgeon target audience</li>
                                                                    <li>Leverages Edwards’ legacy but emphasizes our forward looking Surgical Structural Heart strategy</li>
                                                                    <li>Provides consistent positioning for global communication of new Surgical Structural Heart products</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion-brand-story1" href="#collapse2-brand-story1" class="collapsed" aria-expanded="false">
            Campaign strategy<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
          </h4>
                                                </div>
                                                <div id="collapse2-brand-story1" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Campaign strategy</h3>
                                                        <p><strong>Our pillars express
our commitment</strong></p>
                                                        <p>to continually work with cardiac surgeons to drive revolutionary transformation in surgical heart valve therapy and advance patient care.</p>
                                                        <div class="row dot-borders">
                                                            <div class="col-sm-4 match-height">
                                                                <p class="text-center"><strong><u>Confidence</u></strong></p>
                                                                <ul>
                                                                    <li>Established <strong>industry leaders</strong>, both in innovation and quality.</li>
                                                                    <li>We are focused on long-term strategies and technologies that
                                                                        <strong>evolve the standard
 of care</strong> for patients.</li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-4 match-height">
                                                                <p class="text-center"><strong><u>Commitment</u></strong></p>
                                                                <ul>
                                                                    <li><strong>Nearly 60 years</strong> of partnering with surgeons to continually investment in clinical research and product R&amp;D.</li>
                                                                    <li>Our innovations have always started with a commitment to providing leading heart valve therapies for surgeons and their patients.
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-4 match-height">
                                                                <p class="text-center"><strong><u>Trust</u></strong></p>
                                                                <ul>
                                                                    <li>The brand <strong>selected
            by surgeons</strong> for themselves and their loved ones.</li>
                                                                    <li>Edwards is built on quality products that
                                                                        <strong>instill trust</strong> in the brand.
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-brand-story1 -->
                                        <!-- accordion-brand-story2 -->
                                        <h4>Campaign validation</h4>
                                        
                                        <div class="panel-group" id="accordion-brand-story2">
                                        <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion-brand-story2" href="#collapse1-brand-story2" class="collapsed" aria-expanded="false">
              Campaign validation<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
            </h4>
                                                </div>
                                                <div id="collapse1-brand-story2" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Campaign validation</h3>
                                                        <div class="row">
                                            <div class="col-sm-4">
                                                <p><strong>Conducted 20</strong></p>
                                                <p>telephone-based, internet-assisted interviews with CT surgeons, qualitative doubleblinded research.
                                                </p>
                                            </div>
                                            <div class="col-sm-8">
                                                <img src="../img/ttc-campaign-validation.png" alt="Campaign validation" class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <p><strong>Most Surgeons:</strong></p>
                                        <ul>
                                            <li>Recommended
                                                <strong>highlighting a
 commitment to
 innovation</strong> in the headline statement</li>
                                            <li>Considered it important to <strong>reference how
product innovations
can improve patient
outcomes</strong></li>
                                            <li><strong>14 of 20</strong> noted that they prefer ads that are product vs. company specific</li>
                                        </ul>
                                        <p><strong>Please contact Surgical Structural Heart global brand team to obtain full market research report.</strong></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion-brand-story2" href="#collapse2-brand-story2" class="collapsed" aria-expanded="false">
              Other research<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
            </h4>
                                                </div>
                                                <div id="collapse2-brand-story2" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Other research</h3>
                                                        <ul>
                                                            <li>A comprehensive competitive landscape audit was completed to ensure a unique
presence, messaging and creative strategy within the market. This included both a
visual and message audit within our direct competition and in a broader sense for both
B2B and B2C worldwide organizations. Although particular words were leveraged by
competition, the core messaging strategy held a unique position in the market.</li>
                                                            <li>Headline and subhead messaging were translated into 14 key languages to ensure
consistency and accuracy of the intended connotation and effectiveness. Based on a
double blind translation and evaluation, the core campaign messaging was found to be
universal.</li>
<p><strong>Please contact Surgical Structural Heart global brand team to obtain reports.</strong>
</p>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-brand-story2 -->
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Brand components</h3>
                                        <!-- accordion-key-messages -->
                                        <div class="panel-group" id="accordion-key-messages">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-key-messages" href="#collapse1-key-messages" class="collapsed" aria-expanded="false">
          Key messages<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-key-messages" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Key messages</h3>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p><strong>Headline</strong></p>
                                                                <p>Our headline, “Innovating Today to
Transform Tomorrow” pays tribute
to a legacy of innovation that has
transformed heart valve therapy
and reaffirms our commitment
to innovation for surgeons today.
The headline should be used in all
elements.</p>
<p><strong>Subhead</strong></p>
<p>The subhead should immediately
follow the headline and be used in
all campaign materials to reinforce
our focus on surgical heart valve
therapies and prevent from any
association with general Edwards or
other divisional campaigns.</p>
<p><strong>Body</strong></p>
<p>The body copy is the abbreviate
storyline only used in campaign
tactics that have the appropriate
design real estate and that delivered
in a manner that those engaging have
the appropriate time and proximity
to read the entire story.</p>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <p><img src="../img/ttc-key-messages.png" alt="Key messages" class="img-responsive"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-key-messages -->
                                        <!-- accordion-campaign-platform -->
                                        <h4>Campaign platform</h4>
                                        <div class="panel-group" id="accordion-campaign-platform">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-campaign-platform" href="#collapse1-campaign-platform" class="collapsed" aria-expanded="false">
          EDWARDS INTUITY Elite valve<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-campaign-platform" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>EDWARDS INTUITY Elite valve</h3>
                                                        <p><img src="../img/ttc-intuity-elite-valve.png" alt="EDWARDS INTUITY Elite valve" class="img-responsive center-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-campaign-platform" href="#collapse2-campaign-platform" class="collapsed" aria-expanded="false">
          INSPIRIS RESILIA valve<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse2-campaign-platform" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>INSPIRIS RESILIA valve</h3>
                                                        <p><img src="../img/ttc-inspiris-resilia-valve.png" alt="INSPIRIS RESILIA valve" class="img-responsive center-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-campaign-platform -->
                                        <!-- accordion-campaign-execution -->
                                        <h4>Campaign execution</h4>
                                        <div class="panel-group" id="accordion-campaign-execution">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-campaign-execution" href="#collapse1-campaign-execution" class="collapsed" aria-expanded="false">
          Extreme horizontal formats<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-campaign-execution" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Extreme horizontal formats</h3>
                                                        <p>As a secondary approach in extreme horizontal formats, the evolution line will shift to a horizontal layout. As illustrated in the example below, when shifting
to a horizontal format of the evolution line, product statements should remain the dominant visual presence and the line should lead to the featured product.
As a result of the extreme horizontal format, it is important to both start and end with the headline and subhead messaging.</p>
<p><img src="../img/ttc-extreme-horizontal-formats.png" alt="Extreme horizontal formats" class="img-responsive"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-campaign-execution -->
                                        <!-- accordion-design-strategy -->
                                        <h4>Design strategy</h4>
                                        <div class="panel-group" id="accordion-design-strategy">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-design-strategy" href="#collapse1-design-strategy" class="collapsed" aria-expanded="false">
          Design strategy<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-design-strategy" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Design strategy</h3>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p>Our campaign design is built on the foundation
of the Edwards corporate brand standards.
Please refer to the most current version of the
Brand Identity Standards for logo usage, legal
requirements and additional details.</p>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <p><img src="../img/ttc-design-strategy.png" alt="Design strategy" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-design-strategy" href="#collapse2-design-strategy" class="collapsed" aria-expanded="false">
          Transforming tomorrow<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse2-design-strategy" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Transforming tomorrow</h3>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <p>When we look toward tomorrow, we often picture
the first light of dawn and the warm colors of the
sunrise that represent new opportunities, new
ideas, and taking another step on our journey
of life.</p>
<p>Each step on the journey of innovation contributes
to the success of the next. When we honor our
past achievements, we reinforce the rich history
of research and development behind our current
technology. The movement of time, represented
by the white line, converges with the light of
dawn at the featured moment of innovation. This
journey does not end with today’s technology, but
continues into the future where our innovation will
continue to advance patient care.</p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/ttc-transforming-tomorrow.png" alt="Transforming tomorrow" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-design-strategy" href="#collapse3-design-strategy" class="collapsed" aria-expanded="false">
          Large secondary graphic usage<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse3-design-strategy" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Large secondary graphic usage</h3>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p>The secondary graphic consists
of one small square – positioned
lower left – connected to a larger
square above and to the right.
The smaller square should remain
solid red (PANTONE 186), without
transparency or tints.</p>
<p><strong>Large secondary graphic</strong></p>
<p>The large secondary graphic allows
for photos to be combined with titles
or headlines into one frame.</p>
<p>The campaign background graphic is
inserted into the large graphic area
of the layout and bleeds off the top
and right of the page.</p>
<p>See the Edwards Brand Identity
Standards Guide, page 25
(“Secondary Graphic Overview”) for
more information.</p>
<p>Please note, the small secondary
graphic templates should not be
used for the campaign.</p>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <p><img src="../img/ttc-large-secondary-graphic-usage.png" alt="Large secondary graphic usage" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-design-strategy" href="#collapse4-design-strategy" class="collapsed" aria-expanded="false">
          Selected colors<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse4-design-strategy" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Selected colors</h3>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p>For the Surgical Structural Heart brand campaign, the background fill is
a gradient that starts in the upper left corner with
PMS 7535@25% (C2 M2.2 Y4.6 K3.8) and moves to
PMS 7530@35% (C3.5 M6.3 Y8.75 K11.2) at the
lower right corner.</p>

                                                            </div>
                                                            <div class="col-sm-8">
                                                                 <p><img src="../img/ttc-selected-colors.png" alt="Selected colors" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                        <p><strong>Primary corporate colors</strong></p>
<p>From the red secondary graphic to the titanium
logo, these two colors are our primary colors and
should dominate our materials. Use red for titles
and headlines, charts and diagrams. <strong>Do not tint
the red</strong>. The gray may be tinted to 75%, 50%, 25%
or lighter when used for information graphics.
It may also be used as a field of color behind the
secondary graphic, but tinted to 50% or lighter.</p>
<p>For the Surgical Structural Heart brand campaign, the Primary Red is
used for the Headline, small red box, calls to action,
and other accents. The Primary Gray is used
for the subheading, product callouts, some
body copy type, and other accents.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-design-strategy" href="#collapse5-design-strategy" class="collapsed" aria-expanded="false">
          Background<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse5-design-strategy" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Background</h3>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p>Our campaign background features a closeup
image of a surgeon at work on a neutral
background. This image is softened to help the
audience member see himself/herself in the
position of the surgeon and their own role in
transforming tomorrow for their patients.</p>
<p><strong>Background</strong></p>
<p>The background fill is a gradient that starts in the upper left
corner with PMS 7535@25% (C2 M2.2 Y4.6 K3.8) and moves
to PMS 7530@35% (C3.5 M6.3 Y8.75 K11.2) at the lower right
corner. Sweeps of semi-transparent white are draped across
the background to create the movement of the campaign.</p>


                                                            </div>
                                                            <div class="col-sm-7">
                                                                <p><img src="../img/ttc-background.png" alt="Background" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                        <p><strong>Highlight</strong></p>
<p>The highlight that radiates from the upper left corner is a
placed image screened over the background. See the next page
for more information about the highlight.</p>
                                                        <p><strong>Evolution line (timeline)</strong></p>
<p>The white arc that runs somewhat vertically is the timeline. It
converges at the intersection of the two sweeps in the lower
right.</p>
<p>The timeline should only be used with timeline points. These
points are not included in the background file, as they will
vary in shape and position. The timeline should never be left
without at least a timeline point at the convergence of the
three arcs.</p>
<p>The sweeps and timeline all fade before reaching the edges of
the frame of the background so there is no visible division of
the background shape.</p>
<p><strong>Alternate backgrounds</strong></p>
<p>Versions of the artwork without the surgeon photo and
without the white timeline may also be used.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-design-strategy" href="#collapse6-design-strategy" class="collapsed" aria-expanded="false">
          Background highlight<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse6-design-strategy" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Background highlight</h3>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <p>The subtle highlight that radiates
from the upper left corner of the
background frame brings energy and
optimism to the campaign.</p>
<p><strong>Highlight image</strong></p>
<p>The highlight image is added to the
artwork on top of the background
image, and the mode of the highlight
is set to “Screen” and used at 100%
opacity.</p>
<p>The center of the burst in the image
should be offset to the upper right,
outside of the background image
frame, as shown to the right. The
center convergence point of the
burst should never appear on the
artwork.</p>
<p>The highlight image is not included
in the background file, as its size and
position will change with the size and
proportion of the artwork overall.</p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/ttc-background-highlight.png" alt="Background highlight" class="img-responsive"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-design-strategy" href="#collapse7-design-strategy" class="collapsed" aria-expanded="false">
          Timeline/evolution line<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse7-design-strategy" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Timeline/evolution line</h3>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p>The timeline/evolution line is at the
heart of the Surgical Structural Heart brand campaign.
It reinforces the headline and
shows the rich history of innovation
and dedication to improving our
product line.</p>
<p><strong>Timeline points</strong></p>
<p>The timeline point for the featured
product is centered on the
convergence point of the arcs in
the background. The other points
are centered on the timeline, and
are evenly spaced with the featured
product point.</p>

                                                            </div>
                                                            <div class="col-sm-7">
                                                                <p><img src="../img/ttc-timeline.png" alt="Timeline/evolution line" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                        <p><strong>Highlighting the featured product</strong></p>
<p>The photo and callout for the
featured product are larger than the
rest. On most layouts, the image of
the featured product will be to the
left of the timeline while the rest of
the products are on the right. The
photo of the featured product may
be moved to the right of the timeline
if more space is needed. A white glow
appears behind the photo of the
featured product to further highlight
its importance.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-design-strategy" href="#collapse8-design-strategy" class="collapsed" aria-expanded="false">
          Featured products and attributes<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse8-design-strategy" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Featured products and attributes</h3>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p><strong>Early innovation products</strong></p>
                                                                <p>Early product innovation are a key
element to telling the campaign
story in a visual manner. Building
creditability through game changing
technology, these selected product
visuals and messaging allow for
featured new products to have an
immediate association with industry
changing innovation.</p>
<p><strong>Featured products</strong></p>
<p>Featured product usage should be
limited to one product in almost all
cases. The featured product should
be the latest product released
within the specific region and
should resonate with surgeons as an
illustration of Edwards’ commitment
to innovation as it relates to surgical
heart valve therapy.</p>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <p><img src="../img/ttc-attributes.png" alt="Featured products and attributes" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-design-strategy -->
                                        <!-- accordion-legal-copy -->
                                        <h4>Required legal copy (US)</h4>
                                        <div class="panel-group" id="accordion-legal-copy">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-legal-copy" href="#collapse1-legal-copy" class="collapsed" aria-expanded="false">
          Required legal copy (US)<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-legal-copy" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Required legal copy (US)</h3>
                                                       
                                                                <div class="well intro"><p>Required legal copy is noted for
                                                                U.S. executions only, but is intended
                                                                to showcase the disclaimers
                                                                necessary for product claims.</p>
                                                                <p>Please verify necessary legal for
                                                                regional execution.</p></div>
                                                            
                                                            <div class="small">
                                                                <p><sup>*</sup>X-ray cutout view to demonstrate the valve’s stent design.</p>
                                                                <p><strong>Important Safety Information: Surgical Aortic Bioprosthesis/ EDWARDS INTUITY Elite Valve System (Aortic Valve and Delivery System)</strong></p>
                                                                <p><strong>Indications:</strong> For use in patients whose aortic valvular disease warrants the replacement of diseased, damaged or malfunctioning native or prosthetic valve.
<strong>Contraindications:</strong> Do not use the bioprosthetic heart valve if the surgeon believes it would be contrary to the patient’s best interests. The EDWARDS INTUITY Elite
valve is contraindicated for use in patients with pure aortic insufficiency and aneurysms of the aortic root or ascending aorta. <strong>Warnings:</strong> Alternative therapies should
be considered in the presence of conditions affecting calcium metabolism or when calcium containing chronic drug therapies are used, including children, adolescents,
young adults, and patients on a high calcium diet or maintenance hemodialysis. Bioprosthetic valve should be used with caution in the presence of severe systemic
hypertension or when anticipated patient longevity is longer than the known longevity of the prosthesis. As with any implanted device, there is potential for an
immunological response. The safety and effectiveness of the EDWARDS INTUITY Elite valve has not been studied in the following specific populations: patients who
are pregnant or lactating; patients with chronic renal impairment or calcium metabolism disorders; patients with active endocarditis or myocarditis; or children or
adolescents. Use of the EDWARDS INTUITY Elite valve system may be associated with new or worsened conduction system disturbances, which may require a permanent
pacemaker implant. <strong>Potential Adverse Events:</strong> Adverse events potentially associated with the use of bioprosthetic heart valves including the EDWARDS INTUITY
Elite valve and aortic valve replacement surgery include but are not limited to: stenosis; paravalvular (perivalvular) leak; transvalvular regurgitation annulus damage;
dissection, or tear; hemolysis; valve thrombosis; anemia; hemorrhage; cardiac arrhythmias/conduction disturbances; congestive heart failure; endocarditis; leaflet
impingement (aortic or mitral); myocardial infarction (MI); angina; neurologic events (transient ischemic attack/stroke); patient-prosthesis mismatch (PPM) (due to
inappropriate sizing); reoperation or re-intervention, structural/non-structural valve dysfunction; permanent disability; explantation and death.</p>
<p>Additional potential risks associated with the use of a bioprosthetic valve with a reduced number of sutures similar to the EDWARDS INTUITY Elite valve include: valve
frame distortion (from chest compression or trauma); and valve malposition, instability, dislodgement or migration/embolization.</p>
<p><strong>CAUTION: Federal (United States) law restricts this device to sale by or on the order of a physician. See instructions for use for full prescribing information. See
instructions for use for full prescribing information, including indications, contraindications, warnings, precautions and adverse events.</strong></p>
<p>Edwards, Edwards Lifesciences, the stylized E logo, Carpentier-Edwards, Magna, Magna Ease, EDWARDS INTUITY, EDWARDS INTUITY Elite, Starr-Edwards, PERI,
PERIMOUNT, and PERIMOUNT Magna are trademarks of Edwards Lifesciences Corporation. All other trademarks are the property of their respective owners.</p>
<p>&copy; 2017 Edwards Lifesciences Corporation. All rights reserved. PP--US-XXXX v1.0</p>
<p><strong>Edwards Lifesciences</strong> • One Edwards Way, Irvine CA 92614 • edwards.com</p>
                                                            </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-legal-copy -->
                                        <!-- accordion-advertising -->
                                        <h4>Advertising</h4>
                                        <div class="panel-group" id="accordion-advertising">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-advertising" href="#collapse1-advertising" class="collapsed" aria-expanded="false">
          Print<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-advertising" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Print</h3>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p>Print ads should include the
following elements:</p>
<ul>
    <li>Core evolution line and campaign
backgrounds</li>
    <li>CGI product images included in
core campaign assets</li>
    <li>Headline</li>
    <li>Subhead</li>
</ul>

                                                            </div>
                                                            <div class="col-sm-7">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <p><img src="../img/ttc-print1.png" alt="Print" class="img-responsive center-block"></p>
                                                                    <p class="text-center"><strong>PP--US-2046</strong></p>
                                                                </div><!-- /.col-sm-6 -->
                                                                <div class="col-sm-6">
                                                                    <p><img src="../img/ttc-print2.png" alt="Print" class="img-responsive center-block"></p>
                                                                    <p class="text-center"><strong>Example only. Not currently approved in VAULT.</strong></p>
                                                                </div><!-- /.col-sm-6 -->
                                                            </div><!-- /.row -->
                                                            </div>
                                                        </div>
                                                        <p>Each element is placed according
to the Edwards Brand Standards.
The usage of the body copy will be
determined based on the size of the
ads. Please ensure to leverage a callto-
action below the large secondary
box and brief summary should be
included in all ads.</p>
<p>When placing a featured product,
please ensure it aligns with the
convergence of lines within the
background and is placed to the left
of the red point on the evolution line.</p>
<p>Do not crowd the visuals with copy.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-advertising" href="#collapse2-advertising" class="collapsed" aria-expanded="false">
          Digital<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse2-advertising" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Digital</h3>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p>Digital ads should only placed as animated ads
in order to effectively represent the story of the
brand. Ads should be developed in an HTML5
or GIF format and should include the following
elements:</p>
<ul>
    <li>Core evolution line and campaign backgrounds</li>
    <li>The featured product images included in core
campaign assets</li>
    <li>Headline</li>
    <li>Subhead</li>
</ul>

                                                            </div>
                                                            <div class="col-sm-7">
                                                                <p><img src="../img/ttc-digital.png" alt="Digital" class="img-responsive center-block"></p>
                                                                <p class="text-center"><strong>Examples are provided for reference only. Not currently approved in VAULT.</strong>
</p><!-- /.text-center -->
                                                            </div>
                                                        </div>
                                                        <p>Headline should always be the first visual, then
subhead, then introduce the product and call to
action necessary.</p>
<p>Each element is placed according to the Edwards
Brand Standards. The usage of the body copy will
be determined based on the size of the ads. Please
ensure to leverage a call-to-action below the
large secondary box and brief summary should be
included in all ads scrolling across the bottom.</p>
<p>When placing a featured product, please ensure
it aligns with the convergence of lines within the
background and is placed to the left of the red
point on the evolution line.</p>
<p>Do not crowd the visuals with copy.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-advertising -->
                                        <!-- accordion-templates -->
                                        <h4>Templates</h4>
                                        <div class="panel-group" id="accordion-templates">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-templates" href="#collapse1-templates" class="collapsed" aria-expanded="false">
          Newsletter<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-templates" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Newsletter</h3>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <p><strong>Campaign core messaging</strong></p>
                                                                <p>Ensure the campaign background and headline
are leveraged as shown within the template as
masthead in Newsletter communication.</p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                 <p><img src="../img/ttc-newsletter.png" alt="Newsletter" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-templates" href="#collapse2-templates" class="collapsed" aria-expanded="false">
          Email<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse2-templates" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Email</h3>
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p>Campaign emails should leverage the core background with
circular convergence on the call to action or core message
of the email.</p>
<p>For desktop emails that are 700px wide, the logo is 50px
(based on the height of the E symbol); the area surrounding
the logo is 102x102px For mobile, the logo area scales
down proportionately to 92x92px, so that the E symbol is
approximately 45px high</p>

                                                            </div>
                                                            <div class="col-sm-7">
                                                                <p><img src="../img/ttc-email.png" alt="Email" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                        <p><strong>Desktop</strong></p>
<p>The larger red shape in the secondary graphic holds the title,
but it may also include a photo HTML can be used for fluid
width e-mails to ensure the red graphic extends the width
of the template Images should be used when the width of
each e-mail is fixed for mobile and desktop The height of the
larger square may increase to hold longer titles or photos,
but for brevity, avoid titles longer than two lines Align the
content on desktop emails to the right edge of the logo –
this margin is equal to 78px Use the same measurement
for the right margin. Use Arial Regular for content Use Arial
Bold for subheads or to add emphasis</p>
<p><strong>Mobile</strong></p>  
<p>HTML can be used for fluid width e-mails to ensure the red
graphic extends the width of the template Images should
be used when the width of each e-mail is fixed for mobile
and desktop The height of the larger square may increase
to hold longer titles or photos Align the content on mobile
layouts to the left edge of the Edwards logo which is equal
to 25px The right margin is also 25px</p>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-templates" href="#collapse3-templates" class="collapsed" aria-expanded="false">
          PowerPoint presentation<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse3-templates" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>PowerPoint presentation</h3>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <p>The PowerPoint presentation templates
contain approved layouts and sample slides
for cover, content (including text, chart,
graphic and table options), section header,
disclosure and end slides.</p>
<p>The templates have been carefully constructed.
Do not change the size or position of master
slide elements. Read the sample slides for tips
on how to use and customize the template.</p>
<p><strong>Proportion</strong></p>
<p>There are templates for 16:9 (10” x 5.63”,
shown here), 4:3 (10” x 7.5”) and vertical
(7.5” x 10”) proportions. The same master
and sample slides are in all templates.</p>

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/ttc-powerpoint-presentation.png" alt="Powerpoint presentation" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                        <p><strong>Colors</strong></p>
<p>The built-in colors are based on the approved
RGB color formulas, with a few adjustments
to enhance chart legibility. See the “Our color
palette” slide in the template for details and
specifications.</p>
<p><strong>Fonts</strong></p>
<p>For optimal compatibility, the PowerPoint
templates use Arial exclusively for all live text.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-templates" href="#collapse4-templates" class="collapsed" aria-expanded="false">
          Infographic<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse4-templates" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Infographic</h3>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <p>The infographic is designed to
showcase a visual storytelling
graphic that supports Edwards
evolution as an innovating, gamechanging
market leader. It is through
the dynamic layout that key visuals
should be used to support short, key
messages or statistics.</p>
                                                                <p>The infographic examples provided
should be leveraged with minimal
changes to overall approach.</p>
                                                                <p>The graphics are intended to be
used in email communications and
web experiences, where a scrolling
experience is natural for readers.</p>
<p><strong>Note: infographic is for example purposes only. Not currently approved in VAULT.</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/ttc-infographic.png" alt="Infographic" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-templates -->
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Campaign in action</h3>
                                        <!-- accordion-campaign1 -->
                                        <div class="panel-group" id="accordion-campaign1">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-campaign1" href="#collapse1-campaign1" class="collapsed" aria-expanded="false">
          Conference advertisements<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-campaign1" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Conference advertisements</h3>
                                                        <div class="row text-center">
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/ttc-conference-advertisements1.png" alt="Conference advertisements" class="img-responsive center-block"></p>
                                                                <p>Featuring EDWARDS INTUITY Elite valve system</p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/ttc-conference-advertisements2.png" alt="Conference advertisements" class="img-responsive center-block"></p>
                                                                <p>Featuring INSPIRIS RESILIA aortic valve</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-campaign1" href="#collapse2-campaign1" class="collapsed" aria-expanded="false">
          2017 AATS booth graphics<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse2-campaign1" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>2017 AATS booth graphics</h3>
                                                        <p><img src="../img/ttc-aats.png" alt="2017 AATS booth graphics" class="img-responsive center-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-campaign1" href="#collapse3-campaign1" class="collapsed" aria-expanded="false">
          Sample booth graphics<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse3-campaign1" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Sample booth graphics</h3>
                                                        <p><img src="../img/ttc-sample-booth.png" alt="Sample booth graphics" class="img-responsive center-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-campaign1 -->
                                        <!-- accordion-campaign2 -->
                                        <h4>Environmental signage</h4>
                                        <div class="panel-group" id="accordion-campaign2">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-campaign2" href="#collapse1-campaign2" class="collapsed" aria-expanded="false">
          Billboard and banner style<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-campaign2" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Billboard and banner style</h3>
                                                        <p><img src="../img/ttc-billboard.png" alt="Billboard and banner style" class="img-responsive center-block"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-campaign2" href="#collapse2-campaign2" class="collapsed" aria-expanded="false">
          Gallery style<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse2-campaign2" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Gallery style</h3>
                                                        <p>As a secondary approach in extreme horizontal formats, the evolution line will shift to a horizontal layout. As illustrated in the example below, when shifting
to a horizontal format of the evolution line, product statements should remain the dominant visual presence and the line should lead to the featured product.
As a result of the extreme horizontal format, it is important to both start and end with the headline and subhead messaging.</p>
<p><strong>Featuring EDWARDS INTUITY Elite valve</strong></p>
<p><img src="../img/ttc-gallery1.png" alt="Gallery style" class="img-responsive"></p>
<p><strong>Featuring INSPIRIS RESILIA aortic valve</strong></p>
<p><img src="../img/ttc-gallery2.png" alt="Gallery style" class="img-responsive"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-campaign2 -->
                                    </div>
                                    <div class="pane" id="tabs-4">
                                        <h3>Launch strategies</h3>
                                        <!-- accordion-launch-strategies1 -->
                                        <div class="panel-group" id="accordion-launch-strategies1">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-launch-strategies1" href="#collapse1-launch-strategies1" class="collapsed" aria-expanded="false">
          Designing the surgeon journey<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-launch-strategies1" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Designing the surgeon journey</h3>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <ul>
                                                                    <li>Launch with a statement</li>
                                                                    <li>Reinforce the commitment through
high-level attention grabbing brand
messaging and communication tactics
to engage the conversation</li>
                                                                    <li>Leading messaging strategy will transition
each surgeon into the product-level
communication pipeline</li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><img src="../img/ttc-journey.png" alt="Designing the surgeon journey" class="img-responsive center-block"></p>
                                                            </div>
                                                        </div>
                                                        <p>Integration &amp; collaboration is key to successful cohesive and effective communication</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-launch-strategies1" href="#collapse2-launch-strategies1" class="collapsed" aria-expanded="false">
          360&deg; launch strategy<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse2-launch-strategies1" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>360&deg; launch strategy</h3>
                                                        <p><img src="../img/ttc-launch-strategy.png" alt="360&deg; launch strategy" class="img-responsive"></p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-launch-strategies1 -->
                                        <!-- accordion-launch-strategies2 -->
                                        <h4>eLaunch programs</h4>
                                        <div class="panel-group" id="accordion-launch-strategies2">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-launch-strategies2" href="#collapse1-launch-strategies2" class="collapsed" aria-expanded="false">
          Email<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse1-launch-strategies2" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Email</h3>
                                                        <p><img src="../img/ttc-elaunch-email.png" alt="Email" class="img-responsive"></p>
                                                        <p>Integration &amp; collaboration is key to successful cohesive and effective communication</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-launch-strategies2" href="#collapse2-launch-strategies2" class="collapsed" aria-expanded="false">
          Webinar<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse2-launch-strategies2" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>Webinar</h3>
                                                        <p><img src="../img/ttc-elaunch-webinar.png" alt="Webinar" class="img-responsive"></p>
                                                        <p>Case/surgeon highlight / product highlight / research highlight / innovation highlight</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-launch-strategies2" href="#collapse3-launch-strategies2" class="collapsed" aria-expanded="false">
          eNewsletter<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
        </h4>
                                                </div>
                                                <div id="collapse3-launch-strategies2" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <h3>eNewsletter</h3>
                                                        <p><img src="../img/ttc-elaunch-enewsletter.png" alt="enewsletter" class="img-responsive"></p>
                                                        <p>Blog highlight style newsletter driving to microsite with product feature per release</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- //accordion-launch-strategies2 -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
