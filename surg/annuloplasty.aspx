﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-annuloplasty" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Surgical Structural Heart brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
                                <li>Tier 3 brand books</li>
                                <li>General annuloplasty rings</li>
                            </ol>
                            <div id="tabslider">
                                <h2>General annuloplasty rings<a href="../downloads/print-annuloplasty.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Carpentier-Edwards Physio annuloplasty ring</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Carpentier-Edwards Classic annuloplasty ring</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Carpentier-Edwards Physio annuloplasty ring</h3>
                                        <h4>Product positioning / key messages</h4>
                                        <p><strong>Note:</strong> All statements are approved in vault and some statements may require an accompanying disclaimer.</p>
                                        <p>For more detailed product messaging, please visit Vault to access up-to-date claims matrices, or contact your local marketing or brand team.</p>
                                        <p><strong>Approved statements</strong></p>
                                        <ul>
                                            <li>Provides the advantage of the remodeling concept while preserving flexibility</li>
                                            <li>Remodeling preserves 3:4 ratio between the anteroposterior diameer and transverse diameter during systole</li>
                                            <li>Restores anatomical size and shape to provide optimal orifice area</li>
                                            <li>Progressive posterior flexibility allows for physiologic contractility of the mitral valve annulus during systole</li>
                                        </ul>
                                        <h4>Digital images and illustrations</h4>
                                        <div class="row">
                                            
                                            <div class="col-sm-6">
                                            <p><strong>Core visual</strong></p>
                                            <p>If this product is used along with other Heart Valve Therapy products in the same communications, please use the primary photo of the product on a background consisting of the corporate color palette.</p></div>
                                            <div class="col-sm-6">
                                            <p><img src="../img/annuloplasty-digital.png" alt="Digital images and illustrations" class="img-responsive center-block"></p></div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Carpentier-Edwards Classic annuloplasty ring</h3>
                                        <h4>Product positioning / key messages</h4>
                                        <p><strong>Note:</strong> All statements are approved in Vault and some statements may require an accompanying disclaimer.</p>
                                        <p>For more detailed product messaging, please visit Vault to access up-to-date claims matrices, or contact your local marketing or brand team.</p>
                                        <table class="table table-striped">
                                            <thead>
                                              <tr>
                                                <th class="col-sm-6">Key messages</th>
                                                <th class="col-sm-6">Supportive statements</th>
                                                
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                <td>Designed to restore the anatomical size and shape of the mitral valve</td>
                                                <td>
                                                    <ul>
                                                        <li>Designed to remodel the annulus, to restore the 3 : 4 ratio between the anteroposterior and transverse diameters for optimal leaflet coaptation</li>
                                                        <li>Solid titanium core provides strength and durability</li>
                                                        <li>Prevents recurrent dilatation on deformation</li>
                                                        <li>Colored thread indicates anterior and posterior commissures</li>
                                                    </ul>
                                                </td>
                                               
                                              </tr>
                                              <tr>
                                                <td>Designed to restore the anatomical size and shape of the tricuspid valve</td>
                                                <td><ul>
                                                <li>Oval shape conforms to the systolic configuration of the normal tricuspid orifice</li>
                                                <li>Prevents recurrent dilation</li>
                                                <li>Opening in the anteroseptal commissure avoids the conduction system</li>
                                                <li>Solid titanium core provides strength and durability</li>
                                                <li>Transverse colored thread facilitates location of the commissures and proper positioning of the ring</li>
                                                </ul></td>
                                                
                                              </tr>
                                             
                                            </tbody>
                                          </table>
                                        <h4>Campaign story</h4>
                                        <p><strong>Headline</strong></p>
                                        <p>The repair device of choice for more than 30 years.</p>
                                        <div class="well intro">
                                          <p>Product images, procedural images and collateral pdf files for regional and cross-functional team use can be accessed via <a href="https://edwards.webdamdb.com/"><strong><u>Webdam</u></strong></a>. You may also contact your local marketing or brand team.</p>
                                          <p><a href="https://edwards.webdamdb.com/" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Access Webdam</a></p>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
