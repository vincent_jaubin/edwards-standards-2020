﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <title>Heart valve therapy brand book | Edwards Lifesciences</title>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-intro" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Surgical Structural Heart brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
                                <li>Contents</li>
                                <li>Introduction</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Introduction<a href="../downloads/print-intro.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Global Brand Management Mission</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Unified brand process and support</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Access to your resources</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>Contacts</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Global Brand Management Mission</h3>
                                        <div class="well intro">
                                            <h4>Our mission, in global brand management and events, is to be strategic partners in building and consistently promoting Edwards brands globally.</h4>
                                        </div>
                                        <p><strong>The advantages of consistent branding</strong></p>
                                        <ol>
                                            <li><strong>Reinforcement.</strong> Consistent repetition is what drives brand recognition. When a brand is repeated constantly and consistently over time and in different arenas, customers learn to recognize that brand and come to rely on it.</li>
                                            <li><strong>Loyalty.</strong> When a brand is consistent, it creates a sense of reliability and trust that is transferred to the product. A brand that consistently performs anywhere, anytime, under any circumstances, will lead to loyal customers.</li>
                                            <li><strong>Efficiency.</strong> In the development of materials, unified branding maximizes promotional budgets across disciplines. It also creates a common language that is used and understood by everyone involved with the brand.</li>
                                        </ol>
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Unified brand process and support</h3>
                                        <p>Each region will have varying legal and regulatory approval processes. Please refer to your local SOP for these processes and resources to guide the approval of your communications.</p>
                                        <div class="panel-group" id="accordion-brand-process">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-brand-process" href="#collapse1-brand-process">
        Needs analysis<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-brand-process" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Needs analysis</h3>
                                                        <ul>
                                                            <li>Identify communication needs.</li>
                                                            <li>Determine proper messaging balance for local audience.</li>
                                                            <li>Determine if metrics are needed (i.e. what does success look like?).</li>
                                                            <li>Collaborate with your global brand partners at the beginning of the project, to determine the best process, vendors, and brand communications solutions.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-brand-process" href="#collapse2-brand-process">
        Collateral development<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse2-brand-process" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Collateral development</h3>
                                                        <ul>
                                                            <li>Refer to brand book and/or Surgical Structural Heart global marketing team to determine if collateral exists. If so, please use existing collateral.</li>
                                                            <li>Source existing key visual (KV) files and key messages from global intranet site, and the DRL.</li>
                                                            <li>Refer to the brand book and Edwards brand guidelines for all layout and messaging guidance.</li>
                                                            <li>Work with your local vendor with these tools to build collateral layout and design.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-brand-process" href="#collapse3-brand-process">
        Global + regional / functional collaboration<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse3-brand-process" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Global + regional / functional collaboration</h3>
                                                        <ul>
                                                            <li>Send the designed communication to global Surgical Structural Heart brand management for brand and messaging approval prior to finalizing.</li>
                                                            <li>Please indicate your timing and deadlines for approvals. 1-3 days is appropriate.</li>
                                                            <li>Conduct regional copy review for regulatory, legal, and trademark – if required.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-brand-process" href="#collapse4-brand-process">
        Product and execution<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse4-brand-process" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Product and execution</h3>
                                                        <ul>
                                                            <li>Produce collateral through your local vendor.</li>
                                                            <li>Upload final art files through WebDam.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-brand-process" href="#collapse5-brand-process">
        Evaluation<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse5-brand-process" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Evaluation</h3>
                                                        <ul>
                                                            <li>Capture feedback and metrics as designated in needs analysis.</li>
                                                            <li>Convey key learning’s to Surgical Structural Heart brand management and global marketing.</li>
                                                            <li>Determine as a team if future communications are necessary.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Access to your resources</h3>
                                        <p><strong>WebDam</strong></p>
                                        <p>WebDam is a cloud-based software providing an efficient way of centralizing, tracking, managing, locating and sharing digital assets within our company. It is an easy-to-use central repository for employees and vendors to locate and retrieve digital media — perfect for photos, graphics, videos, presentations, documents and other media.</p>
                                        <p><strong>New users (Edwards employees):</strong></p>
                                        <p>Request access by clicking "Register" on the WebDam home page. Please allow 1-2 business days for approval.</p>
                                        <p><strong>New users (outside vendors):</strong></p>
                                        <p>If you are a vendor and require access to WebDam, please contact Jade Jourdan in corporate brand at extension 3560 or at <a href="mailto:jade_jourdan@edwards.com">jade_jourdan@edwards.com</a> to request access. Please provide Jade Jordan your internal Edwards contact, vendor name, contact information and a copy of the signed non-disclosure agreement issued at the beginning of the project or initiative. You will then be able to set up a user name and password to access the system.</p>
                                        <div class="row marg20">
                                            <div class="col-sm-6">
                                                <p><strong>Current users (external):</strong>
                                                </p>
                                                <p>How to gain access from outside the Edwards intranet system: To access this library click on the following link: <a href="https://edwards.webdamdb.com/">https://edwards.webdamdb.com/</a> Enter your user ID and password, then open “b. Heart Valve Therapy”, click on the folder to access all assets associated with Edwards campaigns, branded products, and services.</p>
                                                <p><strong>Current users (internal):</strong>
                                                </p>
                                                <p>How to gain access from inside the Edwards Intranet system: To access this library type drl into the address field in your browser or click on the following link <a href="https://edwards.webdamdb.com/">https://edwards.webdamdb.com/</a> Enter your user ID and password, then you can click on the appropriate folder for your project.</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <img class="img-responsive" src="../img/drl.jpg" alt="Digital asset library">
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                     <div class="pane" id="tabs-4">
                                     <h3>Surgical Structural Heart global brand management and events contacts:</h3>
                                        <div class="row marg20">
                                            <div class="col-sm-5">
                                                <strong>Michelle Bywater</strong>
                                                <br>Director, Global Brand and Events
                                                <br>
                                                <span class="icon-phone"></span> 949-250-6893
                                                <br>
                                                <a href="mailto:michelle_bywater@edwards.com"><span class="icon-envelop"></span> michelle_bywater@edwards.com</a>
                                            </div>
                                            <div class="col-sm-5">
                                            <strong>Lisa Mahar</strong><br>
Group Manager, Global Brand<br>
949-250-0259<br>
<a href="mailto:lisa_mahar@edwards.com"><span class="icon-envelop"></span> lisa_mahar@edwards.com</a>

                                            </div></div>
                                            <div class="row marg20">
											<div class="col-sm-5">
                                                <strong>Dianne Kelly</strong>
                                                <br>Sr. Specialist, Congress Management<br>
                                                <span class="icon-phone"></span> 949-250-3774
                                                <br>
                                                <a href="mailto:dianne_kelly@edwards.com"><span class="icon-envelop"></span> dianne_kelly@edwards.com</a>
                                            </div>
                                            
                                            		<div class="col-sm-5">
                                                    
                                                <strong>Megan Cortez</strong>
                                                <br>Specialist, Global Brand and Events<br>
                                                <span class="icon-phone"></span> 949-250-1467
                                                <br>
                                                <a href="mailto:megan_cortez@edwards.com"><span class="icon-envelop"></span> megan_cortez@edwards.com</a>
                                            </div>
                                            
                                            
                                        </div>
                                     </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
