﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="hvt-perimount-aortic" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Surgical Structural Heart brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
                                <li>Tier 3 brand books</li>
                                <li>PERIMOUNT aortic valve</li>
                            </ol>
                            <div id="tabslider">
                                <h2>PERIMOUNT aortic valve<a href="../downloads/print-perimount-aortic.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Brand story</span></a></li>
                                    <li><a href="#tabs-2" class="match-height"><span>Brand components</span></a></li>
                                    <li><a href="#tabs-3" class="match-height"><span>Communications</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Brand story</h3>
                                        <h4>Brand strategy</h4>
                                        <p>The brand strategy of PERIMOUNT Aortic valves is to drive awareness of an aortic heart valve that delivers uncompromised durability, and excellent hemodynamic performance.</p>
                                       
                                    </div>
                                    <div class="pane" id="tabs-2">
                                        <h3>Brand components</h3>
                                        <div class="panel-group" id="accordion-brand-components">
                                          <div class="panel panel-primary">
                                            <div class="panel-heading">
                                              <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-brand-components" href="#collapse1-brand-components">
                                                Brand messaging<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                              </h4>
                                            </div>
                                            <div id="collapse1-brand-components" class="panel-collapse collapse">
                                              <div class="panel-body">
                                             <h3>Product positioning</h3>
                                             <p>The PERIMOUNT Aortic Valve is a proven high performing aortic bioprosthetic valve, providing exceptional long-term durability and excellent and stable hemodynamics by incorporating unique, time-tested PERIMOUNT design features.</p>
                                             <p><strong>Note:</strong> All statements are approved in the VAULT and some statements may require an accompanying disclaimer.</p>
                                             <h3>Body copy and building copy tone</h3>
                                            <p><strong>Note:</strong> All statements are approved in Vault and some statements may require an accompanying disclaimer.</p>
                                            <table class="table table-striped">
                                              <thead>
                                                <tr>
                                                  <th class="col-sm-6">Key message platform</th>
                                                  <th class="col-sm-6">Supporting statements</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                <tr>
                                                  <td class="col-sm-6">Proven PERIMOUNT design</td>
                                                  <td class="col-sm-6">
                                                  <p>Mathematically modeled, bioengineered design optimized for hemodynamics, durability, and implantability</p>
                                                  <p>Flexible cobalt-chromium alloy stent absorbs energy to reduce leaflet stress</p>
                                                  <p>Three independent bovine pericardial leaflets matched for thickness and elasticity to optimize stress distribution</p>
                                                  </td>
                                                  
                                                </tr>
                                                <tr>
                                                  <td class="col-sm-6">Exceptional long-term durability</td>
                                                  <td class="col-sm-6">Published clinical durability of up to 20 years</td>
                                                 
                                                </tr>
                                                <tr>
                                                  <td class="col-sm-6">Ease of implant</td>
                                                  <td class="col-sm-6">Exceptional annular conformity, accessible suturing areas, and efficient deliverability to challenging anatomy</td>
                                                  
                                                </tr>
                                                <tr>
                                                  <td>Advanced tissue treatment</td>
                                                  <td>ThermaFix process that confronts both major calcium binding sites (residual glutaraldehyde and phospholipids)</td>
                                                </tr>
                                              </tbody>
                                            </table>
                                             <h3>Campaign story</h3>
                                             <p>The brand campaign for the PERIMOUNT Aortic valve helps customers and prospects understand the product’s value. It articulates the brand promise and stimulates a desire to trial or adopt the product.</p>
                                             <p>Typically, the brand messaging is lead by a compelling headline and closes with a tagline that combines the product values, consequences and attributes, differentiating it from other product brands.</p>
                                             <p><strong>Headline</strong></p>
                                             <p>No current campaign dedicated to PERIMOUNT aortic valve</p>
                                             <p><strong>Tagline</strong></p>
                                             <p>n/a</p>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="panel panel-primary">
                                            <div class="panel-heading">
                                              <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-brand-components" href="#collapse2-brand-components">
                                                Digital images and illustrations<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                              </h4>
                                            </div>
                                            <div id="collapse2-brand-components" class="panel-collapse collapse">
                                              <div class="panel-body">
                                              <h3>Digital images - core visual</h3>
                                              <div class="row marg20">
                                                <div class="col-sm-6">
                                                  <p>The PERIMOUNT Aortic valve core visuals adhere to the same color and typographic standards of the Edwards corporate brand globally.</p>
                                                  <p>If this product is used along with other Heart Valve Therapy products in the same communications, please use the primary photo of the product on a background consisting of the corporate color palette, unless it is part of a localized campaign.</p>
                                                  <p><a href="https://edwards.webdamdb.com/" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Access Webdam</a></p>
                                                </div>
                                                <div class="col-sm-6">
                                                  <p><img src="../img/perimount-aortic-digital.png" alt="Digital images core visual" class="img-responsive center-block"></p>
                                                </div>
                                              </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="panel panel-primary">
                                            <div class="panel-heading">
                                              <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-brand-components" href="#collapse3-brand-components">
                                                Trademarks<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
                                              </h4>
                                            </div>
                                            <div id="collapse3-brand-components" class="panel-collapse collapse">
                                              <div class="panel-body">
                                              <h3>Trademarks</h3>
                                              <p><strong>Trademark / legal statement</strong></p>
                                                  <p>Edwards, Edwards Lifesciences, the stylized E logo, Carpentier-Edwards, and PERIMOUNT are trademarks of Edwards Lifesciences Corporation. All other trademarks are the property of their respective owners.</p>

                                                  <p>&copy; Edwards Lifesciences Corporation. All rights reserved. PPXXXXX</p>
                                                  <p><strong>Regulatory statements (as determined by Regional Regulatory Affairs) must be included as needed.</strong></p>
                                                   <p>For further trademark information, please refer to <a href="http://standards.edwards.com/pdf/Trademark_021616.pdf" target="_blank">Trademarks Style Guide</a>. Note: Exceptions or questions should be addressed to <a href="mailto:debbie_sklar@edwards.com">Debbie Sklar</a> or Edwards Intellectual Property Counsel.</p>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="pane" id="tabs-3">
                                        <h3>Communications</h3>
                                        <div class="well intro">
                                          <p>Commercialization materials such as product images, animations, procedural images and collateral pdf files for regional and cross-functional team use can be accessed via <a href="https://edwards.webdamdb.com/"><strong><u>Webdam</u></strong></a>. As new executions and tools are created, they will be uploaded to this site. You may also contact your local marketing or brand team.</p>
                                          <p><a href="https://edwards.webdamdb.com/" class="btn btn-info" role="button"><span class="icon-circle-right"></span> Access Webdam</a></p>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
