<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- #include file="../meta-top.html" -->
    </head>

    <body data-spy="scroll" data-target="#tabslider">
        <!-- #include file="../contact-form.html" -->
        <div id="edwards-wrapper">
            <!-- #include file="../header.html" -->
            <div id="employee" class="page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="redboxes-horz">
                            <div class="redbox-horz-sm"></div>
                            <div class="redbox-horz-lg">
                                <h1>Surgical Structural Heart brand standards</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.basic-header-edw -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 main-content">
                            <ol class="breadcrumb">
                                <li><a href="/surg/intro.aspx">Surgical Structural Heart brand standards</a></li>
                                <li>Tier 2 brand books</li>
                                <li>Employee communication initiative</li>
                            </ol>
                            <div id="tabslider">
                                <h2>Employee communication initiative<a href="../downloads/print-employee.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div>
<a href="../downloads/employee-communication-initiative.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Download PDF"><i class="icon-download"></i></a>
</h2>
                                <div class="clearfix"></div>
                                <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
                                    <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Surgical Structural Heart employee communication initiative</span></a></li>
                                    <li><a href="#tabs-4" class="match-height"><span>Communication templates</span></a></li>
                                </ul>
                                <div class="edw-tabslider">
                                    <div class="pane" id="tabs-1">
                                        <h3>Surgical Structural Heart employee communication initiative</h3>
                                        <h4>Campaign objectives</h4>
                                     
                                        <ul>
                                            <li>Build and strengthen the Edwards Surgical culture to increase morale in the midst of a changing environment</li>
                                            <li>Unify team under one vision
</li>
                                            <li>Increase focus on prioritization
</li>
                                            <li>Grow employee morale and engagement
</li>
                                        </ul>
                                        <div class="panel-group" id="accordion-communications">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion-communications" href="#collapse1-communications">
        Core messaging<span class="accordion-arrow glyphicon glyphicon-triangle-bottom"></span></a>
      </h4>
                                                </div>
                                                <div id="collapse1-communications" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <h3>Core messaging</h3>
                                                        <ul>
                                                            <li>Campaign headline
                                                                <ul>
                                                                    <li>Advance. Transform. Expand. Serve.
</li>
                                                                </ul>
                                                            </li>
                                                            <li>Pillar specific headlines:
                                                                <ul>
                                                                    <li>Advance - Extend our leadership and establish RESILIA as the standard of care</li>
<li>Transform - Develop innovative procedural solutions for surgical structural heart disease</li>
<li>Expand - Improve more patients’ lives around the globe and enter into adjacent therapies</li>
<li>Serve - Commit to patients, customers, colleagues, shareholders and the global community</li>

                                                                </ul>
                                                            </li>
                                                            <li>Supporting message
                                                                <ul>
                                                                    <li>Transform patients' lives by advancing surgical structural heart innovations
</li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="pane" id="tabs-2">
                                        <h3>Communication templates</h3>
                                        <h4>Powerpoint templates</h4>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <p><img src="../img/ppt-standard-hvt.PNG" alt="" class="img-responsive"></p>
                                                 <p><a href="../templates/surg/EDW_Surgical_PPT-Standard_v3.pptx" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Standard PPT template</a></p>
                                            </div><!-- /.col-sm-4 -->
                                               <div class="col-sm-4">
                                                <p><img src="../img/ppt-widescreen-hvt.PNG" alt="" class="img-responsive"></p>
                                                 <p><a href="../templates/surg/EDW_Surgical_PPTtemplate_Widescreen_v3.pptx" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Widescreen PPT template</a></p>
                                            </div><!-- /.col-sm-4 -->
                                        </div><!-- /.row -->
                                        <h4>Newsletter or memo templates</h4>
                                        <div class="row">
                                             <div class="col-sm-4">
                                                <p><img src="../img/word-1-hvt.PNG" alt="" class="img-responsive edw-border"></p>
                                                 <p><a href="../templates/surg/Edwards_Surgical_Template_v1.docx" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Word template 1</a></p>
                                            </div><!-- /.col-sm-4 -->
                                            <div class="col-sm-4"> <p><img src="../img/word-2-hvt.PNG" alt="" class="img-responsive edw-border"></p>
                                                 <p><a href="../templates/surg/Edwards_Surgical_Template_v2.docx" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Word template 2</a></p>
                                            </div><!-- /.col-sm-4 -->
                                             <div class="col-sm-4">
                                                <p><img src="../img/word-3-hvt.PNG" alt="" class="img-responsive edw-border"></p>
                                                 <p><a href="../templates/surg/Edwards_Surgical_Template_v3.docx" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Word template 3</a></p>
                                            </div><!-- /.col-sm-4 -->
                                        </div><!-- /.row -->
                                        
                                          <h4>Email header templates</h4>
                                        <div class="row">
                                             <div class="col-sm-4">
                                                <p><img src="../templates/surg/email header v1.png" alt="" class="img-responsive edw-border"></p>
                                                 <p><a href="../templates/surg/email header v1.png" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Email header 1 template</a></p>
                                            </div><!-- /.col-sm-4 -->
                                            <div class="col-sm-4"> <p><img src="../templates/surg/email header v2.png" alt="" class="img-responsive edw-border"></p>
                                                 <p><a href="../templates/surg/email header v2.png" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Email header 2 template</a></p>
                                            </div><!-- /.col-sm-4 -->
                                             <div class="col-sm-4">
                                                <p><img src="../templates/surg/email header v3.png" alt="" class="img-responsive edw-border"></p>
                                                 <p><a href="../templates/surg/email header v3.png" class="btn btn-info" role="button"><span class="icon-file-zip"></span> Email header 3 template</a></p>
                                            </div><!-- /.col-sm-4 -->
                                        </div><!-- /.row -->
                                        
                                        
                                        <h4>Resources</h4>
                                        <p>For access to templates, please visit <a href="https://edwards.webdamdb.com/">WebDam</a>, or contact your global brand team.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9">
                            <!-- #include file="../sidemenu-hvt.html" -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page -->
            <!-- #include file="../footer.html" -->
        </div>
        <!-- /wrapper -->
        <!-- #include file="../meta-bottom.html" -->
    </body>

    </html>
