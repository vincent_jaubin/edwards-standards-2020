﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="elements-altlogo" class="page">
  
  <div class="jumbotron banner-edw banner-branding">
    <div class="container">
      <div class="redboxes">
      <div class="redbox-sm"></div>
      <div class="redbox-lg">
        <h1>Legal terms</h1>
      </div>
      </div>
    </div>
  </div><!-- /.banner-edw --> 
  
  <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-12 main-content">
  
  <ol class="breadcrumb">
  <li><a href="index.aspx">Home</a></li>
  <li>Legal terms</li>
  </ol>
  
  
  
  <h2>Legal terms</h2>
  
  <h4>Please Read this Terms of Use Agreement and Privacy Policy Carefully Before Using These Sites</h4>
<p>By using these Sites, you signify your assent to these Terms of Use. If you do not agree to these Terms of Use, please do not use these Sites. This Terms of Use Agreement may be updated by us from time to time without notice to you. You can review the most current version of this Agreement at any time at:&nbsp; <a href="http://www.edwards.com/legal/legalterms#legal">www.edwards.com/LegalTerms.</a><br />In addition, when using particular Edwards Lifesciences services or participating in the events, you and Edwards Lifesciences shall be subject to any posted guidelines, additional terms or rules applicable to such services or events. All such guidelines or rules are hereby incorporated by reference into the Agreement.</p>
<h4>Disclaimers</h4>
<p>These Sites may contain information relating to various medical, health and fitness conditions and their treatment. This is for informational purposes only and is not meant to be a substitute for the advice provided by your own physician or other medical professional. You should not use the information contained herein for diagnosing a medical, health or fitness condition or disease. You should always consult your own physician and medical advisors.Although Edwards Lifesciences may from time to time monitor or review the Content posted in the Forums on its Sites, Edwards Lifesciences is under no obligation to do so and assumes no responsibility or liability arising from the Content contained in its Sites, or for any error, defamation, libel, slander, omission, falsehood, obscenity, pornography, profanity, danger or inaccuracy contained in any information within its Sites.</p>
<p>Edwards Lifesciences provides the contents of its Sites for informational purposes and for general interest and entertainment only. The materials and related graphics published on the Sites or accessed via the Sites could include technical inaccuracies or typographical errors. By using the Sites, you agree not to rely on any of the information contained herein.<br /><br />THE INFORMATION ON THESE SITES IS PROVIDED "AS IS" AND EDWARDS LIFESCIENCES MAKES NO REPRESENTATIONS OR WARRANTY THAT THE INFORMATION CONTAINED ON THESE SITES WILL BE TIMELY OR ERROR-FREE. EDWARDS LIFESCIENCES AND/OR ITS RESPECTIVE SUPPLIERS MAKE NO REPRESENTATIONS OR WARRANTY ABOUT THE SUITABILITY OF THE INFORMATION CONTAINED IN THE MATERIALS AND RELATED GRAPHICS PUBLISHED, OR ANY PRODUCTS OR SERVICES OFFERED OR REFERRED TO, ON THESE SITES FOR ANY PURPOSE, INCLUDING WITHOUT LIMITATION, ANY MEDICAL, MEDICAL TESTING, HEALTH OR FITNESS PURPOSE. ALL SUCH MATERIALS, RELATED GRAPHICS, PRODUCTS OR SERVICES ARE PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND.<br /><br />EDWARDS LIFESCIENCES AND/OR ITS RESPECTIVE SUPPLIERS DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO SUCH INFORMATION, PRODUCTS AND SERVICES, INCLUDING ALL IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL EDWARDS LIFESCIENCES BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY OTHER DAMAGES WHATSOEVER, WHETHER IN AN ACTION IN CONTRACT, TORT OR OTHER ACTION, ARISING OUT OF OR IN CONNECTION WITH THE INFORMATION, MATERIALS, PRODUCTS, OR PROVISION OF OR FAILURE TO PROVIDE SERVICES AVAILABLE ON THESE SITES.</p>
<h4>Links to Third Party Sites</h4>
<p>The links on our Sites will allow you to leave our Sites and transfer to sites that are not under the control of Edwards Lifesciences. Edwards Lifesciences is not responsible for the contents of any linked site or any links contained in a linked site, or any changes or updates to such sites. Edwards Lifesciences is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by Edwards Lifesciences of the site.</p>
<h4>Submissions</h4>
<p>Edwards Lifesciences Corporation appreciates hearing from its customers, however, our policy does not allow us to accept or consider creative ideas, suggestions, inventions or materials other than those we have specifically requested. Accordingly, we must insist that you do not send us any such original creative materials. If, despite our request that you not send us any creative materials, you send us ideas, notes, inventions, concepts or other information ("Disclosures"), none of such Disclosures shall be subject to any obligation of confidence on the part of Edwards Lifesciences and Edwards Lifesciences shall not be liable for any use or disclosure of any Disclosures.</p>
<h4>Forward Looking Statements</h4>
<p>No confidential relationship shall be established in the event that any user of these Sites should make any oral, written or electronic response to Edwards Lifesciences Corporation (such as feedback, questions, comments, suggestions, ideas, etc.). Such response and any information submitted therewith shall be considered non-confidential and Edwards Lifesciences shall be free to reproduce, publish or otherwise use such information for any purposes, including without limitation, the research, development, manufacture, use or sale of products incorporating such information. <br /><br />Certain portions of these Sites may include forward-looking statements that involve risks and uncertainties that could cause actual results or experience to differ materially from historical results or those anticipated. Actual future results may differ materially depending on a variety of factors, including technological advances in the medical field, foreign currency exchange rates, product demand and market acceptance, the impact of competitive products and pricing, the effect of economic conditions, and other risks detailed in Edwards Lifesciences' filings with the Securities and Exchange Commission. All forward-looking statements are based on estimates and assumptions made by management of the company as of the date of publication.</p>
<h4>Applicable Laws</h4>
<p>These Sites are controlled and operated from the United States of America and Edwards Lifesciences makes no representations that materials in the Sites are appropriate or available for use in other locations. These Sites contain information about products which may or may not be available in any particular country, may be available under different trademarks or names and, if applicable, may have received approval or market clearance by a governmental regulatory body for sale or use with different indications and restrictions in different countries. Each country has specific laws, regulations and medical practices governing the communication of medical information and information about medical products on Internet. You should not construe anything on these Sites as a promotion or solicitation for any product or for the use of any product that is not authorized by the laws and regulations of the country where you are located. <br /><br />These Sites may link to other sites produced by Edwards Lifesciences' various operating divisions and subsidiaries. Those sites may have information that is appropriate only to the particular originating country or region where the site is based.</p>
<h4>Actions in Response to Intellectual Property Infringement</h4>
<p>It is the policy of Edwards Lifesciences to respect intellectual property rights of others. Edwards Lifesciences will process and investigate notices of alleged infringement and will take appropriate actions under the Digital Millennium Copyright Act, Title 17, United States Code, Section 512(c)(2) ("DMCA") and other applicable intellectual property laws.<br /><br />Pursuant to the DMCA, notifications of claimed copyright infringement should be sent to a Service Provider's Designated Agent Notification must be submitted to the following Designated Agent for these sites:</p>
<p>Service Provider(s): Edwards Lifesciences Corporation<br />Name of Agent Designated to Receive Notification of Claimed Infringement: Nolan Taira</p>
<p>Full Address of Designated Agent to Which Notification Should Be Sent:</p>
<p><span>Attn: Nolan Taira</span><br /><span>Edwards Lifesciences Corporation <br />One Edwards Way</span><br /><span>Irvine, CA 92614</span></p>
<h4>Content Posted to Edwards Lifesciences' Sites</h4>
<p>Certain areas on our Sites may enable you to access on-line forums or message boards where you can post messages and read messages posted by other users ("Forums"). Edwards Lifesciences is not responsible for and does not endorse any information, data, text, software, music, sound, photographs, graphics or other materials, including without limitation any information relating to procedures or applications which are not approved by the Food and Drug Administration (i.e. off-label uses) ("Content") submitted to us or posted on such Forums by Site users. By posting Content on the Sites, however, you grant Edwards Lifesciences a world-wide, royalty free, non-exclusive and fully sublicenseable right and license to reproduce, modify, publish, translate and otherwise use such Content for any purpose Edwards Lifesciences chooses, commercial, public or otherwise without compensation whatsoever.</p>
<p>You agree to be fully responsible for your own postings and agree to access and use the Forums at your own risk on an as-is basis. While Edwards Lifesciences has no obligation to monitor the Forums, it may, from time to time, monitor the Forums to determine whether postings comply with this Agreement, to guide the discussion on the Forums, or for other reasons. Edwards Lifesciences cannot and does not guarantee that it will post or continue to post every message or other Content that you or others submit to the Forums. Edwards Lifesciences reserves the right to edit or abridge postings for any reason and to disclose any information as necessary to satisfy any applicable law, regulation, legal process or government request, or to edit, refuse to post or to remove any information or materials, in whole or part, in Edwards Lifesciences' sole discretion.You must abide by the following rules in connection with your use of the Forums:</p>
<ul>
<li>You agree to post only messages that relate to the subject matter of the Forum.</li>
<li>You agree not to defame, abuse, harass, stalk, threaten or otherwise violate the legal rights of others. You agree not to impersonate any person or entity, including, for example, an Edwards Lifesciences employee or officer, a Forum leader, or falsely state or otherwise misrepresent your affiliation with a person or entity.</li>
<li>You agree not to post messages that contain material that is inappropriate, profane, pornographic, threatening, defamatory, inflammatory, infringing, obscene, or indecent or that could constitute or encourage conduct that would be considered a criminal offense, give rise to civil liability, or would otherwise violate the law. This includes any Content that infringes any patent, trademark, trade secret, copyright or other proprietary rights of any third party.</li>
<li>You agree to give attribution to others when you quote or paraphrase materials owned by others, and to limit your quotations to no more than 50 words.</li>
<li>You agree not to upload files, or cause others to upload files, that contain viruses, corrupted files, or any other similar software or programs that may adversely affect the operation of another&rsquo;s computer.</li>
<li>You agree not to advertise or promote any goods or services in the Forums. This includes, among other things, &ldquo;contests,&rdquo; &ldquo;junk mail&rdquo;, &ldquo;spam&rdquo;, &ldquo;chain letters&rdquo;, and &ldquo;pyramid schemes.&rdquo;</li>
<li>You agree to indemnify and hold Edwards Lifesciences harmless from and against any claim or demand, including reasonable attorneys&rsquo; fees, made by any third party due to or arising out of Content you submit, post or transmit through Edwards Lifesciences&rsquo; sites, your use of the Sites, or your violation of any rights of another.</li>
</ul>
<h4>Miscellaneous</h4>
<p>This agreement shall be governed by and construed in accordance with the laws of the State of California, without giving effect to any principles of conflicts of law. If any provision of this agreement shall be deemed unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from this agreement and shall not affect the validity and enforceability of any remaining provisions</p>
<p>Determinadas partes de estos Sitios puede incluir declaraciones futuras que impliquen riesgos e incertidumbres que podr&iacute;an derivar en unos resultados o experiencias reales que difirieran considerablemente de los resultados hist&oacute;ricos o anticipados. Los resultados reales obtenidos en el futuro pueden variar sustancialmente en funci&oacute;n de diversos factores, incluidos los avances tecnol&oacute;gicos en el &aacute;mbito m&eacute;dico, los tipos de cambio de divisas, la demanda de productos y la aceptaci&oacute;n del mercado, el impacto de los productos y los precios de la competencia, el efecto de la situaci&oacute;n econ&oacute;mica y otros riesgos que se detallan en la documentaci&oacute;n presentada por Edwards Lifesciences ante la Comisi&oacute;n de Bolsa y Valores de los Estados Unidos. Todas las declaraciones futuras se basan en estimaciones y asunciones efectuadas por la direcci&oacute;n de la compa&ntilde;&iacute;a en la fecha de la publicaci&oacute;n.</p>

<h4>
Trademarks Information</h4>


<p>Edwards, Edwards Lifesciences, the stylized E logo, 1-800-4-A-Heart, 1-800-Fogarty, Advanced Venous Access, Alterra, AMC Thromboshield, Anastaflo, Ascendra, Ascendra2, Ascendra 3, Ascendra+, AUTHORITY, AutoIncisor, AVA 3Xi, AVA HF, AViD, Axela, Axion, BMEYE, CADENCE, CADENCE-MIS, Calibrex, CardiAQ, CardiAQ-Edwards, Cardiocath, CardioVations, Carpentier-Edwards, Carpentier-Edwards Classic, Carpentier-Edwards Physio, Carpentier-Edwards Physio II, Carpentier-Edwards S.A.V., Carpentier-McCarthy-Adams IMR ETlogix, ccNexfin, CCOmbo, CCOmbo EDV, CCOmbo V, CENTERA, Certitude, Chandler, Clarity in Every Moment, ClearSight, ClearSight IQ, COMMENCE, COM-1, COM-2, COMPASSION, COMPASSION S3, COMPASSION XT, ControlCath, Corkscrew, CO-Set, CO-Set+, Cosgrove-Edwards, Cribier-Edwards, DataSphere, Dedicated to Brighter Futures, dETlogix, Dispersion, Duraflex, DualFit Duraflo, Duris, Edwards CENTERA, EDWARDS COMMANDER, Edwards Critical Care System, Edwards eSheath, Edwards Essence, Edwards FloTrac, Edwards Fortis, Edwards Helio, EDWARDS INTUITY, EDWARDS INTUITY Elite, Edwards MC3, Edwards NanoClip, Edwards Prima Plus, Edwards Protection Cannulae, Edwards SAPIEN, Edwards SAPIEN XT, Edwards SAPIEN 3, Edwards SAPIEN 3 Ultra, EIP, EMBOL-X, EMBOL-X Glide, EMBOL-X and Design, Embrella, EndoClamp, EndoDirect, EndoPlege, EndoReturn, EndoVent, Enhanced Surgical Recovery Program and Design, Ergonic, eSheath, EverClip, EverGrip, Every Heartbeat Matters, EVOQUE, EV1000, Explorer, EZ Glide, Fem-Flex, Fem-Flex II, FemTrak, Flex-Tip, FLOtector, FloTrac, FloTrac IQ, Fogarty, Fogarty Corkscrew, Fogarty Fortis, Fogarty Hydragrip, FORMA , Fortis, FOUNDATION, GeoForm, GlucoClear, GLX, Heart Master, Helio, HemoSphere, Hi-Shore, Hydrajaw, IMR ETlogix, INSPIRIS, INSPIRIS RESILIA, INSPIRUS, Intellicath, IntraClude, Intro-Flex, iProctor, Janke-Barron, KONECT, Life is Now, Magna, Magna Ease, Magna Mitral Ease, Master of Valve Therapy, MC3, MC3 Tricuspid, MDOCK, Miller, Multi-Med, Myxo ETlogix, NanoClip, Neutralogic, newheartvalve.com, newheartvalve.com design logo, Nexfin, Nexfin CO-Trek, NovaFlex, NovaFlex+, Odyssey, OptiClip, OptiSite, Paceport, PARTNER, PARTNER II, PARTNER design logo, PATRIOT, PediaSat, PERI, PeriMap, PERIMOUNT, PERIMOUNT Magna, PERIMOUNT Plus, PERIMOUNT Theon, PeriVue, Physio, Physio II, Physio Tricuspid, Physiocal, PORT ACCESS, PORT ACCESS MVR, PreSep, ProPlege, Qualcrimp, QuickDraw, REF-1, REF/OX, RESILIA, Retractaguard, RetroFlex, RetroFlex II, RetroFlex 3, Retroplegia, RF3, SAPIEN, SAPIEN Therapeutics, SAPIEN XT, SAPIEN 3, SAPIEN 3 Ultra, SAT-1, SAT-2, S.A.V., Safejaw, SoftClamp, Softjaw, Starr-Edwards, SVVxtra, Swan, Swan-Ganz, Swan-Ganz IQ, S3, TAVRbyEdwards.com, TFX, Theon, ThermaFix, Thin-Flex, ThruPort, Time-in-Target, TRANSFORM, Tricentrix, Trim-Flex, TruCal, TruClip, TruSite, True-Size, TRUE-SIZE, TruWave, Uniflow, ValveNet, ValvePoint, VAMP, VAMP Flex, VAMP Jr., VAMP Plus, Vantex, Vascushunt, VFit, Vigilance, Vigilance II, Vigileo, VIP, VIP+, VisuFlo II, VolumeView, and XenoLogiX are trademarks or service marks of Edwards Lifesciences Corporation or its affiliates.</p>


  
 </div>
 </div>
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
