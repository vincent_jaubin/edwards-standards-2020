﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Api.Management.Account" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Exception" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Models.ResponseModels.UserProfile" %>
<%@ Import Namespace="ProfileApi=LoginRadiusSDK.V2.Api.Authentication.Account.ProfileApi" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    public bool isAdmin = false;
    public bool ipAllowed = false;
    public string fullName = "";
    public void LogOut_Onclick(Object sender, EventArgs args) {
        FormsAuthentication.SignOut();
        Session.Clear();
        FormsAuthentication.RedirectToLoginPage("DisableAnonymousAccess=true");       
    }
    protected void Page_Load(Object sender, EventArgs e) {

        var roles = Session["roles"] as List<String>;
        var profile = Session["profile"] as Identity;

        if (Session["roles"] == null)
        {
            LogOut_Onclick(null, null);
        }

        else if (roles.Contains("EdwardAdmin")) {
            profile = new Identity();
            profile.FullName = "Edwards";
        }

        if (profile == null) {
            FormsAuthentication.RedirectToLoginPage();
            if (Session["roles"] == null)
            {
                LogOut_Onclick(null, null);
            }

        }
        else {
            fullName = profile.FullName;

            if (roles != null) {
                if (roles.Contains("Admins") || roles.Contains("Standards Manager"))
                    isAdmin = true;
                if (roles.Contains("AllowedIp"))
                    ipAllowed = true;
            }
        }
    }
</script>
<html lang="en">
<head>
    <title>Edwards Brand Identity Standards | Edwards Lifesciences</title>


    <!-- #include file="meta-top.html" -->
</head>

<body>
    <!-- #include file="contact-form.html" -->


    <div id="edwards-wrapper">

        <!-- #include file="header.html" -->

        <!-- #include file="home.html" -->



        <!-- #include file="footer.html" -->


        <form runat="server">
            <div class="user-display">
                <span class='user-actions welcome'>Welcome, <%=fullName%>!</span>
                <% if (isAdmin == true && !ipAllowed) {%> <span class='user-actions'><a href='Admin.aspx'>Admin</a></span> | <% } %>

                <asp:LinkButton ID="logout" runat="server" OnClick="LogOut_Onclick" Text="Log Out" />
            </div>
        </form>
    </div>
    <!-- /wrapper -->

    <!-- #include file="meta-bottom.html" -->
</body>
</html>
