﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body data-spy="scroll" data-target="#tabslider">
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="env-banners" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Environmental Graphics</li>
  <li>Banners</li>
  </ol>
  
  
   <div id="tabslider">
  <h2>Banners<a href="pdf/p-banners.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div><a href="templates/edwards-banners.zip" class="btn-share" role="button" data-toggle="tooltip" title="Download the templates"><i class="icon-download"></i></a>
</h2>  <div class="clearfix"></div>
  

 <ul class="nav nav-tabs edw-tabs" role="tablist" data-offset-top="520" data-spy="affix">
  <li class="first-tab"><a href="#tabs-1" class="match-height"><span>Small secondary graphic</span></a></li>
  <li><a href="#tabs-2" class="match-height"><span>Large secondary graphic</span></a></li>
</ul>

 <div class="edw-tabslider">
    <div class="pane" id="tabs-1">
 <h3>Small secondary graphic</h3>   

<p>The grid determines the placement for each
element on these 33.0"W x 85.0"H banners.</p>
<p>The logo is scaled to 6.50", based on the
height of the E symbol.</p>
<div class="row">
<div class="col-md-7">

<p><strong>Banner layout</strong></p>
<p>The small secondary graphic is a fixed size. The
graphic may shift up or down as needed to
work with the image; do not shift the graphic
left or right. In this layout, the secondary
graphic equals 10 square units high and wide,
scaled as shown.</p>
<p>The headline and subtitle appear in the larger
red square. The background may be a image,
the approved background pattern or tinted
neutral color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>), or white.</p>
<p>Legal and address information should be
6.0" below the image, background color or
secondary graphic – whichever is lowest. This
copy must be at least 12.0" from the bottom
of the banner. Copy is typeset in Vesta Std
Regular, 30 pt on 36 pt line spacing; <em>Edwards
Lifesciences</em> is Bold.</p>
</div>
<div class="col-md-5"><img src="img/banners-1a.png" class="img-responsive img-margin center-block"></div>
</div>

<div class="row">
<div class="col-md-8">


<p><strong>Sample layout</strong></p>
<p>When using a image, background pattern
or neutral color (see <a href="elements-bg-pattern.aspx" target="_blank">backgrounds page</a>), the top
should align to the bottom horizontal line
of the E symbol; left and right edges bleed.
The bottom edge of this area may extend
downward, however, be sure the legal and
address information is clearly visible and
at least 12" from the bottom of the banner.
Support copy and call-to-action may appear
on the background image or neutral color, or
below the image on white.</p>
<p>The larger square may use a multiply effect as
described <a href="elements-secondary.aspx" target="_blank">here</a> when placed over an image.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>

</div>

<div class="col-md-4"><img src="img/banners-1b.png" class="img-responsive"></div>

</div>



    
 </div>
    <div class="pane" id="tabs-2">
    <h3>Large secondary graphic</h3>   

<p>The grid determines the placement for each
element on these 33.0"W x 85.0"H banners.</p>
<p>The logo is scaled to 6.50", based on the
height of the E symbol.</p>
<div class="row">
<div class="col-md-7">
<p><strong>Fixed graphic</strong></p>
<p>In this layout, the larger square in the
secondary graphic may be used to place a
image. Titles should be a solid red from
the approved palette. Subtitles may be gray
or black.</p>
<p>The width of the secondary graphic in this
layout equals 15 square units wide. Do not
adjust the width of the graphic, only the
height of the larger square as described below.</p>
<p>Legal and address information should be
6.0" below the image, background color or
secondary graphic – whichever is lowest. This
copy must be at least 12.0" from the bottom
of the banner. Copy is typeset in Vesta Std
Regular, 30 pt on 36 pt line spacing; <em>Edwards
Lifesciences</em> is Bold.</p>
</div>
<div class="col-md-5"><img src="img/banners-2a.png" class="img-responsive center-block img-margin"></div>
</div>

<div class="row">
<div class="col-md-7">


<p><strong>Flexible graphic</strong></p>
<p>In this layout, the larger shape in the
secondary graphic can be made taller to allow
for a larger image. It may extend as high as
the bottom horizontal line of the E symbol,
otherwise it should start 14.0" from the top
of the banner. The smaller red square should
remain a square – do not stretch or scale the
smaller square.</p>
<p>Headlines, subheads and body copy may
shift up or down within the larger shape as
necessary, but should remain aligned to the
left margin.</p>
<p><em>Note: The type sizes shown are recommendations.
Type sizes may be adjusted if necessary.</em></p>

</div>

<div class="col-md-5"><img src="img/banners-2b.png" class="img-responsive center-block img-margin"></div>

</div>

    
    
   </div>
    
    

</div>

</div>

</div>


    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
