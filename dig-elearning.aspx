﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>


<!-- #include file="meta-top.html" -->
</head>

<body>
<!-- #include file="contact-form.html" -->


<div id="edwards-wrapper">

<!-- #include file="header.html" -->



 <div id="dig-elearning" class="page">    <div class="container-fluid">
    <div class="row">
      <div class="redboxes-horz">
      <div class="redbox-horz-sm"></div>
      <div class="redbox-horz-lg">
        <h1>Brand identity standards</h1>
      </div>
      </div>
      </div>
    </div><!-- /.basic-header-edw --> <div class="container-fluid">
  <div class="row">
  
  <div class="col-md-9 col-md-push-3 main-content">
  
  <ol class="breadcrumb">
  <li><a href="contents.aspx">Brand identity standards</a></li>
  <li>Digital media</li>
  <li>eLearning</li>
  </ol>
  
  
  
  <h2>eLearning
  <a href="pdf/p-patient-awareness.pdf" class="btn-share" target="_blank" role="button" data-toggle="tooltip" title="Print this page"><i class="icon-printer"></i></a>
 <!-- Go to www.addthis.com/dashboard to customize your tools --><div class="addthis_toolbox share-email"><a class="addthis_button_mailto btn-share" style="text-decoration:none;" title="Email this page" data-toggle="tooltip"><span class="icon-envelop"></span></a></div></h2>
  <div class="clearfix"></div>
 
<div class="row">
<div class="col-md-4">
    <p><strong>Secondary graphic</strong></p>

    <p>
        For eLearning modules, use a small secondary
        graphic with the name on the home screen.
        Do not include other messaging within the
        secondary graphic. The secondary graphic
        does not need to appear on other screens. If a
        splash screen is used, see the app samples as
        described on page 73–76.
    </p>

    <p>
        When using sample screens or images for
        navigation purposes, do not place the images
        or shapes right next to the secondary graphic.
        Allow extra space around the secondary
        graphic.
    </p>

    <p><strong>Large secondary graphic</strong></p>

    <p>
        Use a large secondary graphic at the top of
        the screen to place only the name. Do not
        include other rectangles or image blocks
        within the large secondary graphic.
    </p>

    
</div>
<div class="col-md-8">
    <p><img src="img/dig-elearning-image.png" class="img-responsive"></p>
</div>

</div>
<div class="row">
    <div class="col-md-12">
        <p><strong>Logo</strong></p>

    <p>
        Place the logo in the top left corner, anchored
        to a dark gray bar. The bar may include
        the eLearning module name and primary
        navigation if necessary.
    </p>

    <p><strong>Backgrounds</strong></p>

    <p>
        The background of the screen may be an
        image, an approved background color or
        pattern.
    </p>

    <p><strong>Fonts</strong></p>

    <p>
        Use Merriweather Sans Regular for digital
        applications. Use Bold or Italic to add
        emphasis when needed.
    </p>

    <p><strong>Different screen sizes</strong></p>

    <p>
        Base other size screens on the samples
        shown here, maintaining the same general
        proportions.
    </p>
    </div>
</div>



  
    </div>
    
    
  <div class="col-md-3 col-md-pull-9">
  
<!-- #include file="sidemenu-branding.html" -->
  
  
  </div>
    
    
 </div>
 
 </div>
 </div>  <!-- end page -->
 
 
 
<!-- #include file="footer.html" --> 

  
  
</div> <!-- /wrapper -->

<!-- #include file="meta-bottom.html" --> 
</body>
</html>
