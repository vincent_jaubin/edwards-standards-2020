﻿<%@ Page Language="C#" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Api" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Api.Management.Account" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Exception" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Models.ResponseModels.UserProfile" %>
<%@ Import Namespace="ProfileApi=LoginRadiusSDK.V2.Api.Authentication.Account.ProfileApi" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    public void Login_OnClick(object sender, EventArgs args) {
        //string accessToken = Request.Form["__EVENTARGUMENT"];
        string accessToken = hdnaccesstoken.Value;

        if (!string.IsNullOrEmpty(accessToken)) {
            OnLogin(accessToken);
        }

    }

    protected void Page_Load(object sender, EventArgs e) {

      if (!IsPostBack)
        {
            var disableAnonymousAccess = Convert.ToBoolean(Request.QueryString["DisableAnonymousAccess"]);
            if(disableAnonymousAccess == false)
            {
                if (AllowAnonymousAccess())
                {
                    List<string> roles = new List<string> { "AllowedIp" , "EdwardAdmin"};
                    Session["roles"] = roles;
                    FormsAuthentication.RedirectFromLoginPage("Edwards", true);
                }
            }           
        }
        else {
            if (string.IsNullOrEmpty(hdnaccesstoken.Value) == false) {
                Login_OnClick(null, null);
            }
        }

    }


    public void OnLogin(string accessToken) {
        try {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; 
            var api = new ProfileApi();
            var response = api.GetProfileByAccessToken(accessToken);
            if (response.ApiExceptionResponse != null) {

            }
            else {
                Session["profile"] = response.Response;

                try {
                    var roleapi = new RoleApi();
                    var rolesResponse = roleapi.GetRolesByUid(response.Response.Uid);


                    if (response.ApiExceptionResponse != null) {

                    }
                    else {

                        Session["roles"] = rolesResponse.Response.Roles;
                        FormsAuthentication.RedirectFromLoginPage(response.Response.FullName, true);
                    }
                }
                catch (LoginRadiusException ex) {

                }
                catch (Exception ex) {

                }

            }
        }
        catch (LoginRadiusException ex) {

        }
        catch (Exception ex) {

        }
    }

    private bool AllowAnonymousAccess() {

        string clientIPString = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "";
        string allowedUserIPList = "";
        if (ConfigurationManager.AppSettings["allowedIpList"] != null) {
            allowedUserIPList = ConfigurationManager.AppSettings["allowedIpList"];
        }
        if (allowedUserIPList == "")
            return false;

        IPAddress clientIPAddr;
        if (IPAddress.TryParse(clientIPString.Trim(), out clientIPAddr)) {
            string[] ipAddresArray = allowedUserIPList.Split(',');
            List<string> ipAddresses = new List<string>();

            foreach (string ipAddress in ipAddresArray) {
                if (ipAddress.Contains("/"))
                    ipAddresses.AddRange(IpAddressRange(ipAddress));

                else
                    ipAddresses.Add(ipAddress.Trim());
            }

            if (ipAddresses.Contains(clientIPString))
                return true;
        }

        return false;
    }
    protected void Page_PreRender(object sender, EventArgs e) {
        ClientScript.GetPostBackEventReference(UsernameTextbox, string.Empty);
    }
    private List<string> IpAddressRange(string ipAddress) {
        try {
            string[] ipParts = ipAddress.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string[] startIP = ipParts[0].Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            int ipCount = Convert.ToInt32(ipParts[1]);
            int ipSuffix = Convert.ToInt32(startIP[3]);

            List<string> ipList = new List<string>();
            for (int i = 0; i < ipCount; i++)
                ipList.Add(string.Format("{0}.{1}.{2}.{3}", startIP[0], startIP[1], startIP[2], (ipSuffix + i).ToString()));

            return ipList;
        }
        catch (Exception) { }

        return new List<string>();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <script src="https://auth.lrcontent.com/v2/js/LoginRadiusV2.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" type="text/javascript"></script>

    <script src="/loginradius/config.js" type="text/javascript"></script>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:300);

        #login-form {
            width: 360px;
            padding: 8% 0 0;
            margin: auto;
        }

        #login-form-container {
            text-align: left;
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 360px;
            margin: 0 auto 0px;
            padding: 45px;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

        form input {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;
        }

        .loginradius-submit {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #c8102e !important;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
        }

         .logintype-select-button {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: rgb(121,153,172) !important;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
            width:360px; 
            border-style: solid;
            border-width: 1px;
        }

        form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

            form .message a {
                color: #4CAF50;
                text-decoration: none;
            }

        form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

            .container:before, .container:after {
                content: "";
                display: block;
                clear: both;
            }

            .container .info {
                margin: 50px auto;
                text-align: center;
            }

                .container .info h1 {
                    margin: 0 0 15px;
                    padding: 0;
                    font-size: 36px;
                    font-weight: 300;
                    color: #1a1a1a;
                }

                .container .info span {
                    color: #4d4d4d;
                    font-size: 12px;
                }

                    .container .info span a {
                        color: #000000;
                        text-decoration: none;
                    }

                    .container .info span .fa {
                        color: #EF3B3A;
                    }

        body {
            background: #505759; /* fallback for old browsers */
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .chk-public {
            display: inline;
            float: left;
            width: 36px;
        }
    </style>

</head>
<body>
   <%--<h2 class="lr-form-login-message">Login</h2>--%>
    <div id="login-form">
        
        <div id="login-form-container">
            <div id="login-container"></div>
            <div id="error-msg" style="color: red;"></div>
            <div id="success-msg" style="color: green;"></div>
            <asp:HyperLink ID="forgetPassword" runat="server" NavigateUrl="~/ForgotPassword.aspx" Text="Forgot Password" />
        </div>
        <button type="submit" title="Employees will be logged in using thier AD account through single signon" id="login-employee" class="logintype-select-button"  > Edwards employee login </button>
       
    </div>
    <footer>
        <div class="overlay" id="fade" style="display: none;">
            <div class="circle">
                <div id="imganimation">
                    <img src="//cdn.loginradius.com/demo/common/loading_spinner.gif" alt="LoginRadius Processing"
                         style="margin-top: -66px;margin-left: -73px;width: 338px;height: 338px;">
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </footer>
   <div style="display: none;">
        <form class="login-form" id="form1" runat="server">
            <asp:TextBox ID="UsernameTextbox" runat="server" />
            <asp:Button ID="LoginButton" Text="Login" OnClick="Login_OnClick" runat="server" CssClass="form-button" />
           <asp:HiddenField ID="CommandArg" runat="server" />
            <asp:HiddenField ID="hdnaccesstoken" runat="server" />

        </form>
    </div>
    <script type="text/javascript">

        $("#login-employee").click(function () {
            window.location.href = "/SSOLogin.aspx"
        });

        function redirect(token) {
            var form = document.createElement("form");
            form.method = "POST";
            form.action = "/OnLogin";
            var _token = document.createElement("input");
            _token.type = "hidden";
            _token.name = "accessToken";
            _token.value = token;
            form.appendChild(_token);
            document.body.appendChild(form);
            form.submit();
        }

        var LRObject = new LoginRadiusV2(commonOptions);

        var login_options = {};
        login_options.onSuccess = function (response) {
          
            $("#hdnaccesstoken").val(response.access_token);
            __doPostBack();

            //redirect(response.access_token);
         //   __doPostBack("<= LoginButton >", response.access_token);
          //  var button = document.getElementById('LoginButton');
          //  button.click();
        };
        login_options.onError = function (errors) {
            console.log(errors);
            var successDiv = document.getElementById('success-msg');
            successDiv.innerHTML = "";
            var errorDiv = document.getElementById('error-msg');
            if (errors[0].Description) {
                errorDiv.innerHTML = errors[0].Description;
            } else {
                errorDiv.innerHTML = errors[0].Message;
            }

        };
        login_options.container = "login-container";

        LRObject.util.ready(function () {
            LRObject.init("login", login_options);
            $(".loginradius-submit").after($("#forgetPassword"));
            $(".loginradius-submit").after($("#success-msg"));
            $(".loginradius-submit").after($("#error-msg"));

           
        });    
        $("body").on('DOMSubtreeModified', "#login-container", function () {
            if($(".content-loginradius-emailid").find("label").text() !== "Username")
                    $(".content-loginradius-emailid").find("label").text("Username");
        });
              
      
    </script>
</body>
</html>
