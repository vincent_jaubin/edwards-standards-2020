﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>

<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Api.Management.Account" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Exception" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Models.RequestModels" %>
<%@ Import Namespace="LoginRadiusSDK.V2.Models.ResponseModels.UserProfile" %>
<%@ Import Namespace="ProfileApi=LoginRadiusSDK.V2.Api.Management.Account.ProfileApi" %>
<%@ Import Namespace="PasswordApi=LoginRadiusSDK.V2.Api.Authentication.Account.PasswordApi" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    public string successMsg = String.Empty;
    public string errorMsg = String.Empty;
    public void AddUser_Onclick(object sender, EventArgs args) {

        var roles = Session["roles"] as List<string>;
        var profile = Session["profile"] as Identity;

        if (roles.Contains("EdwardAdmin")) {
            profile = new Identity();
            profile.FullName = "Edwards";
        }

        if (profile == null) {
            FormsAuthentication.RedirectToLoginPage();
        }
        else {
            if (roles == null || !roles.Contains("Admin")) {
                if (!roles.Contains("Standards Manager")) {
                    Response.Redirect("Index.aspx");
                }
            }
        }
        var model = new AddUserModel();
        model.FirstName = Firstname.Text;
        model.LastName = LastName.Text;
        model.Password = Password.Text.Trim();
        model.ConfirmPassword = ConfirmPassword.Text.Trim();
        model.Company = Company.Text;
        model.EmailId = Emailid.Text;
        model.CustomFields = new Dictionary<string, string>();
        model.CustomFields.Add(CustomFields_0__Key.Text, CustomFields_0__Value.Text);
        AddUser(model);

    }
    protected void Page_Load(object sender, EventArgs e) {

        var roles = Session["roles"] as List<string>;
        var profile = Session["profile"] as Identity;

        if (roles.Contains("EdwardAdmin")) {
            profile = new Identity();
            profile.FullName = "Edwards";
        }

        if (profile == null) {
            FormsAuthentication.RedirectToLoginPage();
        }
        else {
            if (roles == null || !roles.Contains("Admin")) {
                if (!roles.Contains("Standards Manager")) {
                    Response.Redirect("Index.aspx");
                }
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e) {
    }
    public void AddUser(AddUserModel model) {
        ErrorMsg.Style["display"] = "none";
        SuccessMsg.Style["display"] = "none";
        errorMsg = String.Empty;
        successMsg = String.Empty;

        if (model.Password.Length < 6)
            errorMsg = "Your password must be 6 or more characters";
        else if (model.Password.Trim() != model.ConfirmPassword.Trim())
            errorMsg = "Your passwords do not match";

        if (!String.IsNullOrEmpty(errorMsg)) {
            ErrorMsg.Text = errorMsg;
            ErrorMsg.Style["display"] = "block";
            return;
        }

        var user = new UserProfileServerSideModel();
        user.FirstName = model.FirstName;
        user.LastName = model.LastName;

        user.Email = new List<EmailModel>();
        user.Email.Add(new EmailModel { Type = "Primary", Value = model.EmailId });

        user.Company = model.Company;

        user.CustomFields = model.CustomFields;

        user.Password = model.Password;
        user.EmailVerified = true;

        var api = new ProfileApi();
        try {
            var response = api.CreateProfile(user);
            if (response.ApiExceptionResponse != null) {
                errorMsg = response.ApiExceptionResponse.Message;
                if (response.ApiExceptionResponse.ErrorCode == 936) {
                    var resp = api.GetProfileByEmail(model.EmailId);
                    if (resp.ApiExceptionResponse != null) {
                        errorMsg = resp.ApiExceptionResponse.Message;
                    }
                    else {
                        var primaryEmail = resp.Response.Email.Where(g => g.Type == "Primary").FirstOrDefault();
                        errorMsg = String.Format("User {0} {1} - {2} already exist in the system.", resp.Response.FirstName, resp.Response.LastName, primaryEmail.Value);
                    }
                }
            }
            else {
                string errorMessage;
                var roleModel = new AccountRolesModel();
                roleModel.Roles = new List<string> { "Standards Restricted Access" };

                var roleResponse = AssignRolesToUser(response.Response.Uid, roleModel, out errorMessage);
                if (errorMessage == null) {
                    //var resp = SendWelcomeEmailForSetPassword(model.EmailId, out errorMessage);
                    //if (resp)
                    //{
                    //     var primaryEmail = user.Email.Where(g => g.Type == "Primary").FirstOrDefault();
                    //    successMsg = String.Format("User {0} {1} - {2} , has been created.",user.FirstName,user.LastName,primaryEmail.Value);

                    //}
                    //else
                    //{
                    //    errorMsg = "User created successfully with default role. but error occurred when sending email, ("+ errorMessage + ")";
                    //}
                    var primaryEmail = user.Email.Where(g => g.Type == "Primary").FirstOrDefault();
                    successMsg = String.Format("User {0} {1} - {2} , has been created.", user.FirstName, user.LastName, primaryEmail.Value);
                }
                else {
                    errorMsg = "User created successfully. but error occurred when assigning role to user, (" + errorMessage + ")";
                }

            }
        }
        catch (LoginRadiusException ex) {
            errorMsg = ex.Message;
        }
        catch (Exception ex) {
            errorMsg = ex.GetBaseException().Message;
        }
        finally {
            if (!String.IsNullOrEmpty(errorMsg)) {
                ErrorMsg.Text = errorMsg;
                ErrorMsg.Style["display"] = "block";
            }
            if (!String.IsNullOrEmpty(successMsg)) {
                SuccessMsg.Text = successMsg;
                SuccessMsg.Style["display"] = "block";
                Firstname.Text = "";
                LastName.Text = "";
                Company.Text = "";
                Emailid.Text = "";
                CustomFields_0__Value.Text = "";
                Password.Text = "";
                ConfirmPassword.Text = "";
            }
        }
        //return View();
    }

    bool SendWelcomeEmailForSetPassword(string email, out string errorMessage) {
        errorMessage = null;
        try {
            var api = new PasswordApi();
            var response = api.ForgotPassword("ResetPassword.aspx", email, "reset-welcome");
            if (response.ApiExceptionResponse != null) {
                errorMessage = response.ApiExceptionResponse.Message;
            }
            else {
                if (response.Response.IsPosted) {
                    return true;
                }
            }
        }
        catch (LoginRadiusException ex) {
            errorMessage = ex.Message;
        }
        catch (Exception ex) {
            errorMessage = ex.GetBaseException().Message;
        }
        return false;
    }

    List<string> AssignRolesToUser(string uid, AccountRolesModel roleModel, out string errorMessage) {
        errorMessage = null;

        try {
            var api = new RoleApi();
            var response = api.AssignRolesToUser(uid, roleModel);
            if (response.ApiExceptionResponse != null) {
                errorMessage = response.ApiExceptionResponse.Message;
            }
            else {
                return response.Response.Roles;

            }
        }
        catch (LoginRadiusException ex) {
            errorMessage = ex.Message;
        }
        catch (Exception ex) {
            errorMessage = ex.GetBaseException().Message;
        }
        return null;
    }
    public class AddUserModel {
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string UserName { get; set; }

        public string Prefix { get; set; }
        public string Suffix { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string PhoneNumber { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public string Company { get; set; }

        public bool EmailSubscription { get; set; }

        public Dictionary<string, string> CustomFields { get; set; }

    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Admin | Add New User</title>
    <script src="https://auth.lrcontent.com/v2/js/LoginRadiusV2.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js" type="text/javascript"></script>

    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:300);

        #login-form {
            width: 360px;
            padding: 8% 0 0;
            margin: auto;
        }

        form {
            text-align: left;
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 360px;
            margin: 0 auto 100px;
            padding: 45px;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

            form input {
                font-family: "Roboto", sans-serif;
                outline: 0;
                background: #f2f2f2;
                width: 100%;
                border: 0;
                margin: 0 0 15px;
                padding: 15px;
                box-sizing: border-box;
                font-size: 14px;
            }

        #submitButton {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #c8102e !important;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
        }


        form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

            form .message a {
                color: #4CAF50;
                text-decoration: none;
            }

        form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

            .container:before, .container:after {
                content: "";
                display: block;
                clear: both;
            }

            .container .info {
                margin: 50px auto;
                text-align: center;
            }

                .container .info h1 {
                    margin: 0 0 15px;
                    padding: 0;
                    font-size: 36px;
                    font-weight: 300;
                    color: #1a1a1a;
                }

                .container .info span {
                    color: #4d4d4d;
                    font-size: 12px;
                }

                    .container .info span a {
                        color: #000000;
                        text-decoration: none;
                    }

                    .container .info span .fa {
                        color: #EF3B3A;
                    }

        body {
            background: #505759; /* fallback for old browsers */
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .chk-public {
            display: inline;
            float: left;
            width: 36px;
        }
    </style>
</head>
<body>

    <div id="login-container" class="lr-widget-container">
        <form class="add-user-form" id="form1" runat="server">
            <div>

                <label for="firstname">First Name</label>
                <%--<input id="firstname" name="firstname" type="text" value=""/>--%>
                <asp:TextBox ID="Firstname" name="firstname" type="text" value="" runat="server" required="required" />
            </div>
            <div>

                <label for="lirstname">Last Name</label>
                <%--<input id="firstname" name="firstname" type="text" value=""/>--%>
                <asp:TextBox ID="LastName" name="lirstname" type="text" value="" runat="server" required="required" />
            </div>
            <div>

                <label for="company">Company</label>
                <%--<input id="company" name="company" type="text" value=""/>--%>
                <asp:TextBox ID="Company" name="company" type="text" value="" runat="server" required="required" />

            </div>
            <div>

                <label for="emailid">Email Id</label>
                <%--<input id="emailid" name="emailid" type="text" value=""/>--%>
                <asp:TextBox ID="Emailid" name="emailid" type="email" value="" runat="server" required="required" />

            </div>

            <div>
                <asp:TextBox ID="CustomFields_0__Key" name="CustomFields[0].Key" type="hidden" value="EdwardsContact" runat="server" />
                <label for="CustomFields[0].Value">Edwards Contact</label>
                <%--<input id="CustomFields_0__Value" name="CustomFields[0].Value" type="text" value=""/>--%>
                <asp:TextBox ID="CustomFields_0__Value" name="CustomFields[0].Value" type="text" value="" runat="server" required="required" />


            </div>
            <div>
                <label for="password">Password</label>
                <%--<input id="CustomFields_1__Value" name="CustomFields[1].Value" type="text" value=""/>--%>
                <asp:TextBox ID="Password" name="password" type="password" value="" runat="server" required="required" />
            </div>
            <div>
                <label for="confirmPassword">Confirm Password</label>
                <%--<input id="CustomFields_1__Value" name="CustomFields[1].Value" type="text" value=""/>--%>
                <asp:TextBox ID="ConfirmPassword" name="confirmPassword" type="password" value="" runat="server" required="required" />

                <asp:Label type="hidden" runat="server" Style="color: red;" ID="ErrorMsg"></asp:Label>
                <asp:Label type="hidden" runat="server" Style="color: green;" ID="SuccessMsg"></asp:Label>
                <br />
            </div>
            <asp:Button ID="submitButton" runat="server" Text="Add User" OnClick="AddUser_Onclick" />

            <asp:HyperLink ID="backToHome" runat="server" NavigateUrl="~/Index.aspx" Text="Back to Homepage" />
            <%--  <% if (!string.IsNullOrEmpty(successMsg)) {%> 
            <div class="success-msg" style="color: green;"> <%# successMsg %></div>
         <% } %>
        <% if (!string.IsNullOrEmpty(errorMsg)) {%> 
            <div class="error-msg" style="color: red;"> <%# errorMsg %></div>
         <% } %>--%>
        </form>
    </div>
</body>

</html>
